<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(auth()->check()){
        return view('pages.index');
    }else{
    return redirect()->route('login');
    }
});
Route::middleware(['auth'])->group(function () {    Route::get('dashboard', function () {return view('pages.index');})->name('dashboard');
    Route::get('guide', function () {return view('guide');})->name('guide');
    Route::get('settings', function () {
        $user = auth()->user();
        return view('settings',compact('user'));
    })->name('settings');
    Route::resources([
        'locations' => 'LocationController',
        'ads' => 'adsCtrl',
        //'marketingBenefactors' => 'MarketingBenefactorCtrl',
        //'callBenefactors' => 'callBenefactorsCtrl',
        'permissions' => 'permissionsCtrl',
        'roles' => 'rolesCtrl',
        //'assignmentsToCallEmps' => 'AssignToCallEmpCtrl',
        //'call_assignments' => 'callCenterAssignmentsCtrl',
        //'collect_assignments' => 'collectAssignmentsCtrl',
       // 'calls' => 'CallController',
    ]);
    //Route::post('marketing/benefactor/{newBenefactor}/send','MarketingBenefactorCtrl@send')->name('marketing.benefactor.send');
    Route::prefix('users')->group(static function () {
        Route::get('all','adminCtrl@users')->name('users.all');
        Route::post('roles/update/{user}','adminCtrl@updateRoles')->name('users.roles.update');
        Route::post('permissions/update/{user}','adminCtrl@updatePermissions')->name('users.permissions.update');

    });

    Route::prefix('notifications')->group(static function () {
        Route::post('/markAsRead','notficationCtrl@markAsRead')->name('readNotification');
    });

//    Route::get('/benefactors/dataFile/{id}/{assignment?}','benefactorCtrl@dataFile' )->name('benefactor.data.file');
//
//    Route::prefix('calls')->group(static function () {
//        //Route::get('{assignment?}/benefactor/{id}','callBenefactorsCtrl@show')->name('assignment.Benefactors.show');
//       // Route::Post('{assignment}/benefactor/{id}','callBenefactorsCtrl@store')->name('assignment.Benefactors.store');
////        Route::Post('{assignment}/collect','CollectRequestsController@store')->name('assignment.collectRequest.store');
////        Route::Put('{collectRequests}/update','CollectRequestsController@update')->name('assignment.collectRequest.update');
//
//    });

//    Route::get('assignments-old','callCenterAssignmentsCtrl@old')->name('old_assignments.callCenter');
//
//    Route::prefix('callcenter_manager')->group(static function () {
//        Route::get('/benefactors/new', 'CallManagerCtrl@newBenefactors')->name('callManager.new.benefactors');
//        Route::post('/newBenefactors/{benefactor}/assignToCallemp', 'CallManagerCtrl@assignTocallemp')->name('callManager.newBenefactors.assignTocallemp');
//        Route::get('/benefactors/actual', 'CallManagerCtrl@actualBenefactors')->name('actualBenefactors');
//        Route::get('/benefactors/kafalah', 'CallManagerCtrl@kafalah')->name('all.kafalah');
//        Route::get('/benefactors/stopped', 'CallManagerCtrl@AllStopped')->name('all.stopped');
//        Route::get('/benefactors/forCollect', 'CallManagerCtrl@forCollectBenefactors')->name('callManager.benefactors.forCollect');
//        Route::get('/assignments/team', 'CallManagerCtrl@team')->name('callManager.assignments.team');
//        Route::get('/assignments/emp/{emp}', 'CallManagerCtrl@emp')->name('callManager.assignments.temp');
//        Route::get('/EmpAssignment/{id}', 'CallManagerCtrl@empAssignment')->name('call.emp.assignment');
//        Route::get('/excelfile', 'ExcelCtrl@uploadFile')->name('callManager.uploadFile');
//        Route::post('/excelfile', 'ExcelCtrl@storeFile')->name('callManager.storeFile');
//
//        Route::post('/EditRequestRefuse/{id}', 'CallManagerCtrl@refuseEditRequest')->name('callManager.editRequest.refuse');
//        Route::post('/EditRequestAccept/{id}', 'CallManagerCtrl@acceptEditRequest')->name('callManager.editRequest.accept');
//
//        Route::get('/readyAssignment/forcollect','CallManagerCtrl@forCollectAssignments')->name('callManager.forCollect.assignments');
//        Route::post('/sendAssignment/forcollect/{assignment}','CallManagerCtrl@sendAssignmentforCollectManager')->name('send.assignment.to.collectManager');
//
//    });
//
//    Route::post('benefactors/{benefactor}/stopAllHisActivities', 'benefactorCtrl@stopAllHisActivities')->name('stopAllHisActivities');
//    Route::post('benefactors/{benefactor}/restore', 'benefactorCtrl@restore')->name('benefactor.restore');
//    Route::post('collectAssignments/{collection}/collected','collectAssignmentsCtrl@collectedDone')->name('collect.action.done');
//    Route::post('collectAssignments/{collection}/failed','collectAssignmentsCtrl@failedCollect')->name('collect.action.fail');
//    Route::post('collectAssignments/{collection}/changeEmp','collectAssignmentsCtrl@changeEmpForCollect')->name('changeEmpForCollect');
//    Route::post('collectAssignments/{assignment}/markAsfailed','collectAssignmentsCtrl@markAsfailed')->name('markAsfailed');
//    Route::get('newCollectAssignments','collectAssignmentsCtrl@nowatime')->name('collectAssignments.myNew');
//    Route::get('oldCollectAssignments','collectAssignmentsCtrl@old')->name('collectAssignments.old');
//    Route::get('failedCollectAssignments','collectAssignmentsCtrl@allFailds')->name('collectAssignments.faild');
//    Route::get('doneCollectAssignments','collectAssignmentsCtrl@done')->name('collectAssignments.done');
//    Route::get('immediateCollectAssignments','collectAssignmentsCtrl@immediate')->name('collectAssignments.immediate');
//
//    Route::prefix('reports')->group(static function () {
//    #reports
//        Route::view('/ad/report', 'reports.singlAd')->name('ad.report');
//        Route::view('/ads_benefactors/report', 'reports.allAds')->name('ads_benefactors.report');
//        Route::view('/ads_costs/report', 'reports.allAdsCosts')->name('ads_costs.report');
//    #Reports
//        Route::view('/all_benefactors/report', 'reports.allBenefactors')->name('all_benefactors.report');
//        Route::view('/all_kafalah/report', 'reports.allKafalah')->name('all_kafalah.report');
//        Route::view('/all_Aini/report', 'reports.allAini')->name('all_aini.report');
//
//    });
//
//    Route::prefix('account_manager')->group(static function () {
//        Route::get('/ToConfirmCall/collects','accountManagerCtrl@toCall')->name('toCollectConfirmCall');
//        Route::get('/assignments/delayed','accountManagerCtrl@delayed')->name('toCollectDelayedCalls');
//        Route::get('/assignments/failed','accountManagerCtrl@faild')->name('toCollectFaildCalls');
//        Route::get('/assignments/confirmed','accountManagerCtrl@confirmed')->name('toCollectconfirmedCalls');
//        Route::get('/team','accountManagerCtrl@team')->name('account.team');
//        Route::get('/team/{emp}','accountManagerCtrl@emp')->name('account.emp');
//
//        //calls results:
//        Route::post('/assignments/{assignment}/confirm','accountManagerCtrl@confirm')->name('Confirm.Call');
//        Route::post('/assignments/{assignment}/delay','accountManagerCtrl@delay')->name('delay.Call');
//        Route::post('/assignments/{assignment}/reject','accountManagerCtrl@reject')->name('reject.Call');
//        Route::post('/assignments/{assignment}/assignToCollect','accountManagerCtrl@assignToCollect')->name('assignToCollect');
//    });
//
//    Route::prefix('manager')->group(static function () {
//        Route::view('/benefactors', 'pages.manager.allBenefactors')->name('manager.benefactors');
//        Route::view('/benefactors/single', 'pages.manager.benefactor')->name('manager.benefactor');
//        Route::view('/kafalat', 'pages.manager.allKafalat')->name('manager.allKafalat');
//        Route::view('/kafalat/single', 'pages.manager.kafalah')->name('manager.kafalah');
//        Route::view('/emps', 'pages.manager.allEmps')->name('manager.emps');
//        Route::view('/emps/single', 'pages.manager.emp')->name('manager.emp');
//        Route::view('/ads', 'pages.manager.allAds')->name('manager.ads');
//        Route::view('/ads/single', 'pages.manager.ad')->name('manager.ad');
//        Route::view('/ads/costs', 'pages.manager.adsCosts')->name('manager.ads.cost');
//        Route::view('/ads/locations', 'pages.manager.locations')->name('manager.locations');
//        Route::view('/aini', 'pages.manager.aini')->name('manager.aini');
//        Route::view('/mony', 'pages.manager.mony')->name('manager.mony');
//        Route::view('/callManager', 'pages.manager.blank')->name('manager.callManager');
//        Route::view('/marketingManager', 'pages.manager.blank')->name('manager.marketingManager');
//        Route::view('/accountManager', 'pages.manager.blank')->name('manager.accountManager');
//
//
//    });
});
Auth::routes();

Route::get('/home', function () {

        return redirect()->route('login');

})->name('home');

<?php
Route::middleware(['auth'])->group(function () {

    //new Benefactors:
    Route::prefix('newAssignments')->group(static function () {
        Route::prefix('get')->group(static function () {
            Route::get('/','newAssignmentsCtrl@index')->name('newAssignments.index');
            Route::get('{assignment}','newAssignmentsCtrl@show')->name('newAssignments.show');

        });
        Route::prefix('post')->group(static function () {
            Route::post('{assignment}/{newBenefactor}','newAssignmentsCtrl@storeData')->name('newAssignments.storeData');
            Route::post('{benefactor}','newAssignmentsCtrl@updateData')->name('newAssignments.updateData');
            //Route::post('Calls/{assignment?}/new','CallController@store')->name('newAssignment.storeCall');
            Route::post('updateCall/{call}','newAssignmentsCtrl@updateCall')->name('newAssignment.updateCall');
            Route::post('done/{assignment}/send','newAssignmentsCtrl@done')->name('newAssignment.done');
            Route::post('failed/{assignment}/send','newAssignmentsCtrl@failed')->name('newAssignment.failed');
        });
    });


    // actual Benefactors:
    Route::prefix('oldAssignments')->group(static function () {
        Route::prefix('get')->group(static function () {
            Route::get('/','oldAssignmentsCtrl@index')->name('oldAssignments.index');
            Route::get('{assignment}','oldAssignmentsCtrl@show')->name('oldAssignments.show');

        });
        Route::prefix('post')->group(static function () {
        });
    });

});
?>

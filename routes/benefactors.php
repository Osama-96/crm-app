<?php
Route::middleware(['auth'])->group(function () {

    //new Benefactors:
    Route::prefix('newClients')->group(static function () {
        Route::prefix('get')->group(static function () {
            Route::get('create','newBenefactorsCtrl@create')->name('newBenefactors.create');
            Route::get('all','newBenefactorsCtrl@all')->name('newBenefactors.allNew');
            Route::get('new','newBenefactorsCtrl@new')->name('newBenefactors.new');
            Route::get('sent','newBenefactorsCtrl@sent')->name('newBenefactors.sent');
            Route::get('assigned','newBenefactorsCtrl@assigned')->name('newBenefactors.assigned');
            Route::get('failed','newBenefactorsCtrl@failed')->name('newBenefactors.failed');
            Route::get('moved','newBenefactorsCtrl@moved')->name('newBenefactors.moved');
            Route::get('notMoved','newBenefactorsCtrl@notMoved')->name('newBenefactors.notMoved');
            Route::get('{benefactor}','newBenefactorsCtrl@show')->name('newBenefactors.show');
        });
        Route::prefix('post')->group(static function () {
            Route::post('store','newBenefactorsPostCtrl@store')->name('newBenefactors.store');
            Route::post('update/{newBenefactor}','newBenefactorsPostCtrl@update')->name('newBenefactors.update');
            Route::post('destroy/{newBenefactor}','newBenefactorsPostCtrl@destroy')->name('newBenefactors.destroy');
            Route::post('send/{newBenefactor}','newBenefactorsPostCtrl@send')->name('newBenefactors.send');
            Route::post('assign/{newBenefactor}','newBenefactorsPostCtrl@assign')->name('newBenefactors.assign');
            Route::post('changeEmp/{newBenefactor}','newBenefactorsPostCtrl@changeEmp')->name('newBenefactors.changeEmp');
            Route::post('moveToCollect/{newBenefactor}','newBenefactorsPostCtrl@end')->name('newBenefactors.end');
        });
    });


    // actual Benefactors:
    Route::prefix('actualClients')->group(static function () {
        Route::prefix('get')->group(static function () {
            Route::get('all','actualBenefactorsCtrl@all')->name('allActual');
            Route::get('withCollectingManager','actualBenefactorsCtrl@withCollectingManager')->name('withCollectingManager');
            Route::get('withCollectingSupervisor','actualBenefactorsCtrl@withCollectingSupervisor')->name('withCollectingSupervisor');
            Route::get('newOfMeAsSupervisor','actualBenefactorsCtrl@newOfMeAsSupervisor')->name('newOfMeAsSupervisor');
            Route::get('oldOfMeAsSupervisor','actualBenefactorsCtrl@oldOfMeAsSupervisor')->name('oldOfMeAsSupervisor');
            Route::get('stopped','actualBenefactorsCtrl@stopped')->name('stopped');
            Route::get('myStopped','actualBenefactorsCtrl@myStopped')->name('myStopped');
            Route::get('ofCall','actualBenefactorsCtrl@ofCall')->name('ofCall');
            Route::get('ofCollected','actualBenefactorsCtrl@ofCollected')->name('ofCollected');
            Route::get('{benefactor}','actualBenefactorsCtrl@show')->name('actualBenefactors.show');

        });
        Route::prefix('post')->group(static function () {
            Route::post('{benefactor}/toSupervisor','actualBenefactorsPostCtrl@toSupervisor')->name('toSupervisor');
            Route::post('{benefactor}/confirm','actualBenefactorsPostCtrl@confirm')->name('toSupervisor.confirm');
            Route::post('{benefactor}/stop','actualBenefactorsPostCtrl@stop')->name('toSupervisor.stop');
            Route::post('{benefactor}/delay','actualBenefactorsPostCtrl@delay')->name('toSupervisor.delay');
        });
    });

});
?>

<?php
Route::middleware(['auth'])->group(function () {

    //new Benefactors:
    Route::prefix('newCollectAssignments')->group(static function () {
        Route::prefix('get')->group(static function () {

        });
        Route::prefix('post')->group(static function () {
            Route::post('sendToAccountantAsRegular/{benefactor}','collectAssignmentsCtrl@sendToAccountantAsRegular')->name('sendToAccountantAsRegular');
            Route::post('sendToAccountantAsIrregular/{benefactor}','collectAssignmentsCtrl@sendToAccountantAsIrregular')->name('sendToAccountantAsIrregular');
            Route::post('changeEmpInCollectAssignment/{collectAssignment}','collectAssignmentsCtrl@changeEmpInCollectAssignment')->name('changeEmpInCollectAssignment');
        });
    });


    // actual Benefactors:
    Route::prefix('oldCollectAssignments')->group(static function () {
        Route::prefix('get')->group(static function () {

        });
        Route::prefix('post')->group(static function () {
        });
    });

});
?>

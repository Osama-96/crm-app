<?php


    Route::prefix('callCenter')->group(static function () {
        Route::post('newCall/{assignment}','callCenterCallsCtrl@store')->name('storeNewCall');
        Route::post('update/{call}','callCenterCallsCtrl@update')->name('updateNewCall');
    });

    Route::prefix('collectCenter')->group(static function () {
        Route::post('newCall/{benefactor}','collectCenterCallsCtrl@store')->name('storeNewCollectCall');
        Route::post('update/{call}','collectCenterCallsCtrl@update')->name('updateNewCollectCall');
    });

?>

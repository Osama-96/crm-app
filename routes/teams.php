<?php
Route::middleware(['auth'])->group(function () {

    Route::prefix('callCenter')->group(static function () {
        Route::get('team','callTeamCtrl@index')->name('callCenter.team');
        Route::get('team/{emp}','callTeamCtrl@show')->name('callCenter.show');
        Route::get('team/assignments/{assignment}','callTeamCtrl@assignment')->name('callCenter.emp.assignment');
    });

    Route::prefix('collectCenter')->group(static function () {
        Route::get('team','collectTeamCtrl@index')->name('collectCenter.team');
        Route::get('team/addAccountant','collectTeamCtrl@addAccountant')->name('addAccountant');

    });
});
?>

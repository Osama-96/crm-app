<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rate extends Model
{
    use SoftDeletes;
    protected $guarded = [''];

    public function rateable()
    {
        return $this->morphTo();
    }
    public function benefactorable()
    {
        return $this->morphTo();
    }
    public function collectionable()
    {
        return $this->morphTo();
    }
}

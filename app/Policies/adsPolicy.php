<?php

namespace App\Policies;

use App\Ad;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class adsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any ads.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        $users = User::wherePermissionIs('see_all_ads')->pluck('id')->toArray();
        return in_array($user->id,$users);
    }

    /**
     * Determine whether the user can view the ad.
     *
     * @param  \App\User  $user
     * @param  \App\Ad  $ad
     * @return mixed
     */
    public function view(User $user, Ad $ad)
    {
        $users = User::wherePermissionIs('see_single_ad')->pluck('id')->toArray();
        return in_array($user->id,$users);
    }

    /**
     * Determine whether the user can create ads.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        $users = User::wherePermissionIs('create_ads')->pluck('id')->toArray();
        return in_array($user->id,$users);
    }

    /**
     * Determine whether the user can update the ad.
     *
     * @param  \App\User  $user
     * @param  \App\Ad  $ad
     * @return mixed
     */
    public function update(User $user, Ad $ad)
    {
        $users = User::wherePermissionIs('update_ads')->pluck('id')->toArray();
        return in_array($user->id,$users);
    }

    /**
     * Determine whether the user can delete the ad.
     *
     * @param  \App\User  $user
     * @param  \App\Ad  $ad
     * @return mixed
     */
    public function delete(User $user, Ad $ad)
    {
        $users = User::wherePermissionIs('delete_ads')->pluck('id')->toArray();
        return in_array($user->id,$users);
    }

    /**
     * Determine whether the user can restore the ad.
     *
     * @param  \App\User  $user
     * @param  \App\Ad  $ad
     * @return mixed
     */
    public function restore(User $user, Ad $ad)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the ad.
     *
     * @param  \App\User  $user
     * @param  \App\Ad  $ad
     * @return mixed
     */
    public function forceDelete(User $user, Ad $ad)
    {
        //
    }
}

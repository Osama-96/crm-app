<?php

namespace App\Policies;

use App\Benefactor;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class NewBenefactorsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any benefactors.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        $users = User::wherePermissionIs('see_all_new_benefactor')->pluck('id')->toArray();
        return in_array($user->id,$users);
    }

    /**
     * Determine whether the user can view the benefactor.
     *
     * @param  \App\User  $user
     * @param  \App\Benefactor  $benefactor
     * @return mixed
     */
    public function view(User $user, Benefactor $benefactor)
    {
        //
    }

    /**
     * Determine whether the user can create benefactors.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        $users = User::wherePermissionIs('create_new_benefactor')->pluck('id')->toArray();
        return in_array($user->id,$users);
    }

    /**
     * Determine whether the user can update the benefactor.
     *
     * @param  \App\User  $user
     * @param  \App\Benefactor  $benefactor
     * @return mixed
     */
    public function update(User $user, Benefactor $benefactor)
    {
        $users = User::wherePermissionIs('update_new_benefactor')->pluck('id')->toArray();
        return in_array($user->id,$users);
    }

    /**
     * Determine whether the user can delete the benefactor.
     *
     * @param  \App\User  $user
     * @param  \App\Benefactor  $benefactor
     * @return mixed
     */
    public function delete(User $user, Benefactor $benefactor)
    {
        $users = User::wherePermissionIs('delete_new_benefactor')->pluck('id')->toArray();
        return in_array($user->id,$users);
    }

    /**
     * Determine whether the user can restore the benefactor.
     *
     * @param  \App\User  $user
     * @param  \App\Benefactor  $benefactor
     * @return mixed
     */
    public function restore(User $user, Benefactor $benefactor)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the benefactor.
     *
     * @param  \App\User  $user
     * @param  \App\Benefactor  $benefactor
     * @return mixed
     */
    public function forceDelete(User $user, Benefactor $benefactor)
    {
        //
    }
}

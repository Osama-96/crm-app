<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;
use App\Assignment;
use App\Benefactor;
use App\Call;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // check user has any of roles
    public function hasRoles(...$roles){ foreach ($roles as $role) if ($this->hasRole($role)) return true;}

    public function assignments()
    {
        return $this->morphMany( Assignment::class, 'assignmentable');
    }
    public function employeeAssignments()
    {
        return $this->morphMany( Assignment::class, 'employeeable');
    }
    public function newBenefactors()
    {
        return $this->morphMany( newBenefactor::class, 'benefactorable');
    }
    public function benefactors()
    {
        return $this->morphMany( Benefactor::class, 'userable');
    }
    public function calls()
    {
        return $this->morphMany( Call::class, 'userable');
    }
    public function benefactorsOfMeAsSupervisor()
    {
        return $this->morphMany( Benefactor::class, 'supervisorable');
    }
    public function collectAssignments()
    {
        return $this->morphMany( CollectAssignment::class, 'assignmentable');
    }
    public function collections()
    {
        return $this->morphMany( Collection::class, 'accountantable');
    }
    public function benefactorLocation()
    {
        return $this->morphMany( BenefactorLocation::class, 'userable');
    }

    public function benefactorRates()
    {
        return $this->morphMany( Rate::class, 'rateable');
    }
    public function faildCollectionsSupervisor()
    {
        return $this->morphMany( FaildCollection::class, 'supervisorable');
    }
    public function faildCollectionsemployee()
    {
        return $this->morphMany( FaildCollection::class, 'employeeable');
    }






}

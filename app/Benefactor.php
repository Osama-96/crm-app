<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Benefactor extends Model
{
    use SoftDeletes;
    protected $guarded = [''];

    public function adable()
    {
        return $this->morphTo();
    }
    public function assignments()
    {
        return $this->morphMany(Assignment::class ,'benefactorable');
    }
    public function benefactorable()
    {
        return $this->morphTo();
    }
    public function supervisorable()
    {
        return $this->morphTo();
    }
    public function calls()
    {
        return $this->morphMany(Call::class ,'benefactorable');
    }
    public function benefactorLocation()
    {
        return $this->morphMany(BenefactorLocation::class ,'benefactorable');
    }
    public function userable()
    {
        return $this->morphTo();
    }

    public function collectAssignments()
    {
        return $this->morphMany(CollectAssignment::class ,'benefactorable');
    }
    public function collections()
    {
        return $this->morphMany(Collection::class ,'benefactorable');
    }
    public function failedCollections()
    {
        return $this->morphMany(FaildCollection::class ,'benefactorable');
    }
    public function rates()
    {
        return $this->morphMany(Rate::class ,'benefactorable');
    }
    public function stopper()
    {
        return $this->belongsTo(User::class );
    }


    public function scopeWithCollectingManager($query)
    {
        return $query->where('withCollectingManager',1)->where('withCollectingSupervisor',0)->where('stopped',0);
    }

    public function scopeWithCollectingSupervisor($query)
    {
        return $query->where('withCollectingManager',0)->where('withCollectingSupervisor',1)->where('stopped',0);
    }

    public function scopeConfirmed($query)
    {
        return $query->where('confirmed',1);
    }

    public function scopeNotConfirmed($query)
    {
        return $query->where('confirmed',0);
    }

    public function scopeStopped($query)
    {
        return $query->where('stopped',1);
    }

    public function scopeMyStopped($query)
    {
        return $query->where('stopped',1)->where('supervisorable_id',auth()->user()->id);
    }

    public function getBenefactorStatus(){
        if ($this->active == 0) return 'جديد';
        elseif($this->active == 1 and $this->stopped == 1) return '<span data-toggle="tooltip" title=" توقف في:'.$this->stopped_at.' بسبب : '.$this->stopped_reason.'">'.'توقف تماماً'.'</span>';
        elseif($this->active == 1 and $this->delayed == 1) return 'مؤجل إلى: <br>'.'<small>'.getDateTime($this->delayed_to).'</small>';
        else return 'تم التأكيد';
    }
    public function isAyniBene(){
        if ($this->isAyni == 1) return true;
        else false ;
    }
    public function isMoneyBene(){
        if ($this->isMoney == 1) return true;
        else false ;
    }







}

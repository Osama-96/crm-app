<?php

namespace App\Http\Controllers\calls;

use App\Assignment;
use App\Call;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class callCenterCallsCtrl extends Controller
{
    public function store(Request $request ,Assignment $assignment)
    {
        $user = auth()->user();

        $call = $request->validate([
            'date'=>['required','date','before_or_equal:today'],
            'time'=>['required','string'],
            'answer_status'=>['required','string'],
            'status_more'=>['nullable','string'],
            'reacting'=>['nullable','integer','in:0,1'],
            'call_rate'=>['nullable','integer'],
            'question'=>['nullable','string'],
            'call_samary'=>['nullable','string'],
            'notes'=>['nullable','string'],
        ]);
        $call['userable_id'] = $user->id;
        $call['userable_type'] = User::class;
        $call['benefactorable_id'] = $assignment->benefactorable->id;
        $call['benefactorable_type'] = $assignment->benefactorable_type;
        $new=$assignment->calls()->create($call);
        alert()->success('عملية ناجحة','تم حفظ المكالمة بنجاح');
        return back();
    }
    public function update(Request $request, Call $call)
    {
        $user = auth()->user();
        $data= $request->validate([
            'date'=>['required','date','before_or_equal:today'],
            'time'=>['required','string'],
            'answer_status'=>['required','string'],
            'status_more'=>['nullable','string'],
            'reacting'=>['nullable','integer','in:0,1'],
            'call_rate'=>['nullable','integer'],
            'question'=>['nullable','string'],
            'call_samary'=>['nullable','string'],
            'notes'=>['nullable','string'],
        ]);
        $data['userable_id'] = $user->id;
        $data['userable_type'] = User::class;
        $call->update($data);
        alert()->success('عملية ناجحة','تم تحديث بيانات المكالمة بنجاح');
        return back();
    }

}

<?php

namespace App\Http\Controllers;

use App\Location;
Use Alert;
use App\User;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
//        if(!$user->can('see_all_locations')){
//            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
//            return back();
//        }
        //$this->authorize('viewAny',Location::class);
        $data = Location::orderBy('id','DESC')->get();
        return view('pages.marketing.locations',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
//        if(!$user->can('see_all_locations')){
//            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
//            return back();
//        }
        //$this->authorize('create',Location::class);
        $location= $request->validate([
            'name'=>['required','string','unique:locations'],
        ]);
        Location::create($location);
        alert()->success('عملية ناجحة','تم حفظ البيانات بنجاح');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        $user = auth()->user();
//        if(!$user->can('update_locations')){
//            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
//            return back();
//        }
        //$this->authorize('update',$location);
        $request->validate([
            'name'=>['required','string'],
        ]);
         $location->update(['name'=>$request->name]);
        alert()->toast('تم تعديل بيانات هذا العنصر بنجاح' , 'success');
        return  back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        $user = auth()->user();
//        if(!$user->can('delete_locations')){
//            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
//            return back();
//        }
        //$this->authorize('delete',$location);
        if ($location->ads()->count()){
            alert()->error('عملية فاشلة','لا يمكنك حذف هذه المنطقة لارتباط بعض الاعلانات بها');
            return  back();

        }
        $location->delete();
        alert()->toast('تم حذف هذا العنصر بنجاح!' , 'warning');
        return  back();
    }
}

<?php

namespace App\Http\Controllers\excel;

use App\Benefactor;
use App\Http\Controllers\Controller;
use App\newBenefactor;
use App\User;
use Illuminate\Http\Request;

class excelCtrl extends Controller
{
    public function showMarketingSheet()
    {
        $users = [];
        return view('pages.excel.marketingOne',compact('users'));
    }
    public function storeMarketingFile(Request $request)
    {
        $user = auth()->user();

        if($this->checkExcelFile($request->file('file')->getClientOriginalExtension()) == false) {
            //return validator with error by file input name
            alert()->errors()->add('file', 'لا بد وأن يكون الملف بإحدى هذه الصيغ: csv, xlsx, xls');
            return back();
        }
        $user =auth()->user();
        $users = fastexcel()->import($request->file);
        if(!in_array('name',array_keys(fastexcel()->import($request->file)[0]))
            || !in_array('email',array_keys(fastexcel()->import($request->file)[0]))
            || !in_array('phone',array_keys(fastexcel()->import($request->file)[0]))
            || !in_array('address',array_keys(fastexcel()->import($request->file)[0]))
            || !in_array('device',array_keys(fastexcel()->import($request->file)[0]))
        ){
            alert()->error('عملية خاطئة','من فضلك قم بإدخال البيانات في ترتيب وصيغة مقبولة');
            return back();
        }
        try {
            foreach ($users as $item){
                if($this->chickNotBenefactor($item['email'] , $item['phone'])){

                    newBenefactor::firstOrCreate([
                        'name'=>$item['name'],
                        'email'=>$item['email'],
                        'phone'=>$item['phone'],
                        'address'=>$item['address'],
                        'device'=>$item['device'],
                        'benefactorable_id'=>$user->id,
                        'benefactorable_type'=>User::class,
                    ]);
                }
            }
        }catch (\Exception $e){
            alert()->error('عملية فاشلة','لم يتم حفظ بيانات المتبرعين لوجود خطأ في أسماء الحقول، من فضلك تأكد من النمط الصحيح لملف الايكسيل '.$e);
            return view('pages.excel.marketingOne',compact('users'));
        }
        alert()->success('عملية ناجحة','تم حفظ بيانات المتبرعين ');
        return view('pages.excel.marketingOne',compact('users'));
    }










    public function checkExcelFile($file_ext){
        $valid=array(
            'csv','xls','xlsx' // add your extensions here.
        );
        return in_array($file_ext,$valid) ? true : false;
    }

    public function chickNotBenefactor($email=null , $phone=null)
    {
        if($email){
            if(newBenefactor::where('email',$email)->count()){
                return false;
            }
        }
        if($phone){
            if(newBenefactor::where('phone',$phone)->count()){
                return false;
            }
        }
        return true;
    }
}

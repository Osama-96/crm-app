<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;
use App\User;

class adminCtrl extends Controller
{

    public function users()
    {
        $for= auth()->user();
//        if(!$for->can('users_control')){
//            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
//            return back();
//        }
        $users = User::all();
        return view('pages.users.all',compact('users'));
    }


    public function updateRoles(Request $request, User $user)
    {
        $for= auth()->user();
//        if(!$for->can('users_control')){
//            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
//            return back();
//        }
        $request->validate([
            'roles'=>'required|min:1',
        ]);

        $user->syncRoles($request->roles);
        alert()->success('عملية ناجحة','تم تحديث صلاحيات هذا المستخدم بنجاح');
        return back();
    }
    public function updatePermissions(Request $request, User $user)
    {
        $for= auth()->user();
//        if(!$for->can('users_control')){
//            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
//            return back();
//        }
        $request->validate([
            'permissions'=>'required|min:1',
        ]);

        $user->syncPermissions($request->permissions);
        alert()->success('عملية ناجحة','تم تحديث صلاحيات هذا المستخدم بنجاح');
        return back();
    }


}

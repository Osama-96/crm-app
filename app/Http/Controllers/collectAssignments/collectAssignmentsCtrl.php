<?php

namespace App\Http\Controllers\collectAssignments;

use App\Benefactor;
use App\CollectAssignment;
use App\Http\Controllers\Controller;
use App\Notifications\NewCollectionAssignment;
use App\User;
use Illuminate\Http\Request;

class collectAssignmentsCtrl extends Controller
{
    public function sendToAccountantAsRegular(Request $request, Benefactor $benefactor)
    {
        $user = auth()->user();
        $data=$request->validate([
             'emp_id'=>'required|numeric',
             'notes'=>'string|nullable',
        ]);
        $emp = User::find($data['emp_id']);

        $assignment=$benefactor->collectAssignments()->create([
            'supervisorable_id'=>$user->id,
            'supervisorable_type'=>User::class,
            'accountantable_id'=>$emp->id,
            'accountantable_type'=>User::class,
            'regular'=>1,
            'notes'=>$data['notes'],
            'dueDate'=>$benefactor->dueDate,
        ]);
        if (\carbon\carbon::create(date('d.m.Y',strtotime($assignment->dueDate))) == today()){
            $emp->notify( new NewCollectionAssignment($assignment));
        }


        alert()->success('عملية ناجحة','تم إرسال الى المحصل وسوف يظهر له في يوم التحصيل ..');
        return back();
    }
    public function sendToAccountantAsIrregular(Request $request, Benefactor $benefactor)
    {
        $user = auth()->user();
        $data=$request->validate([
             'emp_id'=>'required|numeric',
             'donation'=>'required|string',
             'date'=>'required|date',
             'time'=>'required|string',
             'notes'=>'string|nullable',
        ]);
        $emp = User::find($data['emp_id']);
        $dueDate = \carbon\carbon::create(date('d.m.Y',strtotime($data['date'])).' '.date('H:i',strtotime($data['time'])));


        $assignment = $benefactor->collectAssignments()->create([
            'supervisorable_id'=>$user->id,
            'supervisorable_type'=>User::class,
            'accountantable_id'=>$emp->id,
            'accountantable_type'=>User::class,
            'irregular'=>1,
            'donation'=>$data['donation'],
            'notes'=>$data['notes'],
            'dueDate'=>$dueDate,
        ]);

        if (\carbon\carbon::create(date('d.m.Y',strtotime($assignment->dueDate))) == today()){
            $emp->notify( new NewCollectionAssignment($assignment));
        }

        alert()->success('عملية ناجحة','تم إرسال الى المحصل وسوف يظهر له في يوم التحصيل ..');
        return back();
    }

    public function changeEmpInCollectAssignment(Request $request, CollectAssignment $collectAssignment)
    {
        $user = auth()->user();
        $data=$request->validate([
             'emp_id'=>'required|numeric',
             'date'=>'required|date',
             'time'=>'required|string',
             'notes'=>'string|nullable',
        ]);
        $emp = User::find($data['emp_id']);
        $dueDate = \carbon\carbon::create(date('d.m.Y',strtotime($data['date'])).' '.date('H:i',strtotime($data['time'])));



        $collectAssignment->update([
            'supervisorable_id'=>$user->id,
            'supervisorable_type'=>User::class,
            'accountantable_id'=>$emp->id,
            'accountantable_type'=>User::class,
            'notes'=>$data['notes'],
            'dueDate'=>$dueDate,
        ]);
        if (\carbon\carbon::create(date('d.m.Y',strtotime($collectAssignment->dueDate))) == today()){
            $emp->notify( new NewCollectionAssignment($collectAssignment));
        }

        alert()->success('عملية ناجحة','تم استبدال المحصل بنجاح وسوف يظهر له المتبرع في يوم التحصيل المحدد ..');
        return back();
    }
}

<?php

namespace App\Http\Controllers\teams;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class collectTeamCtrl extends Controller
{
    public function addAccountant()
    {
        return view('pages.users.addUser');
    }
    public function storeAccountant(Request $request)
    {
        $request->validate([
           'name'=>'required|string' ,
           'email'=>'required|email|unique:users',
           'phone'=>'required|string'
        ]);
    }

}

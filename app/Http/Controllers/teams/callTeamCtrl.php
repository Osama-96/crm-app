<?php

namespace App\Http\Controllers\teams;

use App\Assignment;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class callTeamCtrl extends Controller
{
    public function index()
    {

        $emps = User::whereRoleIs('call_emp')->orWhereRoleIs('call_manager')->get();
        return view('pages.callCenter.team.index',compact('emps'));

    }

    public function show(User $emp)
    {
        $assignments = $emp->employeeAssignments()->orderBy('id','DESC')->get();
        return view('pages.callCenter.team.show',compact('emp','assignments'));
    }
    public function assignment( Assignment $assignment)
    {
        return view('pages.callCenter.team.assignment',compact('assignment'));
    }
}

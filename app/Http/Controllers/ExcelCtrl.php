<?php

namespace App\Http\Controllers;

use App\Benefactor;
use App\User;
use Illuminate\Http\Request;

class ExcelCtrl extends Controller
{
    public function checkExcelFile($file_ext){
        $valid=array(
            'csv','xls','xlsx' // add your extensions here.
        );
        return in_array($file_ext,$valid) ? true : false;
    }
    public function uploadFile()
    {
        $user = auth()->user();
        if(!$user->can('import_from_excel_sheet')){
            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
            return back();
        }
        $users = [];
        return view('pages.excel.marketingOne',compact('users'));
    }
    public function storeFile(Request $request)
    {
        $user = auth()->user();
        if(!$user->can('import_from_excel_sheet')){
            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
            return back();
        }
        if($this->checkExcelFile($request->file('file')->getClientOriginalExtension()) == false) {
            //return validator with error by file input name
            alert()->errors()->add('file', 'The file must be a file of type: csv, xlsx, xls');
            return back();
        }
        $user =auth()->user();
        $users = fastexcel()->import($request->file);
        try {
            foreach ($users as $item){
                if($this->chickNotBenefactor($item['email'] , $item['phone'])){

                    Benefactor::firstOrCreate([
                        'name'=>$item['name'],
                        'email'=>$item['email'],
                        'phone'=>$item['phone'],
                        'address'=>$item['address'],
                        'how'=>$item['how'],
                        'benefactorable_id'=>$user->id,
                        'benefactorable_type'=>User::class,
                    ]);
                }
            }
        }catch (\Exception $e){
            alert()->error('عملية فاشلة','لم يتم حفظ بيانات المتبرعين لوجود خطأ في أسماء الحقول، من فضلك تأكد من النمط الصحيح لملف الايكسيل '.$e);
            return view('pages.callManager.uploadExcel',compact('users'));
        }
        alert()->success('عملية ناجحة','تم حفظ بيانات المتبرعين ');
        return view('pages.callManager.uploadExcel',compact('users'));
    }

    public function chickNotBenefactor($email=null , $phone=null)
    {
        if($email){
            if(Benefactor::where('email',$email)->count()){
                return false;
            }
        }
        if($phone){
            if(Benefactor::where('phone',$phone)->count()){
                return false;
            }
        }
        return true;
    }
    public function storeBenefactors($users)
    {
        if($users){
            try {
                foreach ($users as $item){
                    Benefactor::create($item);
                }
            }catch (\Exception $e){
                alert()->error('عملية فاشلة','لم يتم حفظ بيانات المتبرعين لوجود خطأ في أسماء الحقول، من فضلك تأكد من النمط الصحيح لملف الايكسيل ');
                return view('pages.callManager.uploadExcel',compact('users'));
            }
        alert()->success('عملية ناجحة','تم حفظ بيانات المتبرعين ');
        return view('pages.callManager.uploadExcel');
        }else{
            alert()->error('عملية فاشلة','لم يتم حفظ بيانات المتبرعين ');
            return back();
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications;
use Illuminate\Support\Facades\Notification;


class notficationCtrl extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function markAsRead(Request $request)
    {
        $user = auth()->user();
        $notification = $user->unreadNotifications()->find($request->id);
        $notification->markAsRead();
        alert()->toast('تم تحديد هذا الإشعار كمقروء بنجاح','success');
        return back();
    }
    public function markAllAsRead()
    {
        dd('dsadad');
        $user=auth()->user();
        $user->unreadNotifications->markAsRead();
        alert()->toast('تم تحديد جميع الإشعارات كمقروءة','success');
        return back();
    }
}

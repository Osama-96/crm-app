<?php

namespace App\Http\Controllers\callAssignments;

use App\Assignment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class oldAssignmentsCtrl extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $assignments = $user->employeeAssignments()->where('done',1)->orWhere('failed',1)->orderBy('id','DESC')->get();
        return view('pages.callCenter.assignments.index',['assignments'=>$assignments]);
    }
    public function show( Assignment $assignment)
    {
        $user = auth()->user();
        if($assignment->employeeable_id != $user->id ){
            alert()->error('خطأ..','هذه العملية لم تخصص إليك');
            return back();
        }
        $assignments = $user->employeeAssignments()->orderBy('id','DESC')->get();
        return view('pages.callCenter.assignments.show',compact('assignment','assignments'));
    }
}

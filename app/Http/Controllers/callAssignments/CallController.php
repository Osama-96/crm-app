<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Call;
use App\User;
use Illuminate\Http\Request;

class CallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request ,Assignment $assignment)
    {
        $user = auth()->user();
        if(!$user->can('save_call')){
            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
            return back();
        }
        $user = auth()->user();

        $call = $request->validate([
            'date'=>['required','date','before_or_equal:today'],
            'time'=>['required','string'],
            'answer_status'=>['required','string'],
            'status_more'=>['nullable','string'],
            'reacting'=>['nullable','integer','in:0,1'],
            'call_rate'=>['nullable','integer'],
            'question'=>['nullable','string'],
            'call_samary'=>['nullable','string'],
            'notes'=>['nullable','string'],
        ]);
        $call['userable_id'] = $user->id;
        $call['userable_type'] = User::class;
        $assignment->calls()->create($call);
        alert()->success('عملية ناجحة','تم حفظ المكالمة بنجاح');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Call  $call
     * @return \Illuminate\Http\Response
     */
    public function show(Call $call)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Call  $call
     * @return \Illuminate\Http\Response
     */
    public function edit(Call $call)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Call  $call
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Call $call)
    {
        $user = auth()->user();
         $data= $request->validate([
            'date'=>['required','date','before_or_equal:today'],
            'time'=>['required','string'],
            'answer_status'=>['required','string'],
            'status_more'=>['nullable','string'],
            'reacting'=>['nullable','integer','in:0,1'],
            'call_rate'=>['nullable','integer'],
            'question'=>['nullable','string'],
            'call_samary'=>['nullable','string'],
            'notes'=>['nullable','string'],
        ]);
        $data['userable_id'] = $user->id;
        $data['userable_type'] = User::class;
        $call->update($data);
        alert()->success('عملية ناجحة','تم تحديث بيانات المكالمة بنجاح');
        return back();
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Call  $call
     * @return \Illuminate\Http\Response
     */
    public function destroy(Call $call)
    {
        //
    }
}

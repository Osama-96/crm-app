<?php

namespace App\Http\Controllers\callAssignments;

use App\Assignment;
use App\Benefactor;
use App\EditBenefactorRequest;
use App\Http\Controllers\Controller;
use App\newBenefactor;
use App\Notifications\callAssignDone;
use App\Notifications\callAssignFailed;
use App\Notifications\EditBenefactor;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class newAssignmentsCtrl extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $assignments = $user->employeeAssignments()->where('done',0)->where('failed',0)->orderBy('id','DESC')->get();
        return view('pages.callCenter.assignments.index',['assignments'=>$assignments]);
    }
    public function show( Assignment $assignment)
    {
        $user = auth()->user();
        if($assignment->employeeable_id != $user->id ){
            alert()->error('خطأ..','هذه العملية لم تخصص إليك');
            return back();
        }
        $assignments = $user->employeeAssignments()->orderBy('id','DESC')->get();
        return view('pages.callCenter.assignments.show',compact('assignment','assignments'));
    }

    public function storeData(Request $request,Assignment $assignment, newBenefactor $newBenefactor)
    { // edit benefactor request ........
        $user = auth()->user();
        $data=$request->validate([
            'name'=>['required','string'],
            'phone'=>['required','string'],
            'email'=>['required','email'],
            'address'=>['nullable','string'],
            'bene_type'=>['nullable','string'],
            'donation_type'=>['nullable','string'],
            'donation'=>['nullable','string'],
            'date'=>['nullable','date'],
            'time'=>['nullable','string'],
            'cases'=>['required','string'],
            ]);
        if($request->donation_type == 1 && (isset($data['donation']) && !is_numeric($data['donation'])) ){
            alert()->error('خطأ في البيانات المدخلة','لقد حددت نوع التبرع أنه تبرع مالي، من فضلك أدخل قيمة مالية صحيحة..');
            return back();
        }
        if($assignment->benefactorable_id != $newBenefactor->id || $assignment->employeeable_id != auth()->user()->id){
            alert()->error('خطأ !','خطأ في البيانات !');
            return back();
        }
        if (Benefactor::where('phone',$request->phone)->orWhere('email',$request->email)->count()){
            $item = Benefactor::where('phone',$request->phone)->orWhere('email',$request->email)->first();

            alert()->warning('البيانات مسجلة بالفعل','هذا المتبرع ليس بمحتمل أو جديد في البرنامج، فإن له بيانات مسجلة لدينا وقمنا بعمليات سابقة له   <table class="table table-bordered  ">
                        <tbody>
                        <tr>
                            <th> الاسم</th> <td> '.$item->name.'</td>
                        </tr>

                        <tr>
                            <th> الهاتف</th> <td>'.$item->phone.'</td>
                        </tr>
                        <tr>
                            <th>الايميل</th> <td>'.$item->email.'</td>
                        </tr>
                        <tr>
                            <th>الوسيلة</th> <td>'.$item->device.'</td>
                        </tr>

                        <tr>
                            <th>الاعلان </th> <td>'.($item->adable)? "رقم".$item->adable->id :"لا يوجد" .' </td>
                        </tr>

                        </tbody>
                    </table>'
            )->html()->persistent(true);
            return back();

        }
        $frequency = null;
        $isMoney = null;
        $moneyAmount = null;
        $isAyni = null;
        $donation = null;
        if(isset($data['bene_type']) && $data['bene_type'] == 1) $frequency = 'OneTime';
        elseif(isset($data['bene_type']) && $data['bene_type'] == 2) $frequency = 'EveryMonth';
        elseif(isset($data['bene_type']) && $data['bene_type'] == 3) $frequency = 'EveryFourMonths';
        elseif(isset($data['bene_type']) && $data['bene_type'] == 4) $frequency = 'EveryYear';
        else $frequency = 'OneTime';

        if(isset($data['donation_type']) && $data['donation_type'] == 1){ $isMoney = 1 ; $moneyAmount = $data['donation']; }
        if(isset($data['donation_type']) && $data['donation_type'] == 2){ $isAyni = 1 ; $donation = $data['donation']; }
        if(isset($data['date']) ){
        $dueDate = $data['date'].' '.$data['time'];
        }else{
            $dueDate = null;
        }

        try {
            DB::beginTransaction();
            $benefactor = $newBenefactor->actualBenefactor()->create([
                'name' => $data['name'],
                'phone' => $data['phone'],
                'address' => $data['address'],
                'email' => $data['email'],
                'cases' => $data['cases'],
                'adable_id' => $newBenefactor->adable_id,
                'adable_type' => $newBenefactor->adable_type,
                'userable_id' => $user->id,
                'userable_type' => User::class,
                'device' => $newBenefactor->device,
                'frequency' => $frequency,
                'dueDate' => $dueDate,
                'isAyni' => $isAyni,
                'donation' => $donation,
                'isMoney' => $isMoney,
                'moneyAmount' => $moneyAmount,
                'withCallCenter'=>1,
            ]);
            $newBenefactor->update(['moved'=>1]);
            $user = auth()->user();
            $users = User::whereRoleIs('call_manager')->get();
            Notification::send($users,new EditBenefactor($user,$benefactor));
            alert()->success('عملية ناجحة','تم تعديل بيانات المتبرع بنجاح');
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            alert()->error('خطأ !','حدث خطا ما!'.$exception);
        }
        return back();
    }
    public function updateData(Request $request, Benefactor $benefactor)
    { // edit benefactor request ........
        $data=$request->validate([
            'name'=>['required','string'],
            'phone'=>['required','string'],
            'email'=>['required','email'],
            'address'=>['nullable','string'],
            'bene_type'=>['nullable','string'],
            'donation_type'=>['nullable','string'],
            'donation'=>['nullable','string'],
            'date'=>['nullable','date'],
            'time'=>['nullable','string'],
            'cases'=>['required','string'],
            ]);
        if($request->donation_type == 1 && !is_numeric($request->donation) ){
            alert()->error('خطأ في البيانات المدخلة','لقد حددت نوع التبرع أنه تبرع مالي، من فضلك أدخل قيمة مالية صحيحة..');
            return back();
        }


        $frequency = null;
        $isMoney = null;
        $moneyAmount = null;
        $isAyni = null;
        $donation = null;
        if(isset($data['bene_type']) && $data['bene_type'] == 1) $frequency = 'OneTime';
        if(isset($data['bene_type']) && $data['bene_type'] == 2) $frequency = 'EveryMonth';
        if(isset($data['bene_type']) && $data['bene_type'] == 3) $frequency = 'EveryFourMonths';
        if(isset($data['bene_type']) && $data['bene_type'] == 4) $frequency = 'EveryYear';
        if(isset($data['donation_type']) && $data['donation_type'] == 1){ $isMoney = 1 ; $moneyAmount = $data['donation']; }
        if(isset($data['donation_type']) && $data['donation_type'] == 2){ $isAyni = 1 ; $donation = $data['donation']; }
        $dueDate = $data['date'].' '.$data['time'];

        try {
             $benefactor->update([
                'name' => $data['name'],
                'phone' => $data['phone'],
                'address' => $data['address'],
                'email' => $data['email'],
                'cases' => $data['cases'],
                'frequency' => $frequency,
                'dueDate' => $dueDate,
                'isAyni' => $isAyni,
                'donation' => $donation,
                'isMoney' => $isMoney,
                'moneyAmount' => $moneyAmount,
            ]);
           alert()->success('عملية ناجحة','تم تعديل بيانات المتبرع بنجاح');
        }catch (\Exception $exception){
            DB::rollBack();
            alert()->error('خطأ !','حدث خطا ما!'.$exception);
        }
        return back();
    }
    public function done(Request $request,Assignment $assignment)
    {

        if($assignment->benefactorable_type == 'App\newBenefactor'){
            if(!$assignment->benefactorable->actualBenefactor ){
                $assignment->benefactorable->update([
                    'notMoved'=>1,
                ]);
            }
        }

        $user = auth()->user();
        $assignment->update([
            'done'=>1,
            'done_at'=>now(),
        ]);

        $assignment->benefactorable()->update([
            'failed'=>0,
        ]);

        Notification::send($assignment->assignmentable,new callAssignDone($user,$assignment));
        alert()->success('عملية ناجحة','تم إرسال البيانات إلى رئيس القسم وتحديث حالة العملية بنجاح');
        return redirect()->route('oldAssignments.index');
    }
    public function failed(Request $request,Assignment $assignment)
    {
        $user = auth()->user();
        $assignment->update([
            'failed'=>1,
            'failed_at'=>now(),
        ]);
        $assignment->benefactorable()->update([ 'failed'=>1]);
        Notification::send($assignment->assignmentable,new callAssignFailed($user,$assignment));
        alert()->success('عملية ناجحة','تم إرسال البيانات إلى رئيس القسم وتحديث حالة العملية بنجاح');
        return redirect()->route('oldAssignments.index');
    }
}

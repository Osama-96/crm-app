<?php

namespace App\Http\Controllers;
use App\Assignment;
use App\Benefactor;
use Illuminate\Http\Request;

class benefactorCtrl extends Controller
{
    public function dataFile($id , $assignment=null) {

        $user = auth()->user();
//        if(!$user->can('see_basicData_benefactor')){
//            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
//            return back();
//        }
        $benefactor = Benefactor::findOrFail($id);
        $assignments = $benefactor->assignments;
        if($assignment){
            $assignment= Assignment::where('employeeable_id',$user->id)->where('id',$assignment)->first();
        }
        $collectAssignments = $benefactor->collectAssignments()->get();
        return view('pages.benefactors.dataFile',compact('benefactor','assignments','collectAssignments','assignment'));
    }
    public function stopAllHisActivities(Request $request,Benefactor $benefactor)
    {
        $user = auth()->user();
//        if(!$user->can('stop_benefactor')){
//            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
//            return back();
//        }
        $request->validate([
            'faild_reason'=>['sometimes','string']
        ]);
        $benefactor->update([
            'isStopped'=>1,
            'stopped_at'=>now(),
            'stopped_reason'=>$request->faild_reason,
            'stopper_id'=>auth()->user()->id,

        ]);
        alert()->success('عملية ناجحة','تم تأكيد العملية بنجاح');
        return back();
    }
    public function restore(Request $request,Benefactor $benefactor){
        $user = auth()->user();
//        if(!$user->can('reActive_benefactor')){
//            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
//            return back();
//        }
        $benefactor->update([
            'isStopped'=>0,
            'stopped_at'=>null,
            'stopped_reason'=>null,
            'stopper_id'=>null,
        ]);
        alert()->success('عملية ناجحة','تم تأكيد العملية بنجاح');
        return back();
    }



}

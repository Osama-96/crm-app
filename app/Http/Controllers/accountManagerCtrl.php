<?php

namespace App\Http\Controllers;

use App\Benefactor;
use App\CollectAssignment;
use App\Notifications\NewCollectionAssignment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class accountManagerCtrl extends Controller
{
    public function toCall()
    {

        $user= auth()->user();
        if(!$user->can('see_all_need_to_confirm')){
            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
            return back();
        }
        $data = CollectAssignment::where('confirmed',0)->where('done',0)->where('failed',0)->get();
        return view('pages.accountManager.toCall.index',compact('data'));
    }
    public function confirmed()
    {


        $user= auth()->user();
        if(!$user->can('see_all_confirmed_to_collect')){
            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
            return back();
        }
        $data = CollectAssignment::where('confirmed',1)->where('done',0)->where('failed',0)->orderBy('id','ASC')->get();
        return view('pages.accountManager.toCall.confirmed',compact('data'));
    }
    public function faild()
    {

        $user= auth()->user();
        if(!$user->can('see_all_refused_from_ConfirmCall_to_collect')){
            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
            return back();
        }
        $data = CollectAssignment::where('failed',1)->orderBy('id','DESC')->get();
        return view('pages.accountManager.toCall.faild',compact('data'));
    }
    public function delayed()
        {

            $user= auth()->user();
            if(!$user->can('see_all_delayed_to_collect')){
                alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
                return back();
            }
            $data = CollectAssignment::where('delayed',1) ->where('confirmed',0)
                ->where('failed',0)->orderBy('delayed_to','ASC')->get();
            return view('pages.accountManager.toCall.delayed',compact('data'));
        }

    public function confirm(Request $request,CollectAssignment $assignment)
    {

        $user= auth()->user();
        if(!$user->can('confirm_collect_call')){
            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
            return back();
        }
        $request->validate([
            'confirmed_note'=>['sometimes','string']
        ]);
        $assignment->update([
            'confirmed'=>1,
            'confirmed_at'=>now(),
            'confirmed_notes'=>$request->confirmed_notes,
        ]);
        alert()->success('عملية ناجحة','تم تأكيد العملية بنجاح');
        return back();
    }
    public function delay(Request $request,CollectAssignment $assignment)
    {

        $user= auth()->user();
        if(!$user->can('delay_collect_call')){
            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
            return back();
        }
        $request->validate([
            'delayed_to'=>['required','date','after:today'],
            'delayed_reason'=>['sometimes','string']
        ]);
        $assignment->update([
            'confirmed'=>0,
            'delayed'=>1,
            'delayed_to'=>$request->delayed_to,
            'delayed_reason'=>$request->delayed_reason,
            'failed'=>0,
        ]);
        alert()->success('عملية ناجحة','تم تأكيد العملية بنجاح');
        return back();
    }
    public function reject(Request $request,CollectAssignment $assignment)
    {

        $user= auth()->user();
        if(!$user->can('refuse_collect_call')){
            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
            return back();
        }
        $request->validate([
            'faild_reason'=>['sometimes','string']
        ]);
        $assignment->update([
            'failed'=>1,
            'failed_reason'=>$request->faild_reason,
        ]);
        alert()->success('عملية ناجحة','تم تأكيد رفض العملية بنجاح');
        return back();
    }

    public function assignToCollect(Request $request,CollectAssignment $assignment)
    {

        $user= auth()->user();
        if(!$user->can('assign_to_collect_team')){
            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
            return back();
        }
        $user = auth()->user();
        $request->validate([
            'notes'=>['nullable','string'],
            'employeeable_id'=>['sometimes','integer'],
            'subervisorable_id'=>['sometimes','integer'],
        ]);
        if($request->employeeable_id == 0 && $request->subervisorable_id == 0 ){
            alert()->error('خطأ','لم تختر أي شخص ليقوم بالتحصيل؟؟  ... أروح أحصل أنا يعني؟؟؟؟؟؟؟؟؟؟؟؟؟؟');
            return back();
        }
        try {
             $collection = $assignment->collections()->create([
                'benefactorable_id'=>$assignment->benefactorable_id,
                'benefactorable_type'=>Benefactor::class,
               'supervisorable_id'=>$request->subervisorable_id,
                'supervisorable_type'=>User::class,
                'employeeable_id'=>$request->employeeable_id,
                'employeeable_type'=>User::class,
                'notes'=>$request->notes,
            ]);
             $users = User::where('id',$request->subervisorable_id)->orWhere('id',$request->subervisorable_id)->get();
             Notification::send($users ,new NewCollectionAssignment($user, $assignment));
        }catch (\Exception $e){
            dd($e);
            alert()->error('خطأ','حدث خطأ ما ....');
            return back();
        }
        alert()->success('عملية ناجحة','تمت العملية بنجاح');
        return back();
    }

    public function team()
    {

        $user= auth()->user();
        if(!$user->can('see_all_collection_team')){
            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
            return back();
        }
        $emps = User::whereRoleIs('account_supervisor')->orWhereRoleIs('accountant')->orWhereRoleIs('account_manager')->get();
        return view('pages.accountManager.team',compact('emps'));
    }
    public function emp(User $emp)
    {

        $user= auth()->user();
        if(!$user->can('see_collection_emp')){
            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
            return back();
        }
        $superCollections = $emp->collectionsSupervisor()->get();
        $empCollecttions = $emp->collectionsemployee()->get();
        return view('pages.accountManager.emp',compact('emp','superCollections','empCollecttions'));
    }

}

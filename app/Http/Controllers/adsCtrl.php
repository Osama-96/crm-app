<?php

namespace App\Http\Controllers;

use App\Notifications\NewAdSaved;
use App\Notifications\NewBenefactorSaved;
use App\User;
use Illuminate\Http\Request;
Use App\Ad;
use Illuminate\Support\Facades\Notification;
use Redirect;
use DB;
use Exception;
Use Alert;

use App\Location;
class adsCtrl extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(
            'destroy'
        );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user= auth()->user();
//        if(!$user->can('see_all_ads')){
//            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
//            return back();
//        }
        //$this->authorize('viewAny',Ad::class);
        $data = Ad::orderBy('id','DESC')->get();
        return view('pages.ads.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user= auth()->user();
//        if(!$user->can('create_ads')){
//            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
//            return back();
//        }
        //$this->authorize('create',Ad::class);
        $locations = Location::all();
        return view('pages.ads.create',compact('locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user= auth()->user();
//        if(!$user->can('create_ads')){
//            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
//            return back();
//        }
        //$this->authorize('create',Ad::class);
        $user = auth()->user();
        $users = User::whereRoleIs('admin')->orWhereRoleIs('marketing_officer')->orWhereRoleIs('call_manager')->get();
        $ad=$request->validate([
            'type'=>['required','string'],
            'cost'=>['required','integer','max:100000'],
            'starts_at'=>['required','date'],
            'ends_at'=>['nullable','date'],
            'place'=>['sometimes','integer'],
            'link'=>['sometimes','url'],
        ]);
        request()->validate([
            'file' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        if($request->hasFile('file')){
            $file = $request->file('file');
            $path = 'uploads/ads/'; // upload path
            $filename = 'image'.date('Y-m-d-h-i-s').'.'.$file->getClientOriginalExtension() ;
            $file->move(public_path().'/'.$path,$filename);
            $ad['file'] = $path.$filename;
        }

        try{
            $ad = Ad::create($ad);
            Notification::send($users,new NewAdSaved( $ad, $user));
            alert()->success('عملية ناجحة','تم حفظ الإعلان بنجاح');
        }catch (Exception $ex){
            alert()->error('خطأ','حدث خطأ ما أثناء معالجة البيانات');
            return back();
        }
        return redirect()->route('ads.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Ad $ad)
    {
        $user= auth()->user();
//        if(!$user->can('see_single_ad')){
//            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
//            return back();
//        }
        //$this->authorize('view',$ad);
        $locations = Location::all();
        return view('pages.ads.show',compact('ad','locations'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ad $ad)
    {
        $user= auth()->user();
//        if(!$user->can('update_ads')){
//            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
//            return back();
//        }
        //$this->authorize('update',$ad);
        $new = $request->validate([
            'type'=>['required','string'],
            'cost'=>['required','integer','max:100000'],
            'starts_at'=>['required','date'],
            'ends_at'=>['nullable','date'],
            'place'=>['sometimes','integer'],
            'link'=>['sometimes','url'],
        ]);

        if($request->hasFile('file')){
            request()->validate([
                'file' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            \File::Delete($ad->file);
            $file = $request->file('file');
            $path = 'uploads/ads/'; // upload path
            $filename = 'image'.date('Y-m-d-h-i-s').'.'.$file->getClientOriginalExtension() ;
            $file->move(public_path().'/'.$path,$filename);
            $new['file'] = $path.$filename;
        }
        $ad->update($new);
        alert()->toast('تم تعديل بيانات هذا العنصر بنجاح' , 'success');
        return  back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ad $ad , Request $request)
    {
        $user= auth()->user();
//        if(!$user->can('delete_ads')){
//            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
//            return back();
//        }
        //$this->authorize('delete',$ad);
        $user = auth()->user();
        if (password_verify($request->password, $user->password)) {
            \File::Delete($ad->file);
            $ad->delete();
            alert()->toast('تم حذف هذا العنصر بنجاح!' , 'warning');
            return  redirect()->route('ads.index');
        }else{
            alert()->error('لا توجد لديك صلاحية','لا يمكنك حذف هذا العنصر');
            return back();
        }
    }
}

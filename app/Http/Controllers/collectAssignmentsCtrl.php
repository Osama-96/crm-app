<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Benefactor;
use App\CollectAssignment;
use App\Notifications\DoneCollection;
use App\Notifications\FailedCollection;
use App\Notifications\newAssignForCollect;
use App\User;
use foo\bar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use phpDocumentor\Reflection\Types\Collection;

class collectAssignmentsCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function nowatime()
    {
        $user = auth()->user();

        $superCollections = $user->collectionsSupervisor()->where('collected',0)->where('failed',0)->get();
        $empCollecttions = $user->collectionsemployee()->where('collected',0)->where('failed',0)->get();

        return view('pages.accountTeam.new',compact('superCollections','empCollecttions'));
    }
     public function old()
    {
        $user = auth()->user();

        $superCollections = $user->collectionsSupervisor()->get();
        $empCollecttions = $user->collectionsemployee()->get();

        return view('pages.accountTeam.old',compact('superCollections','empCollecttions'));
    }
    public function immediate ()
    {
        $user = auth()->user();
        if(!$user->can('see_all_now_atime_collections')){
            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
            return back();
        }
        $assignments = CollectAssignment::where('done',0)->where('failed',0)->get();
        return view('pages.accountTeam.immdiate',compact('assignments'));
    }
    public function allFailds()
    {
        $user = auth()->user();
        if(!$user->can('see_all_failed_collections')){
            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
            return back();
        }
        $assignments = CollectAssignment::where('failed',1)
            ->where('confirmed',1)
            ->where('done',0)->get();
        return view('pages.accountTeam.faild',compact('assignments'));
    }
    public function done()
    {

        $assignments = CollectAssignment::where('done',1)->get();
        return view('pages.accountTeam.done',compact('assignments'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = auth()->user();
        $assignment =Assignment::find($id);
        return view('pages.collect.assignments.show',compact('assignment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function collectedDone(\App\Collection $collection,Request $request)
    {
        $user = auth()->user();

        $request->validate([
            'date'=>['date','required'],
            'time'=>['required'],
            'benefactor_rate'=>['integer','required'],
            'comment'=>['nullable','string'],
            'location'=>['nullable','string'],
            'notes'=>['nullable','string'],
            'money_amount'=>['integer','nullable'],
            'donation'=>['string','nullable'],
            'bill_no'=>['integer','nullable'],
            'notes'=>['string','nullable'],
        ]);
        $date   = $request->date.' '.$request->time;
        $rate   = $request->benefactor_rate;
        $comment=$request->comment;
        $location=$request->location;
        $notes=$request->notes;
        $money_amount = $request->money_amount;
        $donation =$request->donation;
        $bill_no = $request->bill_no;
        if(!$money_amount && !$donation){
            alert()->error('خطأ','حضرتك مدخلتش لا فلوس ولا تبرعات تانيه.. طيب ليه حضرتك شايف انها عملية تحصيل ناجحة يعني... اقنعني!!');
            return back();
        }
        try {
            DB::beginTransaction();
            $collection->update([
                'failed'=>0,
                'failed_at'=>null,
                'collected'=>1,
                'collected_at'=>$date,
                'collected_notes'=> $notes,
                'bill_no'=> $bill_no,
                'money_amount'=> $money_amount,
                'donation_amount'=> $donation,
            ]);
            if($location){
                $collection->benefactorable()->update([
                    'location_pin'=>$location,
                ]);
            }
            $user->benefactorRates()->create([
                'benefactorable_id'=>$collection->benefactorable->id,
                'benefactorable_type'=>Benefactor::class,
                'assignmentable_id'=>$collection->id,
                'assignmentable_type'=>\App\Collection::class,
                'rate'=>$rate,
                'comment'=>$comment,
            ]);
            if($collection->benefactorable->frequency == 'EveryMonth'){
                $date = \carbon\carbon::create($collection->benefactorable->dueDate);
                $date->addDays(30);
                $collection->benefactorable()->update([
                    'dueDate'=>$date,
                ]);
            }
            $collection->collectassignmentable()->update([
                'done'=>1,
            ]);
            $managers = User::whereRoleIs('account_manager')->get();
            Notification::send($managers,new DoneCollection($user,$collection));

            DB::commit();
            alert()->success('عملية ناجحة','تم حفظ البيانات بنجاح');
            return  back();
        }catch (\Exception $e){
            DB::rollBack();
            dd($e);
            alert()->error('خطأ','حدث خطأ ما'.$e);
            return  back();
        }
    }
    public function failedCollect(\App\Collection $collection,Request $request)
    {
        $user = auth()->user();
        $request->validate([
            'date'=>['date','required'],
            'time'=>['required'],
            'benefactor_rate'=>['integer','required'],
            'comment'=>['nullable','string'],
            'location'=>['nullable','string'],
            'notes'=>['nullable','string'],
        ]);
        $date   = $request->date.' '.$request->time;
        $rate   = $request->benefactor_rate;
        $comment=$request->comment;
        $location=$request->location;
        $notes=$request->notes;
        try {
            DB::beginTransaction();
            $faildCollection = $collection->collectassignmentable->failedCollections()->create([
                'benefactorable_id'=>$collection->benefactorable_id,
                'benefactorable_type'=>Benefactor::class,
                'supervisorable_id'=>$collection->supervisorable_id,
                'supervisorable_type'=>User::class,
                'employeeable_id'=>$collection->employeeable_id,
                'employeeable_type'=>User::class,
                'failed_at'=>$date,
                'failed_reason'=> $notes,
            ]);
            if($location){
                $collection->benefactorable()->update([
                    'location_pin'=>$location,
                ]);
            }
            $user->benefactorRates()->create([
                'benefactorable_id'=>$collection->benefactorable->id,
                'benefactorable_type'=>Benefactor::class,
                'assignmentable_id'=>$faildCollection->id,
                'assignmentable_type'=>\App\FaildCollection::class,
                'rate'=>$rate,
                'comment'=>$comment,
            ]);
            $managers = User::whereRoleIs('account_manager')->get();
            Notification::send($managers,new FailedCollection($user,$faildCollection));
            DB::commit();
            alert()->success('عملية ناجحة','تم حفظ البيانات بنجاح');
            return  back();
        }catch (\Exception $e){
            DB::rollBack();
            dd($e);
            alert()->error('خطأ','حدث خطأ ما');
            return  back();
        }
    }
    public function changeEmpForCollect(\App\Collection $collection,Request $request)
    {

        $user = auth()->user();
        $request->validate([
            'employeeable_id'=>['required','integer'],
        ]);


        $collection->update([
            'employeeable_id'=>$request->employeeable_id,
        ]);
        $assignment = $collection->collectassignmentable->callAssignmentable;
        Notification::send($collection->employeeable,new newAssignForCollect($user,$assignment));
        alert()->success('عملية ناجحة','تم حفظ البيانات بنجاح');
        return  back();

    }
    public function markAsfailed(CollectAssignment $assignment,Request $request)
    {
        $user = auth()->user();
        $request->validate([
            'failed_reason'=>['string','required'],
        ]);
        $assignment->update([
            'failed'=>1,
            'failed_reason'=>$request->failed_reason,
        ]);
        alert()->success('عملية ناجحة','تم غيير حالة العملية بنجاح البيانات بنجاح');
        return  back();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

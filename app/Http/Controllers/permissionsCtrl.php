<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;

class permissionsCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user= auth()->user();
        if(!$user->can('permissions_control')){
            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
            return back();
        }
        $permissions = Permission::all();
        return view('pages.users.permissions',compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user= auth()->user();
        if(!$user->can('permissions_control')){
            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
            return back();
        }
        $request->validate([
            'name'=>'required|string',
            'display_name'=>'required|string',
            'description'=>'nullable|string',
        ]);

        Permission::create([
            'name' => $request->name,
            'display_name' => $request->display_name,
            'description' => $request->description,
        ]);
        alert()->success('عملية ناجحة','تم حفظ صلاحية جديدة بنجاح');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        $user= auth()->user();
        if(!$user->can('permissions_control')){
            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
            return back();
        }
        $request = $request->validate([
            'display_name'=>'required|string',
            'description'=>'nullable|string',
        ]);

        $permission->update($request);
        alert()->success('عملية ناجحة','تم حفظ صلاحية جديدة بنجاح');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        //
    }
}

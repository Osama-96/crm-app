<?php

namespace App\Http\Controllers\benefactors;

use App\Benefactor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use carbon\carbon;
class actualBenefactorsCtrl extends Controller
{
    // all actual benefactors:
    public function all(){
        $data = Benefactor::all();
//        dd($data);
        return view('pages.actualBenefactors.index',compact('data'));
    }

    // all with accountCenter manager && not with supervisors yet:
    public function withCollectingManager()
    {
        $data = Benefactor::withCollectingManager()->get();
        //        dd($data);
        return view('pages.actualBenefactors.index',compact('data'));
    }

    // all with supervisors:
    public function withCollectingSupervisor()
    {
        $data = Benefactor::withCollectingSupervisor()->orderBy('dueDate')->get();
        //        dd($data);
        return view('pages.actualBenefactors.index',compact('data'));
    }

    // all Stopped Benefactors:
    public function stopped()
    {
        $data = Benefactor::stopped()->get();
        return view('pages.actualBenefactors.index',compact('data'));
    }
    // all Stopped Benefactors of mine:
    public function myStopped()
    {
        $data = Benefactor::myStopped()->get();
        return view('pages.actualBenefactors.index',compact('data'));
    }

    public function newOfMeAsSupervisor()
    {
        $user = auth()->user();
        $data = $user->benefactorsOfMeAsSupervisor()->where('confirmed',0)->get();
//        dd($data);
        return view('pages.actualBenefactors.index',compact('data'));
    }

    public function oldOfMeAsSupervisor()
    {
        $user = auth()->user();
        $data = $user->benefactorsOfMeAsSupervisor()->where('confirmed',1)->get();
        dd($data);

        return view('pages.actualBenefactors.index',compact('data'));
    }
    public function ofCall()
    {
        $user = auth()->user();
        $end = carbon::today()->addDays(5);

        $data = $user->benefactorsOfMeAsSupervisor()->where('dueDate','<=',$end)->where('dueDate','>=',now())->orderByDesc('confirmed')->orderBy('dueDate')->get();
        return view('pages.actualBenefactors.index',compact('data'));
    }
    public function ofCollected()
    {
        $user = auth()->user();
        $start = carbon::today()->subDays(5);
        $data = $user->benefactorsOfMeAsSupervisor()->orderBy('dueDate')->whereHas('collectAssignments', function($query) use ($start) {
            $query->where('created_at', '<', now())->where('created_at', '>=', $start);
        })->get();
        return view('pages.actualBenefactors.index',compact('data'));
    }

    public function show(Benefactor $benefactor)
    {
        return view('pages.actualBenefactors.show',compact('benefactor'));
    }

}

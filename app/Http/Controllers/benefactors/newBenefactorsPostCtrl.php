<?php

namespace App\Http\Controllers\benefactors;

use App\Ad;
use App\Benefactor;
use App\Http\Controllers\Controller;
use App\newBenefactor;
use App\Notifications\AssignToCallEmp;
use App\Notifications\NewBenefactorSaved;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class newBenefactorsPostCtrl extends Controller
{
    public function store(Request $request)
    {
        $user = auth()->user();
        $users = User::whereRoleIs('admin')->orWhereRoleIs('admin')->orWhereRoleIs('call_manager')->get();
        $benefactor=$request->validate([
            'name'=>['required','string'],
            'phone'=>['sometimes','string','unique:benefactors'],
            'email'=>['sometimes','email','unique:benefactors'],
            'device'=>['nullable','string'],
            'ad'=>['required','integer'],
        ]);
        try{

            DB::beginTransaction();
            $benefactor = $user->newBenefactors()->create([
                'name'=>$request->name,
                'phone'=>$request->phone,
                'email'=>$request->email,
                'device'=>$request->device,
                'adable_id'=>$request->ad,
                'adable_type'=>Ad::class,
            ]);
            Notification::send($users,new NewBenefactorSaved($user,$benefactor));
            alert()->success('عملية ناجحة','تم حفظ البيانات بنجاح');
            DB::commit();
        }catch (Exception $ex){
            DB::rollback();
            alert()->error('خطأ','حدث خطأ ما أثناء معالجة البيانات'.$ex);
            return back();
        }
        return redirect()->route('newBenefactors.new');
    }
    public function update(Request $request, newBenefactor $newBenefactor)
    {
        $user = auth()->user();

        $request->validate([
            'name'=>['required','string'],
            'phone'=>['sometimes','string','regex:/(0)[0-9]{9}/'],
            'email'=>['nullable','email'],
            'device'=>['nullable','string'],
            'ad'=>['required','integer'],
        ]);
        try{
            $newBenefactor->update([
                'name'=>$request->name,
                'phone'=>$request->phone,
                'email'=>$request->email,
                'device'=>$request->device,
                'adable_id'=>$request->ad,
                'adable_type'=>Ad::class,
            ]);
            //dd($newBenefactor);
            alert()->success('عملية ناجحة','تم تحديث البيانات بنجاح');
        }catch (Exception $ex){
            alert()->error('خطأ','حدث خطأ ما أثناء معالجة البيانات');
        }
        return back();
    }

    public function send(newBenefactor $newBenefactor)
    {
        try {
            $newBenefactor->update([
                'sent'=>1
            ]);

            alert()->success('عملية ناجحة','تم الارسال لرئيس قسم الكول سنتر بنجاح');
            return back();
        }catch (\Exception $e){
            alert()->error('عملية فاشلة','حدث خطأ ما'.$e);
            return back();
        }
    }
    public function assign(Request $request, newBenefactor $newBenefactor)
    {
        $emps = $this->callEmps();
        $request->validate([
            'message'=>['sometimes','string'],
            'emp_id'=>['required','integer',],
        ]);
        $user = auth()->user();
        if(!in_array($request->emp_id ,$emps )){
            alert()->error('خطأ','هذا الموظف ليس موظف في قسم الكول سنتر');
            return back();
        }
        if(Benefactor::where('phone',$newBenefactor->phone)->orWhere('email',$newBenefactor->email)->count()){
            $item= Benefactor::where('phone',$newBenefactor->phone)->orWhere('email',$newBenefactor->email)->count();
            alert()->warning('البيانات مسجلة بالفعل','هذا المتبرع ليس بمحتمل أو جديد في البرنامج، فإن له بيانات مسجلة لدينا وقمنا بعمليات سابقة له ')->footer('<a href="'.route('actualBenefactors.show',$item).'" class="btn btn-warning">الاطلاع على الأرشيف الخاص به</a>')->persistent(true);
            return back();
        }
        try{
            DB::beginTransaction();
            $assignment = $user->assignments()->create([
                'employeeable_id'=>$request->emp_id,
                'employeeable_type' =>User::class,
                'benefactorable_id'=>$newBenefactor->id,
                'benefactorable_type' =>newBenefactor::class,
                'message'=>$request->message,
            ]);
            $newBenefactor->update([
                'assigned'=>1
            ]);

            Notification::send($assignment->employeeable,new AssignToCallEmp($user,$assignment));
            alert()->success('عملية ناجحة','تم إرسال بيانات المتبرع للموظف المحدد بنجاح');
            DB::commit();
        }catch (Exception $ex){
            DB::rollback();
            alert()->error('خطأ','حدث خطأ ما أثناء معالجة البيانات'.$ex);
        }
        return back();

    }

    public function callEmps()
    {
        return User::whereRoleIs('admin')->orWhereRoleIs('call_emp')->orWhereRoleIS('call_manager')->pluck('id')->toArray();
    }
    public function changeEmp(Request $request , newBenefactor $newBenefactor)
    {
        $emps = $this->callEmps();
        $request->validate([
            'message'=>['sometimes','string'],
            'emp_id'=>['required','integer',],
        ]);
        $user = auth()->user();
        if(!in_array($request->emp_id ,$emps )){
            alert()->error('خطأ','هذا الموظف ليس موظف في قسم الكول سنتر');
            return back();
        }
        if(Benefactor::where('phone',$newBenefactor->phone)->orWhere('email',$newBenefactor->email)->count()){
            alert()->warning('البيانات مسجلة بالفعل','هذا المتبرع ليس بمحتمل أو جديد في البرنامج، فإن له بيانات مسجلة لدينا وقمنا بعمليات سابقة له ')->footer('<a href="'.route('newBenefactors.show',$newBenefactor) .'" class="btn btn-warning">الاطلاع على الأرشيف الخاص به</a>')->persistent(true);
            return back();
        }
        try {
            DB::beginTransaction();
            $newBenefactor->assignment()->update([
                'employeeable_id'=>$request->emp_id,
                'message'=>$request->message,
                'done'=>0,
                'done_at'=>null,
                'failed'=>0,
                'failed_at'=>null,
            ]);
            $newBenefactor->update([
                'failed'=>0,
                'notMoved'=>0,
            ]);

            Notification::send($newBenefactor->assignment->employeeable,new AssignToCallEmp($user,$newBenefactor->assignment));
            alert()->success('عملية ناجحة','تم إرسال بيانات المتبرع للموظف المحدد بنجاح');
            DB::commit();
        }catch (\Exception $e){
            DB::rollback();
            alert()->error('خطأ','حدث خطأ ما أثناء معالجة البيانات'.$e);
        }
        return back();
    }

    public function end(newBenefactor $newBenefactor)
    {
        try {
            DB::beginTransaction();
            $newBenefactor->update(['end'=>1]);
            $newBenefactor->actualBenefactor->update(['withCallCenter'=>0,'withCollectingManager'=>1]);
            alert()->success('عملية ناجحة','تم نقل الموظف لرئيس قسم التحصيل بنجاح');

            DB::commit();
        }catch (\Exception $ex){
            DB::rollback();
            alert()->error('خطأ','حدث خطأ ما أثناء معالجة البيانات'.$ex);
        }
        return back();

    }
    public function destroy(Request $request, newBenefactor $newBenefactor)
    {
        $user = auth()->user();

        if (password_verify($request->password, $user->password)) {
            $newBenefactor->delete();
            alert()->toast('تم حذف هذا الملف بنجاح!' , 'warning');
            return  redirect()->route('newBenefactors.new');
        }else{
            alert()->error('لا توجد لديك صلاحية','لا يمكنك حذف هذا العنصر');
            return back();
        }
    }
}

<?php

namespace App\Http\Controllers\benefactors;

use App\Http\Controllers\Controller;
use App\newBenefactor;
use Illuminate\Http\Request;

class newBenefactorsCtrl extends Controller
{
    public function create()
    {
        return view('pages.newBenefactors.create');
    }
    public function all()
    {
        $data = newBenefactor::all();
        return view('pages.newBenefactors.all',compact('data'));
    }
    public function new()
    {
        $data = newBenefactor::new()->get();
        return view('pages.newBenefactors.new',compact('data'));
    }
    public function sent()
    {
        $data = newBenefactor::sent()->get();
        return view('pages.newBenefactors.sent',compact('data'));

    }
    public function assigned()
    {
        $data = newBenefactor::assigned()->get();
        return view('pages.newBenefactors.assigned',compact('data'));
    }

    public function failed()
    {
        $data = newBenefactor::failed()->get();
        return view('pages.newBenefactors.faild',compact('data'));
        dd($data);
    }
    public function moved()
    {
        $data = newBenefactor::moved()->get();
        return view('pages.newBenefactors.moved',compact('data'));

    }
    public function notMoved()
    {
        $data = newBenefactor::notMoved()->get();
        return view('pages.newBenefactors.notMoved',compact('data'));

    }
    public function show(newBenefactor $benefactor)
    {
        return view('pages.newBenefactors.show',compact('benefactor'));

    }


}

<?php

namespace App\Http\Controllers\benefactors;

use App\Benefactor;
use App\Http\Controllers\Controller;
use App\Notifications\toSupervisorNotification;
use App\User;
use Illuminate\Http\Request;
use App\Traits\benefactors;

class actualBenefactorsPostCtrl extends Controller
{
    use benefactors;
    public function toSupervisor(Request $request, Benefactor $benefactor)
    {
        $emps = $this->supervisors();
        $request->validate([
            'emp_id'=>['required','integer',],
        ]);
        $user = auth()->user();
        if(!in_array($request->emp_id ,$emps )){
            alert()->error('خطأ','هذا الموظف ليس مشرفاً في قسم التحصيل');
            return back();
        }else{
            $emp = User::find($request->emp_id);
        }
        $benefactor->update([
            'withCollectingManager'=>0,
            'withCollectingSupervisor'=>1,
            'supervisorable_id'=>$emp->id,
            'supervisorable_type'=>User::class,
        ]);

        // need notify ;
        $emp->notify(new toSupervisorNotification($benefactor));
        alert()->success('عملية ناجحة',' تم إرسال المتبرع إلى:'.$emp->name.' بنجاح ');
        return back();
    }

    public function confirm(Request $request , Benefactor $benefactor)
    {

        $data= $request->validate([
            'address'=>'string|required',
            'time'=>'required|string',
            'supervisorable_notes'=>'nullable|string',
        ]);
        $dueDate = \carbon\carbon::create(date('d.m.Y',strtotime($benefactor->dueDate)).' '.date('H:i',strtotime($data['time'])));
        $supervisorable_notes = null;

        if (isset($data['supervisorable_notes'])){
            $supervisorable_notes =' <span> # '. $data['supervisorable_notes'] .'<small class="text-warning mx-2"> في عملية تأجيل بتاريخ: ('.date("d.m.Y").')</small> </span> </br>';
        }
        //dd( $dueDate);
        try {
            $benefactor->update([
                'active'=>1,
                'confirmed'=>1,
                'confirmed_at'=>now(),
                'dueDate' => $dueDate,
                'supervisorable_notes' =>$benefactor->supervisorable_notes.$supervisorable_notes,
                'delayed'=>0,
                'delayed_at'=>null,
                'delayed_to'=>null,
                'stopped'=>0,
                'stopped_at'=>null,
                'stopped_reason'=>null,
            ]);
        alert()->success('عملية ناجحة','تمت العملية بنجاح..');
        }catch(\Exception $e){
            alert()->error('خطأ','حدث خطأ ما أثناء معالجة البيانات، من فضلك تأكد من ادخال بيانات صحيحة..'.$e);
            return back();
        }
        return back();
    }
    public function delay(Request $request , Benefactor $benefactor)
    {
        $data=$request->validate([
            'address'=>'string|required',
            'date'=>'required|date',
            'time'=>'required|string',
            'supervisorable_notes'=>'nullable|string',
        ]);
        $supervisorable_notes = null;

        if (isset($data['supervisorable_notes'])){
        $supervisorable_notes =' <span> -'. $data['supervisorable_notes'] .'<small class="text-warning mx-2"> في عملية تأجيل بتاريخ: ('.date("d.m.Y").')</small> </span> </br>';
        }
        try {
            $benefactor->update([
                'active'=>1,
                'confirmed'=>0,
                'confirmed_at'=>null,
                'delayed'=>1,
                'delayed_at'=>now(),
                'delayed_to'=>$data['date'].' '.$data['time'],
                'supervisorable_notes' =>$benefactor->supervisorable_notes.$supervisorable_notes,
            ]);
        alert()->success('عملية ناجحة','تمت العملية بنجاح..');
        }catch(\Exception $e){
            alert()->error('خطأ','حدث خطأ ما أثناء معالجة البيانات، من فضلك تأكد من ادخال بيانات صحيحة..');
            return back();
        }
        return back();
    }
    public function stop(Request $request , Benefactor $benefactor)
    {
        $data=$request->validate([
            'stopped_reason'=>'required|string',
        ]);
        try {
            $benefactor->update([
                'active'=>1,
                'stopped'=>1,
                'stopped_at'=>now(),
                'stopped_reason'=>$data['stopped_reason'],
            ]);
            alert()->success('عملية ناجحة','تمت العملية بنجاح..');
        }catch(\Exception $e){
            alert()->error('خطأ','حدث خطأ ما أثناء معالجة البيانات، من فضلك تأكد من ادخال بيانات صحيحة..');
            return back();
        }
        return back();
    }



}

<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;

class rolesCtrl extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user= auth()->user();
        if(!$user->can('roles_control')){
            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
            return back();
        }
        $roles = Role::all();
        return view('pages.users.roles',compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {

        $user= auth()->user();
        if(!$user->can('roles_control')){
            alert()->error('صلاحيات المستخدم','لا تملك صلاحية الوصول لهذه العملية !!');
            return back();
        }
        $request->validate([
            'permissions'=>'required|min:1',
        ]);

        $role->syncPermissions($request->permissions);
        alert()->success('عملية ناجحة','تم تحديث صلاحيات جميع المستخدمين الذين لهم هذا الدور بنجاح');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //
    }
}

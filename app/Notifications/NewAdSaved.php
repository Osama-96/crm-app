<?php

namespace App\Notifications;

use App\Ad;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewAdSaved extends Notification
{
    use Queueable;
    /**
     * @var Ad
     */
    public $ad;
    /**
     * @var User
     */
    public $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Ad $ad , User $user)
    {
        $this->ad = $ad;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "title" => "إعلان جديد",
            "notes" => $this->user->name .  "  قام بتسجيل إعلان جديد يبدأ نشره بتاريخ: ".$this->ad->starts_at .' تستطيع زيارة الإعلان من من خلال الرابط التالي ',
            'url'=>$this->ad->link,
        ];
    }
}

<?php

namespace App\Notifications;

use App\Collection;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class DoneCollection extends Notification
{
    use Queueable;

    public $user;
    public $collection;

    /**
     * Create a new notification instance.
     *
     * @param User $user
     * @param Collection $collection
     */
    public function __construct(Collection $collection)
    {
        $this->user = auth()->user();
        $this->collection = $collection;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "title" => "عملية تحصيل ناجحة بكود: " . $this->collection->id,
            "notes" => $this->user->name .  " قام  بتسجيل عملية تحصيل ناجحة خاصة  بالمتبرع: ".$this->collection->benefactorable->name ,
            'url'=>'',
        ];
    }
}

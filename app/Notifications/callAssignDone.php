<?php

namespace App\Notifications;

use App\Assignment;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class callAssignDone extends Notification
{
    use Queueable;
    public $user;
    public $assignment;
    /**
     * Create a new notification instance.
     *
     * @param User $user
     * @param Assignment $assignment
     */
    public function __construct(User $user, Assignment $assignment)
    {
        $this->user = $user;
        $this->assignment = $assignment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "title" => "تسليم عملية تواصل تامة",
            "notes" => $this->user->name .  " قام بتسليم عملية التواصل الخالصة بالمتبرع: ".$this->assignment->benefactorable->name .'  التي أوكلت إليه بتاريخ:  '.$this->assignment->created_at,
            'url'=>'',
        ];
    }
}

<?php

namespace App\Notifications;

use App\Benefactor;
use App\EditBenefactorRequest;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EditBenefactor extends Notification
{
    use Queueable;
    public $user;
    public $assignment;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, Benefactor $benefactor)
    {
        $this->user = $user;
        $this->benefactor = $benefactor;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "title" => " تعديل بيانات متبرع جديد ",
            "notes" => $this->user->name .  " قام بتعديل الملف الخاص بالمتبرع: ".$this->benefactor->name ,
            'url'=>null ,
        ];
    }
}

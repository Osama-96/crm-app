<?php

namespace App\Notifications;

use App\Benefactor;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class toSupervisorNotification extends Notification
{
    use Queueable;
    /**
     * @var Benefactor
     */
    public $benefactor;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    public function __construct( Benefactor $benefactor)
    {
        //
        $this->benefactor = $benefactor;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "title" => "متبرع جديد",
            "notes" => "تم ادراج متبرع جديد لقائمة المتبرعين لديك باسم :".$this->benefactor->name,
            'url'=>'',
        ];
    }
}

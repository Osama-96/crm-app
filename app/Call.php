<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Call extends Model
{
    use SoftDeletes;
    protected $guarded = [''];

    public function getReactingAttribute($attribute)
    {
        return [
            '0' =>'غير متجاوب' ,
            '1' => 'متجاوب',
        ] [$attribute];
    }
    public function assignable()
    {
        return $this->morphTo();
    }
    public function userable()
    {
        return $this->morphTo();
    }
    public function benefactorable()
    {
        return $this->morphTo();
    }

    public function scopeAccept($query)
    {
        $query->where('reacting',1);
    }
    public function scopeFailed($query)
    {
        $query->where('reacting',0);
    }
    public function scopeAnswered($query)
    {
        $query->where('answer_status', 'تم الرد');
    }
    public function scopeOff($query)
    {
        $query->where('answer_status','مغلق أو غير متاح');
    }
    public function scopeBusy($query)
    {
        $query->where('answer_status','مشغول');
    }
    public function scopeAnother($query)
    {
        $query->where('answer_status','رد شخص آخر');
    }
    public function scopeWrong($query)
    {
        $query->where('answer_status','رقم خطأ');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ad extends Model
{
    use SoftDeletes;
    protected $guarded = [''];


    public function location()
    {
      return $this->belongsTo( Location::class ,'place' );
    }

    public function getTypeAttribute($attribute)
    {
        return [
            '1' => 'محتوى مرئي',
            '2' => 'محتوى مكتوب',
        ] [$attribute];
    }

    public function newBenefactors()
    {
        return $this->morphMany(newBenefactor::class,'adable');
    }
    public function benefactors()
    {
        return $this->morphMany(Benefactor::class,'adable');
    }
}

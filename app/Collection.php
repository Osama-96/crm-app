<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Collection extends Model
{
    use SoftDeletes;
    protected $guarded = [''];

    public function assignmentable()
    {
        return $this->morphTo();
    }
    public function accountantable()
    {
        return $this->morphTo();
    }

    public function benefactorable()
    {
        return $this->morphTo();
    }
    public function rate()
    {
        return $this->morphOne(Rate::class,'collectionable');
    }
    public function failedCollections()
    {
        return $this->morphMany(FaildCollection::class,'collectionable');
    }
}

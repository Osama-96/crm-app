<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class newBenefactor extends Model
{
    use SoftDeletes;
    protected $guarded = [''];


    // scopes:


    public function scopeNew($query)
    {
        return $query->where('sent',0)->where('assigned',0)->where('moved',0)->where('end',0);
    }
    public function scopeSent($query)
    {
        return $query->where('sent',1)->where('assigned',0)->where('moved',0)->where('end',0);
    }
    public function scopeAssigned($query)
    {
        return $query->where('sent',1)->where('assigned',1)->where('failed',0)->where('moved',0)->where('end',0);
    }
    public function scopeFailed($query)
    {
        return $query->where('sent',1)->where('assigned',1)->where('failed',1)->where('moved',0)->where('end',0);
    }
    public function scopeMoved($query) //moved To Benefactor Table
    {
        return $query->where('sent',1)->where('assigned',1)->where('failed',0)->where('moved',1)->where('end',0);
    }
    public function scopeNotMoved($query) //moved To Benefactor Table
    {
        return $query->where('sent',1)->where('assigned',1)->where('failed',0)->where('notMoved',1)->where('end',0);
    }
    public function scopeEnd($query) // sent To Collections center
    {
        return $query->where('end',1);
    }





    // relations :

    public function adable()
    {
        return $this->morphTo();
    }

    public function assignment()
    {
        return $this->morphOne(Assignment::class ,'benefactorable');
    }
    public function actualBenefactor()
    {
        return $this->morphOne(Benefactor::class ,'benefactorable');
    }
    public function calls()
    {
        return $this->morphMany(Call::class ,'benefactorable');
    }



    // git attribute
    public function getStatusAttribute()
    {
        if($this->sent == 0) return 'جديد';
        if($this->sent == 1 && $this->assigned == 0) return 'جديد';
        if($this->sent == 1 && $this->assigned == 1 && $this->failed == 0 && $this->moved == 0) return 'جاري التواصل';
        if($this->sent == 1 && $this->assigned == 1 && $this->failed == 1 && $this->moved == 0) return 'فشل التواصل';
        if($this->sent == 1 && $this->assigned == 1 && $this->moved == 1) return 'متبرع فعلي';
    }
    public function getIsNewAttribute()
    {
        if($this->sent == 0) return true;
        return false;
    }
    public function getIsSentAttribute()
    {
        if($this->sent == 1 && $this->assigned == 0) return true;
        return false;
    }
    public function getIsAssignedAttribute()
    {
        if($this->sent == 1 && $this->assigned == 1 && $this->moved == 0) return true;
        return false;
    }
    public function getIsFaildAttribute()
    {
        if($this->sent == 1 && $this->assigned == 1 && $this->moved == 0) return true;
        return false;
    }
    public function getIsMovedAttribute()
    {
        if($this->sent == 1 && $this->assigned == 1 && $this->moved == 1) return true;
        return false;
    }

}

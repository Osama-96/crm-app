<?php

namespace App\Traits;

use App\Benefactor;
use App\CollectAssignment;
use App\Collection;
use App\Notifications\DoneCollection;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

trait benefactors {

    public function supervisors()
    {
        return User::whereRoleIs('admin')->orWhereRoleIs('account_supervisor')->orWhereRoleIS('account_manager')->pluck('id')->toArray();
    }

    public function storeNewCollection(Request $request, $method , Benefactor $benefactor,  $collectAssignment)
    {
        $user = auth()->user();
        $assignmentable_id = null;
        $assignmentable_type = null;
        if($collectAssignment){
        $collectAssignment = CollectAssignment::find($collectAssignment);
        $assignmentable_id = $collectAssignment->id;
        $assignmentable_type = CollectAssignment::class;
        }
        if (strtolower($method) == 'emp'){
            $request->validate([
                'date'=>['date','required'],
                'time'=>['required'],
                'benefactor_rate'=>['integer','required'],
                'comment'=>['nullable','string'],
                'location'=>['nullable','string'],
                'notes'=>['nullable','string'],
                'money_amount'=>['integer','nullable'],
                'donation'=>['string','nullable'],
                'bill_no'=>['integer','nullable'],
                'notes'=>['string','nullable'],
            ]);
            $date           = $request->date.' '.$request->time;
            $rate           = $request->benefactor_rate;
            $comment        =$request->comment;
            $location       =$request->location;
            $notes          =$request->notes;
            $money_amount   = $request->money_amount;
            $donation       =$request->donation;
            $bill_no        = $request->bill_no;
            $failed         = 0;
            $failed_at      = null;

            if(!$money_amount && !$donation){
                alert()->error('خطأ','من فضلك قم بإدخال قيمة التبرع');
                return back();
            }
            try {
                DB::beginTransaction();
                $collection=$user->collections()->create([
                    'failed'=>$failed,
                    'failed_at'=>$failed_at,
                    'collected'=>1,
                    'assignmentable_id'=>$assignmentable_id,
                    'assignmentable_type'=>$assignmentable_type,
                    'benefactorable_id'=>$benefactor->id,
                    'benefactorable_type'=>Benefactor::class,
                    'collected_at'=>$date,
                    'collected_notes'=> $notes,
                    'bill_no'=> $bill_no,
                    'location'=> $location,
                    'money_amount'=> $money_amount,
                    'donation_amount'=> $donation,
                ]);
                //dd($collection);
                if($location){
                    $benefactor->benefactorLocation()->create([
                        'location'=>$location,
                        'userable_id'=>$user->id,
                        'userable_type'=>User::class,
                    ]);
                }
                $user->benefactorRates()->create([
                    'benefactorable_id'=>$collection->benefactorable->id,
                    'benefactorable_type'=>Benefactor::class,
                    'collectionable_id'=>$collection->id,
                    'collectionable_type'=>Collection::class,
                    'rate'=>$rate,
                    'comment'=>$comment,
                ]);
                if($collectAssignment){
                    if ($collectAssignment->regular == 1){
                        if ($this->changeDueDate($benefactor)){
                            $collectAssignment->update([
                               'done'
                            ]);
                        }
                    }
                }
            $managers = User::whereRoleIs('account_supervisor')->get();
                foreach ($managers as $key=>$emp){
                //dd($emp->notify(new DoneCollection($user,$collection)));
                    $emp->notify(new DoneCollection($collection));
                }
            //Notification::send($managers,new DoneCollection($user,$collection));

                DB::commit();
                alert()->success('عملية ناجحة','تم حفظ البيانات بنجاح');
            }catch (\Exception $e){
                DB::rollBack();
                //dd($e);
                alert()->error('خطأ','حدث خطأ ما'.$e);
                return  false;
            }
            return true;
        }else{

        }
    }

    public function changeDueDate(Benefactor $benefactor)
    {
        if($benefactor->frequency != 'OneTime') {
            if ($benefactor->frequency == 'EveryMonth') {
                $date = \carbon\carbon::create($benefactor->dueDate);
                $date->addMonth();
            } elseif ($benefactor->frequency == 'EveryFourMonths') {
                $date = \carbon\carbon::create($benefactor->dueDate);
                $date->addMonths(4);

            } elseif ($benefactor->frequency == 'EveryYear') {
                $date = \carbon\carbon::create($benefactor->dueDate);
                $date->addYear();
            }
            $benefactor->update([
                'dueDate' => $date,
                'confirmed' => 0,
                'confirmed_at' => null,
                'delayed' => 0,
                'delayed_at' => null,
                'delayed_to' => null,
            ]);
        }else{
            $benefactor->update([
                'delayed' => 0,
                'delayed_at' => null,
                'delayed_to' => null,
            ]);
        }
        return true;
    }
}
?>

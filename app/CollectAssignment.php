<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CollectAssignment extends Model
{
    use SoftDeletes;
    protected $guarded = [''];

    public function supervisorable(){
        return $this->morphTo();
    }

    // call manager
    public function accountantable()
    {
        return $this->morphTo();
    }
    public function benefactorable()
    {
        return $this->morphTo();
    }
    public function collection()
    {
        return $this->morphOne(Collection::class,'assignmentable');
    }

    public function getStatusAttribute()
    {
        if (!$this->has('collection')->count()) return 'لم تتم بعد';
        else{
            if($this->collection->failed == 1) return 'فشل التحصيل';
            elseif ($this->collection->collected == 1) return 'تم التحصيل بنجاح';
        }
    }
    public function getTypeAttribute()
    {
        if( $this->regular == 1) return 'اعتيادية';
        if( $this->irregular == 1) return 'غير اعتيادية';
    }


}

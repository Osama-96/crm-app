<?php

    function collectCenter(){
        return App\User::whereRoleIs('account_manager')->orWhereRoleIs('account_supervisor')->orWhereRoleIs('accountant')->get();
    }
    function getDateTime($date){
        if (!$date) return null;
        return date('H:i', strtotime($date)) .
        ' | '. date('d', strtotime($date)) .
        '/ '. config('months.month.'.date('m', strtotime($date))) .
        '/ '.date('Y', strtotime($date)).' م';
    }

    function getDonationType($benefactor){
        if($benefactor->isMoney == 1 && $benefactor->isAyni == 1 ) return "تبرع مالي وعيني";
        if($benefactor->isMoney == 1 && $benefactor->isAyni == 0) return "تبرع مالي";
        if($benefactor->isAyni == 1 && $benefactor->isMoney == 0) return "تبرع عيني";
        else return "لم يسجل";
    }

    function getBenefactorType($benefactor){
        if($benefactor->frequency == 'OneTime' ) return "متبرع عادي";
        if($benefactor->frequency == 'EveryMonth' ) return "كفيل شهري";
        if($benefactor->frequency == 'EveryFourMonths' ) return "كفيل موسمي";
        if($benefactor->frequency == 'EveryYear' ) return "كفيل سنوي";
        else return "لم يسجل";
    }

    function getDonation($benefactor){
        if($benefactor->moneyAmount != null &&  $benefactor->donation != null) return $benefactor->moneyAmount ." جنيه  بالإضافة إلى :".$benefactor->donation;
        if($benefactor->moneyAmount != null &&  $benefactor->donation == null) return $benefactor->moneyAmount ." جنيه ";
        if($benefactor->moneyAmount == null &&  $benefactor->donation != null) return $benefactor->donation ;
        else return "لم يسجل";
    }
    function getCollectStatusOfLast5Days($benefactor){
        if($benefactor->collectAssignments()->where('created_at','<',now())->where('created_at','>=',$benefactor->dueDate->subDays(5))->count()) {
            $count = $benefactor->collectAssignments()->where('created_at','<',now())->where('created_at','>=',$benefactor->dueDate->subDays(5))->count();
            $done = $benefactor->collectAssignments()->where('created_at','<',now())->where('created_at','>=',$benefactor->dueDate->subDays(5))->where('done',1)->count();
            return 'تم إرساله للتحصيل عدد: '.$count.' خلال الخمسة أيام الماضية، عدد التحصيلات الناجحة = '.$done;
        }
    }

?>

<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [

         \App\Ad::class => \App\Policies\adsPolicy::class,
         \App\Location::class => \App\Policies\locationsPolicy::class,
         \App\Benefactor::class => \App\Policies\NewBenefactorsPolicy::class,
         \App\Assignment::class => \App\Policies\assignToCallEmpsPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}

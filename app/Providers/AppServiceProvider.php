<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Schema::defaultStringLength(191);
        $this->ifBlade('user');
        // student Blade
        $this->ifBlade('admin');
        $this->ifBlade('manager');
        // Admin Blade
        $this->ifBlade('marketing_officer');
        //Center Blade
        $this->ifBlade('call_manager');
        $this->ifBlade('call_emp');
        $this->ifBlade('account_manager');
        $this->ifBlade('account_supervisor');
        $this->ifBlade('accountant');
        $this->Blade();
    }

    public function Blade(){

        Blade::if('notme', function ($userId){
            return (auth()->check() && auth()->user()->id !=$userId);
        });

        Blade::if('me', function ($userId){
            return (auth()->check() && auth()->user()->id ==$userId);
        });

    }
    public function ifBlade($name){
        Blade::if($name, function (...$roles) use($name) {
            if (auth()->check()){
                $roles[]=$name;
                return auth()->user()->hasRoles(... $roles);
            }
            return false;
        });
    }
}

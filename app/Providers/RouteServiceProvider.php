<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    protected $benefactors = 'App\Http\Controllers\benefactors';
    protected $calls = 'App\Http\Controllers\calls';
    protected $CallAssignments = 'App\Http\Controllers\callAssignments';
    protected $collectAssignments = 'App\Http\Controllers\collectAssignments';
    protected $teams = 'App\Http\Controllers\teams';
    protected $excel = 'App\Http\Controllers\excel';
    protected $reports = 'App\Http\Controllers\reports';
    protected $collection = 'App\Http\Controllers\collections';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();
        $this->mapBenefactorsRoutes();
        $this->mapCallsRoutes();
        $this->mapCallAssignmentsRoutes();
        $this->mapCollectAssignmentsRoutes();
        $this->mapTeamsRoutes();
        $this->mapReportsRoutes();
        $this->mapExcelRoutes();
        $this->mapCollectionRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }
    protected function mapExcelRoutes()
    {
        Route::middleware(['web','auth'])
             ->namespace($this->excel)
             ->group(base_path('routes/excel.php'));
    }
    protected function mapReportsRoutes()
    {
        Route::middleware(['web','auth'])
             ->namespace($this->reports)
             ->group(base_path('routes/reports.php'));
    }
    protected function mapBenefactorsRoutes()
    {
        Route::middleware(['web','auth'])
             ->namespace($this->benefactors)
             ->group(base_path('routes/benefactors.php'));
    }
    protected function mapCallsRoutes()
    {
        Route::middleware(['web','auth'])
             ->namespace($this->calls)
             ->group(base_path('routes/calls.php'));
    }
    protected function mapCallAssignmentsRoutes()
    {
        Route::middleware(['web','auth'])
             ->namespace($this->CallAssignments)
             ->group(base_path('routes/callAssignments.php'));
    }
    protected function mapCollectAssignmentsRoutes()
    {
        Route::middleware(['web','auth'])
             ->namespace($this->collectAssignments)
             ->group(base_path('routes/collectAssignments.php'));
    }
    protected function mapTeamsRoutes()
    {
        Route::middleware(['web','auth'])
             ->namespace($this->teams)
             ->group(base_path('routes/teams.php'));
    }
    protected function mapCollectionRoutes()
    {
        Route::middleware(['web','auth'])
             ->namespace($this->collection)
             ->group(base_path('routes/collection.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}

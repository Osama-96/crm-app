<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assignment extends Model
{
    use SoftDeletes;
    protected $guarded = [''];


    public function assignmentable()
    {
        return $this->morphTo();
    }
    public function employeeable()
    {
        return $this->morphTo();
    }
    public function benefactorable()
    {
        return $this->morphTo();
    }
    public function calls()
    {
        return $this->morphMany( Call::class, 'assignable');
    }


    public function getStatusAttribute ()
    {
        if($this->done == 0 && $this->failed ==  0) return "جاري التواصل";
        if($this->done == 1 ) return "تمت بنجاح";
        if($this->failed == 1 ) return "فشل التواصل";
        else return " ";
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BenefactorLocation extends Model
{
  protected $guarded=[];

    public function userable()
    {
        return $this->morphTo();
    }
    public function benefactorable()
    {
        return $this->morphTo();
    }
}

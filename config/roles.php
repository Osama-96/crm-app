<?php

return [
    'role' => [
        'user' => 'مستخدم عادي',
        'admin' => 'مدير عام',
        'marketing_officer' => 'مسئول تسويق',
        'call_manager' => 'مسئول قسم الكول سنتر',
        'call_emp' => 'موظف بقسم الكول سنتر',
        'account_manager' => 'مسئول قسم التحصيل',
        'account_supervisor' => 'مشرف بقسم التحصيل',
        'accountant' => 'محصل',
    ],
];

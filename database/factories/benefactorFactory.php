<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Benefactor::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'phone' => $faker->unique()->phoneNumber,
        'benefactorable_id' => 3,
        'benefactorable_type' => User::class,
        'address' => $faker->address,
        'how' => $faker->address,
        'email' => $faker->unique()->safeEmail,
        'ad' =>factory(\App\Location::class)->create(),
    ];
});

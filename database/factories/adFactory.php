<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Ad;
use Faker\Generator as Faker;

$factory->define(Ad::class, function (Faker $faker) {
    return [
        'type' => random_int(1,2),
        'cost' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 5000),
        'starts_at' => now(),
        'ends_at' => $faker->date('Y-m-d'),
        'place' => factory(\App\Location::class)->create(),
        'link' => 'https://www.youtube.com/watch?v=Iu1T7j2FA4M&list=PLDoPjvoNmBAzAeIcXA3_JsmSkPKOs9W-Y&index=1',
        'file' => 'uploads/ads/image2020-02-24-07-42-59.png',
    ];
});

<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Permission::create([
            'name'=>'see_all_locations',
            'display_name'=>'الاطلاع على جميع المناطق الجغرافية المحفوظة',
            'description'=>'',
        ]);

        //Create New Location:
        \App\Permission::create([
            'name'=>'create_locations',
            'display_name'=>'إنشاء منطقة جديدة',
            'description'=>'',
        ]);

        //Update Locations:
        \App\Permission::create([
            'name'=>'update_locations',
            'display_name'=>'تحديث بيانات المناطق الجغرافية',
            'description'=>'',
        ]);

        //Delete Locations:
        \App\Permission::create([
            'name'=>'delete_locations',
            'display_name'=>'حذف المناطق الجغرافية',
            'description'=>'',
        ]);


        //See All Ads :
        \App\Permission::create([
            'name'=>'see_all_ads',
            'display_name'=>'الاطلاع على جميع الإعلانات',
            'description'=>'',
        ]);
        //See single Ads :
        \App\Permission::create([
            'name'=>'see_single_ad',
            'display_name'=>'الاطلاع على تفاصيل كل إعلان',
            'description'=>'',
        ]);


        //Create Ads:
        \App\Permission::create([
            'name'=>'create_ads',
            'display_name'=>'إنشاء إعلانات جديدة',
            'description'=>'',
        ]);


        // Update Ads:
        \App\Permission::create([
            'name'=>'update_ads',
            'display_name'=>'تحديث بيانات الاعلانات',
            'description'=>'',
        ]);


        //Delete ads:
        \App\Permission::create([
            'name'=>'delete_ads',
            'display_name'=>'حذف الإعلانات',
            'description'=>'',
        ]);


        //see all New Benefactor :
        \App\Permission::create([
            'name'=>'see_all_new_benefactor',
            'display_name'=>' الاطلاع على جميع المتبرعين المحتملين',
            'description'=>'',
        ]);

        //see single New Benefactor :
        \App\Permission::create([
            'name'=>'see_single_benefactor',
            'display_name'=>' الاطلاع على التفاصيل  الأساسية أحد المتبرعين المحتملين',
            'description'=>'',
        ]);

        //Create New Benefactor :
        \App\Permission::create([
            'name'=>'create_new_benefactor',
            'display_name'=>'إدخال بيانات متبرع محتمل',
            'description'=>'',
        ]);


        //Update New Benefactors:
        \App\Permission::create([
            'name'=>'update_new_benefactor',
            'display_name'=>'تحديث بيانات المتبرعين المحتملين',
            'description'=>'',
        ]);


        //Delete New Benefactors:
        \App\Permission::create([
            'name'=>'delete_new_benefactor',
            'display_name'=>'حذف بيانات المتبرعين المحتملين',
            'description'=>'',
        ]);


        /*
        // ads :
                create , update , delete, show all, show single
        // locations:
                create , update , delete,
        //benefactors:

                (call Center)
                see_all_benefactors
        */
        \App\Permission::create([
            'name'=>'see_all_benefactors',
            'display_name'=>'الاطلاع على جميع المتبرعين',
            'description'=>'',
        ]);/*
                see_new_benefactors
                see_basicData_benefactor
*/
        \App\Permission::create([
            'name'=>'see_basicData_benefactor',
            'display_name'=>'الاطلاع على البيانات الأساسية للمتبرع',
            'description'=>'',
        ]);/*

                see_callCenter_operations

                */
                \App\Permission::create([
                    'name'=>'see_callCenter_operations',
                    'display_name'=>'الأطلاع على قسم عمليات الكول سنتر الموجود في ملف المتبرع',
                    'description'=>'',
                ]);/*

                assign_to_callCenter
        */
        \App\Permission::create([
            'name'=>'assign_to_callCenter',
            'display_name'=>'تخصيص المتبرع لموظفي الكول سنتر للقيام بالتواصل معه',
            'description'=>'',
        ]);/*


                see_assignments_from_callCenter_manager
        */
        \App\Permission::create([
            'name'=>'see_assignments_from_callCenter_manager',
            'display_name'=>'الاطلاع جميع العمليات التي تم تخصيص هذا المتبرع فيها لفريق الكول سنتر فيها',
            'description'=>'',
        ]);/*

                see_assignment_details_from_callCenter_manager
         */
        \App\Permission::create([
            'name'=>'see_assignment_details_from_callCenter_manager',
            'display_name'=>'الاطلاع على تفاضيل جميع العمليات التي تم تخصيص هذا المتبرع فيها لفريق الكول سنتر ',
            'description'=>'',
        ]);/*
                see_all_benefactor_calls
         */
        \App\Permission::create([
            'name'=>'see_all_benefactor_calls',
            'display_name'=>'الاطلاع على المكالمات التي تمت لأحد المتبرعين ',
            'description'=>'',
        ]);/*
                see_all_benefactor_done_collections
         */
        \App\Permission::create([
            'name'=>'see_all_benefactor_done_collections',
            'display_name'=>'الاطلاع على كافة عمليات التحصيل الناجحة التي تمت للمتبرع',
            'description'=>'',
        ]);/*
                see_all_benefactor_failed_collections
        */
        \App\Permission::create([
            'name'=>'see_all_benefactor_failed_collections',
            'display_name'=>'الاطلاع على كافة عمليات التحصيل لفاشلة التي تمت للمتبرع',
            'description'=>'',
        ]);/*

                stop_benefactor
        */
        \App\Permission::create([
            'name'=>'stop_benefactor',
            'display_name'=>'تسجيل ايقاف المتبرع عن التبرع تماما',
            'description'=>'',
        ]);


        \App\Permission::create([
            'name'=>'super_collection_control',
            'display_name'=>'تحكم كامل في العمليات المطلوب تحصيلها( تحدد كعملية تامة أو فاشلة أو تغيير المحصل )',
            'description'=>'',
        ]);
        /*
                reActive_benefactor
        */
        \App\Permission::create([
            'name'=>'reActive_benefactor',
            'display_name'=>'تسجيل إعادة عودة المتبرع إلى التبرع ',
            'description'=>'',
        ]);/*
        */
        \App\Permission::create([
            'name'=>'see_all_callCenter_team',
            'display_name'=>'متابعة فريق الكول سنتر',
            'description'=>'',
        ]);/*
    */
        \App\Permission::create([
            'name'=>'see_all_callCenter_emp',
            'display_name'=>'متابعة عمليات موظف الكول سنتر',
            'description'=>'',
        ]);/*
                see_all_near_to_collect_benefactors
        */
        \App\Permission::create([
            'name'=>'see_all_near_to_collect_benefactors',
            'display_name'=>'الاطلاع على كافة المتبرعين الذين اقترب موعد التحصيل منهم',
            'description'=>'',
        ]);/*
                import_from_excel_sheet
        */
        \App\Permission::create([
            'name'=>'import_from_excel_sheet',
            'display_name'=>'استيراد بيانات المتبرعين من ملف ايكسل',
            'description'=>'',
        ]);/*
                send_edit_request
        */
        \App\Permission::create([
            'name'=>'send_edit_request',
            'display_name'=>'تسجيل طلب تعديل بيانات أحد المتبرعين',
            'description'=>'',
        ]);/*
                save_call

        */
        \App\Permission::create([
            'name'=>'save_call',
            'display_name'=>'تسجيل مكالمة لأحد المتبرعين',
            'description'=>'',
        ]);/*
                save_donation_request
        */
        \App\Permission::create([
            'name'=>'save_donation_request',
            'display_name'=>'تسجيل طلب تبرع لأحد المتبرعين',
            'description'=>'',
        ]);/*
                prent_reports
        */
        \App\Permission::create([
            'name'=>'prent_reports',
            'display_name'=>'طباعة التقارير',
            'description'=>'',
        ]);/*


                (account section)

                see_all_need_to_confirm
        */
        \App\Permission::create([
            'name'=>'see_all_need_to_confirm',
            'display_name'=>'الاطلاع جميع العمليات التي تحتاج إلى تأكيد التحصيل',
            'description'=>'',
        ]);/*
                see_all_confirmed_to_collect
        */
        \App\Permission::create([
            'name'=>'see_all_confirmed_to_collect',
            'display_name'=>'الاطلاع جميع العمليات التي تم تأكيد التحصيل',
            'description'=>'',
        ]);/*
                see_all_delayed_to_collect
        */
        \App\Permission::create([
            'name'=>'see_all_delayed_to_collect',
            'display_name'=>'الاطلاع جميع العمليات التي تم تأجيل تحصيلها',
            'description'=>'',
        ]);/*
                see_all_refused_from_ConfirmCall_to_collect
        */
        \App\Permission::create([
            'name'=>'see_all_refused_from_ConfirmCall_to_collect',
            'display_name'=>'الاطلاع جميع العمليات التي لم تستجب للتحصيل بالمكالمة',
            'description'=>'',
        ]);/*
                confirm_collect_call
    */
        \App\Permission::create([
            'name'=>'confirm_collect_call',
            'display_name'=>'القيام بتأكيد التحصيل',
            'description'=>'',
        ]);/*
                delay_collect_call
    */
        \App\Permission::create([
            'name'=>'delay_collect_call',
            'display_name'=>'القيام بتأجيل التحصيل',
            'description'=>'',
        ]);/*
                refuse_collect_call
     */
        \App\Permission::create([
            'name'=>'refuse_collect_call',
            'display_name'=>'تسجيل عدم الاستجابة للتحصيل عن طريق المكالمة',
            'description'=>'',
        ]);/*
    */
        \App\Permission::create([
            'name'=>'send_assignment_for_collect',
            'display_name'=>'إرسال العمليات الجاهزه للتحصيل إلى رئيس قسم التحصيل',
            'description'=>'',
        ]);/*

                see_all_now_atime_collections
        */
        \App\Permission::create([
            'name'=>'see_all_now_atime_collections',
            'display_name'=>'الاطلاع على كافة عمليات التحصيل الحالية',
            'description'=>'',
        ]);/*
                see_all_done_collections
    */
        \App\Permission::create([
            'name'=>'see_all_done_collections',
            'display_name'=>'الاطلاع على كافة عمليات التحصيل التامة',
            'description'=>'',
        ]);/*
                see_all_failed_collections
    */
        \App\Permission::create([
            'name'=>'see_all_failed_collections',
            'display_name'=>'الاطلاع على كافة عمليات التحصيل الفاشلة',
            'description'=>'',
        ]);/*

                see_all_collection_team
    */
        \App\Permission::create([
            'name'=>'see_all_collection_team',
            'display_name'=>'متابعة فريق التحصيل',
            'description'=>'',
        ]);

    \App\Permission::create([
            'name'=>'assign_to_collect_team',
            'display_name'=>'تخصيص عمليات التحصيل للمحصلين',
            'description'=>'',
        ]);/*
                see_collection_emp
    */
        \App\Permission::create([
            'name'=>'see_collection_emp',
            'display_name'=>'متابعة عمليات موظف قسم التحصيل',
            'description'=>'',
        ]);/*
                see_collection_assignment_details
    */
        \App\Permission::create([
            'name'=>'see_collection_assignment_details',
            'display_name'=>'الاطلاع على تفاصيل عملية التحصيل',
            'description'=>'',
        ]);/*
                open_benefactor_dataFile
    */
        \App\Permission::create([
            'name'=>'open_benefactor_dataFile',
            'display_name'=>'الاطلاع على ملف المتبرع',
            'description'=>'',
        ]);/*

                see_all_stopped_benefactors
        */
        \App\Permission::create([
            'name'=>'see_all_stopped_benefactors',
            'display_name'=>'الاطلاع على كافة المتبرعين المتوقفين',
            'description'=>'',
        ]);/*

        see_all_kafalah_benefactors
        */
        \App\Permission::create([
            'name'=>'see_all_kafalah_benefactors',
            'display_name'=>'الاطلاع على كافة الكفالات',
            'description'=>'',
        ]);/*
                (charts)
                see_new_actual_benefactors_chart
        */
        \App\Permission::create([
            'name'=>'see_new_actual_benefactors_chart',
            'display_name'=>'الاطلاع على الرسم البياني للمتبرعين الفعلين والمحتملين',
            'description'=>'',
        ]);/*
                see_stop_active_benefactors_chart
        */
        \App\Permission::create([
            'name'=>'see_stop_active_benefactors_chart',
            'display_name'=>'الاطلاع على الرسم البياني للمتبرعين المتوقفين والنشطين',
            'description'=>'',
        ]);/*
                see_kafalat_normal_benefactors_chart
        */
        \App\Permission::create([
            'name'=>'see_kafalat_normal_benefactors_chart',
            'display_name'=>'الاطلاع على الرسم البياني للمتبرعين العاديين وأصحاب الكفالات',
            'description'=>'',
        ]);/*
                see_reacting_calls_benefactors_chart
        */
        \App\Permission::create([
            'name'=>'see_reacting_calls_benefactors_chart',
            'display_name'=>'الاطلاع على الرسم البياني الخاص انتيجة المكالمات',
            'description'=>'',
        ]);/*
                see_done_failed_collections_chart
        */
        \App\Permission::create([
            'name'=>'see_done_failed_collections_chart',
            'display_name'=>'الاطلاع على الرسم البياني الخاص بعمليات التحصيل الفاشلة والناجحة',
            'description'=>'',
        ]);/*
                see_call_center_team_follow_up
          */
        \App\Permission::create([
            'name'=>'see_call_center_team_follow_up',
            'display_name'=>'الاطلاع على التحليل الخاص بقسم الكول سنتر',
            'description'=>'',
        ]);/*
                see_account_section_team_follow_up
 */
        \App\Permission::create([
            'name'=>'see_account_section_team_follow_up',
            'display_name'=>'الاطلاع على التحليل الخاص بقسم التحصيل',
            'description'=>'',
        ]);/*

        */


        \App\Permission::create([
            'name'=>'permissions_control',
            'display_name'=>'التحكم بصلاحيات المستخدمين',
            'description'=>'',
        ]);
        \App\Permission::create([
            'name'=>'roles_control',
            'display_name'=>'التحكم بصلاحيات الأدوار',
            'description'=>'',
        ]);
        \App\Permission::create([
            'name'=>'users_control',
            'display_name'=>'التحكم بالمستخدمسن',
            'description'=>'',
        ]);




    }



}

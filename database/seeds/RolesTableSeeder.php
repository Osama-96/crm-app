<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\Role::create([
            'name' => 'user',
            'display_name' => 'user',
            'description' => 'can do specific things in the project',
        ]);

        $admin = \App\Role::create([
            'name' => 'admin',
            'display_name' => 'المبرمج',
            'description' => 'can do any thing in the project',
        ]);

        $manager = \App\Role::create([
            'name' => 'manager',
            'display_name' => 'المدير التنفيذي',
            'description' => 'can do any thing in the project',
        ]);

        $marketing_officer= \App\Role::create([
            'name' => 'marketing_officer',
            'display_name' => 'مسئول التسويق الاكتروني',
            'description' => 'can do the marketing processing in the project',
        ]);

        $call_manager= \App\Role::create([
            'name' => 'call_manager',
            'display_name' => 'رئيس قسم الكول سنتر',
            'description' => 'can do the marketing processing in the project',
        ]);

        $call_emp= \App\Role::create([
            'name' => 'call_emp',
            'display_name' => 'موظف قسم الكول سنتر',
            'description' => 'can do the marketing processing in the project',
        ]);

        $account_manager= \App\Role::create([
            'name' => 'account_manager',
            'display_name' => 'رئيس قسم التحصيل',
            'description' => 'can do the marketing processing in the project',
        ]);

        $account_supervisor= \App\Role::create([
            'name' => 'account_supervisor',
            'display_name' => 'مشرف قسم التحصيل',
            'description' => 'can do the marketing processing in the project',
        ]);

        $accounant= \App\Role::create([
            'name' => 'accountant',
            'display_name' => 'محصل',
            'description' => 'can do the marketing processing in the project',
        ]);
    } //end of run
} // end of seeder.

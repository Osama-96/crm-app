<?php

use Illuminate\Database\Seeder;

class benefactorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Benefactor::class,30)->create();
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBenefactorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benefactors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('userable_id');
            $table->string('userable_type');
            $table->unsignedBigInteger('benefactorable_id');
            $table->string('benefactorable_type');
            $table->unsignedBigInteger('adable_id')->nullable(); // will take the id of collecting emp in case he came from collecting
            $table->string('adable_type')->nullable(); // Ad::class , User::class
            $table->boolean('comeFromCollecting')->default(0);
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('device')->nullable();
            $table->text('address')->nullable();
            $table->text('location_pin')->nullable();

            $table->string('frequency')->default('OneTime');
            $table->timestampTz('dueDate')->nullable();
            $table->boolean('isAyni')->nullable();
            $table->string('donation')->nullable();
            $table->boolean('isMoney')->nullable();
            $table->float('moneyAmount')->nullable();
            $table->string('collecting_way')->nullable();
            $table->text('cases')->nullable();
            $table->tinyInteger('class')->nullable();

            $table->boolean('withCallCenter')->default(0); // to be shown in his list
            $table->boolean('withCollectingManager')->default(0); // to be shown in his list
            $table->boolean('withCollectingSupervisor')->default(0); // to be shown in his list
            $table->unsignedBigInteger('supervisorable_id')->nullable(); // the supervisor of him.
            $table->string('supervisorable_type')->nullable(); // the supervisor of him.
            $table->text('supervisorable_notes')->nullable(); // the supervisor of him.
            $table->boolean('active')->default(0);


            // in this case it means the supervisor has made the confirmation call and confirmed.
            $table->boolean('confirmed')->default(0);
            $table->timestampTz('confirmed_at')->nullable();

            $table->boolean('delayed')->default(0);
            $table->timestampTz('delayed_at')->nullable();
            $table->timestampTz('delayed_to')->nullable();

            // in this case it will be shown in the stopped list(it stops in two cases : with the confirmation call & while counting operations).
            $table->boolean('stopped')->default(0);
            $table->timestampTz('stopped_at')->nullable();
            $table->text('stopped_reason')->nullable();

            $table->timestampTz('deleted_at')->nullable();
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benefactors');
    }
}

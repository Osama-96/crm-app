<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBenefactorLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benefactor_locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('location');
            $table->string('userable_id');
            $table->string('userable_type');
            $table->string('benefactorable_id');
            $table->string('benefactorable_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benefactor_locations');
    }
}

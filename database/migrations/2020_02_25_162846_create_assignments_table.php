<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('assignmentable_id');
            $table->string('assignmentable_type');
            $table->unsignedBigInteger('employeeable_id');
            $table->string('employeeable_type');
            $table->unsignedBigInteger('benefactorable_id');
            $table->string('benefactorable_type');
            $table->text('message')->nullable();
            $table->tinyInteger('done')->default(0);
            $table->timestampTz('done_at')->nullable();
            $table->tinyInteger('failed')->default(0);
            $table->timestampTz('failed_at')->nullable();
            $table->unsignedBigInteger('toCollect')->nullable();
            $table->timestampTz('toCollect_at')->nullable();
            $table->timestampTz('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}

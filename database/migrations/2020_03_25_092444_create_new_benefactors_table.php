<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewBenefactorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_benefactors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('benefactorable_id');
            $table->string('benefactorable_type');
            $table->unsignedBigInteger('adable_id')->nullable(); // will take the id of collecting emp in case he came from collecting
            $table->string('adable_type')->nullable(); // Ad::class , User::class
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('email')->nullable();
            $table->string('device')->nullable();
            $table->boolean('sent')->default(0);
            $table->boolean('assigned')->default(0);
            $table->boolean('failed')->default(0);
            $table->boolean('moved')->default(0);
            $table->boolean('notMoved')->default(0);
            $table->boolean('end')->default(0);
            $table->timestampTz('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     *
     *
     */
    public function down()
    {
        Schema::dropIfExists('new_benefactors');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('rateable_id');
            $table->text('rateable_type');
            $table->unsignedBigInteger('benefactorable_id');
            $table->text('benefactorable_type');
            $table->unsignedBigInteger('collectionable_id');
            $table->text('collectionable_type');
            $table->integer('rate');
            $table->text('comment')->nullable();
            $table->timestampTz('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rates');
    }
}

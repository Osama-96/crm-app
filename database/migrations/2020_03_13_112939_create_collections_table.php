<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collections', function (Blueprint $table) {
            $table->bigIncrements('id');


            $table->unsignedBigInteger('assignmentable_id')->nullable();
            $table->string('assignmentable_type')->nullable();
            $table->unsignedBigInteger('accountantable_id');
            $table->string('accountantable_type');
            $table->unsignedBigInteger('benefactorable_id');
            $table->string('benefactorable_type');

            $table->boolean('collected')->default(0);
            $table->timestampTz('collected_at')->nullable();
            $table->string('bill_no')->nullable();
            $table->string('method')->default('emp');
            $table->float('money_amount')->nullable();
            $table->string('donation_amount')->nullable();
            $table->text('collected_notes')->nullable();

            // to inform that we tried many times and he does not accept
            $table->boolean('failed')->default(0);
            $table->timestampTz('failed_at')->nullable();
            $table->text('failed_reason')->nullable();

            $table->text('location')->nullable();
            $table->text('notes')->nullable();
            $table->text('file')->nullable();

            $table->timestampTz('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collections');
    }
}

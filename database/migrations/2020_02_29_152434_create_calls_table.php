<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('assignable_id')->nullable();
            $table->string('assignable_type')->nullable();
            $table->unsignedBigInteger('userable_id')->nullable();
            $table->string('userable_type')->nullable();
            $table->unsignedBigInteger('benefactorable_id')->nullable();
            $table->string('benefactorable_type')->nullable();
            $table->string('date')->nullable();
            $table->string('time')->nullable();
            $table->string('answer_status')->nullable();
            $table->string('status_more')->nullable();
            $table->boolean('reacting')->default(0);
            $table->integer('call_rate')->nullable();
            $table->string('question')->nullable();
            $table->text('call_samary')->nullable();
            $table->text('notes')->nullable();
            $table->boolean('collecting_confirmation')->default(0);
            $table->boolean('collecting')->default(0);
            $table->timestampTz('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calls');
    }
}

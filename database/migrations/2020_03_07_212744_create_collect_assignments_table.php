<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collect_assignments', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('supervisorable_id');
            $table->string('supervisorable_type');
            $table->unsignedBigInteger('accountantable_id');
            $table->string('accountantable_type');
            $table->unsignedBigInteger('benefactorable_id');
            $table->string('benefactorable_type');
            $table->dateTime('dueDate')->nullable();
            $table->string('donation')->nullable();
            $table->tinyInteger('regular')->default(0);
            $table->tinyInteger('irregular')->default(0);
            $table->tinyInteger('done')->default(0);

            // to add any note
            $table->text('notes')->nullable();
            $table->timestampTz('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collect_assignments');
    }
}

@extends('pages.layouts.app')

@section('content')
    <div class="container-fluid">
        <!-- 404 Error Text -->
        <div class="text-center">
            <div class="error mx-auto" data-text="403">403</div>
            <p class="lead text-gray-800 mb-5">لا توجد صلاحية </p>
            <p class="text-gray-500 mb-0">لا تملك صلاحية للقيام بهذا العمل...</p>
            <a href="{{route('home')}}">&larr; العودة إلى الرئيسية</a>
        </div>
    </div>
@stop

@extends('pages.layouts.app')

@section('content')
    <div class="container-fluid">
        <!-- 404 Error Text -->
        <div class="text-center pt-5">
            <div class="error mx-auto" data-text="404">404</div>
            <p class="lead text-gray-800 mb-5">هذه الصفحة ليست موجودة</p>
            </div>
    </div>
@stop

<div class="card position-relative  border-{{$color}}  ">
    <div class="card-header bg-{{$color}} text-center" >
        <h5 class=" text-white">{{$title}}  </h5>
    </div>
    <div class="card-body ">
        {{$slot}}
    </div>

</div>

    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary"> {{$title}}</h6>
        </div>

        <!-- Card Body -->
        <div class="card-body">
            <div class="chart-pie pt-4 pb-2">
                <canvas id="{{str_replace(' ','',$title)}}"></canvas>
            </div>
            <div class="mt-4 text-center small">
                {{$slot}}
{{--            <span class="mr-3">--}}
{{--                <i class="fas fa-circle" style="color: #ffca39"></i> المكالمات المتجاوبة:{{\App\Call::Where('reacting',1)->count()}} مكالمة--}}
{{--            </span>--}}
{{--            <span class="mr-3">--}}
{{--                <i class="fas fa-circle " style="color: #758072"></i> المكالمات الغير المتجاوبة :{{\App\Call::Where('reacting',0)->count()}} مكالمة--}}
{{--            </span>--}}

            </div>
        </div>
    </div>
@push('js')
    <script !src="">
        var ctx1 = document.getElementById({{str_replace(' ','',$title)}});
        var myPieChart1 = new Chart(ctx1, {
            type: 'doughnut',
            data: {
                // labels: [" المكالمات المتجاوبة", "المكالمات الغير متجاوبة"],
                labels: {{$labels}},
                datasets: [{
                    {{--data: [{{$goodCalls}}, {{$badCalls}}],--}}
                    data: {{$data}},
                    // backgroundColor: ['#ffca39', '#758072'],
                    backgroundColor: {{$bColors}},
                    hoverBackgroundColor: {{$hColors}},
                    hoverBorderColor: "{{$border_color}}",
                }],
            },
            options: {
                maintainAspectRatio: false,
                tooltips: {
                    backgroundColor: "rgb(255,255,255)",
                    bodyFontColor: "#858796",
                    borderColor: '#dddfeb',
                    borderWidth: 1,
                    xPadding: 15,
                    yPadding: 15,
                    displayColors: false,
                    caretPadding: 10,
                },
                legend: {
                    display: false
                },
                cutoutPercentage: 80,
            },
        });
    </script>
@endpush

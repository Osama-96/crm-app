@push('css')
    <link href="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endpush
<div class="row">
    <div class="col-lg-12 ">
        <div class="card position-relative border-{{$color}}">
            <div class="card-header bg-{{$color}} text-center">
                <h5 class=" text-white">{{$title}}  </h5>
            </div>
            <div class="card-body " >
                <div id="calls" class="row ">
                    <div class="col-md-12  table-responsive   ">

                            <div class="table-responsive">
                                <table class="table table-bordered" id="{{\Str::slug($title)}}" width="100%" cellspacing="0">
                                {{$slot}}
                                </table>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('js')

<script>

// Call the dataTables jQuery plugin
$(document).ready(function() {
$('#{{\Str::slug($title)}}').DataTable({
    stateSave:true,"paging":   true,
    autoWidth:true,
    "language": {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "الكل"]],
        "decimal": "",
        "emptyTable": "لا يوجد أي بيانات في هذا الجدول",
        "info": "عرض _START_ إلى _END_ من أصل _TOTAL_ من العناصر",
        "infoEmpty": "لا توجد بيانات متطابقة",
        "infoFiltered": "(تمت التصفية من أصل _MAX_ عناصر)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "عرض _MENU_ من العناصر",
        "loadingRecords": "جاري التحميل...",
        "processing": "جاري الفحص...",
        "search": " بحث: ",
        "zeroRecords": "لا توجد بيانات متطابقة",
        "paginate": {"first": "الأولى", "last": "الأخيرة", "next": "التالي", "previous": "السابق"},
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "lengthMenu": "عرض عدد _MENU_ من العناصر في الصفحة الواحدة",
        "info": "إظهار الصفحة رقم _PAGE_ من أصل عدد _PAGES_ من الصفحات",
    },
    buttons: [
        {
            extend: 'copy',
            text: 'Copy to clipboard'
        },
        'excel',
        'pdf'
    ],

} );

});
</script>
@endpush

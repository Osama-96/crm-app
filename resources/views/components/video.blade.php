@push('css')
@endpush

<div class="col-md-4 " >
    <div class="card">
        <div class="card-header bg-primary text-white text-center">
            {{$title}}
        </div>
        <div class="card-bodey">

                <video  width="100%"  loop id="myVideo-{{\Str::slug($title)}}" controls="true">
                    <source src="{{$link}}" type="video/mp4">
                </video>
        </div>
        <div class="card-footer">
            <p style="font-size: 12px">{{$desc}} </p>
        </div>
    </div>
</div>
@push('js')





@endpush

<!-- Nav Item - Utilities Collapse Menu -->
<li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse{{str_replace(' ', '', $title)}}" aria-expanded="true" aria-controls="collapse{{str_replace(' ', '', $title)}}">
        @isset($icon) <i class="{{$icon}}"></i> @endisset
        @isset($title) <span>{{$title}}</span> @endisset
    </a>
    <div id="collapse{{str_replace(' ', '', $title)}}" class="collapse" aria-labelledby="heading{{str_replace(' ', '', $title)}}" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            @isset($header)
                <h6 class="collapse-header">{{$header}}</h6>
            @endisset
            {{ $slot }}
        </div>
    </div>
</li>

@push('css')
    <link href="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endpush

<div class="table-responsive">
    <table class="table table-bordered" id="{{\Str::slug(str_replace(' ','',$title))}}" width="100%" cellspacing="0">
    {{$slot}}
    </table>
</div>

@push('js')

<script>

// Call the dataTables jQuery plugin
$(document).ready(function() {
$('#{{\Str::slug(str_replace(' ','',$title))}}').DataTable({
    stateSave:true,"paging":   true,
    "searching": false,

    "language": {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "الكل"]],
        "decimal": "",
        "emptyTable": "لا يوجد أي بيانات في هذا الجدول",
        "info": "عرض _START_ إلى _END_ من أصل _TOTAL_ ",
        "infoEmpty": "لا توجد بيانات متطابقة",
        "infoFiltered": "(تمت التصفية من أصل _MAX_ عناصر)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "عرض _MENU_ ",
        "loadingRecords": "جاري التحميل...",
        "processing": "جاري الفحص...",
        "search": " بحث: ",
        "zeroRecords": "لا توجد بيانات متطابقة",
        "paginate": {"first": "الأولى", "last": "الأخيرة", "next": "التالي", "previous": "السابق"},
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "lengthMenu": "عرض عدد _MENU_ ",
        "info": "إظهار الصفحة رقم _PAGE_ من  _PAGES_ ",
    },
    buttons: [
        {
            extend: 'copy',
            text: 'Copy to clipboard'
        },
        'excel',
        'pdf'
    ],

} );

});
</script>
<!-- Page level plugins -->
<script src="{{asset('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->


@endpush

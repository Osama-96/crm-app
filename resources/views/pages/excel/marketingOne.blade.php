@extends('pages.layouts.app')
@section('content')

    <!-- Begin Page Content -->
    <div class="container-fluid  ">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-white">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
                <li class="breadcrumb-item active" aria-current="page">استيراد من ملف ايكسل</li>
            </ol>
        </nav>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header py-3 bg-gradient-primary text-center">
                    <h5 class=" text-white">  استيراد بيانات المتبرعين من ملف إيكسل</h5>
                </div>
                <div class="card-body ">
                    <form action="{{route('excel.storeMarketingFile')}}" enctype="multipart/form-data" method="POST" >
                        @csrf

                        <div class="row justify-content-md-center">
                            <div class="form-group col-md-6">
                                <label for=""> اختر ملف file.xlsx </label>
                                <input type="file" name="file"  required>
                            </div>
                        </div>

                        <div class="row justify-content-md-center">
                            <div class="form-group col-md-4 ">
                                <input type="submit" class="btn btn-primary form-control" value="رفع الملف  ">
                            </div>
                        </div>
                    </form>
                </div>
                </div>
            </div>
        </div>
        <!-- Content Row -->
        @if($users)
            @component('components.table',['title'=>'البيانات التي تم تحميلها','color'=>'primary'])
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>الاسم</th>
                        <th>الايميل</th>
                        <th>الهاتف</th>
                        <th> العنوان</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>#</th>
                        <th>الاسم</th>
                        <th>الايميل</th>
                        <th>الهاتف</th>
                        <th> العنوان</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($users as $key=>$item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item['name']}}</td>
                            <td>{{$item['email']}}</td>
                            <td>{{$item['phone']}}</td>
                            <td>{{$item['address']}}</td>
                        </tr>
                    @endforeach
                    </tbody>
            @endcomponent
        @endif
    </div>
    <!-- /.container-fluid -->
@stop


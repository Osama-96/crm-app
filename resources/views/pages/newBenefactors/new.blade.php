@extends('pages.layouts.app')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
            <li class="breadcrumb-item active" aria-current="page">المتبرعين المحتملين</li>
        </ol>
    </nav>
    <!-- Content Row -->
    @component('components.table',['title'=>'جميع المتبرعين المحتملين ','color'=>'primary'])
            <thead>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>الهاتف</th>
                    <th>الايميل</th>
                    <th>الوسيلة</th>
                    <th>الاعلان</th>
                    <th>الحالة</th>
                    <th  class="text-center" >عرض</th>
                    <th  class="text-center" >تعديل</th>
                    <th  class="text-center" >إرسال</th>
                    <th  class="text-center" >حذف</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>الهاتف</th>
                    <th>الايميل</th>
                    <th>الوسيلة</th>
                    <th>الاعلان</th>
                    <th>الحالة</th>
                    <th  class="text-center" >عرض</th>
                    <th  class="text-center" >تعديل</th>
                    <th  class="text-center" >إرسال</th>
                    <th  class="text-center" >حذف</th>
                </tr>
            </tfoot>
            <tbody>
            @foreach($data as $key=>$item)
                <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->phone}}</td>
                <td>{{$item->email}}</td>
                <td>{{$item->device}}</td>
                <td>{{($item->adable)? 'رقم '.$item->adable->id :'لا يوجد' }}</td>
                <td>{{$item->status }}</td>
                <td class="text-center" ><a class="text-white h3 btn btn-warning "  data-toggle="modal" data-target="#show{{$key}}"><i class="fas fa-eye"></i></a></td>
                <td class="text-center" ><a class="text-white h3 btn btn-info "  data-toggle="modal" data-target="#edit{{$key}}"><i class="fas fa-pencil-alt"></i></a></td>
                <td class="text-center" >
                    <form action="{{route('newBenefactors.send',$item->id)}}" method="post">
                        @csrf
                    <button type="submit" class="text-white h3 btn btn-primary " ><i class="fas fa-share"></i></button>
                    </form>
                </td>
                    <td class="text-center" ><a class="text-white h3 btn btn-danger "  data-toggle="modal" data-target="#delete{{$loop->iteration}}"><i class="fas fa-trash-alt"></i></a></td>

                </tr>
            @endforeach
            </tbody>
    @endcomponent
    @foreach($data as $key=>$item)
        @component('components.modal_xl',['title'=>'بيانات متبرع محتمل باسم/ '.$item->name ,'color'=>'warning','id'=>'show'.$key])
            <div class=" row justify-content-center" >
                <div class="col-md-12 table-responsive py-3">
                    <table class="table table-bordered  ">
                        <tbody>
                        <tr>
                            <th> الاسم</th> <td> {{$item->name}}</td>
                        </tr>

                        <tr>
                            <th> الهاتف</th> <td> {{$item->phone}}</td>
                        </tr>
                        <tr>
                            <th>الايميل</th> <td> {{$item->email}}</td>
                        </tr>
                        <tr>
                            <th>الوسيلة</th> <td> {{$item->device}}</td>
                        </tr>

                        <tr>
                            <th>الاعلان </th> <td>{{($item->adable)? 'رقم '.$item->adable->id :'لا يوجد' }} </td>
                        </tr>

                        </tbody>
                    </table>

                </div>

            </div>

        @endcomponent
        @component('components.modal_xl',['title'=>' تعديل بيانات المتبرع/ '.$item->name ,'color'=>'info','id'=>'edit'.$key])
                <form action="{{route('newBenefactors.update',$item->id)}}" method="POST">
                    @csrf

                    <div class="row justify-content-center">
                        <div class="form-group col-md-6">
                            <label for="">الاسم </label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="" value="{{$item->name}}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for=""> الهاتف  </label>
                            <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" id="" value="{{$item->phone}}">
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for=""> الإيميل  </label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="" value="{{$item->email}}">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for=""> الوسيلة التي جاء بها  </label>
                            <select name="device" id="" class="form-control @error('device') is-invalid @enderror">
                                <option disabled selected> اختر الوسيلة </option>
                                <option value="الفيسبوك" {{($item->device == "الفيسبوك" )? 'selected' : '' }}> الفيسبوك </option>
                                <option value="تويتر" {{($item->device == "تويتر")? 'selected' : '' }}>  تويتر</option>
                                <option value="انستجرام" {{($item->device =="انستجرام" )? 'selected' : '' }}>  انستجرام</option>
                                <option value="واتساب" {{($item->device =="واتساب")? 'selected' : '' }}>  واتساب</option>
                                <option value="وسيلة أخرى" {{($item->device == "وسيلة أخرى")? 'selected' : '' }}>  أخرى</option>
                            </select>
                            @error('device')
                            <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for=""> الإعلان  </label>
                            <select name="ad" id="" class="form-control @error('ad') is-invalid @enderror">
                                <option selected value=""> اختر الاعلان </option>
                                @foreach(\App\Ad::all() as $ad)
                                    <option value="{{$ad->id}}" {{($item->adable_id == $ad->id)? 'selected' : '' }}> رقم {{$ad->id}}</option>
                                @endforeach
                            </select>
                            @error('ad')
                            <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                            @enderror
                        </div>

                    </div>
                    <div class="row justify-content-center my-3">
                        <div class="form-group col-md-4 ">
                            <input type="submit" class="btn btn-primary form-control" value="حفظ البيانات">
                        </div>
                    </div>



                </form>


        @endcomponent
            <div class="modal fade" id="delete{{$loop->iteration }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-danger text-white">
                            <h5 class="modal-title" id="exampleModalLabel">تأكيد العملية</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="{{route('newBenefactors.destroy',$item)}}" method="Post">
                            <div class="modal-body">
                                @csrf
                                أدخل الباسورد لتأكيد عملية الحذف
                                <input type="password" name="password" id="" class="form-control">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">الغاء</button>
                                <button type="submit"  class="btn btn-danger">حذف</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    @endforeach



  </div>
  <!-- /.container-fluid -->
@stop

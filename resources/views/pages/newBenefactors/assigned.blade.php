@extends('pages.layouts.app')
@section('css')
<link href="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@stop
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
            <li class="breadcrumb-item active" aria-current="page">المتبرعين المحتملين </li>
        </ol>
    </nav>
    <!-- Content Row -->
    @component('components.table',['title'=>'جميع المتبرعين المحتملين الجاري التواصل معهم ','color'=>'primary'])
            <thead>
                <tr>
                    <th>#</th>
                    <th>اسم المتبرع</th>
                    <th>الموظف</th>
                    <th  class="text-center" >بيانات المتبرع</th>
                    <th  class="text-center" > ملخص العمليات</th>
                    <th  class="text-center" > تغيير الموظف</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>اسم المتبرع</th>
                    <th>الموظف</th>
                    <th  class="text-center" >بيانات المتبرع</th>
                    <th  class="text-center" > ملخص العمليات</th>
                    <th  class="text-center" > تغيير الموظف</th>
                </tr>
            </tfoot>
            <tbody>
            @foreach($data as $key=>$item)
                <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->assignment->employeeable->name}}</td>
                <td class="text-center" ><a class="text-white h3 btn btn-info "  data-toggle="modal" data-target="#show{{$key}}"><i class="fas fa-eye"></i></a></td>
                <td class="text-center" ><a class="text-white h3 btn btn-warning "  data-toggle="modal" data-target="#data{{$key}}"><i class="fas fa-chart-pie"></i></a></td>
                <td class="text-center" ><a class="text-white h3 btn btn-danger "  data-toggle="modal" data-target="#edit{{$key}}"><i class="fas fa-unlink"></i></a>

                </td>
                </tr>
            @endforeach
            </tbody>


    @endcomponent
    @foreach($data as $key=>$item)
        @component('components.modal_xl',['title'=>'بيانات متبرع محتمل باسم/ '.$item->name ,'color'=>'info','id'=>'show'.$key])
            <div class=" row justify-content-center" >
                <div class="col-md-12 table-responsive py-3">
                    <table class="table table-bordered  ">
                        <tbody>
                        <tr>
                            <th> الاسم</th> <td> {{$item->name}}</td>
                        </tr>

                        <tr>
                            <th> الهاتف</th> <td> {{$item->phone}}</td>
                        </tr>
                        <tr>
                            <th>الايميل</th> <td> {{$item->email}}</td>
                        </tr>
                        <tr>
                            <th>الوسيلة</th> <td> {{$item->device}}</td>
                        </tr>

                        <tr>
                            <th>الاعلان </th> <td>{{($item->adable)? 'رقم '.$item->adable->id :'لا يوجد' }} </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        @endcomponent
        @component('components.modal_xl',['title'=>'ملخص العمليات التامة للمتبرع: '.$item->name ,'color'=>'warning','id'=>'data'.$key])
            <div class=" row justify-content-center" >
                <div class="col-md-6 table-responsive py-3">
                    <table class="table table-bordered  ">
                        <tbody>
                        <tr>
                            <th>غرض العملية</th> <td>{{$item->assignment->message}}</td>
                        </tr>
                        <tr>
                            <th> عدد المكالمات</th> <td>{{$item->calls()->count()}}</td>
                        </tr>
                        <tr>
                            <th> حالة تعديل البيانات</th> <td>{{($item->actualBenefactor()->count())?'تم':' لم يتم'}}</td>
                        </tr>
                        </tbody>
                    </table>

                    <h6 class="pt-1">المكالمات التي تمت:</h6>
                    <div class="d-block p-2 ">


                        @component('components.justTable',['title'=>'  المكالمات التي تمت للمتبرع '.$item->name])


                            <thead>
                            <tr>
                                <th>#</th>
                                <th>تاريخ المكالمة</th>
                                <th>عرض</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($item->calls as $key=>$call)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{getDateTime($call->date.$call->time)}}</td>
                                    <td><a class="text-white h3 btn btn-warning "  data-toggle="modal" data-target="#showCall{{$call->id}}"><i class="fas fa-eye"></i></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        @endcomponent
                        @foreach($item->calls as $key=>$call)
                            @component('components.modal',['color'=>'secondary','id'=>'showCall'.$call->id,'title'=>'تفاصيل المكالمة'])
                                <div class="table-responsive">

                                    <table class="table table-bordered">
                                        <tr class="bg-secondary text-white">
                                            <th>غرض المكالمة </th>
                                            <td>{{$call->assignable->message}} </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">

                                        <tr>
                                            <th>توقيت المكالمة</th>
                                            <td> {{getDateTime($call->date.$call->time)}}</td>
                                        </tr>

                                        <tr>
                                            <th>حالة الرد</th>
                                            <td> {{$call->answer_status}} <small class="text-secondary"> {{$call->status_more}} </small></td>
                                        </tr>

                                        <tr>
                                            <th>الجاوب</th>
                                            <td>{{$call->reacting}}</td>
                                        </tr>

                                        <tr>
                                            <th>تقييم المكالمة</th>
                                            <td>{{$call->call_rate??'0'}}  / 10</td>
                                        </tr>

                                        <tr>
                                            <th>أهم سؤال</th>
                                            <td>{{$call->question??'لا يوجد'}}</td>
                                        </tr>

                                        <tr>
                                            <th>ملخص المكالمة</th>
                                            <td>{{$call->call_samary??'لا يوجد'}}</td>
                                        </tr>

                                        <tr>
                                            <th>ملاحظات</th>
                                            <td>{{$call->notes??'لا يوجد'}}</td>
                                        </tr>

                                    </table>
                                </div>
                            @endcomponent
                        @endforeach
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6">
                    @include('tmps.newBenefactorCallsChartTemp',['item'=>$item,'color'=>'primary'])
                </div>

            </div>

        @endcomponent
        @component('components.modal_xl',['title'=>'تغيير الموظف الذي يقوم بالتواصل','color'=>'danger','id'=>'edit'.$key])

                <form action="{{route('newBenefactors.changeEmp',$item->id)}}" method="POST">
                    @csrf
                    <div class="row justify-content-center">
                        <div class="form-group col-md-10">
                            <label class="text-primary"> اختر اسم الموظف  </label>
                            <input type="text" name="benefactor_id" hidden id="" value="{{$item->id}}" required>
                            <select name="emp_id"  class="form-control @error('emp_id') is-invalid @enderror">
                                <option selected > اختر الموظف </option>
                                @foreach(App\User::whereRoleIS('call_emp')->orWhereRoleIS('call_manager')->get() as $emp)
                                    <option value="{{$emp->id}}" {{($item->assignment->employeeable->id == $emp->id)?'selected':''}} {{(old('emp_id') == $emp->id)?'selected':''}}>  {{$emp->name}}</option>
                                @endforeach
                            </select>
                            @error('emp_id')
                            <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                            @enderror
                        </div>


                        <div class="form-group col-md-10">
                            <label class="text-primary"> الرسالة  </label>
                            <textarea class="form-control @error('message') is-invalid @enderror" name="message" rows="7" required>{{$item->assignment->message??''}}</textarea>
                            @error('message')
                            <span class="invalid-feedback" role="alert">
                                                        <small class="text-danger">{{ $message }}</small>
                                                    </span>
                            @enderror
                        </div>

                    </div>
                    <div class="row justify-content-center my-3">
                        <div class="form-group col-md-4 ">
                            <input type="submit" class="btn btn-danger form-control" value="تخصيص">
                        </div>
                    </div>



                </form>


            @endcomponent
    @endforeach



  </div>
  <!-- /.container-fluid -->
@stop

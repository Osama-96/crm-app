@extends('pages.layouts.app')
@section('content')

    <!-- Begin Page Content -->
    <div class="container-fluid  ">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-white">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
                <li class="breadcrumb-item active" aria-current="page">ملف المتبرع {{$benefactor->name}}</li>
            </ol>
        </nav>
        <!-- Content Row -->
        <div class="row">
            <!-- Grow In Utility -->
            <div class="col-lg-8 mb-4 ">
                @component('components.card',['title'=>'ملف المتبرع: '.$benefactor->name ,'color'=>'primary'])
                    <div class="table-responsive">
                    @if(! $benefactor->actualbenefactor()->count())

                    <table class="table table-bordered ">
                        <tbody>
                        <tr>
                            <th> الاسم</th> <td> {{$benefactor->name}}</td>
                        </tr>

                        <tr>
                            <th> الهاتف</th> <td> {{$benefactor->phone}}</td>
                        </tr>
                        <tr>
                            <th>الايميل</th> <td> {{$benefactor->email}}</td>
                        </tr>
                        <tr>
                            <th>الوسيلة</th> <td> {{$benefactor->device}}</td>
                        </tr>

                        <tr>
                            <th>الاعلان </th> <td>{{($benefactor->adable_id != -1 )? 'رقم'.$benefactor->adable_id :'لم يتم تحديد إعلان' }} </td>
                        </tr>


                        </tbody>
                    </table>
                        @else
                            <table class="table table-bordered  ">
                                <tbody>
                                <tr>
                                    <th> الاسم</th> <td> {{$benefactor->actualBenefactor->name??""}}</td>
                                </tr>

                                <tr>
                                    <th> الهاتف</th> <td> {{$benefactor->actualBenefactor->phone??""}}</td>
                                </tr>
                                <tr>
                                    <th>الايميل</th> <td> {{$benefactor->actualBenefactor->email??""}}</td>
                                </tr>
                                <tr>
                                    <th>الايميل</th> <td> {{$benefactor->actualBenefactor->address??''}}</td>
                                </tr>
                                <tr>
                                    <th>الوسيلة</th> <td> {{$benefactor->device??''}}</td>
                                </tr>

                                <tr>
                                    <th>الاعلان </th> <td>{{($benefactor->adable)? 'رقم'.$benefactor->adable->id:'لا يوجد' }} </td>
                                </tr>

                                <tr>
                                    <th>تصنيف المتبرع</th> <td>{{getBenefactorType($benefactor->actualBenefactor)}} </td>
                                </tr>
                                <tr>
                                    <th>نوع المتبرع </th> <td>{{getBenefactorType($benefactor->actualBenefactor)}} </td>
                                </tr>

                                <tr>
                                    <th>نوع التبرع </th> <td>{{ getDonationType($benefactor->actualBenefactor) }} </td>
                                </tr>

                                <tr>
                                    <th>التبرع  </th> <td>{{ getDonation($benefactor->actualBenefactor) }} </td>
                                </tr>

                                <tr>
                                    <th>تاريخ أول تحصيل </th> <td>{{getDateTime($benefactor->actualBenefactor->dueDate)??'' }} </td>
                                </tr>


                                <tr>
                                    <th>أسماء الحالات</th> <td>{{ $benefactor->actualBenefactor->cases??'' }} </td>
                                </tr>

                                </tbody>
                            </table>

                        @endif
                    </div>
                @endcomponent
            </div>
            <div class="col-md-4">
                @if(! $benefactor->actualbenefactor()->count())
                    @include('tmps.newBenefactorCallsChartTemp',['item'=>$benefactor,'color'=>'primary'])
                @else
                    @include('tmps.actualBenefactorCallsChartTemp',['benefactor'=>$benefactor,'color'=>'primary'])
                @endif
            </div>
        </div>

        @component('components.table',['title'=>'المكالمات التي تمت له','color'=>'warning'])
            <thead>
            <tr>
                <th>#</th>
                <th  class="text-center">الكود</th>
                <th  class="text-center">التوقيت</th>
                <th  class="text-center">الموظف</th>
                <th  class="text-center">حالة الرد</th>
                <th  class="text-center"> التقييم</th>
                <th  class="text-center">عرض</th>
            </tr>
            </thead>
            <tbody>
            @foreach($benefactor->calls as  $key=>$call)
                <tr class="text-white  {{($call->reacting == 'غير متجاوب')?'bg-danger':'bg-success'}}">
                    <td  class="text-center">{{$key+1}}</td>
                    <td  class="text-center">c-{{$call->id}}</td>
                    <td  class="text-center">{{getDateTime($call->date.$call->time)}}</td>
                    <td  class="text-center">{{$call->userable->name}}</td>
                    <td  class="text-center">{{$call->answer_status}}</td>
                    <td  class="text-center">10/{{($call->call_rate)??0}}</td>
                    <td   class="text-center"> <a class="text-white btn btn-warning "  data-toggle="modal" data-target="#call_view_{{$key}}"> <i class="fas fa-eye"></i> </a> </td>
                </tr>
            @endforeach
            </tbody>
        @endcomponent
        @foreach($benefactor->calls as  $key=>$call)
            @component('components.modal_xl',['id'=>'call_view_'.$key,'color'=>'primary','title'=>'تفاصيل المكالمة'])
                <div class="table-responsive">

                    <table class="table table-bordered">
                        <tr class="bg-secondary text-white">
                            <th>غرض المكالمة </th>
                            <td>{{$call->assignable->message}} </td>
                        </tr>
                        <tr class="bg-secondary text-white">
                            <th>الموظف الذي قام بالتواصل</th>
                            <td>{{$call->userable->name}} </td>
                        </tr>
                    </table>

                    <table class="table table-bordered">

                        <tr>
                            <th>توقيت المكالمة</th>
                            <td> {{getDateTime($call->date.$call->time)}}</td>
                        </tr>

                        <tr>
                            <th>حالة الرد</th>
                            <td> {{$call->answer_status}} <small class="text-secondary"> {{$call->status_more}} </small></td>
                        </tr>

                        <tr>
                            <th>الجاوب</th>
                            <td>{{$call->reacting}}</td>
                        </tr>

                        <tr>
                            <th>تقييم المكالمة</th>
                            <td>{{$call->call_rate??'0'}}  / 10</td>
                        </tr>

                        <tr>
                            <th>أهم سؤال</th>
                            <td>{{$call->question??'لا يوجد'}}</td>
                        </tr>

                        <tr>
                            <th>ملخص المكالمة</th>
                            <td>{{$call->call_samary??'لا يوجد'}}</td>
                        </tr>

                        <tr>
                            <th>ملاحظات</th>
                            <td>{{$call->notes??'لا يوجد'}}</td>
                        </tr>

                    </table>
                </div>

            @endcomponent
        @endforeach

    </div>

    <!--Edit Modal -->


@stop

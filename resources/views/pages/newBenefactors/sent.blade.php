@extends('pages.layouts.app')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
            <li class="breadcrumb-item active" aria-current="page">المتبرعين المحتملين</li>
        </ol>
    </nav>
    <!-- Content Row -->
    @component('components.table',['title'=>'جميع المتبرعين المحتملين الجدد ','color'=>'primary'])
            <thead>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>الهاتف</th>
                    <th>الايميل</th>
                    <th>الوسيلة</th>
                    <th>الاعلان</th>
                    <th  class="text-center" >عرض</th>
                    <th  class="text-center" > إرسال</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>الهاتف</th>
                    <th>الايميل</th>
                    <th>الوسيلة</th>
                    <th>الاعلان</th>
                    <th  class="text-center" >عرض</th>
                    <th  class="text-center" >إرسال</th>
                </tr>
            </tfoot>
            <tbody>
            @foreach($data as $key=>$item)
                <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->phone}}</td>
                <td>{{$item->email}}</td>
                <td>{{$item->device}}</td>
                <td>{{($item->adable)? 'رقم '.$item->adable->id :'لا يوجد' }}</td>
                <td class="text-center" ><a class="text-white h3 btn btn-warning "  data-toggle="modal" data-target="#show{{$key}}"><i class="fas fa-eye"></i></a></td>
                <td class="text-center" ><a class="text-white h3 btn btn-primary "  data-toggle="modal" data-target="#edit{{$key}}"><i class="fas fa-link"></i></a></td>
                </tr>
            @endforeach
            </tbody>


    @endcomponent
    @foreach($data as $key=>$item)
        @component('components.modal_xl',['title'=>'بيانات متبرع محتمل باسم/ '.$item->name ,'color'=>'warning','id'=>'show'.$key])
            <div class=" row justify-content-center" >
                <div class="col-md-12 table-responsive py-3">
                    <table class="table table-bordered  ">
                        <tbody>
                        <tr>
                            <th> الاسم</th> <td> {{$item->name}}</td>
                        </tr>

                        <tr>
                            <th> الهاتف</th> <td> {{$item->phone}}</td>
                        </tr>
                        <tr>
                            <th>الايميل</th> <td> {{$item->email}}</td>
                        </tr>
                        <tr>
                            <th>الوسيلة</th> <td> {{$item->device}}</td>
                        </tr>

                        <tr>
                            <th>الاعلان </th> <td>{{($item->adable)? 'رقم '.$item->adable->id :'لا يوجد' }} </td>
                        </tr>

                        </tbody>
                    </table>

                </div>

            </div>

        @endcomponent
        @component('components.modal_xl',['title'=>' إرسال متبرع '.$item->name .'  لفريق الكول سنتر','color'=>'primary','id'=>'edit'.$key])
                <form action="{{route('newBenefactors.assign',$item->id)}}" method="POST">
                    @csrf
                    <div class="row justify-content-center">
                        <div class="form-group col-md-10">
                            <label class="text-primary"> اختر اسم الموظف  </label>
                            <select name="emp_id"  required class="form-control @error('emp_id') is-invalid @enderror">
                                @foreach(App\User::whereRoleIS('call_emp')->orWhereRoleIS('call_manager')->get() as $emp)
                                    <option value="{{$emp->id}}" {{(old('emp_id') == $emp->id)?'selected':''}}>  {{$emp->name}}</option>
                                @endforeach
                            </select>
                            @error('emp_id')
                            <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                            @enderror
                        </div>


                        <div class="form-group col-md-10">
                            <label class="text-primary"> الرسالة  </label>
                            <textarea required class="form-control @error('message') is-invalid @enderror" name="message" rows="7" >{{old('message')}}</textarea>
                            @error('message')
                            <span class="invalid-feedback" role="alert">
                                                        <small class="text-danger">{{ $message }}</small>
                                                    </span>
                            @enderror
                        </div>

                    </div>
                    <div class="row justify-content-center my-3">
                        <div class="form-group col-md-4 ">
                            <input type="submit" class="btn btn-primary form-control" value="تخصيص">
                        </div>
                    </div>



                </form>


        @endcomponent
    @endforeach



  </div>
  <!-- /.container-fluid -->
@stop

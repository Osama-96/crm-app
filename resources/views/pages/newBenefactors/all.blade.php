@extends('pages.layouts.app')
@section('css')
<link href="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@stop
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
            <li class="breadcrumb-item active" aria-current="page">المتبرعين المحتملين</li>
        </ol>
    </nav>
    <!-- Content Row -->
    @component('components.table',['title'=>'جميع المتبرعين المحتملين ','color'=>'primary'])
            <thead>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>الهاتف</th>
                    <th>الايميل</th>
                    <th>الوسيلة</th>
                    <th>الاعلان</th>
                    <th>الحالة</th>
                    <th  class="text-center" >عرض</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>الهاتف</th>
                    <th>الايميل</th>
                    <th>الوسيلة</th>
                    <th>الاعلان</th>
                    <th>الحالة</th>
                    <th  class="text-center" >عرض</th>
                </tr>
            </tfoot>
            <tbody>
            @foreach($data as $key=>$item)
                <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->phone}}</td>
                <td>{{$item->email}}</td>
                <td>{{$item->device}}</td>
                <td>{{($item->adable)? 'رقم '.$item->adable->id :'لا يوجد' }}</td>
                <td>{{$item->status }}</td>
                <td class="text-center" ><a class="text-white h3 btn btn-warning " href="{{route('newBenefactors.show',$item)}}"><i class="fas fa-eye"></i></a></td>
                </tr>
            @endforeach
            </tbody>


    @endcomponent
    @foreach($data as $key=>$item)
        @component('components.modal_xl',['title'=>'بيانات متبرع محتمل باسم/ '.$item->name ,'color'=>'primary','id'=>'show'.$key])
            <div class=" row justify-content-center" >
                <div class="col-md-12 table-responsive py-3">
                    <table class="table table-bordered  ">
                        <tbody>
                        <tr>
                            <th> الاسم</th> <td> {{$item->name}}</td>
                        </tr>

                        <tr>
                            <th> الهاتف</th> <td> {{$item->phone}}</td>
                        </tr>
                        <tr>
                            <th>الايميل</th> <td> {{$item->email}}</td>
                        </tr>
                        <tr>
                            <th>الوسيلة</th> <td> {{$item->device}}</td>
                        </tr>

                        <tr>
                            <th>الاعلان </th> <td>{{($item->adable)? 'رقم '.$item->adable->id :'لا يوجد' }} </td>
                        </tr>

                        </tbody>
                    </table>

                </div>

            </div>

        @endcomponent
    @endforeach



  </div>
  <!-- /.container-fluid -->
@stop
@section('js')
    <!-- Page level plugins -->
    <script src="{{asset('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{asset('assets/js/demo/datatables-demo.js')}}"></script>
@stop

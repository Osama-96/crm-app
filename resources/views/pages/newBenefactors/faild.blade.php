@extends('pages.layouts.app')
@section('css')
<link href="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@stop
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
            <li class="breadcrumb-item active" aria-current="page">المتبرعين المحتملين </li>
        </ol>
    </nav>
    <!-- Content Row -->
    @component('components.table',['title'=>'جميع المتبرعين المحتملين الذين فشل التواصل معهم','color'=>'primary'])
            <thead>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>الهاتف</th>
                    <th>الايميل</th>
                    <th>الوسيلة</th>
                    <th>الاعلان</th>
                    <th  class="text-center" > ملخص العمليات</th>
                    <th  class="text-center" >عرض</th>
                    <th  class="text-center" > إرسال</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>الهاتف</th>
                    <th>الايميل</th>
                    <th>الوسيلة</th>
                    <th>الاعلان</th>
                    <th  class="text-center" >عرض</th>
                    <th  class="text-center" > ملخص العمليات</th>
                    <th  class="text-center" >إرسال</th>
                </tr>
            </tfoot>
            <tbody>
            @foreach($data as $key=>$item)
                <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->phone}}</td>
                <td>{{$item->email}}</td>
                <td>{{$item->device}}</td>
                <td>{{($item->adable)? 'رقم '.$item->adable->id :'لا يوجد' }}</td>
                    <td class="text-center" ><a class="text-white h3 btn btn-warning "  data-toggle="modal" data-target="#data{{$loop->iteration}}"><i class="fas fa-chart-pie"></i></a></td>
                    <td class="text-center" ><a class="text-white h3 btn btn-primary "  href="{{route('newBenefactors.show',$item)}}"><i class="fas fa-eye"></i></a></td>
                <td class="text-center" ><a class="text-white h3 btn btn-danger "  data-toggle="modal" data-target="#edit{{$loop->iteration}}"><i class="fas fa-link"></i></a></td>
                </tr>
            @endforeach
            </tbody>

    @endcomponent
    @foreach($data as $key=>$item)
        @component('components.modal_xl',['title'=>'ملخص العمليات التامة للمتبرع: '.$item->name ,'color'=>'warning','id'=>'data'.$loop->iteration])
            <div class=" row justify-content-center" >
                <div class="col-md-6  table-responsive py-3">
                    <table class="table table-bordered  ">
                        <tbody>
                        <tr>
                            <th>غرض العملية</th> <td>{{$item->assignment->message}}</td>
                        </tr>
                        <tr>
                            <th> عدد المكالمات</th> <td>{{$item->calls()->count()}}</td>
                        </tr>
                        <tr>
                            <th> حالة تعديل البيانات</th> <td>{{($item->actualBenefactor()->count())?'تم':' لم يتم'}}</td>
                        </tr>
                        </tbody>
                    </table>

                    <h6 class="pt-1">المكالمات التي تمت:</h6>
                    <div class="d-block">


                        @component('components.justTable',['title'=>'  المكالمات التي تمت للمتبرع '.$item->name])


                            <thead>
                            <tr>
                                <th>#</th>
                                <th>تاريخ المكالمة</th>
                                <th>عرض</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($item->calls as $key=>$call)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{getDateTime($call->date.$call->time)}}</td>
                                <td><a class="text-white h3 btn btn-warning "  data-toggle="modal" data-target="#showCall{{$call->id}}"><i class="fas fa-eye"></i></a></td>
                            </tr>
                            @endforeach
                            </tbody>
                        @endcomponent
                        @foreach($item->calls as $key=>$call)
                            @component('components.modal',['color'=>'secondary','id'=>'showCall'.$call->id,'title'=>'تفاصيل المكالمة'])
                                <div class="table-responsive">

                                    <table class="table table-bordered">
                                        <tr class="bg-secondary text-white">
                                            <th>غرض المكالمة </th>
                                            <td>{{$call->assignable->message}} </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">

                                        <tr>
                                            <th>توقيت المكالمة</th>
                                            <td> {{getDateTime($call->date.$call->time)}}</td>
                                        </tr>

                                        <tr>
                                            <th>حالة الرد</th>
                                            <td> {{$call->answer_status}} <small class="text-secondary"> {{$call->status_more}} </small></td>
                                        </tr>

                                        <tr>
                                            <th>الجاوب</th>
                                            <td>{{$call->reacting}}</td>
                                        </tr>

                                        <tr>
                                            <th>تقييم المكالمة</th>
                                            <td>{{$call->call_rate??'0'}}  / 10</td>
                                        </tr>

                                        <tr>
                                            <th>أهم سؤال</th>
                                            <td>{{$call->question??'لا يوجد'}}</td>
                                        </tr>

                                        <tr>
                                            <th>ملخص المكالمة</th>
                                            <td>{{$call->call_samary??'لا يوجد'}}</td>
                                        </tr>

                                        <tr>
                                            <th>ملاحظات</th>
                                            <td>{{$call->notes??'لا يوجد'}}</td>
                                        </tr>

                                    </table>
                                </div>
                            @endcomponent
                        @endforeach
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6">
                    @include('tmps.newBenefactorCallsChartTemp',['item'=>$item,'color'=>'primary'])
                </div>

            </div>

        @endcomponent

    @component('components.modal_xl',['title'=>'إعادة إرسال متبرع '.$item->name .'  لفريق الكول سنتر','color'=>'danger','id'=>'edit'.$loop->iteration])
                <form action="{{route('newBenefactors.changeEmp',$item->id)}}" method="POST">
                    @csrf
                    <div class="row justify-content-center">
                        <div class="form-group col-md-10">
                            <label class="text-primary"> اختر اسم الموظف  </label>
                            <select name="emp_id"  required class="form-control @error('emp_id') is-invalid @enderror">
                                @foreach(App\User::whereRoleIS('call_emp')->orWhereRoleIS('call_manager')->get() as $emp)
                                    <option value="{{$emp->id}}" {{(old('emp_id') == $emp->id)?'selected':''}}>  {{$emp->name}}</option>
                                @endforeach
                            </select>
                            @error('emp_id')
                            <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                            @enderror
                        </div>


                        <div class="form-group col-md-10">
                            <label class="text-primary"> الرسالة  </label>
                            <textarea required class="form-control @error('message') is-invalid @enderror" name="message" rows="7" >{{old('message')}}</textarea>
                            @error('message')
                            <span class="invalid-feedback" role="alert">
                                                        <small class="text-danger">{{ $message }}</small>
                                                    </span>
                            @enderror
                        </div>

                    </div>
                    <div class="row justify-content-center my-3">
                        <div class="form-group col-md-4 ">
                            <input type="submit" class="btn btn-danger form-control" value="تخصيص">
                        </div>
                    </div>



                </form>


        @endcomponent
    @endforeach



  </div>
  <!-- /.container-fluid -->
@stop
@section('js')
    <!-- Page level plugins -->
    <script src="{{asset('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{asset('assets/js/demo/datatables-demo.js')}}"></script>
@stop

@extends('pages.layouts.app')
@section('css')
<link href="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@stop
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
            <li class="breadcrumb-item active" aria-current="page">المتبرعين المحتملين</li>
        </ol>
    </nav>
    <!-- Content Row -->
    @component('components.table',['title'=>'جميع المتبرعين المحتملين الذين تم تفعيلهم ويمكن إرسال ملفاتهم للتحصيل ','color'=>'primary'])
            <thead>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>تاريخ تسجيله</th>
                    <th>تاريخ انتهاء التواصل</th>
                    <th  class="text-center" >بيانات المتبرع</th>
                    <th  class="text-center" > ملخص العمليات</th>
                    <th  class="text-center" >إرسال</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>الاسم</th>
                    <th>تاريخ تسجيله</th>
                    <th>تاريخ انتهاء التواصل</th>
                    <th  class="text-center" >بيانات المتبرع</th>
                    <th  class="text-center" > ملخص العمليات</th>
                    <th  class="text-center" >إرسال</th>
                </tr>
            </tfoot>
            <tbody>
            @foreach($data as $key=>$item)
                <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->name}}</td>
                <td>{{getDateTime($item->created_at)}}</td>
                <td>{{getDateTime($item->updated_at)}}</td>
                <td class="text-center" ><a class="text-white h3 btn btn-info "  data-toggle="modal" data-target="#show{{$loop->iteration}}"><i class="fas fa-eye"></i></a></td>
                <td class="text-center" ><a class="text-white h3 btn btn-warning "  data-toggle="modal" data-target="#data{{$loop->iteration}}"><i class="fas fa-chart-pie"></i></a></td>
                <td class="text-center" ><a class="text-white h3 btn btn-danger "  data-toggle="modal" data-target="#edit{{$loop->iteration}}" ><i class="fas fa-share"></i></a>
                </tr>
            @endforeach
            </tbody>


    @endcomponent
    @foreach($data as $key=>$item)
        @component('components.modal_xl',['title'=>'بيانات متبرع محتمل باسم/ '.$item->name ,'color'=>'info','id'=>'show'.$loop->iteration])
            <div class=" row justify-content-center" >
                <div class="col-md-12 table-responsive py-3">
                    <table class="table table-bordered  ">
                        <tbody>
                        <tr>
                            <th> الاسم</th> <td> {{$item->actualBenefactor->name??""}}</td>
                        </tr>

                        <tr>
                            <th> الهاتف</th> <td> {{$item->actualBenefactor->phone??""}}</td>
                        </tr>
                        <tr>
                            <th>الايميل</th> <td> {{$item->actualBenefactor->email??""}}</td>
                        </tr>
                        <tr>
                            <th>الايميل</th> <td> {{$item->actualBenefactor->address??''}}</td>
                        </tr>
                        <tr>
                            <th>الوسيلة</th> <td> {{$item->device??''}}</td>
                        </tr>

                        <tr>
                            <th>الاعلان </th> <td>{{($item->adable)? 'رقم '.$item->adable->id :'لا يوجد' }} </td>
                        </tr>
                        <tr>
                            <th>نوع المتبرع </th> <td>{{getBenefactorType($item->actualBenefactor)}} </td>
                        </tr>

                        <tr>
                            <th>نوع التبرع </th> <td>{{ getDonationType($item->actualBenefactor) }} </td>
                        </tr>

                        <tr>
                            <th>التبرع  </th> <td>{{ getDonation($item->actualBenefactor) }} </td>
                        </tr>

                        <tr>
                            <th>تاريخ أول تحصيل </th> <td>{{getDateTime($item->actualBenefactor->dueDate)??'' }} </td>
                        </tr>


                        <tr>
                            <th>أسماء الحالات</th> <td>{{ $item->actualBenefactor->cases??'' }} </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        @endcomponent
        @component('components.modal_xl',['title'=>'ملخص العمليات التامة للمتبرع: '.$item->name ,'color'=>'warning','id'=>'data'.$loop->iteration])
            <div class=" row justify-content-center" >
                <div class="col-md-6 table-responsive py-3">
                    <table class="table table-bordered  ">
                        <tbody>
                        <tr>
                            <th>غرض العملية</th> <td>{{$item->assignment->message}}</td>
                        </tr>
                        <tr>
                            <th> عدد المكالمات</th> <td>{{$item->calls()->count()}}</td>
                        </tr>
                        <tr>
                            <th> حالة تعديل البيانات</th> <td>{{($item->actualBenefactor()->count())?'تم':' لم يتم'}}</td>
                        </tr>
                        </tbody>
                    </table>

                    <h6 class="pt-1">المكالمات التي تمت:</h6>
                    <div class="d-block p-2 ">


                        @component('components.justTable',['title'=>'  المكالمات التي تمت للمتبرع '.$item->name])


                            <thead>
                            <tr>
                                <th>#</th>
                                <th>تاريخ المكالمة</th>
                                <th>عرض</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($item->calls as $key=>$call)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{getDateTime($call->date.$call->time)}}</td>
                                    <td><a class="text-white h3 btn btn-warning "  data-toggle="modal" data-target="#showCall{{$call->id}}"><i class="fas fa-eye"></i></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        @endcomponent
                        @foreach($item->calls as $key=>$call)
                            @component('components.modal',['color'=>'secondary','id'=>'showCall'.$call->id,'title'=>'تفاصيل المكالمة'])
                                <div class="table-responsive">

                                    <table class="table table-bordered">
                                        <tr class="bg-secondary text-white">
                                            <th>غرض المكالمة </th>
                                            <td>{{$call->assignable->message}} </td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered">

                                        <tr>
                                            <th>توقيت المكالمة</th>
                                            <td> {{getDateTime($call->date.$call->time)}}</td>
                                        </tr>

                                        <tr>
                                            <th>حالة الرد</th>
                                            <td> {{$call->answer_status}} <small class="text-secondary"> {{$call->status_more}} </small></td>
                                        </tr>

                                        <tr>
                                            <th>الجاوب</th>
                                            <td>{{$call->reacting}}</td>
                                        </tr>

                                        <tr>
                                            <th>تقييم المكالمة</th>
                                            <td>{{$call->call_rate??'0'}}  / 10</td>
                                        </tr>

                                        <tr>
                                            <th>أهم سؤال</th>
                                            <td>{{$call->question??'لا يوجد'}}</td>
                                        </tr>

                                        <tr>
                                            <th>ملخص المكالمة</th>
                                            <td>{{$call->call_samary??'لا يوجد'}}</td>
                                        </tr>

                                        <tr>
                                            <th>ملاحظات</th>
                                            <td>{{$call->notes??'لا يوجد'}}</td>
                                        </tr>

                                    </table>
                                </div>
                            @endcomponent
                        @endforeach
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6">
                    @include('tmps.newBenefactorCallsChartTemp',['item'=>$item,'color'=>'primary'])
                </div>

            </div>

        @endcomponent
        @component('components.modal',['title'=>'إرسال المتبرع إلى قسم التحصيل','color'=>'danger','id'=>'edit'.$loop->iteration])

            <form action="{{route('newBenefactors.end',$item->id)}}" method="POST">
                @csrf
                <div class="row justify-content-center">
                    <div class="form-group col-md-10">
                        <p>بهذا الإجراء؛ سينتقل ملف المتبرع إلى رئيس قسم التحصيل وتنتهي كافة العمليات التي يقوم بها قسم الكول سنتر..</p>
                    </div>
                </div>
                <div class="row justify-content-center my-3">
                    <div class="form-group col-md-4 ">
                        <input type="submit" class="btn btn-danger form-control" value="تأكيد">
                    </div>
                </div>



            </form>


        @endcomponent
    @endforeach




  </div>
  <!-- /.container-fluid -->
@stop

@extends('pages.layouts.app')
@section('css')
    <link href="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@stop
@section('content')

    <!-- Begin Page Content -->
    <div class="container-fluid  ">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-white">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
                @if($assignment)
                    @if($assignment->done == 0)
                    <li class="breadcrumb-item"><a href="{{route('call_assignments.index')}}">عمليات التخصيص الجديدة</a></li>
                    @else
                    <li class="breadcrumb-item"><a href="{{route('old_assignments.callCenter')}}">عمليات التخصيص القديمة</a></li>
                    @endif
                <li class="breadcrumb-item"><a href="{{route('call_assignments.show',$assignment->id)}}">عملية مخصصة بكود {{$assignment->id}}</a></li>
                @endif
                <li class="breadcrumb-item active" aria-current="page">ملف المتبرع {{$benefactor->name}}</li>
            </ol>
        </nav>
        <!-- Content Row -->
        <div class="row">
            <!-- Grow In Utility -->
            <div class="col-lg-12 mb-4 ">
                <div class="card position-relative">
                    <div class="card-header py-3 bg-primary text-center">
                        <h3 class="mb-1  text-white"> {{$benefactor->name}} </h3>
                    </div>
                    <div class="card-body ">
                        <div class=" row " >
                            <div class="col-md-6 table-responsive ">
                                <table class="table table-striped table-inverse ">
                                    <tbody>
                                    <tr>
                                        <th> الاسم</th> <td> {{$benefactor->name}}</td>
                                    </tr>

                                    <tr>
                                        <th> الهاتف</th> <td> {{$benefactor->phone}}</td>
                                    </tr>
                                    <tr>
                                        <th>الايميل</th> <td> {{$benefactor->email}}</td>
                                    </tr>
                                    <tr>
                                        <th>الوسيلة</th> <td> {{$benefactor->how}}</td>
                                    </tr>

                                    <tr>
                                        <th>الاعلان </th> <td>{{($benefactor->ad != -1 )? 'رقم'.$benefactor->ad :'لم يتم تحديد إعلان' }} </td>
                                    </tr>


                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6 table-responsive ">
                                <table class="table table-striped table-inverse ">
                                    <tbody>
                                    <tr>
                                        <th> نوع التبرع</th> <td> @if($benefactor->isAyni  != null){{'تبرع عيني بــ'. $benefactor->donation}} @elseif($benefactor->isMony != null) {{'تبرع مالي بــ'. $benefactor->moneyAmount}}@else لم يحدد بعد  @endif</td>
                                    </tr>

                                    <tr>
                                        <th>  توقيت التحصيل</th> <td> {{$benefactor->dueDate?? 'لم يسجل بعد'}}</td>
                                    </tr>
                                    <tr>
                                        <th>التبرع</th> <td>{{$benefactor->moneyAmount}}  {{$benefactor->donation}}</td>
                                    </tr>
                                    <tr>
                                        <th> مرات التحصيل الناجحة</th> <td>{{$benefactor->collectAssignments()->where('done',1)->count()}} </td>
                                    </tr>
                                    <tr>
                                        <th>العنوان</th> <td> {{$benefactor->address}}</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                            @if(auth()->user()->hasRoles('call_manager','account_manager','account_supervisor'))
                                @if($benefactor->isStopped != 1)
                                <a class="btn btn-danger text-white  " data-toggle="modal" data-target="#stop"> توقف عن التبرع تماماً </a>
                                <div class="modal fade" id="stop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header  text-white bg-danger">
                                                <h5 class="modal-title" id="exampleModalLabel">  طلب التوقف عن التبرع مرة أخرى </h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p class="text-center">
                                                    هذا المتبرع طلب التوقف عن التبرعات للأسباب التالية:
                                                </p>
                                                <form action="{{route('stopAllHisActivities',$benefactor->id)}}" method="POST">
                                                    @csrf

                                                    <div class="row justify-content-center">
                                                        <div class="form-group col-md-12  ">
                                                            <label >الأسباب: </label>
                                                            <textarea name="faild_reason" class="form-control"  ></textarea>

                                                            @error('faild_reason')
                                                            <span class="invalid-feedback" role="alert">
                                                                <small class="text-danger">{{ $message }}</small>
                                                            </span>
                                                            @enderror
                                                        </div>

                                                    </div>
                                                    <div class="row justify-content-center my-3">
                                                        <div class="form-group col-md-4 ">
                                                            <input type="submit" class="btn btn-danger form-control" value=" تأكيد">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @else
                                    <form action="{{route('benefactor.restore',$benefactor->id)}}" method="post">
                                        @csrf
                                        <input type="submit" class="btn btn-success" value="إعادة تفعيل نشاطات المتبرع">
                                    </form>
                                @endif
                            @endif

                        </div>
                    </div>
                </div>
            </div>
            @marketing_officer('')
            <div class="col-lg-12  my-4">
                <div class="card position-relative ">
                    <div class="card-header py-3 bg-gradient-warning text-center">
                        <h3 class=" mb-1 text-white">عمليات التسويق الاكتروني</h3>
                    </div>
                    <div class="card-body ">
                        <a  class="btn btn-primary m-1 text-white"  data-toggle="modal" data-target="#m-edit"> تحديث البيانات</a>
                        <div class="modal fade" id="m-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">عملية تعديل البيانات</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="{{route('marketingBenefactors.update',['marketingBenefactor'=>$benefactor->id])}}" method="POST">
                                        @method('PUT')
                                        @csrf

                                        <div class="row justify-content-center">
                                            <div class="form-group col-md-8">
                                                <label for="">الاسم </label>
                                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="" value="{{$benefactor->name}}">
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <small class="text-danger">{{ $message }}</small>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-8">
                                                <label for=""> الهاتف  </label>
                                                <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" id="" value="{{$benefactor->phone}}">
                                                @error('phone')
                                                <span class="invalid-feedback" role="alert">
                                                    <small class="text-danger">{{ $message }}</small>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-8">
                                                <label for=""> الإيميل  </label>
                                                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="" value="{{$benefactor->email}}">
                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <small class="text-danger">{{ $message }}</small>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-8">
                                                <label for=""> الوسيلة التي جاء بها  </label>
                                                <select name="how" id="" class="form-control @error('how') is-invalid @enderror">
                                                    <option disabled selected> اختر الوسيلة </option>
                                                    <option value="الفيسبوك" {{($benefactor->how == "الفيسبوك" )? 'selected' : '' }}> الفيسبوك </option>
                                                    <option value="تويتر" {{($benefactor->how == "تويتر")? 'selected' : '' }}>  تويتر</option>
                                                    <option value="انستجرام" {{($benefactor->how =="انستجرام" )? 'selected' : '' }}>  انستجرام</option>
                                                    <option value="واتساب" {{($benefactor->how =="واتساب")? 'selected' : '' }}>  واتساب</option>
                                                    <option value="وسيلة غير معروفة" {{($benefactor->how == "وسيلة غير معروفة")? 'selected' : '' }}>  أخرى</option>
                                                </select>
                                                @error('how')
                                                <span class="invalid-feedback" role="alert">
                                                    <small class="text-danger">{{ $message }}</small>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-8">
                                                <label for=""> الإعلان  </label>
                                                <select name="ad" id="" class="form-control @error('ad') is-invalid @enderror">
                                                    <option selected value=""> اختر الاعلان </option>
                                                    @foreach(\App\Ad::all() as $ad)
                                                        <option value="{{$ad->id}}" {{($benefactor->ad == $ad->id)? 'selected' : '' }}> رقم {{$ad->id}}</option>
                                                    @endforeach
                                                </select>
                                                @error('ad')
                                                <span class="invalid-feedback" role="alert">
                                                    <small class="text-danger">{{ $message }}</small>
                                                </span>
                                                @enderror
                                            </div>

                                        </div>
                                        <div class="row justify-content-center my-3">
                                            <div class="form-group col-md-4 ">
                                                <input type="submit" class="btn btn-primary form-control" value="حفظ البيانات">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endmarketing_officer
            @call_manager('call_emp')
            <div class="col-lg-12  my-4">
                <div class="card position-relative ">
                    <div class="card-header py-3 bg-gradient-warning text-center">
                        <h3 class=" mb-1 text-white">عمليات الكول سنتر</h3>
                    </div>
                    <div class="card-body text-center justify-content-center">
                        @if($benefactor->isStopped == 0)
                        @call_manager()
                        <a class="btn btn-primary text-white " data-toggle="modal" data-target="#assign"> تخصيص لفريق الكول سنتر للمرة رقم {{$benefactor->assignments()->count()+1}} </a>

                        <div class="modal fade" id="assign" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

                            <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">  تخصيص المتبرع {{$benefactor->name}} لأحد موظفي الكول سنتر </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{route('assignmentsToCallEmps.store')}}" method="POST">
                                    @csrf
                                    <div class="row justify-content-center">
                                        <div class="form-group col-md-10">
                                            <label class="text-primary"> اختر اسم الموظف  </label>
                                            <input type="text" name="benefactor_id" hidden id="" value="{{$benefactor->id}}">
                                            <select name="emp_id"  class="form-control @error('emp_id') is-invalid @enderror">
                                                <option selected > اختر الموظف </option>
                                                @foreach(App\User::whereRoleIS('call_emp')->orWhereRoleIS('call_manager')->get() as $emp)
                                                    <option value="{{$emp->id}}" {{(old('emp_id') == $emp->id)?'selected':''}}>  {{$emp->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('emp_id')
                                            <span class="invalid-feedback" role="alert">
                                                        <small class="text-danger">{{ $message }}</small>
                                                    </span>
                                            @enderror
                                        </div>


                                        <div class="form-group col-md-10">
                                            <label class="text-primary"> الرسالة  </label>
                                            <textarea class="form-control @error('message') is-invalid @enderror" name="message" rows="7" >{{old('message')}}</textarea>
                                            @error('message')
                                            <span class="invalid-feedback" role="alert">
                                                        <small class="text-danger">{{ $message }}</small>
                                                    </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="row justify-content-center my-3">
                                        <div class="form-group col-md-4 ">
                                            <input type="submit" class="btn btn-primary form-control" value="تخصيص">
                                        </div>
                                    </div>



                                </form>
                            </div>
                        </div>
                        </div>

                        @endcall_manager
                        @if($assignment && $assignment->done !=1)
                            <a  class="btn btn-primary m-1 px-3 py-2 text-white"  data-toggle="modal" data-target="#emp-edit"> تحديث بيانات المتبرع</a>
                            <!--Edit Modal -->
                            <div class="modal fade" id="emp-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header bg-primary">
                                            <h5 class="modal-title text-white text-center"  id="exampleModalLabel">عملية تعديل البيانات</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="{{route('assignment.Benefactors.store',['id'=>$benefactor->id,'assignment'=>$assignment])}}" method="POST">
                                            @csrf

                                            <div class="row justify-content-center">
                                                <div class="form-group col-md-8">
                                                    <label for="">الاسم </label>
                                                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="" value="{{$benefactor->name}}">
                                                    @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                            <small class="text-danger">{{ $message }}</small>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-8">
                                                    <label for="">العنوان </label>
                                                    <input type="text" class="form-control @error('address') is-invalid @enderror" name="address" id="" value="{{$benefactor->address}}">
                                                    @error('address')
                                                    <span class="invalid-feedback" role="alert">
                                                        <small class="text-danger">{{ $message }}</small>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-8">
                                                    <label for=""> الهاتف  </label>
                                                    <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" id="" value="{{$benefactor->phone}}">
                                                    @error('phone')
                                                    <span class="invalid-feedback" role="alert">
                                                        <small class="text-danger">{{ $message }}</small>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-8">
                                                    <label for=""> الإيميل  </label>
                                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="" value="{{$benefactor->email}}">
                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <small class="text-danger">{{ $message }}</small>
                                                    </span>
                                                    @enderror
                                                </div>
                                                @if($benefactor->isNew == 1 ||$benefactor->isNew != 1  && auth()->user()->hasRole('call_manager'))
                                                    <div class="form-group col-md-8">
                                                        <label for="">نوع المتبرع   </label>
                                                        <select name="bene_type" id="" class="form-control @error('bene_type') is-invalid @enderror">
                                                            <option disabled selected> اختر النوع </option>
                                                            <option value="1" > متبرع عادي</option>
                                                            <option value="2">كفالة</option>
                                                        </select>
                                                        @error('bene_type')
                                                        <span class="invalid-feedback" role="alert">
                                                            <small class="text-danger">{{ $message }}</small>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                @endif
                                                @if($benefactor->isNew == 1 ||$benefactor->isNew != 1  && auth()->user()->hasRole('call_manager'))
                                                    <div class="form-group col-md-8">
                                                        <label for="">نوع التبرع   </label>
                                                        <select name="donation_type" id="" class="form-control @error('donation_type') is-invalid @enderror">
                                                            <option disabled selected> اختر النوع </option>
                                                            <option value="1" > تبرع مالي</option>
                                                            <option value="2" > تبرع عيني</option>
                                                        </select>
                                                        @error('donation_type')
                                                        <span class="invalid-feedback" role="alert">
                                                            <small class="text-danger">{{ $message }}</small>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                @endif
                                                <div class="form-group col-md-8">
                                                    <label for="">التبرع</label>
                                                    <input type="text" name="donation"  class="form-control">

                                                    <small class="text-danger">ملاحظة: إذا كان نوع التبرع الذي أدخلته (تبرع مالي) فلابد من إدخال قيمة مالية بأرقام صحيحة،إذا كان نوع التبرع الذي أدخلته (تبرع عيني) فلابد من إدخال قيمة هذا التبرع مكتوباً </small>
                                                    @error('donation')
                                                    <span class="invalid-feedback" role="alert">
                                                        <small class="text-danger">{{ $message }}</small>
                                                    </span>
                                                    @enderror
                                                </div>
                                                @if($benefactor->dueDate == null ||$benefactor->dueDate != null  && auth()->user()->hasRole('call_manager'))
                                                    <div class="form-group col-md-8">
                                                        <label for=""> تاريخ أول تحصيل</label>
                                                        <input type="date" name="date"  class="form-control">
                                                        <small class="text-danger">ملاحظة: في حالة كون المتبرع كفيلاً ؛ فإن التاريخ الذي ستحفظه هنا هو التاريخ الذي سيتم التحصيل فيه شهرياً...</small>
                                                        @error('date')
                                                        <span class="invalid-feedback" role="alert">
                                                            <small class="text-danger">{{ $message }}</small>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                @endif


                                            </div>
                                            <div class="row justify-content-center my-3">
                                                <div class="form-group col-md-4 ">
                                                    <input type="submit" class="btn btn-primary form-control" value="حفظ البيانات">
                                                </div>
                                            </div>



                                        </form>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @else
                            <p class="col-md-12 alert alert-danger rounded ">
                                هذا المتبرع توقفت كل نشاطاته بناءً على طلبه من الأستاذ /
                                <b>{{'   ( '.$benefactor->stopper->name.'   )   '}}</b>
                                وذلك في تاريخ:
                                <b>
                                    {{'  ( '. date('d', strtotime($benefactor->stopped_at)) .
                                           ','. config('months.month.'.date('m', strtotime($benefactor->stopped_at))) .
                                           ','.date('Y', strtotime($benefactor->stopped_at)).' ) ' }}
                                </b>
                                وهذه هي رسالة الطلب:
                                <b> {{' " '.$benefactor->stopped_reason.' " '}}</b>
                            </p>
                        @endif
                    </div>
                </div>
            </div>
            @call_manager()
            <div class="col-lg-12  my-4">
                <div class="card position-relative ">
                    <div class="card-header py-3 bg-gradient-warning text-center">
                        <h3 class=" mb-1 text-white"> عمليات التخصيص التي تمت له من خلال رئيس الكول سنتر </h3>
                    </div>
                    <div class="card-body ">
                        @if(count($assignments))
                            <table class="table table-bordered" id="dataTableOne" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>الكود</th>
                                <th>التاريخ</th>
                                <th>الموظف</th>
                                <th>عرض</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr >
                                <th>#</th>
                                <th>الكود</th>
                                <th>التاريخ</th>
                                <th>الموظف</th>
                                <th>عرض</th>
                            </tr>
                            </tfoot>

                            @foreach($assignments as  $key=>$assignment)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$assignment->id}}</td>
                                        <td>{{date('H:i', strtotime($assignment->created_at)) .
                                    ' | '. date('d', strtotime($assignment->created_at)) .
                                    ','. config('months.month.'.date('m', strtotime($assignment->created_at))) .
                                    ','.date('Y', strtotime($assignment->created_at)) }}
                                        </td>
                                        <td>{{$assignment->employeeable->name}}</td>
                                        <td class="text-center" > <a class="text-primary h3 " href="{{route('call.emp.assignment',$assignment->id)}}"> <i class="fas fa-eye"></i> </a> </td>
                                    </tr>
                            @endforeach
                            </table>
                        @else
                        <p class="alert alert-danger"> لم يتم تخصيص هذا المتبرع لفريق الكول سنتر بعد</p>
                        @endif
                    </div>
                </div>
            </div>
            @endcall_manager
            @endcall_manager
            @if(auth()->user()->can('see_all_benefactor_calls'))
            <div class="col-lg-12  my-4">
                <div class="card position-relative ">
                    <div class="card-header py-3 bg-gradient-warning text-center">
                        <h3 class=" mb-1 text-white"> المكالمات التي تمت معه </h3>
                    </div>
                    <div class="card-body ">
                        <div class=" row " >
                            <div class="col-md-12 table-responsive">
                                @if(count($assignments))
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>الكود</th>
                                            <th>الموظف</th>
                                            <th>التوقيت</th>
                                            <th>حالة الرد</th>
                                            <th> التقييم</th>
                                            <th>عرض</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr >
                                            <th>#</th>
                                            <th>الكود</th>
                                            <th>الموظف</th>
                                            <th>التوقيت</th>
                                            <th>حالة الرد</th>
                                            <th> التقييم</th>
                                            <th>عرض</th>
                                        </tr>
                                        </tfoot>
                                        @foreach($assignments as $assignment)
                                        @if($assignment->calls()->count())
                                        @foreach($assignment->calls as  $key=>$call)
                                            <tr class="text-white {{($call->reacting == 'غير متجاوب')?'bg-danger':'bg-success'}}">
                                                <td>{{$key+1}}</td>
                                                <td>c-{{$call->id}}</td>
                                                <td>{{$call->userable->name}}</td>
                                                <td>
                                                    {{date('H:i', strtotime($call->time)) .
                                                        ' | '. date('d', strtotime($call->date)) .
                                                        ','. config('months.month.'.date('m', strtotime($call->date))) .
                                                        ','.date('Y', strtotime($call->date)) }}
                                                </td>
                                                <td>{{$call->answer_status}}</td>
                                                <td>10/{{($call->call_rate)??0}}</td>
                                                <td class="text-center" > <a class="text-primary h5 " data-toggle="modal" data-target="#call_view_{{$key}}"> <i class="fas fa-eye"></i> </a> </td>
                                            </tr>

                                        @endforeach
                                        @endif
                                        @endforeach
                                    </table>
                                @else
                                <p class="alert alert-danger"> لا يوجد مكالمات بعد</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if(count($assignments))
                @foreach($assignments as $assignment)
                    @if($assignment->calls()->count())
                        @foreach($assignment->calls as  $key=>$call)
                        <div class="modal fade bd-example-modal-lg" id="call_view_{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
                <div class="modal-dialog modal-lg" role="document" >
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">بيانات المكالمة ذات الكود:  c-{{$call->id}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <!-- Grow In Utility -->
                                <div class="col-lg-12 ">
                                    <div class="card position-relative ">
                                        <div class="card-body">
                                            <label for="">غرض المكالمة</label>
                                            <br>
                                            <small class="m-2 border rounded">
                                                {{$assignment->message}}
                                            </small>
                                            <table class="table table-striped">

                                                <tr>
                                                    <th>توقيت المكالمة</th>
                                                    <td>
                                                        {{date('H:i', strtotime($call->time)) .
                                                        ' | '. date('d', strtotime($call->date)) .
                                                        ','. config('months.month.'.date('m', strtotime($call->date))) .
                                                        ','.date('Y', strtotime($call->date)) }}
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th>حالة الرد</th>
                                                    <td> {{$call->answer_status}} <small class="text-secondary"> {{$call->status_more}} </small></td>
                                                </tr>

                                                <tr>
                                                    <th>الجاوب</th>
                                                    <td>{{$call->reacting}}</td>
                                                </tr>

                                                <tr>
                                                    <th>تقييم المكالمة</th>
                                                    <td>{{$call->call_rate??'0'}}  / 10</td>
                                                </tr>

                                                <tr>
                                                    <th>أهم سؤال</th>
                                                    <td>{{$call->question??'لا يوجد'}}</td>
                                                </tr>

                                                <tr>
                                                    <th>ملخص المكالمة</th>
                                                    <td>{{$call->call_samary??'لا يوجد'}}</td>
                                                </tr>

                                                <tr>
                                                    <th>ملاحظات</th>
                                                    <td>{{$call->notes??'لا يوجد'}}</td>
                                                </tr>

                                            </table>

                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
                        @endforeach
                    @endif
                @endforeach
            @endif
            @endif
            @account_manager('manager')

            <div class="col-lg-12  my-4">
                <div class="card position-relative ">
                    <div class="card-header py-3 bg-warning text-center">
                        <h3 class=" mb-1 text-white"> عمليات التخصيص التي تمت له من خلال رئيس قسم التحصيل </h3>
                    </div>
                    <div class="card-body ">
                        @if(count($collectAssignments))
                            <table class="table table-bordered" id="dataTableTwo" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>الكود</th>
                                    <th>التاريخ</th>
                                    <th>المشرف</th>
                                    <th>الموظف</th>
                                    <th>عرض</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr >
                                    <th>#</th>
                                    <th>الكود</th>
                                    <th>التاريخ</th>
                                    <th>المشرف</th>
                                    <th>الموظف</th>
                                    <th>عرض</th>
                                </tr>
                                </tfoot>

                                @foreach($collectAssignments as  $key=>$assignment)
                                    @foreach($assignment->collections as $collection)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$collection->id}}</td>
                                        <td>{{date('H:i', strtotime($collection->created_at)) .
                                        ' | '. date('d', strtotime($collection->created_at)) .
                                        ','. config('months.month.'.date('m', strtotime($collection->created_at))) .
                                        ','.date('Y', strtotime($collection->created_at)) }}
                                        </td>
                                        <td>{{$collection->supervisorable->name??'لا يوجد'}}</td>
                                        <td>{{$collection->employeeable->name??'لا يوجد'}}</td>
                                        <td ><a class="text-info  btn btn-outline-info" href="{{route('collect_assignments.show',$collection->collectassignmentable->callAssignmentable->id)}}"> <i class="fa fa-eye"></i> </a></td>
                                        </tr>
                                        @endforeach
                                @endforeach
                            </table>
                        @else
                            <p class="alert alert-danger"> لم يتم تخصيص هذا المتبرع لفريق قسم التحصيل بعد</p>
                        @endif
                    </div>
                </div>
            </div>

            @endaccount_manager
            @if(auth()->user()->can('see_all_benefactor_calls'))
            <div class="col-lg-12 my-4">
                <div class="card position-relative ">
                    <div class="card-header py-3 bg-warning text-center">
                        <h5 class=" mb-1 text-1000 text-white"> عمليات التحصيل التي تمت له</h5>
                    </div>
                    <div class="card-body ">
                        <div class=" row " >
                            <div class="col-md-12 table-responsive">

                                <table class="table table-bordered " id="dataTableThree" width="100%" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>الكود</th>
                                        <th>التاريخ</th>
                                        <th>المشرف</th>
                                        <th>المحصل</th>
                                        <th>عرض</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>الكود</th>
                                        <th>التاريخ</th>
                                        <th>المشرف</th>
                                        <th>المحصل</th>
                                        <th>عرض</th>
                                    </tr>
                                    </tfoot>
                                    @foreach($benefactor->collections as $key=>$collection)

                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$collection->id}}</td>
                                            <td>{{
                                             date('d', strtotime($collection->created_at)) .
                                            ','. config('months.month.'.date('m', strtotime($collection->created_at))) .
                                            ','.date('Y', strtotime($collection->created_at))
                                        }}
                                            </td>
                                            <td>{{$collection->supervisorable->name??'لا يوجد'}}</td>
                                            <td>{{$collection->employeeable->name??'لا يوجد'}}</td>
                                            <td class="text-center " > <a class="text-primary h3 " data-toggle="modal" data-target="#collection_view_{{$key}}"> <i class="fas fa-eye"></i> </a> </td>

                                        </tr>

                                    @endforeach
                                </table>
                                @foreach($benefactor->collections as $key=>$collection)
                                <div class="modal fade bd-example-modal-lg" id="collection_view_{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
                                    <div class="modal-dialog modal-lg" role="document" >
                                        <div class="modal-content">
                                            <div class="modal-header bg-info text-white">
                                                <h5 class="modal-title" id="exampleModalLabel">بيانات عملية تحصيل بكود : {{$collection->id}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <!-- Grow In Utility -->
                                                <div class="col-lg-12 text-left">
                                                    <table class="table table-hover ">
                                                        <tr><th>الكود</th><td>{{$collection->id}}</td></tr>
                                                        <tr><th>الحالة</th><td>{{($collection->failed == 1)?'فاشلة في تاريخ :'.$collection->failed_at.' والسبب: '. $collection->failed_reason:''}}
                                                                {{($collection->collected == 1)?'تمت في تاريخ :'.$collection->collected_at.' (  '. $collection->collected_notes . ' )':''}}
                                                                {{($collection->collected == 0 && $collection->failed == 0 && $collection->collectassignmentable->done == 1)?'لقد تم التحصيل بعملية مخصصة أخرى':''}}
                                                                {{($collection->collected == 0 && $collection->failed == 0 && $collection->collectassignmentable->done != 1)?'قيد التحصيل':''}}
                                                            </td></tr>

                                                        <tr><th>تاريخ التخصيص</th><td>{{
                                             date('d', strtotime($collection->created_at)) .
                                            ','. config('montds.montd.'.date('m', strtotime($collection->created_at))) .
                                            ','.date('Y', strtotime($collection->created_at))
                                             }}</td></tr>
                                                        <tr><th>ملاحظات من المدير</th><td>{{$collection->notes??'لا يوجد'}}</td></tr>
                                                        <tr><th>المشرف</th><td>{{$collection->supervisorable->name??'لا يوجد'}}</td></tr>
                                                        <tr><th>المحصل</th><td>{{$collection->employeeable->name??'لا يوجد'}}</td></tr>
                                                        <tr><th>تقييم المتبرع</th><td>@if($collection->rate()->count()){{$collection->rate->rate?$collection->rate->rate.'/10':'لم يتم وضع تقييم'}} @endif</td></tr>
                                                        <tr><th>ملاحظات على المتبرع</th><td>@if($collection->rate()->count()){{$collection->rate->comment??'لم يتم وضع تقييم'}}@endif</td></tr>
                                                        <tr><th>قيمة التبرع المالي</th><td>{{$collection->money_amount??'لا يوجد'}}</td></tr>
                                                        <tr><th>قيمة التبرع العيني</th><td>{{$collection->donation_amount??'لا يوجد'}}</td></tr>
                                                        <tr><th>رقم الإيصال</th><td>{{$collection->bill_no}}</td></tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        @if(auth()->user()->can('see_all_benefactor_failed_collections'))
        <div class="col-lg-12  mt-3">
            <div class="card position-relative">
                <div class="card-header bg-danger text-center text-white">
                    <h4 class="" > عمليات التحصيل الفاشلة</h4>
                </div>
                <div class="card-body justify-content-center text-center table-responsive">

                    <table class="table table-bordered " id="dataTableFour" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>الكود</th>
                            <th>التاريخ</th>
                            <th>المشرف</th>
                            <th>المحصل</th>
                            <th>عرض</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>الكود</th>
                            <th>التاريخ</th>
                            <th>المشرف</th>
                            <th>المحصل</th>
                            <th>عرض</th>
                        </tr>
                        </tfoot>

                        @foreach($benefactor->failedCollections as $key=>$collection)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$collection->id}}</td>
                                <td>{{
                                         date('d', strtotime($collection->created_at)) .
                                        ','. config('months.month.'.date('m', strtotime($collection->created_at))) .
                                        ','.date('Y', strtotime($collection->created_at))
                                    }}
                                </td>
                                <td>{{$collection->supervisorable->name??'لا يوجد'}}</td>
                                <td>{{$collection->employeeable->name??'لا يوجد'}}</td>
                                <td class="text-center " > <a class="text-primary h3 " data-toggle="modal" data-target="#failed_collection_view_{{$key}}"> <i class="fas fa-eye"></i> </a> </td>
                            </tr>
                        @endforeach
                    </table>
                    @foreach($benefactor->failedCollections as $key=>$collection)
                        <div class="modal fade bd-example-modal-lg" id="failed_collection_view_{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
                            <div class="modal-dialog modal-lg" role="document" >
                                <div class="modal-content">
                                    <div class="modal-header bg-danger text-white">
                                        <h5 class="modal-title" id="exampleModalLabel">بيانات عملية تحصيل بكود : {{$collection->id}}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <!-- Grow In Utility -->
                                        <div class="col-lg-12 text-left">
                                            <table class="table table-hover ">
                                                <tr><th>الكود</th><td>{{$collection->id}}</td></tr>


                                                <tr><th>تاريخ التخصيص</th><td>{{
                                         date('d', strtotime($collection->created_at)) .
                                        ','. config('montds.montd.'.date('m', strtotime($collection->created_at))) .
                                        ','.date('Y', strtotime($collection->created_at))
                                         }}</td></tr>
                                                <tr><th>ملاحظات من المدير</th><td>{{$collection->notes??'لا يوجد'}}</td></tr>
                                                <tr><th>المشرف</th><td>{{$collection->supervisorable->name??'لا يوجد'}}</td></tr>
                                                <tr><th>المحصل</th><td>{{$collection->employeeable->name??'لا يوجد'}}</td></tr>
                                                <tr><th>تقييم المتبرع</th><td>@if($collection->rate()->count()){{$collection->rate->rate?$collection->rate->rate.'/10':'لم يتم وضع تقييم'}} @endif</td></tr>
                                                <tr><th>ملاحظات على المتبرع</th><td>@if($collection->rate()->count()){{$collection->rate->comment??'لم يتم وضع تقييم'}}@endif</td></tr>
                                                <tr>
                                                    <th> سبب فشل العملية</th><td>{{($collection->failed == 1)?'فاشلة في تاريخ :'.$collection->failed_at.' والسبب: '. $collection->failed_reason:''}}
                                                        {{($collection->collected == 1)?'تمت في تاريخ :'.$collection->collected_at.' (  '. $collection->collected_notes . ' )':''}}
                                                        {{($collection->collected == 0 && $collection->failed == 0)?'قيد التحصيل':''}}
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
            @endif
    </div>
</div>

<!--Edit Modal -->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">عملية تعديل البيانات</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
</div>


@stop
@section('js')
<!-- Page level plugins -->
<script src="{{asset('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

<!-- Page level custom scripts -->
<script src="{{asset('assets/js/demo/datatables-demo.js')}}"></script>

<script !src="">
$(document).ready(function() {
    $('#dataTableOne').DataTable({
        responsive: true,
        keys: true,
    } );
    $('#dataTableTwo').DataTable({
        responsive: true,
        keys: true,
    } );
    $('#dataTableThree').DataTable({
        responsive: true,
        keys: true,
    } );
    $('#dataTableFour').DataTable({
        responsive: true,
        keys: true,
    } );
});
</script>
@stop

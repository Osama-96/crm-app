@extends('pages.layouts.app')

@section('content')


@if(url()->current() == route('allActual'))
    <!-- Begin All Actual Benefactors Content -->
    <div class="container-fluid  ">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-white">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
                <li class="breadcrumb-item active" aria-current="page">جميع المتبرعين الفعليين</li>
            </ol>
        </nav>
        <!-- Content Row -->
        @component('components.table',['title'=>'جميع المتبرعين الفعليين ','color'=>'primary'])
                <thead>
                    <tr>
                        <th>#</th>
                        <th>الاسم</th>
                        <th>نوع المتبرع</th>
                        <th>نوع التبرع</th>
                        <th>حالة المتبرع</th>
                        <th>تاريخ الاستحقاق</th>
                        <th  class="text-center" >عرض</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>الاسم</th>
                        <th>نوع المتبرع</th>
                        <th>نوع التبرع</th>
                        <th>حالة المتبرع</th>
                        <th>تاريخ الاستحقاق</th>
                        <th  class="text-center" >عرض</th>
                    </tr>
                </tfoot>
                <tbody>
                @foreach($data as $key=>$item)
                    <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{getBenefactorType($item)}}</td>
                    <td>{{getDonationType($item)}}</td>
                    <td>{{$item->getBenefactorStatus()}}</td>
                    <td>{{getDateTime($item->dueDate)}}</td>
                    <td class="text-center" > <a href="{{route('actualBenefactors.show',$item)}}" class="btn btn-warning"> <i class="fas fa-eye"></i> </a></td>

                    </tr>
                @endforeach
                </tbody>
            @endcomponent
      </div>
    <!-- /.container-fluid -->
@elseif(url()->current() == route('withCollectingManager'))
    <!-- Begin withCollectingManager Actual Benefactors Content -->
    <div class="container-fluid  ">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-white">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
                <li class="breadcrumb-item active" aria-current="page">جميع المتبرعين الجدد</li>
            </ol>
        </nav>
        <!-- Content Row -->
        @component('components.table',['title'=>'جميع المتبرعين الجدد ','color'=>'primary'])
            <thead>
            <tr>
                <th>#</th>
                <th>الاسم</th>
                <th>نوع المتبرع</th>
                <th>نوع التبرع</th>
                <th>تاريخ الاستحقاق</th>
                <th  class="text-center" >عرض</th>
                <th  class="text-center" >إرسال</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>#</th>
                <th>الاسم</th>
                <th>نوع المتبرع</th>
                <th>نوع التبرع</th>
                <th>تاريخ الاستحقاق</th>
                <th  class="text-center" >عرض</th>
                <th  class="text-center" >إرسال</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach($data as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{getBenefactorType($item)}}</td>
                    <td>{{getDonationType($item)}}</td>
                    <td>{{getDateTime($item->dueDate)}}</td>
                    <td class="text-center" ><a class="text-white h3 btn btn-warning "  data-toggle="modal" data-target="#show{{$key}}"><i class="fas fa-eye"></i></a></td>
                    <td class="text-center" ><a class="text-white h3 btn btn-primary "  data-toggle="modal" data-target="#edit{{$key}}"><i class="fas fa-link"></i></a></td>
                </tr>
            @endforeach
            </tbody>
        @endcomponent
        @foreach($data as $key=>$item)
            @component('components.modal_xl',['title'=>'بيانات المتبرع/ '.$item->name ,'color'=>'warning','id'=>'show'.$key])
                <div class=" row justify-content-center" >
                    <div class="col-md-12 table-responsive py-3">
                        <table class="table table-bordered  ">
                            <tbody>
                            <tr>
                                <th> الاسم</th> <td> {{$item->name??""}}</td>
                            </tr>

                            <tr>
                                <th> الهاتف</th> <td> {{$item->phone??""}}</td>
                            </tr>
                            <tr>
                                <th>الايميل</th> <td> {{$item->email??""}}</td>
                            </tr>
                            <tr>
                                <th>الايميل</th> <td> {{$item->address??''}}</td>
                            </tr>
                            <tr>
                                <th>الوسيلة</th> <td> {{$item->device??''}}</td>
                            </tr>

                            <tr>
                                <th>الاعلان </th> <td>{{($item->adable)? 'رقم'.$item->adable->id:'لا يوجد' }} </td>
                            </tr>
                            <tr>
                                <th>نوع المتبرع </th> <td>{{getBenefactorType($item)}} </td>
                            </tr>

                            <tr>
                                <th>نوع التبرع </th> <td>{{ getDonationType($item) }} </td>
                            </tr>

                            <tr>
                                <th>التبرع  </th> <td>{{ getDonation($item) }} </td>
                            </tr>

                            <tr>
                                <th>تاريخ أول تحصيل </th> <td>{{getDateTime($item->dueDate)??'' }} </td>
                            </tr>


                            <tr>
                                <th>أسماء الحالات</th> <td>{{ $item->cases??'' }} </td>
                            </tr>

                            </tbody>
                        </table>

                    </div>

                </div>

            @endcomponent
            @component('components.modal',['title'=>' إرسال المتبرع '.$item->name .' لأحد المشرفين ','color'=>'primary','id'=>'edit'.$key])
                <form action="{{route('toSupervisor',$item->id)}}" method="POST">
                    @csrf
                    <div class="row justify-content-center">
                        <div class="form-group col-md-10">
                            <label class="text-primary"> اختر اسم الموظف  </label>
                            <select name="emp_id"  required class="form-control @error('emp_id') is-invalid @enderror">
                                @foreach(App\User::whereRoleIS('account_supervisor')->orWhereRoleIS('account_manager')->get() as $emp)
                                    <option value="{{$emp->id}}" {{(old('emp_id') == $emp->id)?'selected':''}}>  {{$emp->name}}</option>
                                @endforeach
                            </select>
                            @error('emp_id')
                            <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                            @enderror
                        </div>



                    </div>
                    <div class="row justify-content-center my-3">
                        <div class="form-group col-md-4 ">
                            <input type="submit" class="btn btn-primary form-control" value="تخصيص">
                        </div>
                    </div>
                </form>
            @endcomponent
        @endforeach
    </div>
@elseif(url()->current() ==  route('ofCollected'))
    <div class="container-fluid  ">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-white">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
                <li class="breadcrumb-item active" aria-current="page">متابعة تحصيلات آخر خمسة أيام</li>
            </ol>
        </nav>
        <!-- Content Row -->
        @component('components.table',['title'=>'متابعة تحصيلات آخر خمسة أيام','color'=>'primary'])
            <p class="alert alert-secondary text-center">يعرض هنا جميع المتبرعين الذين تم إرسالهم للتحصيل  في الخمسة أيام الماضية </p>

            <thead>
            <tr>
                <th>#</th>
                <th>الاسم</th>
                <th>نوع المتبرع</th>
                <th>نوع التبرع</th>
                <th>المحصل</th>
                <th>تاريخ الارسال</th>
                <th>الحالة </th>
                <th>النوع </th>
                <th  class="text-center" >عرض</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>#</th>
                <th>الاسم</th>
                <th>نوع المتبرع</th>
                <th>نوع التبرع</th>
                <th>المحصل</th>
                <th>تاريخ الارسال</th>
                <th>الحالة </th>
                <th>النوع </th>
                <th  class="text-center" >عرض</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach($data as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{getBenefactorType($item)}}</td>
                    <td>{{getDonationType($item)}}</td>
                    <td>{{$item->collectAssignments()->orderByDesc('created_at')->first()->accountantable->name}}</td>
                    <td>{{getDateTime($item->collectAssignments()->orderByDesc('created_at')->first()->created_at)}}</td>
                    <td>{{$item->collectAssignments()->orderByDesc('created_at')->first()->status}}</td>
                    <td>{{$item->collectAssignments()->orderByDesc('created_at')->first()->type}}</td>
                    <td class="text-center" ><a class="text-white h3 btn btn-warning "  href="{{route('actualBenefactors.show',$item)}}"><i class="fas fa-eye"></i></a></td>
                </tr>
            @endforeach
            </tbody>
        @endcomponent
    </div>

@elseif(url()->current() == route('newOfMeAsSupervisor')||   url()->current() ==route('ofCall'))
    <!-- Begin withCollectingManager Actual Benefactors Content -->
    <div class="container-fluid  ">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-white">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
                <li class="breadcrumb-item active" aria-current="page">جميع المتبرعين الجدد</li>
            </ol>
        </nav>
        <!-- Content Row -->
        @if(url()->current() ==  route('ofCall'))
        @component('components.table',['title'=>'جميع من يجب التواصل معه للتحصيل ','color'=>'primary'])
            <p class="alert alert-secondary text-center">يعرض هنا جميع المتبرعين الذين وقت التحصيل معهم خلال الخمسة أيام المقبلة </p>
        @else
        @component('components.table',['title'=>'جميع المتبرعين الجدد ','color'=>'primary'])
        @endif
            <thead>
            <tr>
                <th>#</th>
                <th>الاسم</th>
                <th>نوع المتبرع</th>
                <th>نوع التبرع</th>
                <th>الحالة</th>
                <th>تاريخ الاستحقاق</th>
                <th  class="text-center" >عرض</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>#</th>
                <th>الاسم</th>
                <th>نوع المتبرع</th>
                <th>نوع التبرع</th>
                <th>الحالة</th>
                <th>تاريخ الاستحقاق</th>
                <th  class="text-center" >عرض</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach($data as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{getBenefactorType($item)}}</td>
                    <td>{{getDonationType($item)}}</td>
                    <td>{{$item->getBenefactorStatus()}}</td>
                    <td>{{getDateTime($item->dueDate)}}</td>
                    <td class="text-center" ><a class="text-white h3 btn btn-warning "  href="{{route('actualBenefactors.show',$item)}}"><i class="fas fa-eye"></i></a></td>
                </tr>
            @endforeach
            </tbody>
        @endcomponent
    </div>
    <!-- /.container-fluid -->

@elseif(url()->current() == route('withCollectingSupervisor'))
    <!-- Begin withCollectingSupervisor Actual Benefactors Content -->
    <div class="container-fluid  ">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-white">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
                <li class="breadcrumb-item active" aria-current="page">قائمة المتبرعين النشطين </li>
            </ol>
        </nav>
        <!-- Content Row -->
        @component('components.table',['title'=>'قائمة المتبرعين النشطين ','color'=>'primary'])
            <thead>
            <tr>
                <th>#</th>
                <th>الاسم</th>
                <th>نوع المتبرع</th>
                <th>نوع التبرع</th>
                <th>تاريخ الاستحقاق</th>
                <th  class="text-center" >عرض</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>#</th>
                <th>الاسم</th>
                <th>نوع المتبرع</th>
                <th>نوع التبرع</th>
                <th>تاريخ الاستحقاق</th>
                <th  class="text-center" >عرض</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach($data as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{getBenefactorType($item)}}</td>
                    <td>{{getDonationType($item)}}</td>
                    <td>{{getDateTime($item->dueDate)}}</td>
                    <td class="text-center" ><a class="text-white h3 btn btn-warning "  href="{{route('actualBenefactors.show',$item)}}"><i class="fas fa-eye"></i></a></td>
                </tr>
            @endforeach
            </tbody>


        @endcomponent

    </div>
    <!-- /.container-fluid -->
@elseif(url()->current() == route('stopped') ||  url()->current() ==route('myStopped' ))
    <!-- Begin stopped Actual Benefactors Content -->
    <div class="container-fluid  ">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-white">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
                <li class="breadcrumb-item active" aria-current="page">المتبرعين المتوقفين</li>
            </ol>
        </nav>
        <!-- Content Row -->
        @if(route('myStopped') == url()->current())
        @component('components.table',['title'=>'جميع المتبرعين المتوقفين تماماً من قائمتي ','color'=>'primary'])
        @else
        @component('components.table',['title'=>'جميع المتبرعين المتوقفين تماماً ','color'=>'primary'])
        @endif
            <thead>
            <tr>
                <th>#</th>
                <th>الاسم</th>
                <th>نوع المتبرع</th>
                <th>نوع التبرع</th>
                <th>تاريخ التوقف</th>
                <th>سبب التوقف</th>
                <th  class="text-center" >عرض</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>#</th>
                <th>الاسم</th>
                <th>نوع المتبرع</th>
                <th>نوع التبرع</th>
                <th>تاريخ التوقف</th>
                <th>سبب التوقف</th>
                <th  class="text-center" >عرض</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach($data as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{getBenefactorType($item)}}</td>
                    <td>{{getDonationType($item)}}</td>
                    <td>{{getDateTime($item->stopped_at)}}</td>
                    <td>{{getDateTime($item->stopped_reason)}}</td>
                    <td class="text-center" ><a class="text-white h3 btn btn-warning "  href="{{route('actualBenefactors.show',$item)}}"><i class="fas fa-eye"></i></a></td>
                </tr>
            @endforeach
            </tbody>
        @endcomponent
    </div>
    <!-- /.container-fluid -->
@endif

@stop

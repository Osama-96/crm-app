@extends('pages.layouts.app')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
            <li class="breadcrumb-item active" aria-current="page">تسجيل متبرع جديد</li>
        </ol>
    </nav>
    <!-- Content Row -->
    <div class="row">
      <!-- Grow In Utility -->
      <div class="col-lg-12 ">
        @component('components.card',['color'=>'primary','title'=>'تسجيل متبرع جديد'])
            <form action="{{route('marketingBenefactors.store')}}" method="POST">
                @csrf
                <div class="row justify-content-center">
                    <div class="form-group col-md-6">
                        <label for="">الاسم </label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="" value="{{old('name')}}">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label for=""> الهاتف  </label>
                        <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" id="" value="{{old('phone')}}">
                        @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label for=""> الإيميل  </label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="" value="{{old('email')}}">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label for=""> الوسيلة التي جاء بها  </label>
                        <select name="device" id="" class="form-control @error('device') is-invalid @enderror">
                            <option disabled selected> اختر الوسيلة </option>
                            <option value="الفيسبوك" {{(old('device') == "الفيسبوك" )? 'selected' : '' }}> الفيسبوك </option>
                            <option value="تويتر" {{(old('device') == "تويتر")? 'selected' : '' }}>  تويتر</option>
                            <option value="انستجرام" {{(old('device') =="انستجرام" )? 'selected' : '' }}>  انستجرام</option>
                            <option value="واتساب" {{(old('device') =="واتساب")? 'selected' : '' }}>  واتساب</option>
                        </select>
                        @error('device')
                        <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <label for=""> الإعلان  </label>
                        <select name="ad" id="" class="form-control @error('ad') is-invalid @enderror">
                            <option selected disabled> اختر الاعلان </option>
                            @foreach($ads as $ad)
                            <option value="{{$ad->id}}" {{(old('ad') == $ad->id)? 'selected' : '' }}> رقم {{$ad->id}}</option>
                            @endforeach
                        </select>
                        @error('ad')
                        <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                        @enderror
                    </div>

                </div>
                <div class="row justify-content-center my-3">
                    <div class="form-group col-md-4 ">
                        <input type="submit" class="btn btn-primary form-control" value="حفظ البيانات">
                    </div>
                </div>



            </form>
          @endcomponent

      </div>

    </div>

  </div>
  <!-- /.container-fluid -->
  @stop

@extends('pages.layouts.app')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid  ">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-white">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
                <li class="breadcrumb-item active" aria-current="page">ملف المتبرع {{$benefactor->name}}</li>
            </ol>
        </nav>
        <!-- Content Row -->
        <div class="row">
            <!-- Grow In Utility -->
            <div class=" {{($benefactor->supervisorable->id && ($benefactor->supervisorable->id == auth()->user()->id))?'col-lg-8 ':'col-lg-12'}}  mb-4">
                @if($benefactor->stopped == 1)
                @component('components.card',['title'=>'ملف المتبرع: '.$benefactor->name ,'color'=>'danger'])
                    <p class="alert alert-danger text-center text-danger"  > هذا المتبرع متوقف عن التحصيل تماماً</p>
                @elseif($benefactor->delayed == 1)
                @component('components.card',['title'=>'ملف المتبرع: '.$benefactor->name ,'color'=>'warning'])
                    <p class="alert alert-warning text-center text-warning"  > هذا المتبرع  مؤجل حتى:  {{getDateTime($benefactor->delayed_to)??''}}ً</p>
                @else
                @component('components.card',['title'=>'ملف المتبرع: '.$benefactor->name ,'color'=>'primary'])
                @endif
                    <div class="table-responsive">
                        <table class="table table-bordered  ">
                            <tbody>
                            <tr>
                                <th> الاسم</th> <td> {{$benefactor->name??""}}</td>
                            </tr>

                            <tr>
                                <th> الهاتف</th> <td> {{$benefactor->phone??""}}</td>
                            </tr>
                            <tr>
                                <th>الايميل</th> <td> {{$benefactor->email??""}}</td>
                            </tr>
                            <tr>
                                <th>طريقة التحصيل</th> <td> {{$benefactor->collection_way??'لم يسجل'}}</td>
                            </tr>
                            <tr>
                                <th>العنوان</th> <td> {{$benefactor->address??''}}</td>
                            </tr>
                            <tr>
                                <th>الوسيلة</th> <td> {{$benefactor->device??''}}</td>
                            </tr>

                            <tr>
                                <th>الاعلان </th> <td>{{($benefactor->adable)? 'رقم'.$benefactor->adable->id:'لا يوجد' }} </td>
                            </tr>

                            <tr>
                                <th>نوع المتبرع </th> <td>{{getBenefactorType($benefactor)}} </td>
                            </tr>

                            <tr>
                                <th>نوع التبرع </th> <td>{{ getDonationType($benefactor) }} </td>
                            </tr>

                            <tr>
                                <th>التبرع  </th> <td>{{ getDonation($benefactor) }} </td>
                            </tr>

                            <tr>
                                <th>تاريخ التحصيل </th> <td>{{getDateTime($benefactor->dueDate)??'لم يسجل بعد' }}</td>
                            </tr>

                            <tr>
                                <th>أسماء الحالات</th> <td>{{ $benefactor->cases??'' }} </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                @endcomponent
            </div>
            @if(($benefactor->supervisorable->id && ($benefactor->supervisorable->id == auth()->user()->id)))
            <div class="col-md-4 px-3">
                @if($benefactor->stopped == 1)
                @component('components.card',['title'=>' ملخص العمليات','color'=>'danger'])
                    <p class="alert alert-danger text-center text-danger"  > هذا المتبرع متوقف عن التحصيل تماماً</p>
                @elseif($benefactor->delayed == 1)
                @component('components.card',['title'=>' ملخص العمليات','color'=>'warning'])
                    <p class="alert alert-warning text-center text-warning"  > هذا المتبرع  مؤجل  {{$benefactor->delayed_to??''}}ً</p>
                @else
                @component('components.card',['title'=>' ملخص العمليات','color'=>'primary'])
                @endif
                    <table class="table ">
                        <tr>
                            <td>حالة المتبرع:</td>  <td class="text-sm-center font-weight-light">{!! $benefactor->getBenefactorStatus()!!}</td>
                        </tr>
                        <tr>
                            <td> عمليات التحصيل: </td>  <td class="text-sm-center font-weight-light">{{$benefactor->collections()->count()}}</td>
                        </tr>
                        <tr>
                            <td> تاريخ آخر تأكيد للتحصيل: </td>  <td class="text-sm-center font-weight-light">{{$benefactor->confirmed_at?getDateTime($benefactor->confirmed_at):'لم يتم التأكيد'}}</td>
                        </tr>
                        <tr>
                            <td >ملاحظات المشرف</td> <td class="text-center "><a  class=" text-warning h2" data-toggle="modal" data-target="#notesS"> <i class="fas fa-info-circle"></i> </a></td>
                        </tr>
                    </table>
                @endcomponent
                @component('components.modal',['title'=>'ملاحظات المشرف المسؤول','color'=>'warning','id'=>'notesS'])
                    <div class="px-3  py2" style="line-height: 20px">
                    {!!   trim($benefactor->supervisorable_notes)??'لا يوجد' !!}
                    </div>
                    @if($benefactor->stopped == 1 )
                        <p class="alert alert-danger mt-2"> توقف بتاريخ: {{ getDateTime($benefactor->stopped_at)??' لم يسجل ' }} بسبب:   {{$benefactor->stopped_reason??'لم يسجل '}}</p>
                    @endif
                @endcomponent
                    <br>
                <div class="form-group">
                <a href="" class="btn btn-primary form-control"data-toggle="modal" data-target="#confirm-call-first">تسجيل مكالمة تأكيد التحصيل</a>
                    @component('components.modal_xl',['title'=>' تسجيل مكالمة تأكيد التحصيل' ,'color'=>'primary','id'=>'confirm-call-first'])
                        <form action="{{route('storeNewCollectCall',$benefactor)}}" method="POST">
                            @csrf
                            <div class="row">
                                <!-- Grow In Utility -->
                                <div class="col-lg-12 ">
                                    <div class="card position-relative ">
                                        <div class="card-body text-left">
                                            <h6 class="text-info"> توقيت المكالمة </h6>
                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                    <input class="form-control @error('date') is-invalid @enderror" name="date" type="date"  id="example-month-input"  value="{{old('date')??date('Y-m-d')}}"required >
                                                    @error('date')
                                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                                    @enderror
                                                </div>
                                                <div class="col-md-6">
                                                    <input class="form-control @error('time') is-invalid @enderror" name="time"  type="time" value="{{old('time')??date('H:i:s')}}" id="example-time-input" required>
                                                    @error('time')
                                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                                    @enderror
                                                </div>
                                            </div>


                                            <h6 class="text-info">  حالة الرد </h6>
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <select name="answer_status"  class="form-control @error('answer_status') is-invalid @enderror" required>
                                                        <option value=""  > اختر الحالة </option>
                                                        <option value="تم الرد" {{(old('answer_status') == "تم الرد")? 'selected': ''}}>   تم الرد </option>
                                                        <option value="مشغول" {{(old('answer_status') == "مشغول")? 'selected': ''}}> مشغول </option>
                                                        <option value="مغلق أو غير متاح" {{(old('answer_status') == "مغلق أو غير متاح")? 'selected': ''}}>مغلق أو غير متاح</option>
                                                        <option value="رد شخص آخر" {{(old('answer_status') == "رد شخص آخر")? 'selected': ''}}>رد شخص آخر</option>
                                                        <option value="رقم خطأ" {{(old('answer_status') == "رقم خطأ")? 'selected': ''}}>رقم خطا</option>
                                                    </select>
                                                    @error('answer_status')
                                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                                    @enderror
                                                </div>
                                                <div class="col-md-8">
                                                    <input class="form-control @error('status_more') is-invalid @enderror" name="status_more" value="{{old('phone')}}"  type="text" placeholder="معلومات أخرى عن الرد" >
                                                    @error('status_more')
                                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <h6 class="text-info"> التجاوب  </h6>
                                            <div class="form-group row px-5">

                                                <div class="custom-control custom-radio col-md-3">
                                                    <input type="radio" name="reacting" id="refuse1" {{(old('reacting') == 0)? 'checked': ''}} value="0" class="custom-control-input @error('reacting') is-invalid @enderror">
                                                    <label class="custom-control-label" for="refuse1" >غير متجاوب</label>
                                                </div>
                                                <div class="custom-control custom-radio col-md-3 px-5">
                                                    <input type="radio" name="reacting"id="accept1" {{(old('reacting') == 1)? 'checked': ''}}value="1" class="custom-control-input @error('reacting') is-invalid @enderror">
                                                    <label class="custom-control-label" for="accept1" >متجاوب</label>
                                                </div>

                                                @error('reacting')
                                                <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                                @enderror
                                            </div>

                                            <div class="form-group row " >
                                                <div class="col-md-3">
                                                    <label for="example-month-input" class="col-10 col-form-label"> تقييم المكالمة</label>

                                                    <select name="call_rate" class="form-control @error('call_rate') is-invalid @enderror accept" required autofocus >
                                                        <option disabled selected >اختر</option>
                                                        @for ($i = 0; $i < 11; $i++)
                                                            <option  value="{{$i}}" @if (old('call_rate') == $i) selected @endif >{{$i}}</option>
                                                        @endfor
                                                    </select>
                                                    @error('call_rate')
                                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                                    @enderror

                                                </div>
                                                <div class="col-md-9">
                                                    <label for="example-month-input" class="col-10 col-form-label"> أهم سؤال من المتبرع </label>

                                                    <input type="text" class="form-control @error('question') is-invalid @enderror" value="{{old('question')}}" name="question" >
                                                    @error('question')
                                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                                    @enderror

                                                </div>
                                            </div>
                                            <div class="form-group row " >
                                                <div class="col-md-12">
                                                    <label for="example-month-input" class="col-12 col-form-label"> ملخص المكالمة  </label>

                                                    <textarea name="call_samary" required class="form-control  @error('call_samary') is-invalid @enderror">{{old('call_samary')}}</textarea>
                                                    @error('call_samary')
                                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                                    @enderror

                                                </div>
                                                <div class="col-md-12">
                                                    <label for="example-month-input" class="col-12 col-form-label">ملاحظات</label>
                                                    <textarea name="notes" class="form-control @error('notes') is-invalid @enderror">{{old('notes')}}</textarea>
                                                    @error('notes')
                                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                                    @enderror
                                                </div>

                                            </div>


                                            <div class="row justify-content-md-center">
                                                <div class="form-group col-md-4 ">
                                                    <input type="submit" class="btn btn-primary form-control" value="حفظ المكالمة  ">
                                                </div>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                            <small class="text-danger">{{ $message }}</small>
                                        </span>
                                                @enderror
                                            </div>


                                        </div>
                                    </div>

                                </div>

                            </div>
                        </form>
                    @endcomponent
                </div>
                <div class="form-group">
                <a href="" class="btn btn-info form-control" data-toggle="modal" data-target="#confirm">تأكيد التحصيل</a>
                    @component('components.modal',['title'=>' تأكيد التحصيل من المتبرع/ '.$benefactor->name ,'color'=>'info','id'=>'confirm'])
                        @if($benefactor->confirmed == 1)
                            <p class="alert alert-secondary"> أخر مرة تم تأكيد التحصيل فيها كانت بتاريخ : {{ getDateTime($benefactor->confirmed_at) }} </p>

                        @endif
                        <form action="{{route('toSupervisor.confirm',$benefactor)}}" method="POST">
                            @csrf

                            <div class="row justify-content-center text-left">

                                <div class="form-group col-md-10">
                                    <label for="">العنوان </label>
                                    <input type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{$benefactor->address}}">
                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <small class="text-danger">{{ $message }}</small>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-10 ">
                                    <label for=""> وقت التحصيل</label>
                                    <input class="form-control d-inline" name="time"  type="time" id="example-time-input" value="{{($benefactor->dueDate)?explode(' ',$benefactor->dueDate)[1]:''}}"/>
                                    @error('time')
                                    <span class="invalid-feedback" role="alert">
                                            <small class="text-danger">{{ $message }}</small>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-10">
                                    <label for="">ملاحظات</label>
                                    <textarea  name="supervisorable_notes"  class="form-control"> {{old('supervisorable_notes')}}</textarea>
                                    @error('supervisorable_notes')
                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                    @enderror
                                </div>

                            </div>
                            <div class="row justify-content-center my-3">
                                <div class="form-group col-md-4 ">
                                    <input type="submit" class="btn btn-info form-control" value="تحديث البيانات">
                                </div>
                            </div>
                        </form>
                    @endcomponent
                </div>
                <div class="form-group">
                <a href="" class="btn btn-warning form-control" data-toggle="modal" data-target="#delay">تأجيل التبرع </a>
                    @component('components.modal',['title'=>' تأجيل التحصيل من المتبرع/ '.$benefactor->name ,'color'=>'warning','id'=>'delay'])
                        @if($benefactor->delayed ==1)
                            <p class="alert alert-secondary"> تم تأجيل التحصيل في تاريخ: {{ getDateTime($benefactor->delayed_at)  }} وذلك لموعد جديد وهو :{{ getDateTime($benefactor->delayed_at)  }} </p>
                        @endif
                        <form action="{{route('toSupervisor.delay',$benefactor)}}" method="POST">
                            @csrf
                            <div class="row justify-content-center text-left">
                                <div class="form-group col-md-10">
                                    <label for="">العنوان </label>
                                    <input type="text" class="form-control @error('address') is-invalid @enderror" name="address" id="" value="{{$benefactor->address}}">
                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                            <small class="text-danger">{{ $message }}</small>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-10">
                                    <label for="">التاريخ المؤجل إليه</label>
                                    <input type="date" name="date"  class="form-control " value="">

                                    @error('date')
                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-10 ">
                                    <label for=""> وقت التحصيل</label>
                                    <input class="form-control d-inline" name="time"  type="time" id="example-time-input" value=""/>
                                    @error('time')
                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-10">
                                    <label for="">ملاحظات</label>
                                    <textarea  name="supervisorable_notes"  class="form-control"> {{old('supervisorable_notes')}}</textarea>
                                    @error('supervisorable_notes')
                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                    @enderror
                                </div>

                            </div>
                            <div class="row justify-content-center my-3">
                                <div class="form-group col-md-4 ">
                                    <input type="submit" class="btn btn-warning form-control" value="تحديث البيانات">
                                </div>
                            </div>
                        </form>
                    @endcomponent
                </div>

                <div class="form-group">
                    <a href="" class="btn btn-danger form-control" data-toggle="modal" data-target="#stop">توقف عن التحصيل</a>
                    @component('components.modal',['title'=>' نقل المتبرع إلى قائمة المتوقفين عن التحصيل' ,'color'=>'danger','id'=>'stop'])
                        <form action="{{route('toSupervisor.stop',$benefactor)}}" method="POST">
                            @csrf

                            <div class="row justify-content-center text-left">

                                <div class="form-group col-md-10">
                                    <label for="reason">سبب التوقف </label>
                                    <textarea name="stopped_reason" id="reason" class="form-control" rows="5" required></textarea>
                                    @error('reason')
                                    <span class="invalid-feedback" role="alert">
                                            <small class="text-danger">{{ $message }}</small>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row justify-content-center my-3">
                                <div class="form-group col-md-4 ">
                                    <input type="submit" class="btn btn-danger form-control" value="تأكيد ">
                                </div>
                            </div>
                        </form>
                    @endcomponent
                </div>

                <div class="form-group">
                    <a href="" class="btn btn-success form-control"data-toggle="modal" data-target="#confirm-call-normal">إرسال المتبرع إلى أحد المحصلين</a>
                    @component('components.modal',['title'=>'إرسال المتبرع إلى أحد المحصلين' ,'color'=>'success','id'=>'confirm-call-normal'])
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" style="width: 50%" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">تحصيل اعتيادي </a>
                                <a class="nav-item nav-link" id="nav-profile-tab" style="width: 50%" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">تحصيل غير اعتيادي</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <div class="card mb-3">
                                    <div class="card-body">
                                        <b>معنى هذا الاختيار:</b>
                                        <ul>
                                            <li>أن المحصل سيقوم بالذهاب للمتبرع في الموعد المحفوظ على النظام</li>
                                            <li>أن المحصل سيقوم بتحصيل المبلغ أو التبرع العيني المحفوظ لهذا المتبرع في النظام</li>
                                            <li>ملحوظة: لا يمكن أن يتم إرسال المتبرع في هذه الحالة إلا قبل موعد التحصيل بخمسة أيام على الأقل</li>

                                        </ul>
                                    </div>
                                </div>
                                @if($benefactor->dueDate <= now()->addDays(5))
                                    @if($benefactor->collectAssignments()->count() && $benefactor->collectAssignments()->orderByDesc('id')->first()->created_at->month  ==  \carbon\Carbon::now()->month)
                                        <p class="alert alert-danger text-center">لقد قمت بإرسال المتبرع لأحد المحصلين مرة في هذا الشهر يمكنك تغيير المحصل في حال كان تاريخ موعد التحصيل لم يأت بعد</p>
                                    @else
                                    <form action="{{route('sendToAccountantAsRegular',$benefactor)}}" method="POST">
                                        @csrf
                                        <div class="row justify-content-center">
                                            <!-- Grow In Utility -->
                                            <div class="col-lg-12 mb-4">
                                                <div class="card position-relative ">
                                                    <div class="card-body text-left">
                                                        <div class="form-group ">
                                                            <h6 class="text-info">  اختر المحصل</h6>
                                                            <select name="emp_id" class="form-control" required>
                                                                @foreach(collectCenter() as  $key=>$emp)
                                                                    <option value="{{$emp->id}}">{{$emp->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('emp_id')
                                                            <span class="invalid-feedback" role="alert">
                                                                <small class="text-danger">{{ $message }}</small>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                        <div class="form-group ">
                                                            <h6 class="text-info"> ملاحظات</h6>
                                                            <textarea name="notes" class="form-control" rows="5"></textarea>

                                                            @error('notes')
                                                            <span class="invalid-feedback" role="alert">
                                                                <small class="text-danger">{{ $message }}</small>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="card-footer justify-content-center bg-white">
                                                        <div class="form-group ">
                                                            <input type="submit" class="btn btn-success form-control" value="إرسال المتبرع">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                    @endif

                                @else
                                    <div class="alert alert-danger text-center">لا يمكنك إرسال هذا المتبرع إلى المحصل قبل خمسة أيام من تاريخ الاستحقاق</div>
                                @endif
                            </div>
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <div class="card mb-3">
                                    <div class="card-body">
                                        <b>معنى هذا الاختيار:</b>
                                        <ul>
                                            <li>أن المحصل سيقوم بالذهاب للمتبرع في موعد غير المسجل في النظام</li>
                                            <li>أن المحصل سيقوم بتحصيل مبلغ أو تبرع آخر</li>
                                            <li>يتم اختيار هذا الاختيار في حالة طلب المتبرع أن يحصل منه للمرة الثانية في نفس الشهر</li>
                                        </ul>
                                    </div>
                                </div>
                                <form action="{{route('sendToAccountantAsIrregular',$benefactor)}}" method="POST">
                                    @csrf
                                    <div class="row justify-content-center">
                                        <!-- Grow In Utility -->
                                        <div class="col-lg-12 mb-4">
                                            <div class="card position-relative ">
                                                <div class="card-body text-left row justify-content-center">
                                                    <div class="form-group  col-md-6">
                                                        <h6 class="text-info">  اختر المحصل</h6>
                                                        <select name="emp_id" class="form-control" required>
                                                            @foreach(collectCenter() as  $key=>$emp)
                                                                <option value="{{$emp->id}}">{{$emp->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('emp_id')
                                                        <span class="invalid-feedback" role="alert">
                                                            <small class="text-danger">{{ $message }}</small>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label class="text-info">قيمة التبرع</label>
                                                        <input type="text" name="donation" required class="form-control" value="@if($benefactor->isAyniBene()) {{$benefactor->donation}} @elseif($benefactor->isMoneyBene())  {{$benefactor->moneyAmount}} @endif">
                                                        @error('donation')
                                                        <span class="invalid-feedback" role="alert">
                                                            <small class="text-danger">{{ $message }}</small>
                                                        </span>
                                                        @enderror
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label class="text-info"> تاريخ التحصيل</label>
                                                        <input type="date" name="date"  class="form-control " value="" required>
                                                        @error('date')
                                                        <span class="invalid-feedback" role="alert">
                                                            <small class="text-danger">{{ $message }}</small>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group col-md-6 ">
                                                        <label class="text-info"> وقت التحصيل</label>
                                                        <input class="form-control d-inline" name="time" required  type="time" id="example-time-input" value=""/>
                                                        @error('time')
                                                        <span class="invalid-feedback" role="alert">
                                                            <small class="text-danger">{{ $message }}</small>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group col-md-12 ">
                                                        <h6 class="text-info"> ملاحظات</h6>
                                                        <textarea name="notes" class="form-control" rows="5"></textarea>

                                                        @error('notes')
                                                        <span class="invalid-feedback" role="alert">
                                                            <small class="text-danger">{{ $message }}</small>
                                                        </span>
                                                        @enderror
                                                    </div>

                                                    <div class="form-group col-md-4 ">
                                                        <input type="submit" class="btn btn-success form-control" value="إرسال المتبرع">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    @endcomponent
                </div>

                <div class="form-group">
                    <a href="" class="btn btn-secondary form-control"data-toggle="modal" data-target="#collect_operation">تسجيل عملية تحصيل</a>
                    @component('components.modal_xl',['title'=>'تسجيل عملية تحصيل' ,'color'=>'secondary','id'=>'collect_operation'])
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" style="width: 50%" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">تحصيل من خلال مقابلة المتبرع </a>
                                <a class="nav-item nav-link" id="nav-profile-tab" style="width: 50%" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">تحصيل بوسيلة أخرى</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

                                @if($benefactor->dueDate <= now()->addDays(5))
                                    @if($benefactor->collectAssignments()->count() && $benefactor->collectAssignments()->orderByDesc('id')->first()->created_at->month  ==  \carbon\Carbon::now()->month)
                                        <p class="alert alert-danger text-center">لقد قمت بإرسال المتبرع لأحد المحصلين مرة في هذا الشهر يمكنك تغيير المحصل في حال كان تاريخ موعد التحصيل لم يأت بعد</p>
                                    @else
                                    <form action="{{route('collection.store',[$benefactor,'emp'])}}" method="POST">
                                        @csrf
                                        <div class="row justify-content-center">
                                            <!-- Grow In Utility -->
                                            <div class="col-lg-12 mb-4">
                                                <div class="card position-relative ">
                                                    <div class="modal-body">
                                                        <div class="row text-left">
                                                            <!-- Grow In Utility -->
                                                            <div class="col-lg-12 ">
                                                                <div class="card position-relative ">
                                                                    <div class="card-body ">
                                                                        <h6 class="text-info"> توقيت العملية </h6>
                                                                        <div class="form-group row">
                                                                            <div class="col-md-6">
                                                                                <input class="form-control @error('date') is-invalid @enderror" name="date" type="date"  id="example-month-input"  value="{{old('date')??date('Y-m-d')}}"required >
                                                                                @error('date')
                                                                                <span class="invalid-feedback" role="alert">
                                                                                    <small class="text-danger">{{ $message }}</small>
                                                                                </span>
                                                                                @enderror
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <input class="form-control @error('time') is-invalid @enderror" name="time"  type="time" value="{{old('time')??date('H:i:s')}}" id="example-time-input" required>
                                                                                @error('time')
                                                                                <span class="invalid-feedback" role="alert">
                                                                                    <small class="text-danger">{{ $message }}</small>
                                                                                </span>
                                                                                @enderror
                                                                            </div>
                                                                        </div>

                                                                        <h6 class="text-info"> التقييم :  </h6>
                                                                        <div class="form-group row " >
                                                                            <div class="col-md-3">
                                                                                <label for="example-month-input" class="col-12 col-form-label"> تقييم المتبرع</label>

                                                                                <select name="benefactor_rate" class="form-control @error('benefactor_rate') is-invalid @enderror accept" required autofocus >
                                                                                    @for ($i = 1; $i < 11; $i++)
                                                                                        <option  value="{{$i}}" @if (old('benefactor_rate') == $i) selected @endif >{{$i}}</option>
                                                                                    @endfor
                                                                                </select>
                                                                                @error('benefactor_rate')
                                                                                <span class="invalid-feedback" role="alert">
                                                                                    <small class="text-danger">{{ $message }}</small>
                                                                                </span>
                                                                                @enderror

                                                                            </div>

                                                                            <div class="col-md-9">
                                                                                <label for="example-month-input" class="col-12 col-form-label"> تعلييق على المتبرع </label>
                                                                                <textarea type="text" class="form-control @error('comment') is-invalid @enderror"name="comment" >{{old('comment')}}</textarea>
                                                                                @error('comment')
                                                                                <span class="invalid-feedback" role="alert">
                                                                                    <small class="text-danger">{{ $message }}</small>
                                                                                </span>
                                                                                @enderror

                                                                            </div>

                                                                        </div>
                                                                        <h6 class="text-info"> الموقع:  </h6>
                                                                        <div class="form-group row">
                                                                            <div class="col-md-12">
                                                                                <label for="example-month-input" class="col-12 col-form-label"> الموقع الجغرافي كما على خريطة جوجل </label>

                                                                                <input type="text" class="form-control @error('location') is-invalid @enderror" value="{{old('location')}}" name="location" >
                                                                                @error('location')
                                                                                <span class="invalid-feedback" role="alert">
                                                                                    <small class="text-danger">{{ $message }}</small>
                                                                                </span>
                                                                                @enderror

                                                                            </div>
                                                                        </div>
                                                                        <h6 class="text-info"> التبرعات:  </h6>
                                                                        <div class="form-group row " >
                                                                            <div class="col-md-4">
                                                                                <label  class="col-12 col-form-label">أدخل قيمة التبرع المالي</label>
                                                                                <input type="number" name="money_amount" min="1"  class="form-control @error('money_amount') is-invalid @enderror" value="{{old('money_amount')}}">

                                                                                @error('money_amount')
                                                                                <span class="invalid-feedback" role="alert">
                                                                                    <small class="text-danger">{{ $message }}</small>
                                                                                </span>
                                                                                @enderror

                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <label  class="col-12 col-form-label">أدخل قيمة التبرع العيني</label>
                                                                                <input type="text" name="donation"  class="form-control @error('donation') is-invalid @enderror" value="{{old('donation')}}">

                                                                                @error('donation')
                                                                                <span class="invalid-feedback" role="alert">
                                                                                        <small class="text-danger">{{ $message }}</small>
                                                                                    </span>
                                                                                @enderror
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <label  class="col-12 col-form-label"> رقم الايصال</label>
                                                                                <input type="number" name="bill_no" min="1" class="form-control @error('bill_no') is-invalid @enderror" value="{{old('bill_no')}}">

                                                                                @error('bill_no')
                                                                                <span class="invalid-feedback" role="alert">
                                                                                    <small class="text-danger">{{ $message }}</small>
                                                                                </span>
                                                                                @enderror

                                                                            </div>

                                                                        </div>

                                                                        <div class="form-group row " >

                                                                            <div class="col-md-12">
                                                                                <label for="example-month-input" class="col-12 col-form-label">ملاحظات </label>
                                                                                <textarea name="notes" class="form-control @error('notes') is-invalid @enderror">{{old('notes')}}</textarea>
                                                                                @error('notes')
                                                                                <span class="invalid-feedback" role="alert">
                                                                                    <small class="text-danger">{{ $message }}</small>
                                                                                </span>
                                                                                @enderror
                                                                            </div>

                                                                        </div>

                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="card-footer justify-content-center bg-white">
                                                        <div class="form-group ">
                                                            <input type="submit" class="btn btn-secondary form-control" value="حفظ العملية">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                    @endif

                                @else
                                    <div class="alert alert-danger text-center">لا يمكنك إرسال هذا المتبرع إلى المحصل قبل خمسة أيام من تاريخ الاستحقاق</div>
                                @endif
                            </div>
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

                                <form action="{{route('sendToAccountantAsIrregular',$benefactor)}}" method="POST">
                                    @csrf
                                    <div class="row justify-content-center">
                                        <!-- Grow In Utility -->
                                        <div class="col-lg-12 mb-4">
                                            <div class="card position-relative ">
                                                <div class="card-body text-left row justify-content-center">
                                                    <div class="form-group  col-md-6">
                                                        <h6 class="text-info">  اختر المحصل</h6>
                                                        <select name="emp_id" class="form-control" required>
                                                            @foreach(collectCenter() as  $key=>$emp)
                                                                <option value="{{$emp->id}}">{{$emp->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('emp_id')
                                                        <span class="invalid-feedback" role="alert">
                                                            <small class="text-danger">{{ $message }}</small>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label class="text-info">قيمة التبرع</label>
                                                        <input type="text" name="donation" required class="form-control" value="@if($benefactor->isAyniBene()) {{$benefactor->donation}} @elseif($benefactor->isMoneyBene())  {{$benefactor->moneyAmount}} @endif">
                                                        @error('donation')
                                                        <span class="invalid-feedback" role="alert">
                                                            <small class="text-danger">{{ $message }}</small>
                                                        </span>
                                                        @enderror
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label class="text-info"> تاريخ التحصيل</label>
                                                        <input type="date" name="date"  class="form-control " value="" required>
                                                        @error('date')
                                                        <span class="invalid-feedback" role="alert">
                                                            <small class="text-danger">{{ $message }}</small>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group col-md-6 ">
                                                        <label class="text-info"> وقت التحصيل</label>
                                                        <input class="form-control d-inline" name="time" required  type="time" id="example-time-input" value=""/>
                                                        @error('time')
                                                        <span class="invalid-feedback" role="alert">
                                                            <small class="text-danger">{{ $message }}</small>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group col-md-12 ">
                                                        <h6 class="text-info"> ملاحظات</h6>
                                                        <textarea name="notes" class="form-control" rows="5"></textarea>

                                                        @error('notes')
                                                        <span class="invalid-feedback" role="alert">
                                                            <small class="text-danger">{{ $message }}</small>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group col-md-4 ">
                                                        <input type="submit" class="btn btn-success form-control" value="إرسال المتبرع">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    @endcomponent
                </div>

            </div>
            @endif
        </div>

        <div class="row">
            <div class="col-lg-12 justify-content-center">
                @component('components.card',['color'=>'info','title'=> 'أرشيف المتبرع'])
                    <ul class="nav nav-pills m-3 justify-content-center " id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="btn btn-secondary m-1  active" id="pills-call-calls-tab" data-toggle="pill" href="#pills-call-calls" role="tab" aria-controls="pills-call-calls" aria-selected="true">مكالمات الكول سنتر </a>
                        </li>
                        <li class="nav-item">
                            <a class="btn btn-secondary  m-1" id="pills-first-conf-tab" data-toggle="pill" href="#pills-first-conf" role="tab" aria-controls="pills-first-conf" aria-selected="false">مكالمات تأكيد التحصيل </a>
                        </li>

                        <li class="nav-item">
                            <a class="btn btn-secondary  m-1" id="pills-collected-tab" data-toggle="pill" href="#pills-collected" role="tab" aria-controls="pills-collected" aria-selected="false">عمليات التحصيل </a>
                        </li>
                    </ul>
                    <div class="tab-content " id="pills-tabContent">
                        <div class="card-body tab-pane fade show active" id="pills-call-calls" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div id="call-calls" class="row justify-content-center"  >
                                <div class="col-md-10 col-sm-12">
                                    @include('tmps.newBenefactorCallsChartTemp',['item'=>$benefactor->benefactorable,'color'=>'primary', 'titlePlus'=>' من الكول سنتر '])
                                </div>

                                <div class="col-md-10 col-sm-12">
                                    @component('components.card',['title'=>'مكالمات الكول سنتر لهذا المتبرع','color'=>'warning'])
                                        @component('components.justTable',['title'=>'المكالمات التي تمت له','color'=>'warning'])
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th  class="text-center">الكود</th>
                                                <th  class="text-center">التوقيت</th>
                                                <th  class="text-center">الموظف</th>
                                                <th  class="text-center">حالة الرد</th>
                                                <th  class="text-center"> التقييم</th>
                                                <th  class="text-center">عرض</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($benefactor->benefactorable->calls as  $key=>$call)
                                                <tr class="text-white  {{($call->reacting == 'غير متجاوب')?'bg-danger':'bg-success'}}">
                                                    <td  class="text-center">{{$key+1}}</td>
                                                    <td  class="text-center">c-{{$call->id}}</td>
                                                    <td  class="text-center">{{getDateTime($call->date.$call->time)}}</td>
                                                    <td  class="text-center">{{$call->userable->name}}</td>
                                                    <td  class="text-center">{{$call->answer_status}}</td>
                                                    <td  class="text-center">10/{{($call->call_rate)??0}}</td>
                                                    <td   class="text-center"> <a class="text-white btn btn-warning "  data-toggle="modal" data-target="#call_view_{{$key.$call->id}}"> <i class="fas fa-eye"></i> </a> </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        @endcomponent
                                    @endcomponent
                                </div>
                            </div>
                        </div>

                        <div class="card-body tab-pane fade" id="pills-first-conf" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div id="first-conf" class="row justify-content-center"  >
                                <div class="col-md-10 col-sm-12">
                                    @include('tmps.actualBenefactorCallsChartTemp',['item'=>$benefactor,'color'=>'primary', 'titlePlus'=>' لتأكيد التحصيل  '])
                                </div>

                                <div class="col-md-10 col-sm-12">
                                    @component('components.card',['title'=>'مكالمات تأكيد التحصيل ','color'=>'warning'])
                                        @component('components.justTable',['title'=>'مكالمات تأكيد التحصيل  ','color'=>'warning'])
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th  class="text-center">الكود</th>
                                                <th  class="text-center">التوقيت</th>
                                                <th  class="text-center">الموظف</th>
                                                <th  class="text-center">حالة الرد</th>
                                                <th  class="text-center"> التقييم</th>
                                                <th  class="text-center">عرض</th>
                                                @if($benefactor->supervisorable->id == auth()->user()->id  && $benefactor->stopped == 0)
                                                    <th  class="text-center">تعديل</th>
                                                @endif
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($benefactor->calls as  $key=>$call)
                                                <tr class="text-white  {{($call->reacting == 'غير متجاوب')?'bg-danger':'bg-success'}}">
                                                    <td  class="text-center">{{$key+1}}</td>
                                                    <td  class="text-center">c-{{$call->id}}</td>
                                                    <td  class="text-center">{{getDateTime($call->date.$call->time)}}</td>
                                                    <td  class="text-center">{{$call->userable->name}}</td>
                                                    <td  class="text-center">{{$call->answer_status}}</td>
                                                    <td  class="text-center">10/{{($call->call_rate)??0}}</td>
                                                    <td   class="text-center"> <a class="text-white btn btn-primary "  data-toggle="modal" data-target="#collect_call_view_{{$key.$call->id}}"> <i class="fas fa-eye"></i> </a> </td>
                                                    @if($benefactor->supervisorable->id == auth()->user()->id  && $benefactor->stopped == 0)
                                                    <td   class="text-center" data-toggle="tooltip" title="يجب ألا يكون قد مضى ساعة من حفظ هذه المكالمة حتى يمكنك التعديل عليها.." > <a class="text-white btn btn-warning "   data-toggle="modal" data-target="#call_edit_{{$key.$call->id}}"> <i class="fas fa-pencil-alt"></i> </a> </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        @endcomponent
                                    @endcomponent
                                </div>

                            </div>
                        </div>

                        <div class="card-body tab-pane fade" id="pills-collected" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div id="collected" class="row justify-content-center"  >
                                <div class="col-md-10 col-sm-12">
                                    @include('tmps.collectionsChartTemp',['item'=>$benefactor,'color'=>'primary', 'titlePlus'=>''])
                                </div>
                                <div class="col-md-10 col-sm-12">
                                    @component('components.card',['title'=>'عمليات تحصيل المحصلين ','color'=>'warning'])
                                        @component('components.justTable',['title'=>' عمليات تحصيل المحصلين','color'=>'warning'])
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th  class="text-center">الموظف</th>
                                                <th  class="text-center">تاريخ الإرسال</th>
                                                <th  class="text-center">تاريخ التحصيل</th>
                                                <th  class="text-center">النوع</th>
                                                <th  class="text-center">الحالة</th>
                                                <th  class="text-center">عرض</th>
                                                @if($benefactor->supervisorable->id == auth()->user()->id  && $benefactor->stopped == 0)
                                                <th  class="text-center">تعديل</th>
                                                @endif
                                            </tr>
                                            </thead>

                                            <tbody>

                                            @foreach($benefactor->collectAssignments()->orderByDesc('id')->get() as  $key=>$collectAssignment)
                                                <tr>

                                                    <td  class="text-center">{{$key+1}}</td>
                                                    <td  class="text-center">{{$collectAssignment->accountantable->name}}</td>
                                                    <td  class="text-center">{{getDateTime($collectAssignment->created_at)}}</td>
                                                    <td  class="text-center">{{getDateTime($collectAssignment->dueDate)}}</td>
                                                    <td  class="text-center">{{$collectAssignment->type}}</td>
                                                    <td  class="text-center">{{$collectAssignment->status}}</td>
                                                    <td   class="text-center"> <a class="text-white btn btn-warning "  data-toggle="modal" data-target="#collect_collection_view_{{$collectAssignment->id}}"> <i class="fas fa-eye"></i> </a> </td>
                                                    @if($benefactor->supervisorable->id == auth()->user()->id && $benefactor->stopped == 0)
                                                    <td   class="text-center"> <a class="text-white btn btn-primary "  data-toggle="modal" data-target="#collect_collection_edit_{{$collectAssignment->id}}"> <i class="fas fa-pencil-alt"></i> </a> </td>
                                                    @endif
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        @endcomponent
                                    @endcomponent
                                </div>
                            </div>
                        </div>
                    </div>
                @endcomponent
            </div>
        </div>
        @foreach($benefactor->benefactorable->calls as  $key=>$call)
            @component('components.modal_xl',['id'=>'call_view_'.$key.$call->id,'color'=>'primary','title'=>'تفاصيل المكالمة'])
                <div class="table-responsive">

                    <table class="table table-bordered">
                        <tr class="bg-secondary text-white">
                            <th>غرض المكالمة </th>
                            <td>تأكيد موعد التحصيل </td>
                        </tr>

                        <tr class="bg-secondary text-white">
                            <th>الموظف الذي قام بالتواصل</th>
                            <td>{{$call->userable->name}} </td>
                        </tr>
                    </table>

                    <table class="table table-bordered">

                        <tr>
                            <th>توقيت المكالمة</th>
                            <td> {{getDateTime($call->date.$call->time)}}</td>
                        </tr>

                        <tr>
                            <th>حالة الرد</th>
                            <td> {{$call->answer_status}} <small class="text-secondary"> {{$call->status_more}} </small></td>
                        </tr>

                        <tr>
                            <th>الجاوب</th>
                            <td>{{$call->reacting}}</td>
                        </tr>

                        <tr>
                            <th>تقييم المكالمة</th>
                            <td>{{$call->call_rate??'0'}}  / 10</td>
                        </tr>

                        <tr>
                            <th>أهم سؤال</th>
                            <td>{{$call->question??'لا يوجد'}}</td>
                        </tr>

                        <tr>
                            <th>ملخص المكالمة</th>
                            <td>{{$call->call_samary??'لا يوجد'}}</td>
                        </tr>

                        <tr>
                            <th>ملاحظات</th>
                            <td>{{$call->notes??'لا يوجد'}}</td>
                        </tr>

                    </table>
                </div>

            @endcomponent
        @endforeach
        @foreach($benefactor->calls as  $key=>$call)
            @component('components.modal_xl',['id'=>'collect_call_view_'.$key.$call->id,'color'=>'primary','title'=>'تفاصيل المكالمة'])
                <div class="table-responsive">

                    <table class="table table-bordered">
                        <tr class="bg-secondary text-white">
                            <th>غرض المكالمة </th>
                            <td>تأكيد التحصيل </td>
                        </tr>

                        <tr class="bg-secondary text-white">
                            <th>الموظف الذي قام بالتواصل</th>
                            <td>{{$call->userable->name}} </td>
                        </tr>
                    </table>

                    <table class="table table-bordered">

                        <tr>
                            <th>توقيت المكالمة</th>
                            <td> {{getDateTime($call->date.$call->time)}}</td>
                        </tr>

                        <tr>
                            <th>حالة الرد</th>
                            <td> {{$call->answer_status}} <small class="text-secondary"> {{$call->status_more}} </small></td>
                        </tr>

                        <tr>
                            <th>الجاوب</th>
                            <td>{{$call->reacting}}</td>
                        </tr>

                        <tr>
                            <th>تقييم المكالمة</th>
                            <td>{{$call->call_rate??'0'}}  / 10</td>
                        </tr>

                        <tr>
                            <th>أهم سؤال</th>
                            <td>{{$call->question??'لا يوجد'}}</td>
                        </tr>

                        <tr>
                            <th>ملخص المكالمة</th>
                            <td>{{$call->call_samary??'لا يوجد'}}</td>
                        </tr>

                        <tr>
                            <th>ملاحظات</th>
                            <td>{{$call->notes??'لا يوجد'}}</td>
                        </tr>

                    </table>
                </div>

            @endcomponent
            @component('components.modal_xl',['title'=>'تعديل بيانات مكالمة بكود: '.$call->id,'id'=>'call_edit_'.$key.$call->id,'color'=>'warning'])
                @if($call->userable->id == auth()->user()->id && $call->created_at->addHour() > now())
                    <form action="{{route('updateNewCollectCall',$call->id)}}" method="POST">
                @csrf
                <div class="row">
                        <!-- Grow In Utility -->
                        <div class="col-lg-12 ">
                            <div class="card position-relative ">
                                <div class="card-body ">
                                    <h6 class="text-info"> توقيت المكالمة </h6>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <input class="form-control @error('date') is-invalid @enderror" name="date" type="date"  id="example-month-input"  value="{{old('date')??$call->date}}"required >
                                            @error('date')
                                            <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-control @error('time') is-invalid @enderror" name="time"  type="time" value="{{old('time')??$call->time}}" id="example-time-input" required>
                                            @error('time')
                                            <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>


                                    <h6 class="text-info">  حالة الرد </h6>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <select name="answer_status"  class="form-control @error('answer_status') is-invalid @enderror" required>
                                                <option value="1" disabled selected> اختر الحالة </option>
                                                <option value="تم الرد" {{($call->answer_status == "تم الرد")? 'selected': ''}}>   تم الرد </option>
                                                <option value="مشغول" {{($call->answer_status == "مشغول")? 'selected': ''}}> مشغول </option>
                                                <option value="مغلق أو غير متاح" {{($call->answer_status == "مغلق أو غير متاح")? 'selected': ''}}>مغلق أو غير متاح</option>
                                                <option value="رد شخص آخر" {{($call->answer_status == "رد شخص آخر")? 'selected': ''}}>رد شخص آخر</option>
                                                <option value="رقم خطأ" {{($call->answer_status == "رقم خطأ")? 'selected': ''}}>رقم خطا</option>
                                            </select>
                                            @error('answer_status')
                                            <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-8">
                                            <input class="form-control @error('status_more') is-invalid @enderror" name="status_more" value="{{old('status_more')??$call->status_more }}"  type="text" placeholder="معلومات أخرى عن الرد" >
                                            @error('status_more')
                                            <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <h6 class="text-info"> التجاوب  </h6>
                                    <div class="form-group row px-5">
                                        <div class="custom-control custom-radio col-md-3">
                                            <input type="radio" name="reacting" id="refuse-{{$key}}" {{($call->reacting == 'غير متجاوب')? 'checked': ''}} value="0" class="custom-control-input @error('reacting') is-invalid @enderror">
                                            <label class="custom-control-label" for="refuse-{{$key}}" >غير متجاوب</label>
                                        </div>
                                        <div class="custom-control custom-radio col-md-3 px-5">
                                            <input type="radio" name="reacting"id="accept-{{$key}}" {{($call->reacting == 'متجاوب')? 'checked': ''}} value="1" class="custom-control-input @error('reacting') is-invalid @enderror">
                                            <label class="custom-control-label" for="accept-{{$key}}" >متجاوب</label>
                                        </div>


                                        @error('reacting')
                                        <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                                        @enderror
                                    </div>

                                    <div class="form-group row " >
                                        <div class="col-md-3">
                                            <label for="example-month-input" class="col-10 col-form-label"> تقييم المكالمة</label>

                                            <select name="call_rate" class="form-control @error('call_rate') is-invalid @enderror accept" required autofocus >
                                                <option disabled selected >اختر</option>
                                                @for ($i = 0; $i < 11; $i++)
                                                    <option  value="{{$i}}" @if ($call->call_rate == $i) selected @endif >{{$i}}</option>
                                                @endfor
                                            </select>
                                            @error('call_rate')
                                            <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                                            @enderror

                                        </div>
                                        <div class="col-md-9">
                                            <label for="example-month-input" class="col-10 col-form-label"> أهم سؤال من المتبرع </label>

                                            <input type="text" class="form-control @error('question') is-invalid @enderror" value="{{old('question')??$call->question}}" name="question" >
                                            @error('question')
                                            <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                                            @enderror

                                        </div>
                                    </div>
                                    <div class="form-group row " >
                                        <div class="col-md-12">
                                            <label for="example-month-input" class="col-12 col-form-label"> ملخص المكالمة  </label>

                                            <textarea name="call_samary" class="form-control  @error('call_samary') is-invalid @enderror">{{old('call_samary')??$call->call_samary}}</textarea>
                                            @error('call_samary')
                                            <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                                            @enderror

                                        </div>
                                        <div class="col-md-12">
                                            <label for="example-month-input" class="col-12 col-form-label">ملاحظات</label>
                                            <textarea name="notes" class="form-control @error('notes') is-invalid @enderror">{{old('notes')??$call->notes}}</textarea>
                                            @error('notes')
                                            <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                                            @enderror
                                        </div>

                                    </div>


                                    <div class="row justify-content-md-center">
                                        <div class="form-group col-md-4 ">
                                            <input type="submit" class="btn btn-warning form-control" value="تحديث المكالمة  ">
                                        </div>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                        <small class="text-danger">{{ $message }}</small>
                    </span>
                                        @enderror
                                    </div>


                                </div>
                            </div>

                        </div>


                    </div>
            </form>
                @else
                    <h5>يشترط للتعديل على أي مكالمة تأكيد التحصيل ما يلي</h5>
                        <ol>
                            <li>أن تكون انت من قام بحفظ هذه المكالمة</li>
                            <li>ألا يكون قد مضى ساعة من حفظ هذه المكالمة</li>
                        </ol>
                @endif

            @endcomponent

        @endforeach
        @foreach($benefactor->collectAssignments()->orderByDesc('id')->get() as  $key=>$collectAssignment)
            @component('components.modal_xl',['title'=>'تغيير المحصل أو التعديل لعملية تحصيل ' ,'color'=>'primary','id'=>'collect_collection_edit_'.$collectAssignment->id])

                <form action="{{route('changeEmpInCollectAssignment',$collectAssignment)}}" method="POST">
                    @csrf
                    <div class="row justify-content-center">
                        <!-- Grow In Utility -->
                        <div class="col-lg-12 mb-4">
                            <div class="card position-relative ">
                                <div class="card-body text-left row justify-content-center">
                                    <div class="form-group  col-md-12">
                                        <h6 class="text-info">  اختر المحصل</h6>
                                        <select name="emp_id" class="form-control" required>
                                            @foreach(collectCenter() as  $key=>$emp)
                                                <option value="{{$emp->id}}" {{($collectAssignment->accountantable->id == $emp->id)?'selected':''}}>{{$emp->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('emp_id')
                                        <span class="invalid-feedback" role="alert">
                                            <small class="text-danger">{{ $message }}</small>
                                        </span>
                                        @enderror
                                    </div>


                                    <div class="form-group col-md-6">
                                        <label class="text-info"> تاريخ التحصيل</label>
                                        <input type="date" name="date"  class="form-control " value="{{explode(' ',$collectAssignment->dueDate)[0]??''}}" required>
                                        @error('date')
                                        <span class="invalid-feedback" role="alert">
                                            <small class="text-danger">{{ $message }}</small>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6 ">
                                        <label class="text-info"> وقت التحصيل</label>
                                        <input class="form-control d-inline" name="time" required  type="time" id="example-time-input" value="{{explode(' ',$collectAssignment->dueDate)[1]??''}}"/>
                                        @error('time')
                                        <span class="invalid-feedback" role="alert">
                                            <small class="text-danger">{{ $message }}</small>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="form-group col-md-12">
                                        <h6 class="text-info"> ملاحظات</h6>
                                        <textarea name="notes" class="form-control" rows="5">{{$collectAssignment->notes}}</textarea>
                                        @error('notes')
                                        <span class="invalid-feedback" role="alert">
                                            <small class="text-danger">{{ $message }}</small>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="form-group mt-2 col-md-4">
                                        <input type="submit" class="btn btn-secondary form-control" value="تغيير المحصل">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>

            @endcomponent
            @component('components.modal_xl',['title'=>'تفاصيل عملية تحصيل  ','color'=>'warning','id'=>'collect_collection_view_'.$collectAssignment->id])
                    <table class="table table-bordered">
                        <tr>
                            <th  class="text-center">تاريخ الارسال</th>
                            <th  class="text-center">الموظف</th>
                            <th  class="text-center">تاريخ التحصيل</th>
                            <th  class="text-center">النوع</th>
                            <th  class="text-center">الحالة</th>
                        </tr>
                        <tr>
                            <td  class="text-center">{{getDateTime($collectAssignment->created_at)}}</td>
                            <td  class="text-center">{{$collectAssignment->accountantable->name}}</td>
                            <td  class="text-center">{{getDateTime($collectAssignment->dueDate)}}</td>
                            <td  class="text-center">{{$collectAssignment->type}}</td>
                            <td  class="text-center">{{$collectAssignment->status}}</td>
                        </tr>
                        @if($collectAssignment->done == 1)
                            @if($collectAssignment->collection->collected == 1)
                            <tr>
                                <th>تاريخ التحصيل</th>
                                <th>رقم الوصل</th>
                                <th>التبرع</th>
                                <th> الموقع</th>
                                <th>ملاحظات</th>
                            </tr>
                            <tr>
                                <td>{{$collectAssignment->collection->collected_at}}</td>
                                <td>{{$collectAssignment->collection->bill_no}}</td>
                                <td>{{($collectAssignment->collection->money_amount && $collectAssignment->collection->donation_amount)?$collectAssignment->collection->money_amount.' & '. $collectAssignment->collection->donation_amount:$collectAssignment->collection->donation_amount.$collectAssignment->collection->money_amount}}</td>
                                <td> الموقع</td>
                                <td>{{$collectAssignment->collection->notes}}</td>
                            </tr>
                            @elseif($collectAssignment->collection->failed == 1)
                                <tr>
                                    <th>تاريخ الفشل</th>
                                    <th colspan="2">سبب الفشل</th>
                                    <th> الموقع</th>
                                    <th>ملاحظات</th>
                                </tr>
                                <tr>
                                    <td>{{getDateTime($collectAssignment->collection->failed_at )}}</td>
                                    <td colspan="2">{{$collectAssignment->collection->failed_reason }}</td>
                                    <td> الموقع</td>
                                    <td>{{$collectAssignment->collection->notes}}</td>
                                </tr>
                            @endif
                        @endif
                    </table>
            @endcomponent
        @endforeach

    </div>
    <!--Edit Modal -->


@stop

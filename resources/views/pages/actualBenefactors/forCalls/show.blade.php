@extends('pages.layouts.app')
@section('css')
    <link href="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@stop
@section('content')

    <!-- Begin Page Content -->
    <div class="container-fluid  ">
        <!-- Content Row -->
        <div class="row">
            <!-- Grow In Utility -->
            <div class="col-lg-12 ">
                <div class="card position-relative border-primary">
                    <div class="card-header py-3 bg-primary text-center">
                        <h3 class=" mb-1 text-white"> {{$benefactor->name}} </h3>
                    </div>
                    <div class="card-body ">
                        <div class=" row " >
                            <div class="col-md-6 table-responsive">
                                <table class="table table-striped table-inverse ">
                                    <tbody>
                                    <tr>
                                        <th> الاسم</th> <td> {{$benefactor->name}}</td>
                                    </tr>

                                    <tr>
                                        <th> الهاتف</th> <td> {{$benefactor->phone}}</td>
                                    </tr>
                                    <tr>
                                        <th>الايميل</th> <td> {{$benefactor->email}}</td>
                                    </tr>
                                    <tr>
                                        <th>الوسيلة</th> <td> {{$benefactor->how}}</td>
                                    </tr>

                                    <tr>
                                        <th>الاعلان </th> <td>{{($benefactor->ad != -1 )? 'رقم'.$benefactor->ad :'لم يتم تحديد إعلان' }} </td>
                                    </tr>


                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6 table-responsive">
                                <table class="table table-striped table-inverse">
                                    <tbody>
                                    <tr>
                                        <th> نوع التبرع</th> <td> @if($benefactor->isAyni  != null){{'تبرع عيني بــ'. $benefactor->donation}} @elseif($benefactor->isMony != null) {{'تبرع مالي بــ'. $benefactor->moneyAmount}}@else لم يحدد بعد  @endif</td>
                                    </tr>

                                    <tr>
                                        <th>  توقيت التحصيل</th> <td> {{$benefactor->dueDate?? 'لم يسجل بعد'}}</td>
                                    </tr>
                                    <tr>
                                        <th>كيفية التحصيل</th> <td>{{$benefactor->collect_way?? 'لم يسجل بعد'}} </td>
                                    </tr>
                                    <tr>
                                        <th>عدد مرات التحصيل</th> <td> </td>
                                    </tr>
                                    <tr>
                                        <th>العنوان</th> <td> {{$benefactor->address}}</td>
                                    </tr>


                                    </tbody>
                                </table>
                            </div>

                        </div>
                        @if($benefactor->isStopped == 1)
                            <p class="col-md-12 alert alert-danger rounded ">
                                هذا المتبرع توقفت كل نشاطاته بناءً على طلبه من الأستاذ /
                                <b>{{'   ( '.$benefactor->stopper->name.'   )   '}}</b>
                                وذلك في تاريخ:
                                <b>
                                    {{'  ( '. date('d', strtotime($benefactor->stopped_at)) .
                                           ','. config('months.month.'.date('m', strtotime($benefactor->stopped_at))) .
                                           ','.date('Y', strtotime($benefactor->stopped_at)).' ) ' }}
                                </b>
                                وهذه هي رسالة الطلب:
                                <b> {{' " '.$benefactor->stopped_reason.' " '}}</b>
                            </p>
                        @else
                            @if($assignment-> done != 1)
                            <div class="row justify-content-center">
                                <div class="col-md-2">
                                    <a  class="btn btn-primary m-1 px-3 py-2 text-white"  data-toggle="modal" data-target="#edit"> تحديث بيانات المتبرع</a>
                                </div>
                            </div>
                            @endif
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Edit Modal -->
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white text-center"  id="exampleModalLabel">عملية تعديل البيانات</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('assignment.Benefactors.store',['id'=>$benefactor->id,'assignment'=>$assignment])}}" method="POST">
                    @csrf

                    <div class="row justify-content-center">
                        <div class="form-group col-md-8">
                            <label for="">الاسم </label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="" value="{{$benefactor->name}}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-8">
                            <label for="">العنوان </label>
                            <input type="text" class="form-control @error('address') is-invalid @enderror" name="address" id="" value="{{$benefactor->address}}">
                            @error('address')
                            <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-8">
                            <label for=""> الهاتف  </label>
                            <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" id="" value="{{$benefactor->phone}}">
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-8">
                            <label for=""> الإيميل  </label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="" value="{{$benefactor->email}}">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                            @enderror
                        </div>
                        @if($benefactor->isNew == 1 ||$benefactor->isNew != 1  && auth()->user()->hasRole('call_manager'))
                            <div class="form-group col-md-8">
                                <label for="">نوع المتبرع   </label>
                                <select name="bene_type" id="" class="form-control @error('bene_type') is-invalid @enderror">
                                    <option disabled selected> اختر النوع </option>
                                    <option value="1" > متبرع عادي</option>
                                    <option value="2">كفالة</option>
                                </select>
                                @error('bene_type')
                                <span class="invalid-feedback" role="alert">
                                    <small class="text-danger">{{ $message }}</small>
                                </span>
                                @enderror
                            </div>
                        @endif
                        @if($benefactor->isNew == 1 ||$benefactor->isNew != 1  && auth()->user()->hasRole('call_manager'))
                            <div class="form-group col-md-8">
                                <label for="">نوع التبرع   </label>
                                <select name="donation_type" id="" class="form-control @error('donation_type') is-invalid @enderror">
                                    <option disabled selected> اختر النوع </option>
                                    <option value="1" > تبرع مالي</option>
                                    <option value="2" > تبرع عيني</option>
                                </select>
                                @error('donation_type')
                                <span class="invalid-feedback" role="alert">
                                    <small class="text-danger">{{ $message }}</small>
                                </span>
                                @enderror
                            </div>
                        @endif
                            <div class="form-group col-md-8">
                                <label for="">التبرع</label>
                                <input type="text" name="donation"  class="form-control">

                                <small class="text-danger">ملاحظة: إذا كان نوع التبرع الذي أدخلته (تبرع مالي) فلابد من إدخال قيمة مالية بأرقام صحيحة،إذا كان نوع التبرع الذي أدخلته (تبرع عيني) فلابد من إدخال قيمة هذا التبرع مكتوباً </small>
                                @error('donation')
                                <span class="invalid-feedback" role="alert">
                                        <small class="text-danger">{{ $message }}</small>
                                    </span>
                                @enderror
                            </div>
                        @if($benefactor->dueDate == null ||$benefactor->dueDate != null  && auth()->user()->hasRole('call_manager'))
                            <div class="form-group col-md-8">
                                <label for=""> تاريخ أول تحصيل</label>
                                <input type="date" name="date"  class="form-control">
                                <small class="text-danger">ملاحظة: في حالة كون المتبرع كفيلاً ؛ فإن التاريخ الذي ستحفظه هنا هو التاريخ الذي سيتم التحصيل فيه شهرياً...</small>
                                @error('date')
                                <span class="invalid-feedback" role="alert">
                                    <small class="text-danger">{{ $message }}</small>
                                </span>
                                @enderror
                            </div>
                        @endif


                    </div>
                    <div class="row justify-content-center my-3">
                        <div class="form-group col-md-4 ">
                            <input type="submit" class="btn btn-primary form-control" value="حفظ البيانات">
                        </div>
                    </div>



                </form>
            </div>
        </div>
    </div>


@stop

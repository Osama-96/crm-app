<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark toggled" id="accordionSidebar">
<?php $thisUser = auth()->user();?>
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('home')}}">
      <div class="sidebar-brand-icon ">
        <img src="{{asset('logo.png')}}" width="100%" alt="" style="width:50px;">

      </div>
      <div class="sidebar-brand-text mx-1"> مصر المحروسة بلدي</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item ">
      <a class="nav-link" href="{{route('dashboard')}}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>
          لوحة التحكم
        </span>
      </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
    @call_manager('')

    <div class="sidebar-heading">
        المتبرعون المحتملون
    </div>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('newBenefactors.allNew')}}">
            <i class="fas fa-fw fa-users"></i>
            <span>جميع المتبرعين</span>
        </a>
    </li>

    <li class="nav-item active">
        <a class="nav-link" href="{{route('newBenefactors.sent')}}">
            <i class="fas fa-fw fa-calendar"></i>
            <span>الجدد</span>
        </a>
    </li>

    <li class="nav-item active">
        <a class="nav-link" href="{{route('newBenefactors.assigned')}}">
            <i class="fas fa-fw fa-clock"></i>
            <span>جاري التواصل معهم</span>
        </a>
    </li>

    <li class="nav-item active">
        <a class="nav-link" href="{{route('newBenefactors.failed')}}">
            <i class="fas fa-fw fa-thumbs-down"></i>
            <span>فشل التواصل معهم</span>
        </a>
    </li>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('newBenefactors.moved')}}">
            <i class="fas fa-fw fa-thumbs-up"></i>
            <span>تم تفعيلهم</span>
        </a>
    </li>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('newBenefactors.notMoved')}}">
            <i class="fas fa-fw fa-fast-backward"></i>
            <span>لم يتم تعديل بياناتهم</span>
        </a>
    </li>
    <hr class="sidebar-divider">

    <div class="sidebar-heading">
         متابعة فريق القسم
    </div>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('callCenter.team')}}">
            <i class="fas fa-fw fa-users"></i>
            <span>  فريق الكول سنتر</span>
        </a>
    </li>
    <hr class="sidebar-divider">
    @endcall_manager

    @call_manager('call_emp')
    <div class="sidebar-heading">
         عمليات التواصل الخاصة بي
    </div>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('newAssignments.index')}}">
            <i class="fas fa-fw fa-clock"></i>
            <span> الحالية</span>
        </a>
    </li>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('oldAssignments.index')}}">
            <i class="fas fa-fw fa-calendar-check"></i>
            <span>التامة</span>
        </a>
    </li>
    <hr class="sidebar-divider">

    @endcall_manager
{{--    @component('components.sideItemList',['icon'=>'fas fa-users','header'=>'المتبرعون المحتملون','title'=>'المتبرعون المحتملون'])--}}
{{--        <a class="collapse-item {{(url()->current() == route('newBenefactors.allNew')?'active':'')}}" href="{{route('newBenefactors.allNew')}}">الكل</a>--}}
{{--        <a class="collapse-item {{(url()->current() == route('newBenefactors.new')?'active':'')}}" href="{{route('newBenefactors.new')}}">لم يتم إرسالهم</a>--}}
{{--        <a class="collapse-item {{(url()->current() == route('newBenefactors.sent')?'active':'')}}" href="{{route('newBenefactors.sent')}}">الجدد</a>--}}
{{--        <a class="collapse-item {{(url()->current() == route('newBenefactors.assigned')?'active':'')}}" href="{{route('newBenefactors.assigned')}}">جاري التواصل معهم</a>--}}
{{--        <a class="collapse-item {{(url()->current() == route('newBenefactors.failed')?'active':'')}}" href="{{route('newBenefactors.failed')}}">فشل التواصل معهم</a>--}}
{{--        <a class="collapse-item {{(url()->current() == route('newBenefactors.moved')?'active':'')}}" href="{{route('newBenefactors.moved')}}">تم تفعيلهم</a>--}}
{{--        <h6 class="collapse-header">العمليات المستلمة من رئيس القسم:</h6>--}}
{{--        <a class="collapse-item {{(url()->current() == route('newAssignments.index')?'active':'')}}" href="{{route('newAssignments.index')}}">الحالية</a>--}}
{{--        <a class="collapse-item {{(url()->current() == route('oldAssignments.index')?'active':'')}}" href="{{route('oldAssignments.index')}}">التامة</a>--}}
{{--        <h6 class="collapse-header"> متابعة الموظفين:</h6>--}}
{{--        <a class="collapse-item {{(url()->current() == route('callCenter.team')?'active':'')}}" href="{{route('callCenter.team')}}">فريق الكول سنتر</a>--}}

{{--@endcomponent--}}




    @marketing_officer()
        <!-- Heading -->

    <div class="sidebar-heading">
       الإعلانات
    </div>

    <li class="nav-item active">
        <a class="nav-link" href="{{route('ads.index')}}">
            <i class="fas fa-fw fa-address-card"></i>
            <span> جميع الاعلانات</span>
        </a>
    </li>

    <li class="nav-item active">
        <a class="nav-link" href="{{route('ads.create')}}">
            <i class="fas fa-fw fa-pencil-alt"></i>
            <span>إنشاء إعلان جديد</span>
        </a>
    </li>
    <hr class="sidebar-divider">
<div class="sidebar-heading">
المناطق
</div>
<li class="nav-item active">
<a class="nav-link" href="{{route('locations.index')}}">
    <i class="fas fa-fw fa-location-arrow"></i>
    <span>إنشاء منطقة جديدة</span>
</a>
</li>
<hr class="sidebar-divider">
<div class="sidebar-heading">
المتبرعين المحتملين
</div>
<li class="nav-item active">
<a class="nav-link" href="{{route('newBenefactors.new')}}">
<i class="fas fa-fw fa-users"></i>
<span>جميع المتبرعين</span>
</a>
</li>
<li class="nav-item active">
<a class="nav-link" href="{{route('newBenefactors.create')}}">
<i class="fas fa-fw fa-user"></i>
<span>تسجيل متبرع جديد</span>
</a>
</li>
<li class="nav-item active">
<a class="nav-link" href="{{route('excel.showMarketing')}}">
<i class="fas fa-fw fa-file-excel"></i>
<span> استيراد من ملف اكسيل </span>
</a>
</li>
<!-- Divider -->
<hr class="sidebar-divider">

@endmarketing_officer

{{--@component('components.sideItemList',['icon'=>'fas fa-users','header'=>'المتبرعون المحتملون','title'=>'المتبرعون المحتملون'])--}}
{{--        <a class="collapse-item {{(url()->current() == route('newBenefactors.allNew')?'active':'')}}" href="{{route('newBenefactors.allNew')}}">الكل</a>--}}
{{--        <a class="collapse-item {{(url()->current() == route('newBenefactors.new')?'active':'')}}" href="{{route('newBenefactors.new')}}">لم يتم إرسالهم</a>--}}
{{--        <a class="collapse-item {{(url()->current() == route('newBenefactors.sent')?'active':'')}}" href="{{route('newBenefactors.sent')}}">الجدد</a>--}}
{{--        <a class="collapse-item {{(url()->current() == route('newBenefactors.assigned')?'active':'')}}" href="{{route('newBenefactors.assigned')}}">جاري التواصل معهم</a>--}}
{{--        <a class="collapse-item {{(url()->current() == route('newBenefactors.failed')?'active':'')}}" href="{{route('newBenefactors.failed')}}">فشل التواصل معهم</a>--}}
{{--        <a class="collapse-item {{(url()->current() == route('newBenefactors.moved')?'active':'')}}" href="{{route('newBenefactors.moved')}}">تم تفعيلهم</a>--}}
{{--        <a class="collapse-item {{(url()->current() == route('newBenefactors.notMoved')?'active':'')}}" href="{{route('newBenefactors.notMoved')}}">لم يتم تعديل بياناتهم</a>--}}
{{--        <h6 class="collapse-header">العمليات المستلمة من رئيس القسم:</h6>--}}
{{--        <a class="collapse-item {{(url()->current() == route('newAssignments.index')?'active':'')}}" href="{{route('newAssignments.index')}}">الحالية</a>--}}
{{--        <a class="collapse-item {{(url()->current() == route('oldAssignments.index')?'active':'')}}" href="{{route('oldAssignments.index')}}">التامة</a>--}}
{{--        <h6 class="collapse-header"> متابعة الموظفين:</h6>--}}
{{--        <a class="collapse-item {{(url()->current() == route('callCenter.team')?'active':'')}}" href="{{route('callCenter.team')}}">فريق الكول سنتر</a>--}}

{{--    @endcomponent--}}

    @account_manager()
    <div class="sidebar-heading">
        المتبرعين
    </div>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('withCollectingManager')}}">
            <i class="fas fa-fw fa-book"></i>
            <span>المتبرعين الجدد </span>
        </a>
    </li>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('allActual')}}">
            <i class="fas fa-fw fa-bars"></i>
            <span>جميع المتبرعين</span>
        </a>
    </li>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('stopped')}}">
            <i class="fas fa-fw fa-ban"></i>
            <span>جميع المتوقفين</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        متابعة الموظفين
    </div>
    <li class="nav-item active">
        <a class="nav-link" href="">
            <i class="fas fa-fw fa-boxes"></i>
            <span> الفرق </span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        الاشراف الخاص بي
    </div>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('withCollectingSupervisor')}}">
            <i class="fas fa-fw fa-bezier-curve"></i>
            <span> قائمة المتبرعين النشطين </span>
        </a>
    </li>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('newOfMeAsSupervisor')}}">
            <i class="fas fa-fw fa-bookmark"></i>
            <span> قائمة المتبرعين الجدد </span>
        </a>
    </li>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('ofCall')}}">
            <i class="fas fa-fw fa-phone"></i>
            <span> قائمة التواصل </span>
        </a>
    </li>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('ofCollected')}}">
            <i class="fas fa-fw fa-calendar-check"></i>
            <span> متابعة آخر عمليات التحصيل  </span>
        </a>
    </li>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('myStopped')}}">
            <i class="fas fa-fw fa-file-excel"></i>
            <span> المتوقفون من قائمتي</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
       عمليات التحصيل
    </div>
    <li class="nav-item active">
        <a class="nav-link" href="">
            <i class="fas fa-fw fa-cart-arrow-down"></i>
            <span> عمليات التحصيل الناجحة</span>
        </a>
    </li>
    <li class="nav-item active">
        <a class="nav-link" href="">
            <i class="fas fa-fw fa-thumbs-down"></i>
            <span>  عمليات التحصيل الفاشلة</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
    @endaccount_manager
    @account_supervisor()
    <div class="sidebar-heading">
        الإشراف
    </div>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('withCollectingSupervisor')}}">
            <i class="fas fa-fw fa-bezier-curve"></i>
            <span> قائمة المتبرعين النشطين </span>
        </a>
    </li>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('newOfMeAsSupervisor')}}">
            <i class="fas fa-fw fa-bookmark"></i>
            <span> قائمة المتبرعين الجدد </span>
        </a>
    </li>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('ofCall')}}">
            <i class="fas fa-fw fa-phone"></i>
            <span> قائمة التواصل </span>
        </a>
    </li>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('ofCollected')}}">
            <i class="fas fa-fw fa-calendar-check"></i>
            <span> متابعة آخر عمليات التحصيل  </span>
        </a>
    </li>
    <li class="nav-item active">
        <a class="nav-link" href="{{route('myStopped')}}">
            <i class="fas fa-fw fa-file-excel"></i>
            <span> المتوقفون من قائمتي</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
       عمليات التحصيل
    </div>
    <li class="nav-item active">
        <a class="nav-link" href="">
            <i class="fas fa-fw fa-cart-arrow-down"></i>
            <span> عمليات التحصيل الناجحة</span>
        </a>
    </li>
    <li class="nav-item active">
        <a class="nav-link" href="">
            <i class="fas fa-fw fa-thumbs-down"></i>
            <span>  عمليات التحصيل الفاشلة</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
    @endaccount_supervisor


@component('components.sideItemList',['icon'=>'fas fa-users','header'=>'قسم التحصيل ','title'=>'قسم التحصيل '])
      <h6 class="collapse-header"> مدير قسم التحصيل:</h6>
        <a class="collapse-item {{(url()->current() == route('allActual')?'active':'')}}" href="{{route('allActual')}}"> جميع المتبرعين </a>
        <a class="collapse-item {{(url()->current() == route('withCollectingManager')?'active':'')}}" href="{{route('withCollectingManager')}}">المتبرعين الجدد</a>
        <a class="collapse-item {{(url()->current() == route('callCenter.team')?'active':'')}}" href="{{route('callCenter.team')}}">المتبرعين الفعليين</a>
        <a class="collapse-item {{(url()->current() == route('stopped')?'active':'')}}" href="{{route('stopped')}}">المتبرعين المتوقفين</a>
        <h6 class="collapse-header"> متابعة الفريق:</h6>
        <a class="collapse-item {{(url()->current() == route('callCenter.team')?'active':'')}}" href="{{route('callCenter.team')}}"> المشرفين</a>
        <a class="collapse-item {{(url()->current() == route('callCenter.team')?'active':'')}}" href="{{route('callCenter.team')}}"> المحصلين</a>
        <a class="collapse-item {{(url()->current() == route('callCenter.team')?'active':'')}}" href="{{route('callCenter.team')}}"> إضافة محصل</a>
        <h6 class="collapse-header">  المشرفين:</h6>
        <a class="collapse-item {{(url()->current() == route('callCenter.team')?'active':'')}}" href="{{route('newOfMeAsSupervisor')}}"> المتبرعون الجدد</a>
        <a class="collapse-item {{(url()->current() == route('callCenter.team')?'active':'')}}" href="{{route('callCenter.team')}}"> قائمة المتبرعين</a>
        <a class="collapse-item {{(url()->current() == route('callCenter.team')?'active':'')}}" href="{{route('callCenter.team')}}">تحصيلهم هذا الأسبوع</a>
        <a class="collapse-item {{(url()->current() == route('callCenter.team')?'active':'')}}" href="{{route('callCenter.team')}}">جاري التحصيل منهم</a>

        <h6 class="collapse-header">  عمليات التحصيل الخاصة بي:</h6>
        <a class="collapse-item {{(url()->current() == route('newOfMeAsSupervisor')?'active':'')}}" href="{{route('newOfMeAsSupervisor')}}">  الحالية</a>
        <a class="collapse-item {{(url()->current() == route('oldOfMeAsSupervisor')?'active':'')}}" href="{{route('oldOfMeAsSupervisor')}}">  الماضية</a>

        <h6 class="collapse-header">  عمليات كل الفريق </h6>
        <a class="collapse-item {{(url()->current() == route('callCenter.team')?'active':'')}}" href="{{route('callCenter.team')}}">  الحالية</a>
        <a class="collapse-item {{(url()->current() == route('callCenter.team')?'active':'')}}" href="{{route('callCenter.team')}}">  الماضية</a>

        <h6 class="collapse-header"> الفريق:</h6>
        <a class="collapse-item {{(url()->current() == route('callCenter.team')?'active':'')}}" href="{{route('callCenter.team')}}">  المحصلين</a>
        <a class="collapse-item {{(url()->current() == route('callCenter.team')?'active':'')}}" href="{{route('callCenter.team')}}"> إضافة محصل لفريقي</a>

@endcomponent
{{--<div class="sidebar-heading">--}}
{{--المتبرعين--}}
{{--</div>--}}


{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('callManager.new.benefactors')}}">--}}
{{--<i class="fas fa-fw fa-hand-holding"></i>--}}
{{--<span> المتبرعين المحتملين </span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('marketingBenefactors.create')}}">--}}
{{--<i class="fas fa-fw fa-save"></i>--}}
{{--<span>تسجيل متبرع جديد</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('callManager.uploadFile')}}">--}}
{{--<i class="fas fa-fw fa-file-excel"></i>--}}
{{--<span> استيراد من ملف اكسيل </span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li class="nav-item active">--}}
{{--    <a class="nav-link" href="{{route('actualBenefactors')}}">--}}
{{--        <i class="fas fa-fw fa-users"></i>--}}
{{--        <span>جميع المتبرعين</span>--}}
{{--    </a>--}}
{{--</li>--}}
{{--<hr class="sidebar-divider">--}}
{{--@call_manager()--}}
{{--<div class="sidebar-heading">--}}
{{--نتائج العمليات المخصصة لفريق الكول سنتر--}}
{{--</div>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('callManager.assignments.team')}}">--}}
{{--<i class="fas fa-fw fa-users"></i>--}}
{{--<span>متابعة فريق الكول سنتر</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('callManager.forCollect.assignments')}}">--}}
{{--<i class="fas fa-fw fa-link"></i>--}}
{{--<span>جاهزة للتحصيل</span>--}}
{{--</a>--}}
{{--</li>--}}

{{--<hr class="sidebar-divider">--}}
{{--<div class="sidebar-heading">--}}
{{--المخصصة لمدير الكول سنتر--}}
{{--</div>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('call_assignments.index')}}">--}}
{{--<i class="fas fa-fw fa-address-card"></i>--}}
{{--<span>الجديدة </span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('old_assignments.callCenter')}}">--}}
{{--<i class="fas fa-fw fa-poll"></i>--}}
{{--<span>القديمة </span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<hr class="sidebar-divider">--}}
{{--<!-- Heading -->--}}
{{--<div class="sidebar-heading">--}}
{{--الإعلانات--}}
{{--</div>--}}
{{--<li class="nav-item">--}}
{{--<a class="nav-link " href="#" data-toggle="collapse" data-target="#collapsenen" aria-expanded="true" aria-controls="collapseCall">--}}
{{--<i class="fas fa-fw fa-folder"></i>--}}
{{--<span> الإعلانات </span>--}}
{{--</a>--}}
{{--<div id="collapsenen" class="collapse" aria-labelledby="headingCalls" data-parent="#accordionSidebar">--}}
{{--<div class="bg-white py-2 collapse-inner rounded">--}}
{{--<h6 class="collapse-header">الإعلانات:</h6>--}}
{{--<a class="collapse-item" href="{{route('ads.index')}}">جميع الاعلانات</a>--}}
{{--<a class="collapse-item" href="{{route('ads.create')}}">إنشاء إعلان جديد</a>--}}
{{--<div class="collapse-divider"></div>--}}
{{--<h6 class="collapse-header"> المانطق</h6>--}}
{{--<a class="collapse-item" href="{{route('locations.index')}}">المناطق</a>--}}
{{--</div>--}}
{{--</div>--}}
{{--</li>--}}
{{--<hr class="sidebar-divider">--}}

{{--@endcall_manager--}}
{{--@call_emp()--}}
{{--<div class="sidebar-heading">--}}
{{--عمليات التخصيص--}}
{{--</div>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('call_assignments.index')}}">--}}
{{--<i class="fas fa-fw fa-users"></i>--}}
{{--<span> الجديدة</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('old_assignments.callCenter')}}">--}}
{{--<i class="fas fa-fw fa-address-card"></i>--}}
{{--<span>القديمة </span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<hr class="sidebar-divider">--}}
{{--<!-- Nav Item - Pages Collapse Menu -->--}}

{{--@endcall_emp--}}

{{--@account_manager('account_supervisor')--}}
{{--<li class="nav-item active">--}}
{{--    <a class="nav-link" href="{{route('callManager.benefactors.forCollect')}}">--}}
{{--        <i class="fas fa-fw fa-phone"></i>--}}
{{--        <span>اقترب وقت التحصيل منهم</span>--}}
{{--    </a>--}}
{{--</li>--}}
{{--<!-- Heading -->--}}
{{--@if($thisUser->can('see_all_need_to_confirm'))--}}
{{--<div class="sidebar-heading">--}}
{{--من الكول سنتر--}}
{{--</div>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('toCollectConfirmCall')}}">--}}
{{--<i class="fas fa-fw fa-users"></i>--}}
{{--<span>تحتاج تأكيد التحصيل</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<hr class="sidebar-divider">--}}
{{--@endif--}}
{{--@if($thisUser->can('see_all_confirmed_to_collect|see_all_delayed_to_collect|see_all_refused_from_ConfirmCall_to_collect'))--}}
{{--<div class="sidebar-heading">--}}
{{--عمليات تم إجراء مكالمة التأكيد لها--}}
{{--</div>--}}
{{--@if($thisUser->can('see_all_confirmed_to_collect'))--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('toCollectconfirmedCalls')}}">--}}
{{--<i class="fas fa-fw fa-thumbs-up"></i>--}}
{{--<span> تم تأكيد التحصيل</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--@endif--}}
{{--@if($thisUser->can('see_all_delayed_to_collect'))--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('toCollectDelayedCalls')}}">--}}
{{--<i class="fas fa-fw fa-clock"></i>--}}
{{--<span>تم تأجيل التحصيل</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--@endif--}}
{{--@if($thisUser->can('see_all_refused_from_ConfirmCall_to_collect'))--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('toCollectFaildCalls')}}">--}}
{{--<i class="fas fa-fw fa-thumbs-down"></i>--}}
{{--<span> تم رفض التحصيل</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--@endif--}}

{{--<hr class="sidebar-divider">--}}
{{--@endif--}}
{{--@if($thisUser->can('see_all_collection_team'))--}}
{{--<div class="sidebar-heading">--}}
{{--متابعة موظفي القسم--}}
{{--</div>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('account.team')}}">--}}
{{--<i class="fas fa-fw fa-link"></i>--}}
{{--<span>أعضاء قسم التحصيل</span>--}}
{{--</a>--}}
{{--</li>--}}

{{--<hr class="sidebar-divider">--}}
{{--@endif--}}
{{--<div class="sidebar-heading">--}}
{{--عمليات التحصيل الخاصة بي--}}
{{--</div>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('collectAssignments.myNew')}}">--}}
{{--<i class="fas fa-fw fa-clock"></i>--}}
{{--<span>الحالية</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('collectAssignments.old')}}">--}}
{{--<i class="fas fa-fw fa-thumbs-up"></i>--}}
{{--<span>التامة</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<hr class="sidebar-divider">--}}
{{--@if($thisUser->can('see_all_now_atime_collections|see_all_done_collections|see_all_failed_collections'))--}}
{{--<div class="sidebar-heading">--}}
{{--عمليات التحصيل--}}
{{--</div>--}}
{{--@if($thisUser->can('see_all_now_atime_collections'))--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('collectAssignments.immediate')}}">--}}
{{--<i class="fas fa-fw fa-clock"></i>--}}
{{--<span> جميع العمليات الحالية</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--@endif--}}
{{--@if($thisUser->can('see_all_done_collections'))--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('collectAssignments.done')}}">--}}
{{--<i class="fas fa-fw fa-thumbs-up"></i>--}}
{{--<span> جميع العمليات التامة</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--@endif--}}
{{--@if($thisUser->can('see_all_failed_collections'))--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('collectAssignments.faild')}}">--}}
{{--<i class="fas fa-fw fa-thumbs-down"></i>--}}
{{--<span> جميع العمليات الفاشلة</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--@endif--}}
{{--<hr class="sidebar-divider">--}}

{{--@endif--}}

{{--@endaccount_manager--}}

{{--@accountant()--}}
{{--<div class="sidebar-heading">--}}
{{--عمليات التحصيل الخاصة بي--}}
{{--</div>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('collectAssignments.myNew')}}">--}}
{{--<i class="fas fa-fw fa-clock"></i>--}}
{{--<span>الحالية</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('collectAssignments.old')}}">--}}
{{--<i class="fas fa-fw fa-thumbs-up"></i>--}}
{{--<span>التامة</span>--}}
{{--</a>--}}
{{--</li>--}}

{{--<hr class="sidebar-divider">--}}

{{--@endaccountant--}}

{{--@admin('manager')--}}
{{--<!-- Heading -->--}}
{{--<div class="sidebar-heading">--}}
{{--المستخدمين والصلاحيات--}}
{{--</div>--}}
{{--<!-- Nav Item - Pages Collapse Menu -->--}}
{{--@if($thisUser->can('users_control'))--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('users.all')}}">--}}
{{--<i class="fas fa-fw fa-users"></i>--}}
{{--<span>  التحكم بالمستخدمين</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--@endif--}}
{{--@if($thisUser->can('roles_control'))--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('roles.index')}}">--}}
{{--<i class="fas fa-fw fa-tags"></i>--}}
{{--<span> الأدوار</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--@endif--}}
{{--@if($thisUser->can('permissions_control'))--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('permissions.index')}}">--}}
{{--<i class="fas fa-fw fa-people-carry"></i>--}}
{{--<span> الصلاحيات</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--@endif--}}
{{--<!-- Divider -->--}}
{{--<hr class="sidebar-divider">--}}
{{--@endadmin--}}
{{--@manager()--}}


{{--<div class="sidebar-heading">--}}
{{--متابعة الموظفين--}}
{{--</div>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('callManager.assignments.team')}}">--}}
{{--<i class="fas fa-fw fa-users"></i>--}}
{{--<span>متابعة فريق الكول سنتر</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('account.team')}}">--}}
{{--<i class="fas fa-fw fa-link"></i>--}}
{{--<span>متابعة فريق قسم التحصيل</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('ads.index')}}">--}}
{{--<i class="fas fa-fw fa-link"></i>--}}
{{--<span>جميع الإعلانات</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<hr class="sidebar-divider">--}}
{{--<div class="sidebar-heading">--}}
{{--عمليات التحصيل--}}
{{--</div>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('collectAssignments.immediate')}}">--}}
{{--<i class="fas fa-fw fa-clock"></i>--}}
{{--<span> جميع العمليات الحالية</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('collectAssignments.done')}}">--}}
{{--<i class="fas fa-fw fa-thumbs-up"></i>--}}
{{--<span> جميع العمليات التامة</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('collectAssignments.faild')}}">--}}
{{--<i class="fas fa-fw fa-thumbs-down"></i>--}}
{{--<span> جميع العمليات الفاشلة</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<hr class="sidebar-divider">--}}

{{--<div class="sidebar-heading">--}}
{{--المتبرعين--}}
{{--</div>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('actualBenefactors')}}">--}}
{{--<i class="fas fa-fw fa-users"></i>--}}
{{--<span>جميع المتبرعين</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('all.kafalah')}}">--}}
{{--<i class="fas fa-fw fa-users"></i>--}}
{{--<span>أصحاب الكفالات</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('all.stopped')}}">--}}
{{--<i class="fas fa-fw fa-users"></i>--}}
{{--<span>المتبرعون المتوقفون </span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('callManager.benefactors.forCollect')}}">--}}
{{--<i class="fas fa-fw fa-phone"></i>--}}
{{--<span>اقترب وقت التحصيل منهم</span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<li class="nav-item active">--}}
{{--<a class="nav-link" href="{{route('callManager.new.benefactors')}}">--}}
{{--<i class="fas fa-fw fa-hand-holding"></i>--}}
{{--<span> المتبرعين المحتملين </span>--}}
{{--</a>--}}
{{--</li>--}}
{{--<hr class="sidebar-divider d-none d-md-block">--}}

{{--@endmanager--}}

{{--@can('prent_reports')--}}
{{--<!-- Nav Item - Pages Collapse Menu -->--}}
{{--<li class="nav-item">--}}
{{--<a class="nav-link " href="#" data-toggle="collapse" data-target="#collapselop" aria-expanded="true" aria-controls="collapseCall">--}}
{{--<i class="fas fa-fw fa-folder"></i>--}}
{{--<span> التقارير </span>--}}
{{--</a>--}}
{{--<div id="collapselop" class="collapse" aria-labelledby="headingCalls" data-parent="#accordionSidebar">--}}
{{--<div class="bg-white py-2 collapse-inner rounded">--}}
{{--<h6 class="collapse-header">التقارير:</h6>--}}
{{--<a class="collapse-item" href="{{route('all_benefactors.report')}}">تقرير بكافة المتبرعين  </a>--}}
{{--<a class="collapse-item" href="{{route('all_kafalah.report')}}">تقرير بأصحاب الكفالات</a>--}}
{{--<a class="collapse-item" href="{{route('all_aini.report')}}">تقرير بأصحاب التبرعات العينية</a>--}}
{{--</div>--}}
{{--</div>--}}
{{--</li>--}}
{{--<hr class="sidebar-divider d-none d-md-block">--}}

{{--@endif--}}

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
<button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

</ul>

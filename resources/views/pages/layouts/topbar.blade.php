
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">


    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
      <i class="fa fa-bars"></i>
    </button>
@if(auth()->check())


    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">
      <!-- Nav Item - Alerts -->
      <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-bell fa-fw"></i>
          <!-- Counter - Alerts -->
            @if(auth()->user()->unReadNotifications->count())
            <span class="badge badge-danger badge-counter">{{( auth()->user()->unReadNotifications->count() < 9 )? auth()->user()->unReadNotifications->count() : '+9' }}</span>
            @endif
        </a>
        <!-- Dropdown - Alerts -->
        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in overflow-auto" aria-labelledby="alertsDropdown" style="max-height: 500px ; ">
          <h6 class="dropdown-header">
            الاشعارات الجديدة:   {{auth()->user()->unReadNotifications->count() }}
          </h6>

            @foreach ( auth()->user()->unReadNotifications as $key=>$notification)
          <a class="dropdown-item d-flex align-items-center" href="#"  data-toggle="modal" data-target="#notification-{{$key}}">
            <div class="mr-3">
              <div class="icon-circle bg-primary">
                <i class="fas fa-file-alt text-white"></i>
              </div>
            </div>
            <div>
              <div class="small text-gray-500"> {{date('H:i', strtotime($notification->created_at)) .' | '. date('d', strtotime($notification->created_at)) .','. config('months.month.'.date('m', strtotime($notification->created_at))) .','.date('Y', strtotime($notification->created_at)) }}</div>
              <span class="font-weight-bold">{{$notification->data['title']}}</span>
            </div>
          </a>
            @endforeach
        </div>
      </li>

      <div class="topbar-divider d-none d-sm-block"></div>
@endif
      @guest
      <a href=" {{route('login')}}" class="my-2 text-primary h2" >  <i class="fas fa-sign-in-alt fa-sm fa-fw mr-2 text-gray-400"></i> </a>
      @else
      <!-- Nav Item - User Information -->
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{Auth::user()->name}}</span>
          <img class="img-profile rounded-circle" src="{{asset('assets/img/user.jpg')}}">
        </a>
        <!-- Dropdown - User Information -->
        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">

          <a class="dropdown-item" href="{{route('settings')}}" >
            <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
            الإعدادات
          </a>
            <a class="dropdown-item" href="{{route('guide')}}" >
            <i class="fas fa-video fa-sm fa-fw mr-2 text-gray-400"></i>
            دليل الاستخدام
          </a>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
            خروج
          </a>
        </div>
      </li>
      @endguest
    </ul>

  </nav>

@section('modal')
    @if(auth()->check())
    @foreach ( auth()->user()->unReadNotifications as $key=>$notification)
        <div class="modal fade" id="notification-{{$key}}" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">إشعار بـ{{$notification->data['title']}} </h5>


                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="small text-gray-500 pr-5"> {{date('H:i', strtotime($notification->created_at)) .' | '. date('d', strtotime($notification->created_at)) .','. config('months.month.'.date('m', strtotime($notification->created_at))) .','.date('Y', strtotime($notification->created_at)) }}</div>

                    <div class="modal-body">{{$notification->data['notes']}} @if($notification->data['url']) <a class="btn btn-link" href="{{$notification->data['url']}}" target="_blank"> اتبع الرابط </a> @endif</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">إلغاء</button>
                        <form method="POST" action="{{ route('readNotification',['id',$notification->id]) }}">

                            @csrf
                            <input type="text" name="id" value="{{$notification->id}}" id="" hidden>
                            <input type="submit" class="btn btn-primary" value="تم">
                        </form>
                    </div>
                </div>
            </div>
        </div>

    @endforeach
    @endif
@stop

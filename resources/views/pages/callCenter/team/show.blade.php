@extends('pages.layouts.app')
@section('css')
<link href="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@stop
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
    <!-- Content Row -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
            <li class="breadcrumb-item"><a href="{{route('callCenter.team')}}">فريق الكول سنتر</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$emp->name}}</li>
        </ol>
    </nav>

    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">عدد كافة عمليات التواصل</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"> {{$emp->employeeAssignments->count()}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x  text-primary"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> عدد عمليات التواصل الحالية</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"> {{$emp->employeeAssignments->where('done','<>',1)->count()}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-link fa-2x  text-primary"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">عدد المكالمات</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$emp->calls->count()}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-phone fa-2x  text-primary"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@component('components.table',['color'=>'primary','title'=>'عمليات التواصل'])
        <thead>
        <tr>
            <th>#</th>
            <th>التاريخ</th>
            <th>من خلال</th>
            <th> المتبرع</th>
            <th> الحالة</th>
            <th class="text-center">عرض</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            <th>التاريخ</th>
            <th>من خلال</th>
            <th> المتبرع</th>
            <th> الحالة</th>
            <th class="text-center">عرض</th>
        </tr>
        </tfoot>
        <tbody>
        @foreach($assignments as $key=>$assignment)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{getDateTime($assignment->created_at)}}</td>
                <td>{{$assignment->assignmentable->name}}</td>
                <td>{{$assignment->benefactorable->name}}</td>
                <td>{{$assignment->status}}</td>
                <td class="text-center"><a class="text-white btn btn-warning" href="{{route('callCenter.emp.assignment',$assignment)}}"> <i class="fa fa-eye"></i> </a></td>
            </tr>


        @endforeach
        </tbody>
@endcomponent
      <!-- Grow In Utility -->


  </div>
  <!-- /.container-fluid -->

@stop
@section('js')
    <!-- Page level plugins -->
    <script src="{{asset('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{asset('assets/js/demo/datatables-demo.js')}}"></script>


@stop

@extends('pages.layouts.app')
@section('css')
<link href="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@stop
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
            <li class="breadcrumb-item"><a href="{{route('callCenter.team')}}">فريق الكول سنتر</a></li>
            <li class="breadcrumb-item"><a href="{{route('callCenter.show',$assignment->employeeable->id)}}">{{$assignment->employeeable->name}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">عملية تواصل بكود: {{$assignment->id}}</li>
        </ol>
    </nav>
    <!-- Content Row -->
    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-12 col-md-12 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">عدد المكالمات</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"> {{$assignment->calls->count()}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-mobile fa-3x  text-warning"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!-- Earnings (Monthly) Card Example -->
      <!-- Grow In Utility -->
        <div class="col-lg-12 justify-content-center">
            @component('components.card',['color'=>'primary','title'=> ' متابعة الموظف:'.$assignment->employeeable->name.' في عملية تواصل له بكود: '. $assignment->id])
                <ul class="nav nav-pills m-3 justify-content-center " id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="btn btn-secondary m-1  active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">المكالمات التامة</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-secondary  m-1" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">  تعديل البيانات</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="card-body tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div id="calls"  >
                            @component('components.table',['color'=>'warning','title'=>'المكالمات التي تمت في هذه العملية'])
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>الكود</th>
                                    <th>تاريخ التسجيل</th>
                                    <th>حالة الرد</th>
                                    <th> التجاوب</th>
                                    <th>عرض</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>الكود</th>
                                    <th>تاريخ التسجيل</th>
                                    <th>حالة الرد</th>
                                    <th> التجاوب</th>
                                    <th>عرض</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($assignment->calls as $key=>$call)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>c-{{$call->id}}</td>
                                        <td>{{getDateTime($assignment->created_at)}}</td>
                                        <td>{{$call->answer_status}}</td>
                                        <td>{{$call->reacting}}</td>
                                        <td ><a class="text-white px-3  btn btn-primary"data-toggle="modal" data-target="#call_view_{{$key}}"> <i class="fa fa-eye"></i> </a></td>
                                    </tr>

                                @endforeach
                                </tbody>
                            @endcomponent
                                @foreach($assignment->calls as $key=>$call)
                                @component('components.modal_xl',['id'=>'call_view_'.$key,'color'=>'primary','title'=>'تفاصيل المكالمة'])
                                    <div class="table-responsive">

                                        <table class="table table-bordered">
                                            <tr class="bg-secondary text-white">
                                                <th>غرض المكالمة </th>
                                                <td>{{$call->assignable->message}} </td>
                                            </tr>
                                            <tr class="bg-secondary text-white">
                                                <th>الموظف الذي قام بالتواصل</th>
                                                <td>{{$call->userable->name}} </td>
                                            </tr>
                                        </table>

                                        <table class="table table-bordered">

                                            <tr>
                                                <th>توقيت المكالمة</th>
                                                <td> {{getDateTime($call->date.$call->time)}}</td>
                                            </tr>

                                            <tr>
                                                <th>حالة الرد</th>
                                                <td> {{$call->answer_status}} <small class="text-secondary"> {{$call->status_more}} </small></td>
                                            </tr>

                                            <tr>
                                                <th>الجاوب</th>
                                                <td>{{$call->reacting}}</td>
                                            </tr>

                                            <tr>
                                                <th>تقييم المكالمة</th>
                                                <td>{{$call->call_rate??'0'}}  / 10</td>
                                            </tr>

                                            <tr>
                                                <th>أهم سؤال</th>
                                                <td>{{$call->question??'لا يوجد'}}</td>
                                            </tr>

                                            <tr>
                                                <th>ملخص المكالمة</th>
                                                <td>{{$call->call_samary??'لا يوجد'}}</td>
                                            </tr>

                                            <tr>
                                                <th>ملاحظات</th>
                                                <td>{{$call->notes??'لا يوجد'}}</td>
                                            </tr>

                                        </table>
                                    </div>

                                @endcomponent
                                @endforeach
                                <div class="col-md-8 mt-5 col-sm-12 offset-md-2 ">
                                    @include('tmps.newBenefactorCallsChartTemp',['color'=>'secondary','item'=>$assignment])
                                </div>
                        </div>
                    </div>

                    <div class="card-body tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div id="edit-requiests">
                            @component('components.card',['color'=>'primary','title'=>' تعديل ملف المتبرع'])
                                @if($assignment->benefactorable->actualbenefactor()->count())
                                    <div class="col-md-12 table-responsive p-2 ">
                                        <div class="table-responsive p-3">
                                            <table class="table table-bordered " style="width: 100%;" >
                                                <thead>
                                                <tr>
                                                    <th>الحقل</th>
                                                    <th>البيانات القديمة</th>
                                                    <th>البيانات المعدلة</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <th>الاسم</th>
                                                    <td>{{$assignment->benefactorable->name??'لا يوجد'}}</td>
                                                    <td>{{$assignment->benefactorable->actualbenefactor->name}}</td>
                                                </tr>
                                                <tr>
                                                    <th>الايميل</th>
                                                    <td>{{$assignment->benefactorable->email??'لا يوجد'}}</td>
                                                    <td>{{$assignment->benefactorable->actualbenefactor->email}}</td>
                                                </tr>
                                                <tr>
                                                    <th>الهاتف</th>
                                                    <td>{{$assignment->benefactorable->phone??'لا يوجد'}}</td>
                                                    <td>{{$assignment->benefactorable->actualbenefactor->phone}}</td>
                                                </tr>
                                                <tr>
                                                    <th>تصنيف المتبرع</th>
                                                    <td>
                                                        {{getBenefactorType($assignment->benefactorable)}}
                                                    </td>
                                                    <td>
                                                        {{getBenefactorType($assignment->benefactorable->actualbenefactor)}}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th> نوع التبرع</th>
                                                    <td>{{getDonationType($assignment->benefactorable)}}</td>
                                                    <td>{{getDonationType($assignment->benefactorable->actualbenefactor)}}</td>
                                                </tr>
                                                <tr>
                                                    <th>قيمة التبرع</th>
                                                    <td>{{getDonation($assignment->benefactorable)}}</td>
                                                    <td>{{getDonation($assignment->benefactorable->actualbenefactor)}}</td>
                                                </tr>
                                                <tr>
                                                    <th> تاريخ أول تحصيل</th>
                                                    <td>
                                                        {{' لا يوجد  ' }}
                                                    </td>
                                                    <td>
                                                        {{getDateTime($assignment->benefactorable->actualbenefactor->dueDate)??'لم يحدد'}}
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                @else
                                    <p class="alert alert-secondary">لم يتم تعديل لبيانات هذا المتبرع </p>
                                @endif
                            @endcomponent
                        </div>
                    </div>
                </div>
            @endcomponent
      </div>
    </div>




  </div>
  <!-- /.container-fluid -->

@stop

@extends('pages.layouts.app')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
            <li class="breadcrumb-item active" aria-current="page">فريق الكول سنتر</li>
        </ol>
    </nav>

    @component('components.table',['color'=>'primary','title'=>'فريق الكول سنتر'])
        <thead>
        <tr>
            <th>#</th>
            <th>الموظف</th>
            <th> عدد العمليات التامة</th>
            <th>عدد العمليات الحالية</th>
            <th>المكالمات</th>
            <th>متابعة</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            <th>الموظف</th>
            <th> عدد العمليات التامة</th>
            <th>عدد العمليات الحالية</th>
            <th>المكالمات</th>
            <th>متابعة</th>
        </tr>
        </tfoot>
        <tbody>
            @foreach($emps as  $key=>$emp)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$emp->name}}</td>
                <td> {{$emp->employeeAssignments->where('done',1)->count()}}  </td>
                <td> {{$emp->employeeAssignments->where('done','<>',1)->count()}}  </td>
                <td>{{$emp->calls->count()}} </td>
                <td > <a class="btn btn-warning " href="{{route('callCenter.show',$emp->id)}}"> <i class="fas fa-eye"></i> </a> </td>
            </tr>
            @endforeach
        </tbody>
    @endcomponent
</div>
@stop




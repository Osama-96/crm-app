@extends('pages.layouts.app')
@section('css')
<link href="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@stop
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
            <li class="breadcrumb-item active" aria-current="page">العمليات الجاهزة للتحصيل</li>
        </ol>
    </nav>
    <!-- Content Row -->
    <div class="row">
        <div class="col-lg-12 ">
            <div class="card position-relative border-primary">
                <div class="card-header py-3 bg-gradient-primary text-center">
                    <h3 class=" mb-1 text-white">عمليات مستعدة للإرسال لمدير التحصيل  </h3>
                </div>
                <div class="card-body " >
                    <div id="calls" class="row ">
                        <div class="col-md-12 table-responsive   ">

                            @if(count($assignments))
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>الكود</th>
                                            <th>المتبرع</th>
                                            <th>عدد طلبات التبرع</th>
                                            <th> الموظف</th>
                                            <th> تاريخ التحصيل</th>
                                            <th>عرض</th>
                                            <th>إرسال</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>الكود</th>
                                            <th>المتبرع</th>
                                            <th>عدد طلبات التبرع</th>
                                            <th> الموظف</th>
                                            <th>  تاريخ التحصيل</th>
                                            <th>عرض</th>
                                            <th>إرسال</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        @foreach($assignments as $key=>$assignment)
                                            @if($assignment->collectRequests()->count() && !$assignment->collectAssignment()->count())
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{$assignment->id}}</td>
                                                <td>{{$assignment->benefactorable->name}}</td>
                                                <td>{{$assignment->collectRequests()->count()}}</td>
                                                <td>{{$assignment->employeeable->name}}</td>
                                                <td>
                                                    @foreach($assignment->collectRequests as $key=>$request)
                                                    {{($key!=0)?' & ':' '}}
                                                    {{date('H:i', strtotime($request->time)) .
                                                    ' | '. date('d', strtotime($request->date)) .
                                                    ','. config('months.month.'.date('m', strtotime($request->date))) .
                                                    ','.date('Y', strtotime($request->date)) }}
                                                    @endforeach
                                                </td>

                                                <td ><a class="text-info  btn btn-outline-info" href="{{route('collect_assignments.show',$assignment->id)}}"> <i class="fa fa-eye"></i> </a></td>

                                                <td >
                                                    <form action="{{route('send.assignment.to.collectManager',$assignment->id)}}" method="post">
                                                        @csrf
                                                    <button class="text-white  btn btn-success" type="submit"> <i class="fa fa-share"></i> </button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @else
                            <div class="alert alert-danger">
                                <p> لا يوجد أي عملية مخصصة  </p>
                            </div>
                            @endif
                                    {{--        view call--}}

                        </div>

            </div>
                </div>
            </div>
      </div>

    </div>

  </div>
  <!-- /.container-fluid -->

@stop
@section('js')
    <!-- Page level plugins -->
    <script src="{{asset('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{asset('assets/js/demo/datatables-demo.js')}}"></script>


@stop

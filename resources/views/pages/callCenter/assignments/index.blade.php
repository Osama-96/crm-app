@extends('pages.layouts.app')

@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
            @if(route('newAssignments.index') == url()->current())
                <li class="breadcrumb-item active" aria-current="page">عمليات التواصل الحالية</li>
            @else
                <li class="breadcrumb-item active" aria-current="page">عمليات التواصل التامة</li>
            @endif
        </ol>
    </nav>
    @if (route('newAssignments.index') == url()->current())


    @component('components.table',['color'=>'primary','title'=>'عمليات التواصل الحالية'])
        <thead>
        <tr>
            <th>#</th>
            <th>الكود</th>
            <th>التاريخ</th>
            <th>من خلال</th>
            <th> المتبرع</th>
            <th class="text-center">عرض</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            <th>الكود</th>
            <th>التاريخ</th>
            <th>من خلال</th>
            <th> المتبرع</th>
            <th class="text-center">عرض</th>
        </tr>
        </tfoot>
        <tbody>
            @foreach($assignments as $key=>$assignment)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$assignment->id}}</td>
                    <td>{{getDateTime($assignment->created_at) }}</td>
                    <td>{{$assignment->assignmentable->name}}</td>
                    <td>{{$assignment->benefactorable->name}}</td>
                    <td class="text-center"><a class="text-white  btn btn-warning" href="{{route('newAssignments.show',$assignment)}}"> <i class="fa fa-eye"></i> </a></td>
                </tr>


            @endforeach
        </tbody>
    @endcomponent
    @elseif(route('oldAssignments.index') == url()->current())
        @component('components.table',['color'=>'primary','title'=>'العمليات التامة'])
            <thead>
            <tr>
                <th>#</th>
                <th>الكود</th>
                <th>التاريخ</th>
                <th>من خلال</th>
                <th> المتبرع</th>
                <th> حالة الإرسال</th>
                <th class="text-center">عرض</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>#</th>
                <th>الكود</th>
                <th>التاريخ</th>
                <th>من خلال</th>
                <th> المتبرع</th>
                <th> حالة الإرسال</th>
                <th class="text-center">عرض</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach($assignments as $key=>$assignment)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$assignment->id}}</td>
                    <td>{{getDateTime($assignment->created_at) }}</td>
                    <td>{{$assignment->assignmentable->name}}</td>
                    <td>{{$assignment->benefactorable->name}}</td>
                    <td>{{$assignment->status}}</td>
                    <td class="text-center"><a class="text-white  btn btn-warning" href="{{route('oldAssignments.show',$assignment)}}"> <i class="fa fa-eye"></i> </a></td>
                </tr>


            @endforeach
            </tbody>
        @endcomponent

    @endif

  </div>
  <!-- /.container-fluid -->
@stop

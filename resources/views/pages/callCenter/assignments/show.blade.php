@extends('pages.layouts.app')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
                @if($assignment->done == 0  && $assignment->failed != 1)
                    <li class="breadcrumb-item"><a href="{{route('newAssignments.index')}}">عمليات التواصل الحالية</a></li>
                @else
                    <li class="breadcrumb-item"><a href="{{route('oldAssignments.index')}}">عمليات التواصل التامة</a></li>
                @endif
                <li class="breadcrumb-item active" aria-current="page">عملية بكود {{$assignment->id}}</li>
        </ol>
    </nav>
    <!-- Content Row -->
        @if($assignment->done == 1  || $assignment->failed == 1)
            <div class="row">
                <div class="col-md-12">
                    <p class="alert alert-danger " > هذه العملية تم تسليمها لرئيس القسم، لذلك لا يمكنك التعديل على أي داتا موجودة في المكالمات أو طلبات تعديل البيانات أو طلبات التبرع!</p>
                </div>
            </div>
        @endif
      <!-- Grow In Utility -->
            @component('components.card',['title'=>' عملية تواصل رقم '.$assignment->id,'color'=>'primary'])
                <div class="row">
                    <div class="col-md-12 table-responsive border rounded badge-light ">
                        <h4 class="text-black-50 text-center py-3">تفاصيل العملية</h4>
                        <table class="table  ">
                            <tr> <th>من طرف:</th>       <td>{{$assignment->assignmentable->name}}</td> </tr>
                            <tr>    <th>المتبرع :</th>         <td>{{$assignment->benefactorable->name}}</td></tr>
                            <tr>    <th>توقيت التخصيص :</th>    <td>{{getDateTime($assignment->created_at)}}</td></tr>
                            <tr>    <th> الغرض:</th>         <td>{{$assignment->message}}</td></tr>

                        </table>
                    </div>
                </div>
                <div class="row justify-content-center ">
                    <div class="col-md-12 pt-2  justify-content-center text-center">
                        <a href="" class=" btn btn-warning  my-2 " data-toggle="modal" data-target="#show">بيانات المتبرع</a>
                        @component('components.modal_xl',['title'=>'بيانات متبرع محتمل باسم/ '.$assignment->benefactorable->name ,'color'=>'warning','id'=>'show'])
                            <div class=" row justify-content-center" >
                                <div class="col-md-12 table-responsive py-3">
                                    @if($assignment->benefactorable->moved ==0)
                                    <table class="table table-bordered  ">
                                        <tbody>
                                        <tr>
                                            <th> الاسم</th> <td> {{$assignment->benefactorable->name}}</td>
                                        </tr>

                                        <tr>
                                            <th> الهاتف</th> <td> {{$assignment->benefactorable->phone}}</td>
                                        </tr>
                                        <tr>
                                            <th>الايميل</th> <td> {{$assignment->benefactorable->email}}</td>
                                        </tr>
                                        <tr>
                                            <th>الوسيلة</th> <td> {{$assignment->benefactorable->device}}</td>
                                        </tr>
                                        <tr>
                                            <th>الاعلان </th> <td>{{ ($assignment->benefactorable->adable)? "رقم".$assignment->benefactorable->adable->id :"لا يوجد"  }} </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                    @else
                                        <p class="alert alert-primary">تم تحديث بيانات هذا المتبرع بنجاح</p>
                                            <table class="table table-bordered table-inverse text-left">
                                                <tbody>
                                                <tr>
                                                    <th> الاسم</th> <td> {{$assignment->benefactorable->actualBenefactor->name}}</td>
                                                </tr>

                                                <tr>
                                                    <th> الهاتف</th> <td> {{$assignment->benefactorable->actualBenefactor->phone}}</td>
                                                </tr>
                                                <tr>
                                                    <th>الايميل</th> <td> {{$assignment->benefactorable->actualBenefactor->email}}</td>
                                                </tr>
                                                <tr>
                                                    <th>العنوان</th> <td> {{$assignment->benefactorable->actualBenefactor->address}}</td>
                                                </tr>
                                                <tr>
                                                    <th>الوسيلة</th> <td> {{$assignment->benefactorable->actualBenefactor->device}}</td>
                                                </tr>

                                                <tr>
                                                    <th>الاعلان </th> <td>{{($assignment->benefactorable->actualBenefactor->adable_id )? 'رقم'.$assignment->benefactorable->actualBenefactor->adable_id :'لم يتم تحديد إعلان' }} </td>
                                                </tr>

                                                <tr>
                                                    <th> نوع التبرع</th> <td>{{getBenefactorType($assignment->benefactorable->actualBenefactor)}}</td>
                                                </tr>

                                                <tr>
                                                    <th>  توقيت التحصيل</th> <td> {{$assignment->benefactorable->actualBenefactor->dueDate?? 'لم يسجل بعد'}}</td>
                                                </tr>
                                                <tr>
                                                    <th>التبرع</th> <td>{{$assignment->benefactorable->actualBenefactor->moneyAmount}}  {{$assignment->benefactorable->actualBenefactor->donation}}</td>
                                                </tr>
                                                <tr>
                                                    <th>أسماء الحالات</th> <td> {{$assignment->benefactorable->actualBenefactor->cases}}</td>
                                                </tr>
                                                </tbody>
                                            </table>

                                    @endif

                                </div>

                            </div>

                        @endcomponent

                    @if($assignment->done != 1   && $assignment->failed != 1)

                        <a href="" class=" btn btn-warning  my-2 " data-toggle="modal" data-target="#edit">تحديث بيانات المتبرع </a>
                        @component('components.modal_xl',['title'=>' تعديل بيانات المتبرع/ '.$assignment->benefactorable->name ,'color'=>'warning','id'=>'edit'])
                            @if(!$assignment->benefactorable->actualBenefactor()->count())
                            <form action="{{route('newAssignments.storeData',['newBenefactor'=>$assignment->benefactorable->id,'assignment'=>$assignment])}}" method="POST">
                                @csrf

                                <div class="row justify-content-center text-left">
                                    <div class="form-group col-md-6">
                                        <label for="">الاسم </label>
                                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="" value="{{$assignment->benefactorable->name}}">
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <small class="text-danger">{{ $message }}</small>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="">العنوان </label>
                                        <input type="text" class="form-control @error('address') is-invalid @enderror" name="address" id="" value="{{$assignment->benefactorable->address}}">
                                        @error('address')
                                        <span class="invalid-feedback" role="alert">
                                            <small class="text-danger">{{ $message }}</small>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for=""> الهاتف  </label>
                                        <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" id="" value="{{$assignment->benefactorable->phone}}">
                                        @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                                        <small class="text-danger">{{ $message }}</small>
                                                    </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for=""> الإيميل  </label>
                                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="" value="{{$assignment->benefactorable->email}}">
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                                        <small class="text-danger">{{ $message }}</small>
                                                    </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="">نوع المتبرع   </label>
                                        <select name="bene_type" id="" class="form-control @error('bene_type') is-invalid @enderror">
                                            <option disabled selected> اختر النوع </option>
                                            <option value="1" > متبرع عادي</option>
                                            <option value="2">كفالة شهرية</option>
                                            <option value="3">كفالة موسمية (كل أربعة أشهر)</option>
                                            <option value="4">كفالة سنوية (كل سنة)</option>
                                        </select>
                                        @error('bene_type')
                                        <span class="invalid-feedback" role="alert">
                                                        <small class="text-danger">{{ $message }}</small>
                                                    </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="">نوع التبرع   </label>
                                        <select name="donation_type" id="" class="form-control @error('donation_type') is-invalid @enderror">
                                            <option disabled selected> اختر النوع </option>
                                            <option value="1" > تبرع مالي</option>
                                            <option value="2" > تبرع عيني</option>
                                        </select>
                                        @error('donation_type')
                                        <span class="invalid-feedback" role="alert">
                                                        <small class="text-danger">{{ $message }}</small>
                                                    </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="">قيمة التبرع</label>
                                        <input type="text" name="donation"  class="form-control">

                                        <small class="text-danger">ملاحظة: إذا كان نوع التبرع الذي أدخلته (تبرع مالي) فلابد من إدخال قيمة مالية بأرقام صحيحة،إذا كان نوع التبرع الذي أدخلته (تبرع عيني) فلابد من إدخال قيمة هذا التبرع مكتوباً </small>
                                        @error('donation')
                                        <span class="invalid-feedback" role="alert">
                                                        <small class="text-danger">{{ $message }}</small>
                                                    </span>
                                        @enderror
                                    </div>
                                        <div class="form-group col-md-3">
                                            <label for=""> تاريخ أول تحصيل</label>
                                            <input type="date" name="date" required class="form-control ">

                                            <small class="text-danger">ملاحظة:التاريخ الذي ستحفظه هنا هو التاريخ الذي سيتم التحصيل فيه شهرياً ...</small>
                                            @error('date')
                                            <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>
                                    <div class="form-group col-md-3 ">
                                            <label for=""> وقت أول تحصيل</label>
                                            <input class="form-control d-inline" name="time" required type="time" value="13:45:00" id="example-time-input" >
                                            @error('time')
                                            <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="">أسماء الحالات</label>
                                            <textarea  name="cases"  class="form-control" required> {{old('cases')}}</textarea>
                                            @error('cases')
                                            <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>


                                </div>
                                <div class="row justify-content-center my-3">
                                    <div class="form-group col-md-4 ">
                                        <input type="submit" class="btn btn-warning form-control" value="حفظ البيانات">
                                    </div>
                                </div>



                            </form>
                            @else
                                <form action="{{route('newAssignments.updateData',['benefactor'=>$assignment->benefactorable->actualBenefactor->id])}}" method="POST">
                                    @csrf

                                    <div class="row justify-content-center text-left">
                                        <div class="form-group col-md-6">
                                            <label for="">الاسم </label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="" value="{{$assignment->benefactorable->actualBenefactor->name}}">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                            <small class="text-danger">{{ $message }}</small>
                                        </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="">العنوان </label>
                                            <input type="text" class="form-control @error('address') is-invalid @enderror" name="address" id="" value="{{$assignment->benefactorable->actualBenefactor->address}}">
                                            @error('address')
                                            <span class="invalid-feedback" role="alert">
                                            <small class="text-danger">{{ $message }}</small>
                                        </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for=""> الهاتف  </label>
                                            <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" id="" value="{{$assignment->benefactorable->actualBenefactor->phone}}">
                                            @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                        <small class="text-danger">{{ $message }}</small>
                                                    </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for=""> الإيميل  </label>
                                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="" value="{{$assignment->benefactorable->actualBenefactor->email}}">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                        <small class="text-danger">{{ $message }}</small>
                                                    </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="">نوع المتبرع   </label>
                                            <select name="bene_type" id="" class="form-control @error('bene_type') is-invalid @enderror">
                                                <option disabled selected> اختر النوع </option>
                                                <option value="1" {{($assignment->benefactorable->actualBenefactor->frequency == 'OneTime')?'selected':''}}> متبرع عادي</option>
                                                <option value="2" {{($assignment->benefactorable->actualBenefactor->frequency == 'EveryMonth')?'selected':''}}>كفالة شهرية</option>
                                                <option value="3" {{($assignment->benefactorable->actualBenefactor->frequency == 'EveryFourMonths')?'selected':''}}>كفالة موسمية (كل أربعة أشهر)</option>
                                                <option value="4" {{($assignment->benefactorable->actualBenefactor->frequency == 'EveryYear')?'selected':''}}>كفالة سنوية</option>
                                            </select>
                                            @error('bene_type')
                                            <span class="invalid-feedback" role="alert">
                                                        <small class="text-danger">{{ $message }}</small>
                                                    </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="">نوع التبرع   </label>
                                            <select name="donation_type" id="" class="form-control @error('donation_type') is-invalid @enderror">
                                                <option disabled selected> اختر النوع </option>
                                                <option value="1" {{($assignment->benefactorable->actualBenefactor->isMoney == 1)?'selected':''}} > تبرع مالي</option>
                                                <option value="2" {{($assignment->benefactorable->actualBenefactor->isAyni == 1)?'selected':''}}> تبرع عيني</option>
                                            </select>
                                            @error('donation_type')
                                            <span class="invalid-feedback" role="alert">
                                                        <small class="text-danger">{{ $message }}</small>
                                                    </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="">قيمة التبرع</label>
                                            <input type="text" name="donation"  class="form-control" value="@if($assignment->benefactorable->actualBenefactor->isMoney == 1){{$assignment->benefactorable->actualBenefactor->moneyAmount}}@elseif($assignment->benefactorable->actualBenefactor->isAyni == 1){{$assignment->benefactorable->actualBenefactor->donation}}@endif">

                                            <small class="text-danger">ملاحظة: إذا كان نوع التبرع الذي أدخلته (تبرع مالي) فلابد من إدخال قيمة مالية بأرقام صحيحة،إذا كان نوع التبرع الذي أدخلته (تبرع عيني) فلابد من إدخال قيمة هذا التبرع مكتوباً </small>
                                            @error('donation')
                                            <span class="invalid-feedback" role="alert">
                                                        <small class="text-danger">{{ $message }}</small>
                                                    </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for=""> تاريخ أول تحصيل</label>
                                            <input type="date" name="date"  class="form-control " value="{{($assignment->benefactorable->actualBenefactor->dueDate)?explode(' ',$assignment->benefactorable->actualBenefactor->dueDate)[0]:''}}">


                                            <small class="text-danger">ملاحظة:التاريخ الذي ستحفظه هنا هو التاريخ الذي سيتم التحصيل فيه دائما ...</small>
                                            @error('date')
                                            <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-3 ">
                                            <label for=""> وقت أول تحصيل</label>
                                            <input class="form-control d-inline" name="time"  type="time" id="example-time-input" value="{{($assignment->benefactorable->actualBenefactor->dueDate)?explode(' ',$assignment->benefactorable->actualBenefactor->dueDate)[1]:''}}"/>
                                            @error('time')
                                            <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="">أسماء الحالات</label>
                                            <textarea  name="cases"  class="form-control"> {{$assignment->benefactorable->actualBenefactor->cases}}</textarea>
                                            @error('cases')
                                            <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>


                                    </div>
                                    <div class="row justify-content-center my-3">
                                        <div class="form-group col-md-4 ">
                                            <input type="submit" class="btn btn-warning form-control" value="تحديث البيانات">
                                        </div>
                                    </div>
                                </form>

                            @endif
                        @endcomponent
                    @endif
{{--                        <a href="" class=" btn btn-warning  my-2 " data-toggle="modal" data-target="#requests"> طلب تعديل البيانات </a>--}}
{{--                        @component('components.modal_xl',['title'=>'طلبات تعديل بيانات المتبرع/ '.$assignment->benefactorable->name ,'color'=>'warning','id'=>'requests'])--}}
{{--                            @if($assignment->requests->count())--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-12 ">--}}
{{--                                        <div class="accordion" id="accordionExample1">--}}
{{--                                            @foreach($assignment->requests as $key=>$item)--}}
{{--                                                <div class="card">--}}
{{--                                                    <div class="card-header bg-info " id="heading-{{$key}}">--}}
{{--                                                        <h5 class="mb-0">--}}
{{--                                                            <button class="btn btn-link  text-lg text-white" type="button" data-toggle="collapse" data-target="#collapse-{{$key}}" aria-expanded="true" aria-controls="collapseOne">--}}
{{--                                                                طلب رقم {{ $key+1 }} تعديل بيانات المتبرع--}}
{{--                                                            </button>--}}
{{--                                                        </h5>--}}
{{--                                                    </div>--}}

{{--                                                    <div id="collapse-{{$key}}" class="collapse {{($key==0)?'show':''}}" aria-labelledby="heading-{{$key}}" data-parent="#accordionExample1">--}}
{{--                                                        <div class="card-body">--}}
{{--                                                            <table class="table text-sm-center">--}}
{{--                                                                <tr>--}}
{{--                                                                    <td></td>--}}
{{--                                                                    <td>الاسم</td>--}}
{{--                                                                    <td>الايميل</td>--}}
{{--                                                                    <td>الهاتف</td>--}}
{{--                                                                    <td>صنف المتبرع </td>--}}
{{--                                                                    <td>نوع التبرع</td>--}}
{{--                                                                    <td>تاريخ أول تحصيل</td>--}}
{{--                                                                    <td rowspan="3"><a href="" class="btn btn-danger">تعديل</a> </td>--}}
{{--                                                                </tr>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td>المحفوظ:</td>--}}
{{--                                                                    <td>{{$item->benefactorable->name??'لا يوجد'}}</td>--}}
{{--                                                                    <td>{{$item->benefactorable->email??'لا يوجد'}}</td>--}}
{{--                                                                    <td>{{$item->benefactorable->phone??'لا يوجد'}}</td>--}}
{{--                                                                    <td>{{($item->benefactorable->isNew == 1)?'لم يسجل':''}}--}}
{{--                                                                        {{ ($item->benefactorable->frequency != 'OneTime')?'كفالة':''}}--}}
{{--                                                                    </td>--}}
{{--                                                                    <td>--}}
{{--                                                                        {{($item->benefactorable->isAyni == 1)?'تبرع عيني':'' }}--}}
{{--                                                                        {{ ($item->benefactorable->isMoney == 1)?'تبرع مادي':''}}--}}
{{--                                                                        {{ ($item->benefactorable->isMoney == 0 && $item->benefactorable->isAyni == 0)?' لم يسجل ':''}}--}}
{{--                                                                    </td>--}}
{{--                                                                    <td>{{$item->benefactorable->dueDate??'لا يوجد'}}</td>--}}
{{--                                                                </tr>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td>التعديل:</td> <td>{{$item->name}}</td>--}}
{{--                                                                    <td>{{$item->email}}</td>--}}
{{--                                                                    <td>{{$item->phone}}</td>--}}
{{--                                                                    <td>--}}
{{--                                                                        {{($item->bene_type== 1)?' متبرع عادي':''}}--}}
{{--                                                                        {{($item->bene_type== 2)?' كفالة':''}}--}}
{{--                                                                    </td>--}}
{{--                                                                    <td>{{($item->type== 1)?'تبرع مادي':''}}--}}
{{--                                                                        {{($item->type== 2)?'تبرع عيني':''}}--}}
{{--                                                                        {{($item->type== 3)?' كفالة':''}}--}}
{{--                                                                        {{$item->type??'لم يسجل'}}</td>--}}
{{--                                                                    <td>{{$item->date}}</td>--}}
{{--                                                                </tr>--}}

{{--                                                            </table>--}}
{{--                                                        </div>--}}

{{--                                                    </div>--}}

{{--                                                </div>--}}
{{--                                            @endforeach--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                            @else--}}
{{--                                <p class="alert alert-secondary">--}}
{{--                                    لم يتم طلب تعديلات لهذه العملية بعد!--}}
{{--                                </p>--}}
{{--                            @endif--}}
{{--                        @endcomponent--}}
                    @if($assignment->done != 1   && $assignment->failed != 1)
                        <a href="" class="btn btn-warning  my-2 " data-toggle="modal" data-target="#call">حفظ مكالمة</a>
                        @component('components.modal_xl',['title'=>'  مكالمة جديدة للمتبرع/ '.$assignment->benefactorable->name ,'color'=>'warning','id'=>'call'])
                            <form action="{{route('storeNewCall',$assignment->id)}}" method="POST">
                                @csrf
                                <div class="row">
                                <!-- Grow In Utility -->
                                <div class="col-lg-12 ">
                                    <div class="card position-relative ">
                                        <div class="card-body text-left">
                                            <h6 class="text-info"> توقيت المكالمة </h6>
                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                    <input class="form-control @error('date') is-invalid @enderror" name="date" type="date"  id="example-month-input"  value="{{old('date')??date('Y-m-d')}}"required >
                                                    @error('date')
                                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                                    @enderror
                                                </div>
                                                <div class="col-md-6">
                                                    <input class="form-control @error('time') is-invalid @enderror" name="time"  type="time" value="{{old('time')??date('H:i:s')}}" id="example-time-input" required>
                                                    @error('time')
                                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                                    @enderror
                                                </div>
                                            </div>


                                            <h6 class="text-info">  حالة الرد </h6>
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <select name="answer_status"  class="form-control @error('answer_status') is-invalid @enderror" required>
                                                        <option value=""  > اختر الحالة </option>
                                                        <option value="تم الرد" {{(old('answer_status') == "تم الرد")? 'selected': ''}}>   تم الرد </option>
                                                        <option value="مشغول" {{(old('answer_status') == "مشغول")? 'selected': ''}}> مشغول </option>
                                                        <option value="مغلق أو غير متاح" {{(old('answer_status') == "مغلق أو غير متاح")? 'selected': ''}}>مغلق أو غير متاح</option>
                                                        <option value="رد شخص آخر" {{(old('answer_status') == "رد شخص آخر")? 'selected': ''}}>رد شخص آخر</option>
                                                        <option value="رقم خطأ" {{(old('answer_status') == "رقم خطأ")? 'selected': ''}}>رقم خطا</option>
                                                    </select>
                                                    @error('answer_status')
                                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                                    @enderror
                                                </div>
                                                <div class="col-md-8">
                                                    <input class="form-control @error('status_more') is-invalid @enderror" name="status_more" value="{{old('phone')}}"  type="text" placeholder="معلومات أخرى عن الرد" >
                                                    @error('status_more')
                                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <h6 class="text-info"> التجاوب  </h6>
                                            <div class="form-group row px-5">

                                                <div class="custom-control custom-radio col-md-3">
                                                    <input type="radio" name="reacting" id="refuse1" {{(old('reacting') == 0)? 'checked': ''}} value="0" class="custom-control-input @error('reacting') is-invalid @enderror">
                                                    <label class="custom-control-label" for="refuse1" >غير متجاوب</label>
                                                </div>
                                                <div class="custom-control custom-radio col-md-3 px-5">
                                                    <input type="radio" name="reacting"id="accept1" {{(old('reacting') == 1)? 'checked': ''}}value="1" class="custom-control-input @error('reacting') is-invalid @enderror">
                                                    <label class="custom-control-label" for="accept1" >متجاوب</label>
                                                </div>

                                                @error('reacting')
                                                <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                                @enderror
                                            </div>

                                            <div class="form-group row " >
                                                <div class="col-md-3">
                                                    <label for="example-month-input" class="col-10 col-form-label"> تقييم المكالمة</label>

                                                    <select name="call_rate" class="form-control @error('call_rate') is-invalid @enderror accept" required autofocus >
                                                        <option disabled selected >اختر</option>
                                                        @for ($i = 0; $i < 11; $i++)
                                                            <option  value="{{$i}}" @if (old('call_rate') == $i) selected @endif >{{$i}}</option>
                                                        @endfor
                                                    </select>
                                                    @error('call_rate')
                                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                                    @enderror

                                                </div>
                                                <div class="col-md-9">
                                                    <label for="example-month-input" class="col-10 col-form-label"> أهم سؤال من المتبرع </label>

                                                    <input type="text" class="form-control @error('question') is-invalid @enderror" value="{{old('question')}}" name="question" >
                                                    @error('question')
                                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                                    @enderror

                                                </div>
                                            </div>
                                            <div class="form-group row " >
                                                <div class="col-md-12">
                                                    <label for="example-month-input" class="col-12 col-form-label"> ملخص المكالمة  </label>

                                                    <textarea name="call_samary" class="form-control  @error('call_samary') is-invalid @enderror">{{old('call_samary')}}</textarea>
                                                    @error('call_samary')
                                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                                    @enderror

                                                </div>
                                                <div class="col-md-12">
                                                    <label for="example-month-input" class="col-12 col-form-label">ملاحظات</label>
                                                    <textarea name="notes" class="form-control @error('notes') is-invalid @enderror">{{old('notes')}}</textarea>
                                                    @error('notes')
                                                    <span class="invalid-feedback" role="alert">
                                                <small class="text-danger">{{ $message }}</small>
                                            </span>
                                                    @enderror
                                                </div>

                                            </div>


                                            <div class="row justify-content-md-center">
                                                <div class="form-group col-md-4 ">
                                                    <input type="submit" class="btn btn-warning form-control" value="حفظ المكالمة  ">
                                                </div>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                            <small class="text-danger">{{ $message }}</small>
                                        </span>
                                                @enderror
                                            </div>


                                        </div>
                                    </div>

                                </div>

                            </div>
                            </form>
                        @endcomponent
                    @endif

                        <a href="" class=" btn btn-warning  my-2 " data-toggle="modal" data-target="#calls">المكالمات التي تمت  </a>
                        @component('components.modal_xl',['title'=>'المكالمات التي تمت مع المتبرع/ '.$assignment->benefactorable->name ,'color'=>'warning','id'=>'calls'])
                            @component('components.table',['title'=>'المكالمات المحفوظة لهذه العملية','color'=>'success'])
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>الكود</th>
                                    <th>التوقيت</th>
                                    <th>حالة الرد</th>
                                    <th>عرض</th>
                                    @if( $assignment->done != 1  && $assignment->failed != 1 )
                                        <th>تعديل</th>
                                    @endif
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>الكود</th>
                                    <th>التوقيت</th>
                                    <th>حالة الرد</th>
                                    <th>عرض</th>
                                    @if( $assignment->done != 1  && $assignment->failed != 1 )
                                    <th>تعديل</th>
                                    @endif
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($assignment->calls as  $key=>$call)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>c-{{$call->id}}</td>
                                        <td>{{getDateTime($call->date.$call->time)}}</td>
                                        <td>{{$call->answer_status}}</td>
                                        <td class="text-center" > <a class="text-primary h5 " data-toggle="modal" data-target="#call_view_{{$key}}"> <i class="fas fa-eye"></i> </a> </td>
                                        @if( $assignment->done != 1  && $assignment->failed != 1 )
                                        <td class="text-center" > <a class="text-danger h5 " data-toggle="modal" data-target="#call_edit_{{$key}}"> <i class="fas fa-pencil-alt"></i> </a> </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            @endcomponent
                        @endcomponent
                </div>
                    @if($assignment->done != 1   && $assignment->failed != 1)
                        <div class="col-md-12 mt-5   px-3 justify-content-center text-center">

                            <a href="" class=" btn btn-primary form-control  my-2 " data-toggle="modal" data-target="#done">إرسال كعملية ناجحة </a>
                            @component('components.modal',['id'=>'done','title'=>'تأكيد الإرسال كعملية ناجحة','color'=>'primary'])
                                @if($assignment->calls()->accept()->count())
                                <form action="{{route('newAssignment.done',$assignment)}}" method="POST">
                                    <p>تأكيد إرسال العملية لرئيس قسم الكول سنتر </p>
                                    @csrf
                                    <input type="submit"  class="btn btn-primary my-2  form-control" value="تأكيد" >
                                </form>
                                @else
                                    <div class="alert alert-success py-4 bx-2 text-left" >
                                        <h4 class="h4 mb-4">يشترط لإرسال العملية للمدير مرة كعملية متجاوبة ما يلي:</h4>
                                        <ol>
                                            <li>أن تقوم بحفظ مكالمات لهذه العملية  </li>
                                            <li>أن تسجل مكالمة متجاوبة واحدة على الأقل من هذه المكالمات  </li>
                                        </ol>
                                    </div>
                                @endif
                            @endcomponent
                            <a href="" class=" btn btn-danger form-control  my-2 " data-toggle="modal" data-target="#failed">عملية غير مستجيبة </a>
                        @component('components.modal',['id'=>'failed','title'=>'تأكيد الإرسال لعدم الاستجابة','color'=>'danger'])
                                @if($assignment->calls()->failed()->count())
                                    <form action="{{route('newAssignment.failed',$assignment)}}" method="POST">
                                    <p>تأكيد إرسال العملية لرئيس قسم الكول سنتر كعملية لم تستجب، أو فشل التواصل معها  </p>
                                    @csrf
                                    <input type="submit"  class="btn btn-danger my-2  form-control" value="تأكيد" >
                                </form>
                                @else
                                    <div class="alert alert-danger py-4 bx-2 text-left" >
                                        <h4 class="h4 mb-4">يشترط لإرسال العملية للمدير كعملية غير مستجيبة ما يلي:</h4>
                                        <ol>
                                            <li>أن تقوم بحفظ مكالمات لهذه العملية  </li>
                                            <li>أن تسجل مكالمة متجاوبة واحدة على الأقل من هذه المكالمات  </li>
                                        </ol>
                                    </div>
                                @endif
                            @endcomponent



                        </div>
                    @endif
                </div>
            @endcomponent

  </div>
  <!-- /.container-fluid -->

@if(count($assignment->calls ))
    @foreach($assignment->calls as  $key=>$call)
        @if( $assignment->done != 1  && $assignment->failed != 1 )
{{--        update call--}}
        <form action="{{route('updateNewCall',$call->id)}}" method="POST">
            @csrf
            @component('components.modal_xl',['title'=>'تعديل بيانات مكالمة بكود: '.$call->id,'id'=>'call_edit_'.$key,'color'=>'danger'])
            <div class="row">
                <!-- Grow In Utility -->
                <div class="col-lg-12 ">
                    <div class="card position-relative ">
                        <div class="card-body ">
                            <h6 class="text-info"> توقيت المكالمة </h6>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input class="form-control @error('date') is-invalid @enderror" name="date" type="date"  id="example-month-input"  value="{{old('date')??$call->date}}"required >
                                    @error('date')
                                    <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <input class="form-control @error('time') is-invalid @enderror" name="time"  type="time" value="{{old('time')??$call->time}}" id="example-time-input" required>
                                    @error('time')
                                    <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                                    @enderror
                                </div>
                            </div>


                            <h6 class="text-info">  حالة الرد </h6>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <select name="answer_status"  class="form-control @error('answer_status') is-invalid @enderror" required>
                                        <option value="1" disabled selected> اختر الحالة </option>
                                        <option value="تم الرد" {{($call->answer_status == "تم الرد")? 'selected': ''}}>   تم الرد </option>
                                        <option value="مشغول" {{($call->answer_status == "مشغول")? 'selected': ''}}> مشغول </option>
                                        <option value="مغلق أو غير متاح" {{($call->answer_status == "مغلق أو غير متاح")? 'selected': ''}}>مغلق أو غير متاح</option>
                                        <option value="رد شخص آخر" {{($call->answer_status == "رد شخص آخر")? 'selected': ''}}>رد شخص آخر</option>
                                        <option value="رقم خطأ" {{($call->answer_status == "رقم خطأ")? 'selected': ''}}>رقم خطا</option>
                                    </select>
                                    @error('answer_status')
                                    <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                                    @enderror
                                </div>
                                <div class="col-md-8">
                                    <input class="form-control @error('status_more') is-invalid @enderror" name="status_more" value="{{old('status_more')??$call->status_more }}"  type="text" placeholder="معلومات أخرى عن الرد" >
                                    @error('status_more')
                                    <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                                    @enderror
                                </div>
                            </div>

                            <h6 class="text-info"> التجاوب  </h6>
                            <div class="form-group row px-5">
                                <div class="custom-control custom-radio col-md-3">
                                    <input type="radio" name="reacting" id="refuse-{{$key}}" {{($call->reacting == 'غير متجاوب')? 'checked': ''}} value="0" class="custom-control-input @error('reacting') is-invalid @enderror">
                                    <label class="custom-control-label" for="refuse-{{$key}}" >غير متجاوب</label>
                                </div>
                                <div class="custom-control custom-radio col-md-3 px-5">
                                    <input type="radio" name="reacting"id="accept-{{$key}}" {{($call->reacting == 'متجاوب')? 'checked': ''}} value="1" class="custom-control-input @error('reacting') is-invalid @enderror">
                                    <label class="custom-control-label" for="accept-{{$key}}" >متجاوب</label>
                                </div>


                                @error('reacting')
                                <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                                @enderror
                            </div>

                            <div class="form-group row " >
                                <div class="col-md-3">
                                    <label for="example-month-input" class="col-10 col-form-label"> تقييم المكالمة</label>

                                    <select name="call_rate" class="form-control @error('call_rate') is-invalid @enderror accept" required autofocus >
                                        <option disabled selected >اختر</option>
                                        @for ($i = 0; $i < 11; $i++)
                                            <option  value="{{$i}}" @if ($call->call_rate == $i) selected @endif >{{$i}}</option>
                                        @endfor
                                    </select>
                                    @error('call_rate')
                                    <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                                    @enderror

                                </div>
                                <div class="col-md-9">
                                    <label for="example-month-input" class="col-10 col-form-label"> أهم سؤال من المتبرع </label>

                                    <input type="text" class="form-control @error('question') is-invalid @enderror" value="{{old('question')??$call->question}}" name="question" >
                                    @error('question')
                                    <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                                    @enderror

                                </div>
                            </div>
                            <div class="form-group row " >
                                <div class="col-md-12">
                                    <label for="example-month-input" class="col-12 col-form-label"> ملخص المكالمة  </label>

                                    <textarea name="call_samary" class="form-control  @error('call_samary') is-invalid @enderror">{{old('call_samary')??$call->call_samary}}</textarea>
                                    @error('call_samary')
                                    <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                                    @enderror

                                </div>
                                <div class="col-md-12">
                                    <label for="example-month-input" class="col-12 col-form-label">ملاحظات</label>
                                    <textarea name="notes" class="form-control @error('notes') is-invalid @enderror">{{old('notes')??$call->notes}}</textarea>
                                    @error('notes')
                                    <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                                    @enderror
                                </div>

                            </div>


                            <div class="row justify-content-md-center">
                                <div class="form-group col-md-4 ">
                                    <input type="submit" class="btn btn-danger form-control" value="تحديث المكالمة  ">
                                </div>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                                @enderror
                            </div>


                        </div>
                    </div>

                </div>


            </div>
           @endcomponent
        </form>
{{--        stop update call--}}
        @endif
{{--        view call--}}
        @component('components.modal_xl',['title'=>'يانات المكالمة ذات الكود: '.$call->id,'id'=>'call_view_'.$key,'color'=>'primary'])
        <div class="row">
            <!-- Grow In Utility -->
            <div class="col-lg-12 ">
                <div class="card position-relative ">
                    <div class="card-body">
                        <table class="table table-bordered">

                            <tr>
                                <th>توقيت المكالمة</th>
                                <td>
                                    {{date('H:i', strtotime($call->time)) .
                                    ' | '. date('d', strtotime($call->date)) .
                                    ','. config('months.month.'.date('m', strtotime($call->date))) .
                                    ','.date('Y', strtotime($call->date)) }}
                                </td>
                            </tr>

                            <tr>
                                <th>حالة الرد</th>
                                <td> {{$call->answer_status}} <small class="text-secondary"> {{$call->status_more}} </small></td>
                            </tr>

                            <tr>
                                <th>الجاوب</th>
                                <td>{{$call->reacting}}</td>
                            </tr>

                            <tr>
                                <th>تقييم المكالمة</th>
                                <td>{{$call->call_rate??'0'}}  / 10</td>
                            </tr>

                            <tr>
                                <th>أهم سؤال</th>
                                <td>{{$call->question??'لا يوجد'}}</td>
                            </tr>

                            <tr>
                                <th>ملخص المكالمة</th>
                                <td>{{$call->call_samary??'لا يوجد'}}</td>
                            </tr>

                            <tr>
                                <th>ملاحظات</th>
                                <td>{{$call->notes??'لا يوجد'}}</td>
                            </tr>

                        </table>

                    </div>
                </div>

            </div>
        </div>
        @endcomponent

{{--        end view call--}}
    @endforeach
@endif
@if($assignment->done != 1)
<form action="{{route('newBenefactors.update',$assignment->id)}}" method="POST">
    @csrf
    <div class="modal fade  " id="choose" tabindex="0" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered ">
            <div class="modal-content ">
                <div class="modal-header">
                    <h4 >بيانات طلب تبرع جديد</h4>
                </div>
                <div class="modal-body">
                    <h6 >العنوان </h6>
                    <div class="row justify-content-center">
                        <div class="form-group col-md-12">
                            <textarea class="form-control @error('address') is-invalid @enderror" name="address">{{$assignment->benefactorable->address}}</textarea>
                            @error('address')
                            <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="form-group col-md-12">
                            <label for="">وسيلة الدفع  </label>
                            <select name="payment_method" id="" class="form-control @error('payment_method') is-invalid @enderror" required>
                                <option value="مندوب الجمعية" >  مندوب الجمعية</option>
                                <option value=" فودافون كاش" >  فودافون كاش</option>
                                <option value="حساب بنكي" >حساب بنكي</option>
                            </select>
                            @error('payment_method')
                            <span class="invalid-feedback" role="alert">
                                    <small class="text-danger">{{ $message }}</small>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-12">
                        <label for="">رقم بحث الحالة</label>
                        <input type="text" name="case_no" id="" class="form-control @error('case_no') is-invalid @enderror" value="{{old('case_no')}}" required>
                        @error('case_no')
                        <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                        @enderror
                    </div>
                    </div>

                    <h6 > توقيت التحصيل </h6>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <input class="form-control" name="date" type="date"  id="example-month-input"  value="{{$assignment->benefactorable->dueDate}}"required>
                        </div>
                        @error('date')
                        <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                        @enderror
                        <div class="col-md-6">
                            <input class="form-control" name="time"  type="time" value="12:00" id="example-time-input" required>
                        </div>
                        @error('time')
                        <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                        @enderror
                    </div>
                    <h6 > ملاحظات أخرى</h6>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <textarea class="form-control" name="notes"  required>{{old('notes')}} </textarea>
                        </div>
                        @error('notes')
                        <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                        @enderror
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info px-4" >حفظ الطلب</button>

                        </div>
                    </div>

                </div>
                <div class="modal-footer justify-content-center">
                </div>
            </div>
        </div>
    </div>
</form>
@endif
@stop

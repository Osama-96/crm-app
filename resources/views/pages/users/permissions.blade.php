@extends('pages.layouts.app')
@section('css')
<link href="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@stop
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
    <!-- Content Row -->
    <div class="row mb-3">
      <!-- Grow In Utility -->
        <div class="col-lg-3 ">
            <a  class="btn btn-primary text-white" data-toggle="modal" data-target="#new_permission"> إنشاء صلاحية جديدة</a>
        </div>
    </div>

      <div class="row">
      <!-- Grow In Utility -->
      <div class="col-lg-12 ">
          <div class="card position-relative border-info">
              <div class="card-header bg-gradient-info py-3 text-center">
                  <h3 class="mb-1 text-white"> جميع الصلاحيات </h3>
            </div>
            <div class="card-body ">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>الصلاحية</th>
                            <th>الاسم المعروض</th>
                            <th>الوصف</th>
                            <th>تعديل</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>الصلاحية</th>
                            <th>الاسم المعروض</th>
                            <th>الوصف</th>
                            <th>تعديل</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($permissions as $key=>$item)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->display_name}}</td>
                                <td style="max-width: 30%">{{$item->description}}</td>
                                <td class="text-center" > <a class="text-primary h3 "  data-toggle="modal" data-target="#edit-{{$key}}"> <i class="fas fa-pencil-alt"></i> </a> </td>
                            </tr>

                            <!--Edit Modal -->
                            <div class="modal fade" id="edit-{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">عملية تعديل احدى الصلاحيات</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                        <form action="{{route('permissions.update',['permission'=>$item->id])}}" enctype="multipart/form-data" method="POST">
                                            <div class="modal-body">
                                                @method('PUT')
                                                @csrf
                                                <div class="form-group">
                                                    <label for="">الاسم:</label>
                                                    <input type="text" class="form-control" name="name" id=""  value="{{$item->name}}" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">الاسم الذي يتم عرضه:</label>
                                                    <input type="text" class="form-control" name="display_name" id="" value="{{$item->display_name}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">الوصف:</label>
                                                    <textarea name="description" class="form-control"> {{$item->description}}</textarea>
                                                </div>
                                                <div class="modal-footer justify-content-center">
                                                    <input type="submit" class="btn btn-primary "  value="تحديث البيانات" >
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

      </div>

    </div>

  </div>
  <!-- /.container-fluid -->
<div class="modal fade" id="new_permission" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">إنشاء صلاحية جديدة</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form action="{{route('permissions.store')}}" enctype="multipart/form-data" method="POST">
                <div class="modal-body">
                    @method('POST')
                    @csrf
                    <div class="form-group">
                        <label for="">الاسم:</label>
                        <input type="text" class="form-control" name="name" id="" value="{{old('name')}}">
                    </div>
                    <div class="form-group">
                        <label for="">الاسم الذي يتم عرضه:</label>
                        <input type="text" class="form-control" name="display_name" id="" value="{{old('display_name')}}">
                    </div>
                    <div class="form-group">
                        <label for="">الوصف:</label>
                        <textarea name="description" class="form-control"> {{old('description')}}</textarea>
                    </div>

                </div>
                <div class="modal-footer justify-content-center">
                    <input type="submit" class="btn btn-primary "  value="حفظ الصلاحية" >
                </div>


            </form>
        </div>
    </div>
</div>

@stop
@section('js')
    <!-- Page level plugins -->
    <script src="{{asset('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{asset('assets/js/demo/datatables-demo.js')}}"></script>
@stop

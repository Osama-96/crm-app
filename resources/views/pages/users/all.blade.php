@extends('pages.layouts.app')
@section('css')
<link href="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@stop
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
    <!-- Content Row -->

      <div class="row">
      <!-- Grow In Utility -->
      <div class="col-lg-12 ">
        <div class="card position-relative border-info">
            <div class="card-header bg-gradient-info py-3 text-center">
                <h3 class="mb-1 text-white"> جميع الموظفين </h3>
            </div>
            <div class="card-body ">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>الاسم</th>
                            <th>الايميل</th>
                            <th>الدور</th>
                            <th>تعديل الأدوار</th>
                            <th> الصلاحيات</th>
                            <th>تعديل الصلاحيات</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>الاسم</th>
                            <th>الايميل</th>
                            <th>الدور</th>
                            <th>تعديل الأدوار</th>
                            <th> الصلاحيات</th>
                            <th>تعديل الصلاحيات</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($users as $key=>$user)

                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td style="width: 20%">
                                    @foreach($user->roles as $role)
                                    {!!'<span class="badge badge-info">'. $role->display_name .'</span>'!!}
                                        <br>
                                    @endforeach
                                </td>
                                <td class="text-center" > <a class="text-primary h3 "  data-toggle="modal" data-target="#role-{{$user->id}}"> <i class="fas fa-pencil-alt"></i> </a> </td>
                                <td style="width: 20%">
                                    @foreach(\App\Permission::all() as $key=> $permission)
                                        @if($user->can($permission->name))
                                        {!!'<span class="badge badge-info">'.$permission->display_name .'</span>'!!}
                                            <br>
                                        @endif
                                    @endforeach

                                </td>
                                <td class="text-center" > <a class="text-primary h3 "  data-toggle="modal" data-target="#edit-permissions-{{$user->id}}"> <i class="fas fa-pencil-alt"></i> </a> </td>
                            </tr>

                            <!--Edit role Modal -->
                            <div class="modal fade " id="role-{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModal" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">عملية تعديل البيانات</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                        <form action="{{route('users.roles.update',['user'=>$user->id])}}" enctype="multipart/form-data" method="POST">
                                            <div class="modal-body">
                                            @method('POST')
                                            @csrf
                                            <div class="form-group">
                                                <label for="">الاسم:</label>
                                                <input type="text" class="form-control" name="" id="" value="{{$user->name}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="">الايميل:</label>
                                                <input type="email" class="form-control" name="" id="" value="{{$user->email}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="">الأدوار</label>
                                                @if(auth()->user()->hasRole('admin'))
                                                <div class="checkbox input-group-lg">
                                                    <lable>
                                                        <input type="checkbox" name="roles[]" {{ $user->hasRole('admin')? 'checked' : '' }} value="admin">  أدمن
                                                    </lable>
                                                </div>
                                                @endif
                                                <div class="checkbox input-group-lg">
                                                    <lable>
                                                        <input type="checkbox" name="roles[]" {{ $user->hasRole('manager')? 'checked' : '' }} value="manager">     مدير تنفيذي
                                                    </lable>
                                                </div>
                                                <div class="checkbox input-group-lg">
                                                    <lable>
                                                        <input type="checkbox" name="roles[]" {{ $user->hasRole('marketing_officer')? 'checked' : '' }} value="marketing_officer"> مسئول تسويق
                                                    </lable>
                                                </div>
                                                <div class="checkbox input-group-lg">
                                                    <lable>
                                                        <input type="checkbox" name="roles[]" {{ $user->hasRole('call_manager')? 'checked' : '' }} value="call_manager"> مدير قسم الكول سنتر
                                                    </lable>
                                                </div>
                                                <div class="checkbox input-group-lg">
                                                    <lable>
                                                        <input type="checkbox" name="roles[]" {{ $user->hasRole('call_emp')? 'checked' : '' }} value="call_emp"> موظف بقسم الكول سنتر
                                                    </lable>
                                                </div>
                                                <div class="checkbox input-group-lg">
                                                    <lable>
                                                        <input type="checkbox" name="roles[]" {{ $user->hasRole('account_manager')? 'checked' : '' }} value="account_manager"> مسئول قسم التحصيل
                                                    </lable>
                                                </div>
                                                <div class="checkbox input-group-lg">
                                                    <lable>
                                                        <input type="checkbox" name="roles[]" {{ $user->hasRole('account_supervisor')? 'checked' : '' }} value="account_supervisor"> مشرف بقسم التحصيل
                                                    </lable>
                                                </div>
                                                <div class="checkbox input-group-lg">
                                                    <lable>
                                                        <input type="checkbox" name="roles[]" {{ $user->hasRole('accountant')? 'checked' : '' }} value="accountant"> محصل
                                                    </lable>
                                                </div>
                                                <div class="checkbox">
                                                    <lable>
                                                        <input type="checkbox" name="roles[]" {{ $user->hasRole('user')? 'checked' : '' }} value="user"> مستخدم عادي
                                                    </lable>
                                                </div>
                                            </div>
                                            </div>
                                            <div class="modal-footer justify-content-center">
                                                <input type="submit" class="btn btn-primary "  value="تحديث البيانات" >
                                            </div>


                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!--Edit Permissions Modal -->
                            <div class="modal fade bd-example-modal-xl" id="edit-permissions-{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">عملية تعديل البيانات</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                        <form action="{{route('users.permissions.update',['user'=>$user->id])}}" enctype="multipart/form-data" method="POST">
                                            <div class="modal-body">
                                            @method('POST')
                                            @csrf
                                            <div class="form-group">
                                                <label for="">الاسم:</label>
                                                <input type="text" class="form-control" name="" id="" value="{{$user->name}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="">الايميل:</label>
                                                <input type="email" class="form-control" name="" id="" value="{{$user->email}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="">الصلاحيات</label>
                                                <br>
                                                <small class="text-info"> *لا يمكنك حذف الصلاحيات التي يقوم بها هذا المستخدم التي أخذها من صلاحيات الدور الخاص به </small>
                                                @foreach(\App\Permission::all() as $key=>$item)
                                                <div class="checkbox input-group-lg">
                                                    <lable>
                                                        <input type="checkbox" name="permissions[]" {{ $user->can($item->name)? 'checked' : '' }} value="{{$item->id}}"> {{$item->display_name}}
                                                    </lable>
                                                </div>
                                                @endforeach
                                            </div>
                                            </div>
                                            <div class="modal-footer justify-content-center">
                                                <input type="submit" class="btn btn-primary "  value="تحديث البيانات" >
                                            </div>


                                        </form>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

      </div>

    </div>

  </div>
  <!-- /.container-fluid -->

@stop
@section('js')
    <!-- Page level plugins -->
    <script src="{{asset('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{asset('assets/js/demo/datatables-demo.js')}}"></script>
@stop

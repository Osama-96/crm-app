@extends('pages.layouts.app')
@section('css')
<link href="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@stop
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
    <!-- Content Row -->
      <div class="row">
      <!-- Grow In Utility -->
      <div class="col-lg-12 ">
          <div class="card position-relative border-info">
              <div class="card-header bg-gradient-info py-3 text-center">
                  <h3 class="mb-1 text-white"> جميع الأدوار </h3>
            </div>
            <div class="card-body ">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>الدور</th>
                            <th>الاسم المعروض</th>
                            <th>الوصف</th>
                            <th>الصلاحيات</th>
                            <th>تعديل</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>الدور</th>
                            <th>الاسم المعروض</th>
                            <th>الوصف</th>
                            <th>الصلاحيات</th>
                            <th>تعديل</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($roles as $key=>$item)
                            @if($item->name != 'admin')
                            <tr>
                                <td>{{$key+1}}</td>
                                <td style="width: 10%">{{$item->name}}</td>
                                <td style="width: 20%">{{ $item->display_name}}</td>
                                <td style="width: 20%">{{$item->description}}</td>
                                <td>
                                    @foreach($item->permissions as $permission)
                                        {!!'<span class="badge badge-info">'.$permission->display_name .'</span>'!!}
                                        <br>
                                    @endforeach
                                </td>
                                <td class="text-center" > <a class="text-primary h3 "  data-toggle="modal" data-target="#edit-{{$key}}"> <i class="fas fa-pencil-alt"></i> </a> </td>
                            </tr>

                                    <!--Edit Modal -->
                            <div class="modal fade bd-example-modal-xl" id="edit-{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">عملية تعديل أحد الأدوار </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                        <form action="{{route('roles.update',['role'=>$item->id])}}" enctype="multipart/form-data" method="POST">
                                            <div class="modal-body">
                                                @method('PUT')
                                                @csrf
                                                <div class="form-group">
                                                    <label for="">الاسم:</label>
                                                    <input type="text" class="form-control" name="name" id=""  value="{{$item->name}}" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">الاسم الذي يتم عرضه:</label>
                                                    <input type="text" class="form-control" name="display_name" id="" value="{{ $item->display_name}}" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">الصلاحيات</label>
                                                    <ul class="list-group">
                                                        <li class="list-group-item list-group-item-primary">
                                                            @foreach(\App\Permission::all() as $key=>$permission)
                                                                <div class="checkbox p-1 input-group-lg">
                                                                    <lable for="#{{$key}}">
                                                                        <span class="pr-4">{{$key+1}}-</span><input id="{{$key}}"  type="checkbox" name="permissions[]" {{ $item->hasPermission($permission->name)? 'checked' : '' }} value="{{$permission->id}}"> {{$permission->display_name}}
                                                                    </lable>
                                                                </div>
                                                            @endforeach
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="modal-footer justify-content-center">
                                                    <input type="submit" class="btn btn-primary "  value="تحديث البيانات" >
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

      </div>

    </div>

  </div>

@stop
@section('js')
    <!-- Page level plugins -->
    <script src="{{asset('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{asset('assets/js/demo/datatables-demo.js')}}"></script>
@stop

@extends('pages.layouts.app')
@section('css')
    <link href="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@stop
@section('content')

    <!-- Begin Page Content -->
    <div class="container-fluid  ">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-white">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
                <li class="breadcrumb-item"><a href="{{route('ads.index')}}">جميع الاعلانات</a></li>
                <li class="breadcrumb-item active" aria-current="page">الاعلان رقم {{$ad->id}}</li>
            </ol>
        </nav>
        <!-- Content Row -->
        <div class="row">
            <!-- Grow In Utility -->
            <div class="col-lg-12 ">
                @component('components.card',['color'=>'primary','title'=>' الاعلان رقم'.$ad->id])
                    <div class=" row " >
                        <div class="col-md-7 table-responsive ">
                            <table class="table table-bordered r">
                                <tbody>
                                <tr>
                                    <th> تاريخ النشر :</th> <td>{{$ad->starts_at}}</td>
                                </tr>

                                <tr>
                                    <th>تاريخ الانتهاء</th> <td>{{$ad->ends_at}}</td>
                                </tr>
                                <tr>
                                    <th>النوع</th> <td>{{$ad->type}}</td>
                                </tr>
                                <tr>
                                    <th>التكلفة</th> <td>{{$ad->cost}} ج.م</td>
                                </tr>
                                <tr>
                                    <th>عدد المتبرعين</th> <td>---</td>
                                </tr>

                                </tbody>
                            </table>
                            <a href="" class="btn btn-primary m-1"  data-toggle="modal" data-target="#edit"> تحديث البيانات</a>

                        </div>
                        <div class="col-md-5">
                            <img src="{{asset($ad->file)}}" class="img-fluid img-thumbnail rounded" alt="" srcset="">
                            <a href="{{$ad->link}}" target="_blank" class="btn btn-link"> <i class="fa fa-share"></i> اتبع الرابط</a>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                        </div>
{{--                        <div class="col-md-2">--}}
{{--                            <a href="" class="btn btn-danger m-1"  data-toggle="modal" data-target="#delete">  حذف الاعلان</a>--}}
{{--                        </div>--}}
                    </div>
                @endcomponent


            </div>
        </div>
    </div>

    <!--Edit Modal -->
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">عملية تعديل البيانات</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('ads.update',['ad'=>$ad->id])}}" enctype="multipart/form-data" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="row justify-content-center" >
                        <div class="form-group col-md-12 p-5">
                            <label for=""> نوع الإعلان </label>
                            <select name="type" id="" class="form-control @error('type') is-invalid @enderror">
                                <option disabled selected > اختر نوعاً </option>
                                <option value="1" {{($ad->type == 'محتوى مرئي')? 'selected' : ''}}> محتوى مرئي </option>
                                <option value="2" {{($ad->type == 'محتوى مكتوب')? 'selected' : '' }}>  محتوى مكتوب</option>
                            </select>
                            @error('type')
                            <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                                @enderror

                            <label for="" class="mt-3"> التكلفة بالجنيه المصري  </label>
                            <input type="number" class="form-control @error('cost') is-invalid @enderror" name="cost" min="0" value="{{$ad->cost}}" max="10000" id="">
                            @error('cost')
                            <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                            @enderror



                            <h6 class="mx-1 text-s text-info mt-3">الخطة الزمنية</h6>


                            <label for="" class="mt-2">  تاريخ النشر </label>
                            <input type="date" class="form-control  @error('starts_at') is-invalid @enderror" name="starts_at"  value="{{ $ad->starts_at}}" id="">
                            @error('starts_at')
                            <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                            @enderror

                            <label for="" class="mt-2"> تاريخ انتهاء النشر  </label>
                            <input type="date" class="form-control @error('ends_at') is-invalid @enderror" name="ends_at"  value="{{ $ad->ends_at }}" id="">
                            @error('ends_at')
                            <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                            @enderror

                            <h6 class="mx-1 text-s text-info mt-2">النطاق الجغرافي </h6>



                            <label for="" class="mt-2"> المنطقة  </label>
                            <select name="place" id="" class="form-control">
                                <option disabled selected> اختر منطقة </option>
                                @foreach($locations as $item)
                                    <option value="{{$item->id}}" {{($ad->place == $item->id)? 'selected' : '' }}  > {{$item->name}}</option>
                                @endforeach
                            </select>
                            @error('place')
                            <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                            @enderror

                            <h6 class="mx-1 text-s text-info mt-3">توثيق الإعلان </h6>

                            <label for=""> رابط الإعلان </label>
                            <input type="text" class="form-control @error('link') is-invalid @enderror" name="link" value="{{ $ad->link}}" id="">
                            @error('link')
                            <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                            </span>
                            @enderror
                            <div class="form-group mt-4">
                                <img src="{{asset($ad->file)}}"  style="width:100%; height:400px;" id="preview">
                                <input type="file" name="file" id="img" class="form-control-file @error('file') is-invalid @enderror" value="{{ old('file') }}">
                                @error('file')
                                <span class="invalid-feedback" role="alert">
                                <small class="text-danger">{{ $message }}</small>
                                </span>
                                @enderror
                            </div>

                            <input type="submit" value="حفظ الإعلان" class="form-control btn-primary">
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>

    {{--    #delete Modal--}}
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">تأكيد العملية</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('ads.destroy',['ad'=> $ad->id])}}" method="Post">
                    <div class="modal-body">
                        @method('DELETE')
                        @csrf
                        أدخل الباسورد لتأكيد عملية الحذف
                        <input type="password" name="password" id="" class="form-control">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">الغاء</button>
                        <button type="submit"  class="btn btn-danger">حذف</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop

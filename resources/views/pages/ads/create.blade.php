@extends('pages.layouts.app')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
            <li class="breadcrumb-item active" aria-current="page">إنشاء إعلان جديد</li>
        </ol>
    </nav>
    <!-- Content Row -->
    <div class="row">
      <!-- Grow In Utility -->
      <div class="col-lg-12 ">
          @component('components.card',['color'=>'primary','title'=>'إنشاء إعلان جديد'])
            <form action="{{route('ads.store')}}" enctype="multipart/form-data" method="POST">
                @csrf
                <div class="row justify-content-center" >
                    <div class="form-group col-md-6 ">
                        <label for=""> نوع الإعلان </label>
                        <select name="type" id="" class="form-control @error('type') is-invalid @enderror">
                            <option disabled selected > اختر نوعاً </option>
                            <option value="1" {{(old('type') == 1)? 'selected' : '' }}> محتوى مرئي </option>
                            <option value="2" {{(old('type') == 2)? 'selected' : '' }}>  محتوى مكتوب</option>
                        </select>
                        @error('type')
                        <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                        @enderror

                        <label for="" class="mt-3"> التكلفة  </label>
                        <div class="input-group mb-3">
                            <input type="number" id class="form-control @error('cost') is-invalid @enderror" name="cost" min="0" value="{{ old('cost') }}" max="10000" id="inputGroupSelect01">
                            <div class="input-group-prepend">
                                <label class="input-group-text text-primary" for="inputGroupSelect01">جنيهاً مصرياً </label>
                            </div>
                        </div>
                        @error('cost')
                        <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                        @enderror



                        <h6 class="mx-1 text-s text-info mt-3">الخطة الزمنية</h6>


                        <label for="" class="mt-2">  تاريخ النشر </label>
                        <input type="date" class="form-control  @error('starts_at') is-invalid @enderror" name="starts_at"  value="{{ old('starts_at') }}" id="">
                        @error('starts_at')
                        <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                        @enderror

                        <label for="" class="mt-2"> تاريخ انتهاء النشر  </label>
                        <input type="date" class="form-control @error('ends_at') is-invalid @enderror" name="ends_at"  value="{{ old('ends_at') }}" id="">
                        @error('ends_at')
                        <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                        @enderror

                        <h6 class="mx-1 text-s text-info mt-2">النطاق الجغرافي </h6>



                        <label for="" class="mt-2"> المنطقة  </label>
                        <select name="place" id="" class="form-control">
                            <option disabled selected> اختر منطقة </option>
                            @foreach($locations as $item)
                            <option value="{{$item->id}}" {{(old('place') == $item->id)? 'selected' : '' }}  > {{$item->name}}</option>
                            @endforeach
                        </select>
                        @error('place')
                        <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <h6 class="mx-1 text-s text-info mt-3">توثيق الإعلان </h6>

                        <label for=""> رابط الإعلان </label>
                        <input type="text" class="form-control @error('link') is-invalid @enderror" name="link" value="{{ old('link') }}" id="">
                        @error('link')
                        <span class="invalid-feedback" role="alert">
                            <small class="text-danger">{{ $message }}</small>
                        </span>
                        @enderror
                        <div class="form-group mt-4">
                        <img src=""  style="width:100%; height:400px;" id="preview">
                        <input type="file" name="file" id="img" class="form-control-file @error('file') is-invalid @enderror" value="{{ old('file') }}">
                        @error('file')
                        <span class="invalid-feedback" role="alert">
                        <small class="text-danger">{{ $message }}</small>
                        </span>
                        @enderror
                        </div>
                </div>
                    <div class="col-md-4" >
                        <input type="submit" value="حفظ الإعلان" class="form-control btn-primary">
                    </div>

            </div>

            </form>
          @endcomponent
        </div>

    </div>

  </div>
  <!-- /.container-fluid -->
  @stop

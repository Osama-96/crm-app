@extends('pages.layouts.app')
@section('css')
<link href="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@stop
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
            <li class="breadcrumb-item active" aria-current="page">جميع الاعلانات </li>
        </ol>
    </nav>
    <!-- Content Row -->
    @component('components.table',['title'=>'جميع الإعلانات','color'=>'primary'])
        <thead>
            <tr>
                <th>#</th>
                <th>تاريخ البدء</th>
                <th>تاريخ الانتهاء</th>
                <th>النوع</th>
                <th> عدد المتبرعين</th>
                <th>المنطقة</th>
                <th>التكلفة</th>
                <th>عرض</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>#</th>
                <th>تاريخ البدء</th>
                <th>تاريخ الانتهاء</th>
                <th>النوع</th>
                <th> عدد المتبرعين</th>
                <th>المنطقة</th>
                <th>التكلفة</th>
                <th>عرض</th>
            </tr>
        </tfoot>
        <tbody>

            @foreach($data as $key => $item)
            <tr>
            <td>{{$key+1}}</td>
            <td>{{$item->starts_at}}</td>
            <td>{{$item->ends_at}}</td>
            <td>{{$item->type}}</td>
            <td>{{$item->newBenefactors->count()}}</td>
            <td>{{$item->location->name??'لا يوجد'}}</td>
            <td>{{$item->cost}} ج.م</td>
            <td class="text-center" >
                <a href="{{route('ads.show',['ad'=>$item->id])}}"  class="text-primary h3 " > <i class="fa fa-eye" aria-hidden="true"></i> </a>
            </td>
            </tr>
            @endforeach
        </tbody>
    @endcomponent

  </div>
  <!-- /.container-fluid -->
@stop
@section('js')
    <!-- Page level plugins -->
    <script src="{{asset('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{asset('assets/js/demo/datatables-demo.js')}}"></script>
@stop

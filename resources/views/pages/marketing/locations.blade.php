@extends('pages.layouts.app')
@section('css')
<link href="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@stop
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid  ">
    <!-- Content Row -->
    @component('components.table',['title'=>' المناطق المحفوظة','color'=>'primary'])
        <a  class="btn btn-success text-white mb-5" data-toggle="modal" data-target="#new">تسجيل منطقة جديدة  </a>
        <!-- new Modal -->
        <div class="modal fade" id="new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">تسجيل منطقة جديدة</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{route('locations.store')}}" method="Post">
                        <div class="modal-body">
                            @method('POST')
                            @csrf
                            <lable>اسم المنطقة</lable>
                            <br>
                            <input type="text" class="form-control "  name="name" required>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">الغاء</button>
                            <button type="submit"  class="btn btn-primary">حفظ </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <thead>
        <tr>
            <th>#</th>
            <th>اسم المنطقة</th>
            <th>تعديل</th>
            <th> حذف </th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            <th>اسم المنطقة</th>
            <th>تعديل</th>
            <th> حذف </th>
        </tr>
        </tfoot>
        <tbody>
        @foreach($data as $key => $item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->name}}</td>
                <td >
                    <a href="" class="btn btn-warning" data-toggle="modal" data-target="#edit{{$key}}">تعديل  </a>
                </td>
                <td>
                    <a href="" class="btn btn-danger" data-toggle="modal" data-target="#delete{{$key}}">حذف  </a>

                </td>
            </tr>

            <!--Edit Modal -->
            <div class="modal fade" id="edit{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">عملية تعديل البيانات</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="{{route('locations.update',['location'=>$item->id])}}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <label for=""> المنطقة</label>
                                <input type="text" name="name" class="form-control" id="" value="{{$item->name}}">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">إلغاء</button>
                                <button type="submit" class="btn btn-primary">حفظ</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Delete Modal -->
            <div class="modal fade" id="delete{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">تأكيد العملية</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="{{route('locations.destroy',['location'=> $item->id])}}" method="Post">
                            <div class="modal-body">
                                @method('DELETE')
                                @csrf
                                هل أنت متأكد من القيام بهذه العملية؟

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">الغاء</button>
                                <button type="submit"  class="btn btn-danger">حذف</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
        </tbody>
    @endcomponent

</div>
  <!-- /.container-fluid -->
@stop
@section('js')
    <!-- Page level plugins -->
    <script src="{{asset('assets/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{asset('assets/js/demo/datatables-demo.js')}}"></script>
@stop

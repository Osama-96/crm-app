@extends('layouts.app')
@section('content')
<div class="container" >

    <!-- Outer Row -->
    <div class="row justify-content-center" >

      <div class="col-xl-12 col-lg-12 col-md-9" >

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image "  style="background-image: url({{asset('frontend/svg/login.svg')}});"> </div>
              <div class="col-lg-6">
                <div class="p-5" style="height: 600px;">
                  <div class="text-center mb-3">
                    <h2 class="text-gray-900 mb-4">جمعية مصر المحروسة بلدي </h2>
                    <h5 class="text-gray-900 mb-5">برنامج متابعة عمليات التبرع</h5>
                  </div>
                    <form class="user" action="{{ route('login') }}" method="POST" id="formSubmit">
                        @csrf
                        <div class="form-group">
                        <input type="email" class="form-control form-control-user  @error('email') is-invalid @enderror" name="email" value="{{old('email')}}" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="أدخل الايميل الخاص بك..">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                        <input type="password" class="form-control form-control-user @error('password') is-invalid @enderror" name="password" required id="exampleInputPassword" placeholder="أدخل كلمة المرور...">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        </div>
                        <div class="form-group">
                        <div class="custom-control custom-checkbox small">
                            <input type="checkbox" class="custom-control-input" name="remember" id="customCheck" {{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label" for="customCheck">تذكرني</label>
                        </div>
                        </div>
                        <input type="submit" class="btn btn-primary btn-user btn-block" value="دخول" id="submitInput">

                    </form>
                    <hr>
{{--                    <div class="text-center">--}}
{{--                        @if (Route::has('password.request'))--}}
{{--                            <a class="btn btn-link small" href="{{ route('password.request') }}">--}}
{{--                                {{ __('نسيت كلمة المرور؟') }}--}}
{{--                            </a>--}}
{{--                        @endif--}}
{{--                    </div>--}}
                    <div class="text-center">
                        <a class="small" href="{{route('register')}}"> لا أملك حساب!</a>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

</div>
@stop
@section('js')
    <script !src="">
        $('#formSubmit').submit(function(){
            $('#submitInput').prop("disabled", true);
        });
    </script>
@stop

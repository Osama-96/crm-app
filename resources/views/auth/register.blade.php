@extends('layouts.app')

@section('content')
    <div class="container" >

        <!-- Outer Row -->
        <div class="row justify-content-center" >

            <div class="col-xl-12 col-lg-12 col-md-9" >

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image "  style="background-image: url({{asset('frontend/svg/login.svg')}});"> </div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center mb-3">
                                        <h2 class="text-gray-900 mb-4">جمعية مصر المحروسة بلدي </h2>
                                        <h5 class="text-gray-900 mb-5">برنامج متابعة عمليات التبرع</h5>
                                    </div>
                                    <form method="POST" action="{{ route('register') }}" id="formSubmit">
                                        @csrf

                                        <div class="form-group row">

                                            <div class="col-md-12">
                                            <label for="name" class=" col-form-label text-md-right">{{ __('الاسم') }}</label>
                                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">

                                            <div class="col-md-12">
                                            <label for="email" class="col-form-label text-md-right">{{ __('الإيميل') }}</label>
                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">

                                            <div class="col-md-12">
                                            <label for="password" class=" col-form-label text-md-right">{{ __('كلمة المرور') }}</label>
                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">

                                            <div class="col-md-12">
                                            <label for="password-confirm" class="col-form-label text-md-right">{{ __('تأكيد كلمة المرور') }}</label>
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group row mb-0">
                                            <div class="col-md-10 offset-md-1">
                                                <button type="submit" class="btn btn-warning form-control "  id="submitInput">
                                                    {{ __('إنشاء حساب') }}
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

@endsection

@section('js')
    <script !src="">
        $('#formSubmit').submit(function(){
            $('#submitInput').prop("disabled", true);
        });
    </script>
@stop

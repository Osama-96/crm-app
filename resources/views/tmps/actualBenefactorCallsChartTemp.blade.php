<div class="card shadow mb-4">
    <!-- Card Header - Dropdown -->
    <div class="card-header  text-center bg-{{$color??''}}">
        <h6 class="m-0 font-weight-bold {{$color?'text-white':''}}">إحصائيات المكالمات التامة</h6>
    </div>

    <!-- Card Body -->
    <div class="card-body">
        @if($benefactor->calls()->count() || $benefactor->benefactorable()->calls()->count())
        <div class="chart-pie pt-4 pb-2 mb-5">
            <canvas id="myPie"></canvas>
        </div>
        <div class="mt-4 text-center small">
            <span class="ml-3">
                <i class="fas fa-circle" style="color: #0bdca0"></i>  تم الرد: {{$benefactor->calls()->answered()->count() }}
            </span>
            <span class="ml-3">
                <i class="fas fa-circle" style="color: #ffca39"></i>مشغول: {{$benefactor->calls()->busy()->count() }}
            </span>
            <span class="ml-3">
                <i class="fas fa-circle " style="color: #ff2e00"></i>  مغلق أو غير متاح: {{$benefactor->calls()->off()->count() }}
            </span>
                <br>
            <span class="ml-3">
                <i class="fas fa-circle" style="color: #5c6d80"></i> رد شخص آخر: {{$benefactor->calls()->another()->count() }}
            </span>
            <span class="ml-3">
                <i class="fas fa-circle" style="color: #87a2bb"></i> رقم خطأ: {{$benefactor->calls()->wrong()->count() }}
            </span>
        </div>
        @else

            <p class="alert alert-secondary text-center">لم يتم حفظ أي مكالمات لهذا المتبرع</p>


        @endif
    </div>
</div>
@if($benefactor->calls()->count() || $benefactor->benefactorable()->calls()->count())
@push('js')
    <script !src="">
        var myPie = document.getElementById("myPie");
        var myPieChart1 = new Chart(myPie, {
            type: 'doughnut',
            data: {
                labels: ["تم الرد" ,"مشغول","مغلق أو غير متاح","رد شخص آخر","رقم خطأ"],
                datasets: [{
                    data: [
                        {{(($benefactor->calls()->answered()->count() ) *100)/ ($benefactor->calls()->count() )}},
                        {{(($benefactor->calls()->busy()->count() )     *100)/  ($benefactor->calls()->count() )}},
                        {{(($benefactor->calls()->off()->count() )      *100)/ ($benefactor->calls()->count() )}},
                        {{(($benefactor->calls()->another()->count() )  *100)/  ($benefactor->calls()->count() )}},
                        {{(($benefactor->calls()->wrong()->count())     *100)/ ($benefactor->calls()->count() )}}],
                    backgroundColor: ['#0bdca0','#ffca39','#ff2e00','#5c6d80','#87a2bb'],
                    hoverBackgroundColor: ['#0be2a3','#ffd62b','#f22d00','#789ba6','#87a2bb'],
                    hoverBorderColor: "rgb(244,246,254)",
                }],
            },
            options: {
                maintainAspectRatio: false,
                tooltips: {
                    backgroundColor: "rgb(255,255,255)",
                    bodyFontColor: "#858796",
                    borderColor: '#dddfeb',
                    borderWidth: 1,
                    xPadding: 15,
                    yPadding: 15,
                    displayColors: false,
                    caretPadding: 10,
                },
                legend: {
                    display: false
                },
                cutoutPercentage: 80,
            },
        });
    </script>
@endpush
@endif

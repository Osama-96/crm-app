<div class="card shadow mb-4">
    <!-- Card Header - Dropdown -->
    <div class="card-header  text-center bg-{{$color??''}}">
        <h6 class="m-0 font-weight-bold {{$color?'text-white':''}}"> إحصائيات المكالمات التامة{{$titlePlus??''}}</h6>
    </div>

    <!-- Card Body -->
    <div class="card-body">
        @if($item->calls()->count())
        <div class="chart-pie pt-4 pb-2 mb-5">
            <canvas id="myNewPie{{$item->id}}"></canvas>

        </div>

        <div class="mt-4 text-center small">
            <span class="ml-3">
                <i class="fas fa-circle" style="color: #0bdca0"></i>  تم الرد: {{$item->calls()->answered()->count()}}
            </span>
            <span class="ml-3">
                <i class="fas fa-circle" style="color: #ffca39"></i>مشغول: {{$item->calls()->busy()->count()}}
            </span>
            <span class="ml-3">
                <i class="fas fa-circle " style="color: #ff2e00"></i>  مغلق أو غير متاح: {{$item->calls()->off()->count() }}
            </span>
                <br>
            <span class="ml-3">
                <i class="fas fa-circle" style="color: #5c6d80"></i> رد شخص آخر: {{$item->calls()->another()->count()}}
            </span>
            <span class="ml-3">
                <i class="fas fa-circle" style="color: #87a2bb"></i> رقم خطأ: {{$item->calls()->wrong()->count()}}
            </span>
        </div>
        @else
            <p class="alert alert-secondary text-center">لم يتم حفظ أي مكالمات لهذا المتبرع</p>


        @endif
    </div>
</div>
@if($item->calls()->count())
@push('js')
    <script !src="">
        var myPie = document.getElementById("myNewPie{{$item->id}}");
        var myPieChart1 = new Chart(myPie, {
            type: 'doughnut',
            data: {
                labels: ["تم الرد" ,"مشغول","مغلق أو غير متاح","رد شخص آخر","رقم خطأ"],
                datasets: [{
                    data: [
                        {{($item->calls()->answered()->count() *100)/ $item->calls()->count()}},
                        {{($item->calls()->busy()->count() *100)/ $item->calls()->count()}},
                        {{($item->calls()->off()->count() *100)/ $item->calls()->count()}},
                        {{($item->calls()->another()->count() *100)/ $item->calls()->count()}},
                        {{($item->calls()->wrong()->count() *100)/ $item->calls()->count()}}],
                    backgroundColor: ['#0bdca0','#ffca39','#ff2e00','#5c6d80','#87a2bb'],
                    hoverBackgroundColor: ['#0be2a3','#ffd62b','#f22d00','#789ba6','#87a2bb'],
                    hoverBorderColor: "rgb(244,246,254)",
                }],
            },
            options: {
                maintainAspectRatio: false,
                tooltips: {
                    backgroundColor: "rgb(255,255,255)",
                    bodyFontColor: "#858796",
                    borderColor: '#dddfeb',
                    borderWidth: 1,
                    xPadding: 15,
                    yPadding: 15,
                    displayColors: false,
                    caretPadding: 10,
                },
                legend: {
                    display: false
                },
                cutoutPercentage: 80,
            },
        });
    </script>
@endpush
@endif

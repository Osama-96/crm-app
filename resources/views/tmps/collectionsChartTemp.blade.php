<div class="card shadow mb-4">
    <!-- Card Header - Dropdown -->
    <div class="card-header  text-center bg-{{$color??''}}">
        <h6 class="m-0 font-weight-bold {{$color?'text-white':''}}"> إحصائيات عمليات التحصيل{{$titlePlus??''}}</h6>
    </div>

    <!-- Card Body -->
    <div class="card-body">
        @if($item->collectAssignments()->count())
        <div class="chart-pie pt-4 pb-2 mb-5">
            <canvas id="mycollectPie{{$item->id}}"></canvas>

        </div>

        <div class="mt-4 text-center small">
            <span class="ml-3">
                <i class="fas fa-circle" style="color:#ffca39"></i>  لم تتم بعد: {{$item->collectAssignments()->where('done',0)->count()}}
            </span>
            <span class="ml-3">
                <i class="fas fa-circle" style="color:#0bdca0 "></i>تم التحصيل:  {{$item->collections()->where('collected',1)->count()}}
            </span>
            <span class="ml-3">
                <i class="fas fa-circle " style="color: #ff2e00"></i> فشل التحصيل: {{$item->collections()->where('failed',1)->count()}}
            </span>
        </div>
        @else
            <p class="alert alert-secondary text-center">لم يتم إرسال المتبرع إلى التحصيل بعد</p>


        @endif
    </div>
</div>
@if($item->collectAssignments()->count())
@push('js')
    <script !src="">
        var myPie = document.getElementById("mycollectPie{{$item->id}}");
        var myPieChart1 = new Chart(myPie, {
            type: 'doughnut',
            data: {
                labels: ["تم التحصيل بنجاح" ,"لم يتم التحصيل بعد","فشل التحصيل"],
                datasets: [{
                    data: [
                        {{($item->collections()->where('collected',1)->count() /$item->collectAssignments()->count()) * 100}},
                        {{($item->collectAssignments()->where('done',0)->count() /$item->collectAssignments()->count()) * 100}},
                        {{($item->collections()->where('failed',1)->count() /$item->collectAssignments()->count()) * 100}},
                    ],
                    backgroundColor: ['#0bdca0','#ffca39','#ff2e00'],
                    hoverBackgroundColor: ['#0be2a3','#ffd62b','#f22d00'],
                    hoverBorderColor: "rgb(244,246,254)",
                }],
            },
            options: {
                maintainAspectRatio: false,
                tooltips: {
                    backgroundColor: "rgb(255,255,255)",
                    bodyFontColor: "#858796",
                    borderColor: '#dddfeb',
                    borderWidth: 1,
                    xPadding: 15,
                    yPadding: 15,
                    displayColors: false,
                    caretPadding: 10,
                },
                legend: {
                    display: false
                },
                cutoutPercentage: 80,
            },
        });
    </script>
@endpush
ّ@endif


@extends('layouts.app')
@section('content')
<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-12 col-lg-12 col-md-12">

        <div class="card o-hidden border-0  my-5">
            <div class="card-header row py-2">

                <div class="col-md-10">
                    <h4 class="h3 mb-1 text-500 text-primary">جمعية مصر المحروسة بلدي </h4>
                    <h5 class="h4 mb-1 text-500 text-primary">برنامج متابعة المتبرعين</h5>
                    <h6 class="h5 mb-1 text-500 text-primary">تقارير الإعلانات</h6>
                </div>
                <div class="col-md-2 ">
                    <img src="{{asset('logo.png')}}" alt="" style="width: 160px;" >
                </div>
            </div>
            <div class="card-body border-bottom-primary px-5" style="padding-bottom:50px;">
                <!-- Nested Row within Card Body -->
                <div class="px-4 row" style="margin-bottom: 100px ; ">
                    <div class="col-md-12 text-center">
                        <h4 class="h3 mb-1  text-primary">  تقرير بالإعلان رقم 1  </h4>
                    </div>
                </div>
                <div class=" row " >
                    <div class="col-md-2 text-lg">
                        رقم الإعلان:
                    </div>
                    <div class="col-md-10">
                        1
                    </div>
                </div>
                <div class=" row my-2" >
                    <div class="col-md-2 text-lg">
                        تاريخ الانشاء
                    </div>
                    <div class="col-md-10">
                        12/11/2022
                    </div>
                </div>
                <div class=" row my-2" >
                    <div class="col-md-2 text-lg">
                        نوع الإعلان
                    </div>
                    <div class="col-md-10">
                        محتوى مرئي
                    </div>
                </div>
                <div class=" row my-2" >
                    <div class="col-md-2 text-lg">
                         التكلفة
                    </div>
                    <div class="col-md-10">
                        5000 جنيها
                    </div>
                </div>
                <div class=" row my-2" >
                    <div class="col-md-2 text-lg">
                        تاريخ النشر
                    </div>
                    <div class="col-md-10">
                        12/11/2022
                    </div>
                </div>
                <div class=" row my-2" >
                    <div class="col-md-2 text-lg">
                        المدة الزمنية
                    </div>
                    <div class="col-md-10">
                        ثلاثة أسابيع
                    </div>
                </div>
                <div class=" row my-2" >
                    <div class="col-md-2 text-lg">
                        النطاق الجغرافي
                    </div>
                    <div class="col-md-10">
                        مصر
                    </div>
                </div>
                <div class=" row my-2" >
                    <div class="col-md-2 text-lg">
                        عدد المتبرعين
                    </div>
                    <div class="col-md-10">
                        22
                    </div>
                </div>
                <small > بتاريخ: {{date('Y-m-d H:i:s')}} </small>
                <br>
                <small > توقيت: {{date(' H:i')}} </small>
            </div>
            <div class="no-print">
                <div class="text-center">
                    <button class="btn btn-warning form-control text-lg" type="button" onclick="window.print()">طباعة </button>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

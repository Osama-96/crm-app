@extends('layouts.app')
@section('content')
<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-12 col-lg-12 col-md-12">

        <div class="card o-hidden border-0  my-5">
            <div class="card-header row py-2">

                <div class="col-md-10">
                    <h4 class="h3 mb-1 text-500 text-primary">جمعية مصر المحروسة بلدي </h4>
                    <h5 class="h4 mb-1 text-500 text-primary">برنامج متابعة المتبرعين</h5>
                    <h6 class="h5 mb-1 text-500 text-primary">تقارير الإعلانات</h6>
                </div>
                <div class="col-md-2 ">
                    <img src="{{asset('logo.png')}}" alt="" style="width: 160px;" >
                </div>
            </div>
            <div class="card-body border-bottom-primary" style="padding-bottom:50px;">
                <!-- Nested Row within Card Body -->
                <div class="px-4 row" style="margin-bottom: 100px ; ">
                    <div class="col-md-12 text-center">
                        <h4 class="h3 mb-1  text-primary">  تقرير المتبرعين المحتملين  </h4>
                    </div>
                </div>
                <div class=" row " >
                    <div class="col-md-3 text-lg">
                        عدد الاعلانات:
                    </div>
                    <div class="col-md-9">
                        11
                    </div>
                </div>
                <div class=" row my-3" >
                    <div class="col-md-3 text-lg">
                        عدد المتبرعين الإجمالي:
                    </div>
                    <div class="col-md-9">
                        500
                    </div>
                </div>
                <br>
                <div class=" row my-2" >
                    <h4> التقرير التفصيلي: </h4>
                    <br>
                    <div class="col-md-12">
                        <table class="table  table-inverse ">
                            <thead class="thead-light">
                                <tr>
                                    <th>رقم الإعلان</th>
                                    <th> تاريخ النشر</th>
                                    <th> مدة النشر</th>
                                    <th> عدد المتبرعين</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td scope="row">1 </td>
                                        <td> 12/12/2019</td>
                                        <td> أسبوعين</td>
                                        <td> 50</td>
                                    </tr>

                                    <tr>
                                        <td scope="row">2 </td>
                                        <td> 2/10/2019</td>
                                        <td> 3 أسابيع</td>
                                        <td> 50</td>
                                    </tr>

                                    <tr>
                                        <td scope="row">3 </td>
                                        <td> 12/02/2019</td>
                                        <td> 4 أسابيع</td>
                                        <td> 100</td>
                                    </tr>

                                    <tr>
                                        <td scope="row">4</td>
                                        <td> 1/12/2019</td>
                                        <td> 5 أسابيع</td>
                                        <td> 28</td>
                                    </tr>

                                    <tr>
                                        <td scope="row">5</td>
                                        <td> 12/12/2019</td>
                                        <td> أسبوعين</td>
                                        <td> 50</td>
                                    </tr>

                                </tbody>
                        </table>
                    </div>
                </div>
                <small > بتاريخ: {{date('Y-m-d H:i:s')}} </small>
                <br>
                <small > توقيت: {{date(' H:i')}} </small>

            </div>
        </div>
          <div class="no-print">
              <div class="text-center">
                  <button class="btn btn-warning form-control text-lg" type="button" onclick="window.print()">طباعة </button>
              </div>
          </div>
      </div>
</div>
@stop

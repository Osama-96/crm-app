@extends('layouts.app')
@section('content')
<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-12 col-lg-12 col-md-12">

        <div class="card o-hidden border-0  my-5">
            <div class="card-header row py-2">

                <div class="col-md-10">
                    <h4 class="h3 mb-1 text-500 text-primary">جمعية مصر المحروسة بلدي </h4>
                    <h5 class="h4 mb-1 text-500 text-primary">برنامج متابعة المتبرعين</h5>
                    <h6 class="h5 mb-1 text-500 text-primary">تقارير الإعلانات</h6>
                </div>
                <div class="col-md-2 ">
                    <img src="{{asset('logo.png')}}" alt="" style="width: 160px;" >
                </div>
            </div>
            <div class="card-body border-bottom-primary" style="padding-bottom:50px;">
                <!-- Nested Row within Card Body -->
                <div class="px-4 row" style="margin-bottom: 100px ; ">
                    <div class="col-md-12 text-center">
                        <h4 class="h3 mb-1  text-primary">  تقرير  تكلفة الإعلانات  </h4>
                    </div>
                </div>
                <div class=" row " >
                    <div class="col-md-3 text-lg">
                        عدد الاعلانات:
                    </div>
                    <div class="col-md-9">
                        {{App\Ad::count()}}
                    </div>
                </div>
                <div class=" row my-3" >
                    <div class="col-md-3 text-lg">
                       التكلفة الإجمالية:
                    </div>
                    <div class="col-md-9">
                        {{App\Ad::sum('cost')}} جنيهاً
                    </div>
                </div>
                <br>
                <div class=" row my-2" >
                    <h4> التقرير التفصيلي: </h4>
                    <br>
                    <div class="col-md-12">
                        <table class="table  table-inverse ">
                            <thead class="thead-light">
                                <tr>
                                    <th>رقم الإعلان</th>
                                    <th> تاريخ النشر</th>
                                    <th> تاريخ الانتهاء</th>
                                    <th> نوعه</th>
                                    <th> التكلفة</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(App\Ad::all() as $key=>$ad)
                                    <tr>
                                        <td scope="row">{{$key+1}} </td>
                                        <td>{{$ad->starts_at}}</td>
                                        <td>{{($ad->ends_at)? $ad->ends_at : 'مستمر حالياً'}}</td>
                                        <td>{{$ad->type}}</td>
                                        <td> {{$ad->cost}} جنيها</td>
                                    </tr>
                                    @endforeach

                                </tbody>
                        </table>
                    </div>
                </div>
                <small > بتاريخ: {{date('Y-m-d H:i:s')}} </small>
                <br>
                <small > توقيت: {{date(' H:i')}} </small>

            </div>

            <div class="no-print">
                <div class="text-center">
                    <button class="btn btn-warning form-control text-lg" type="button" onclick="window.print()">طباعة </button>
                </div>
            </div>
        </div>
      </div>

      </div>
</div>
@stop

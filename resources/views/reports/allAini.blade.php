@extends('layouts.app')
@section('content')

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-12 col-lg-12 col-md-12">

            <div class="card o-hidden border-0  my-5">
                <div class="card-header row py-2">

                    <div class="col-md-10">
                        <h4 class="h3 mb-1 text-500 text-primary">جمعية مصر المحروسة بلدي </h4>
                        <h5 class="h4 mb-1 text-500 text-primary">برنامج متابعة المتبرعين</h5>
                        <h6 class="h5 mb-1 text-500 text-primary">تقارير المتبرعين</h6>
                    </div>
                    <div class="col-md-2 ">
                        <img src="{{asset('logo.png')}}" alt="" style="width: 160px;" >
                    </div>
                </div>
                <div class="card-body border-bottom-primary" style="padding-bottom:50px;">
                    <!-- Nested Row within Card Body -->
                    <div class="px-4 row" style="margin-bottom: 100px ; ">
                        <div class="col-md-12 text-center">
                            <h4 class="h3 mb-1  text-primary">  تقرير لأصحاب التبرعات العينية  </h4>
                        </div>
                    </div>
                    <div class=" row " >
                        <div class="col-md-3 text-lg">
                            عدد المتبرعين:
                        </div>
                        <div class="col-md-9">
                            11
                        </div>
                    </div>

                    <br>
                    <div class=" row my-2" >
                        <h4> التقرير التفصيلي: </h4>

                        <div class="col-md-12 mt-3">
                            <table class="table">

                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>الاسم</th>
                                    <th>الهاتف</th>
                                    <th>الايميل</th>
                                    <th>الوسيلة</th>
                                    <th>الإعلان</th>
                                    <th>تصنيفه</th>
                                    <th>نوع التبرع</th>
                                    <th>التبرع</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach(\App\Benefactor::where('isAyni',1)->get() as $key=>$benefactor)

                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$benefactor->name}}</td>
                                        <td>{{$benefactor->phone}}</td>
                                        <td>{{$benefactor->email}}</td>
                                        <td>{{$benefactor->how}}</td>
                                        <td>{{($benefactor->ad != -1 )? 'رقم'.$benefactor->ad :'لا يوجد' }}</td>
                                        <td>@if($benefactor->isStopped == 1) متوقف @elseif($benefactor->isNew == 1)  جديد@else {{($benefactor->frequency == 'OneTime')?'متبرع عادي' :''}}{{($benefactor->frequency == 'EveryMonth')?'كفيل':''}} @endif</td>
                                        <td>{{($benefactor->isMoney == 1)?'تبرع مالي' :''}}{{ ($benefactor->isAyni == 1)?'تبرع عيني':''}} </td>
                                        <td> {{($benefactor->isMoney == 1)?$benefactor->moneyAmount.' جنيه ' :'' }}{{($benefactor->isAyni == 1)?$benefactor->donation: ''}}</td>
                                    </tr>


                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <small > بتاريخ: {{date('Y-m-d H:i:s')}} </small>
                    <br>
                    <small > توقيت: {{date(' H:i')}} </small>

                </div>
            </div>
            <div class="no-print">
                <div class="text-center">
                    <button class="btn btn-warning form-control text-lg" type="button" onclick="window.print()">طباعة </button>
                </div>
            </div>
        </div>
    </div>
@stop

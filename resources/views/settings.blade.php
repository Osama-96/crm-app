@extends('pages.layouts.app')
@section('content')
    <div class="container-fluid  ">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-white">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
                <li class="breadcrumb-item active" aria-current="page">الإعدادات</li>
            </ol>
        </nav>
        <div class="row justify-content-center ">
            <div class="col-md-7 my-1 mb-5">
                <div class="card">
                    <div class="card-header bg-primary">
                        <h5 class="text-center text-white">
                        تحديث البيانات الأساسية
                        </h5>
                    </div>
                    <div class="card-body">

                        <form action="" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1">الاسم</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$user->name}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">الإيميل</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$user->email}}">
                        </div>



                    </form>
                    </div>
                    <div class="card-footer text-center">
                        <button type="submit" class="btn btn-primary ">تحديث البيانات الأساسية</button>
                    </div>

                </div>
            </div>
            <div class="col-md-7 my-1  mb-5">
                <div class="card">
                    <div class="card-header bg-primary">
                        <h5 class="text-center text-white">
                       تغيير كلمة المرور
                        </h5>
                    </div>
                    <div class="card-body">

                        <form action="" method="POST">
                        @csrf
                        <div class="form-group">
                            <input type="password" class="form-control"  aria-describedby="emailHelp" placeholder="كلمة المرور الحالية">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control"  aria-describedby="emailHelp" placeholder="كلمة المرور الجديدة">
                        </div>
                            <div class="form-group">
                            <input type="password" class="form-control"  aria-describedby="emailHelp" placeholder="أعد إدخال كلمة المرور الجديدة">
                        </div>



                    </form>
                    </div>
                    <div class="card-footer text-center">
                        <button type="submit" class="btn btn-primary ">تغيير كلمة المرور</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop

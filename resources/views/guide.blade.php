@extends('pages.layouts.app')
@section('content')
    <div class="container-fluid  ">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-white">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">لوحة التحكم</a></li>
                <li class="breadcrumb-item active" aria-current="page">دليل الاستخدام</li>
            </ol>
        </nav>
        <div class="row ">
            @component('components.video',['title'=>'عنوان الفيديو','desc'=>'وصف المحتوى','link'=>asset('videos/1.mp4')])
            @endcomponent
        </div>
    </div>
@stop
